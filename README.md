# Color Your Life API

1. [API Documentation](#markdown-header-api-documentation)
1. [Authentication](#markdown-header-authentication)
1. [Pagination](#markdown-header-pagination)
1. [API](#markdown-header-api)
    - [Posts](#markdown-header-posts)
        + [Create Post](#markdown-header-create-post) **(07/15: Updated, add city and country)**
        + [Get Posts](#markdown-header-get-posts) **(07/15: Updated, add city and country)**
        + [Like/Unlike Posts](#markdown-header-likeunlike-posts)
        + [Get Liked Posts](#markdown-header-get-liked-posts)
        + [Update Posts](#markdown-header-update-posts) **(07/15: Updated, add city and country)**
        + [Delete File](#markdown-header-delete-file)
    - [App Users](#markdown-header-app-users)
        + [Register User](#markdown-header-register-user)
        + [Register User via SNS (social networking sites)](#markdown-header-register-user-via-sns-social-networking-sites)
        + [Get Users](#markdown-header-get-users)
        + [Update User](#markdown-header-update-user)
        + [Change password](#markdown-header-change-password)
        + [Login User](#markdown-header-login-user)
        + [Login User via SNS](#markdown-header-login-user-via-sns)
        + [Logout User](#markdown-header-logout-user)
        + [Upload Video](#markdown-header-upload-video)
        + [Get Videos](#markdown-header-get-video)
        + [Follow User](#markdown-header-follow-user)
        + [Get Following List](#markdown-header-get-following-list)
        + [Get Followers](#markdown-header-get-followers)
        + [Get Bonus Points](#markdown-header-get-bonus-points)
        + [Send Referral Code](#markdown-header-send-referral-code)
        + [Get Referrals](#markdown-header-get-referrals)
        + [Forgot Password](#markdown-header-forgot-password)
        + [Forgot Username](#markdown-header-forgot-username)
        + [Get Color Points](#markdown-header-get-color-points)
        + [Get My Market Place](#markdown-header-get-my-market-place)
        + [Select Market Place](#markdown-header-select-market-place)
        + [Get Plan Info](#markdown-header-get-plan-info)
    - [Ads](#markdown-header-ads)
        + [Create Ads](#markdown-header-create-ads)
        + [Get Featured](#markdown-header-get-featured)
    - [Categories](#markdown-header-categories)
        + [Get Categories](#markdown-header-get-categories)
    - [Post Reviews](#markdown-header-post-reviews)
        + [Create post reviews](#markdown-header-create-post-reviews)
        + [Get post reviews](#markdown-header-get-post-reviews)
    - [User Reviews](#markdown-header-user-reviews)
        + [Create user reviews](#markdown-header-create-user-reviews)
        + [Get user reviews](#markdown-header-get-user-reviews)
    - [Membership Plans](#markdown-header-membership-plans)
        + [Get List with details](#markdown-header-list-with-details)
        + [Select Membership Plan](#markdown-header-select-membership-plan)
        + [Apply Plan Now](#markdown-header-apply-plan-now)
    - [Notifications](#markdown-header-notifications)
        + [Get all notifs by user id](#markdown-header-get-all-notifs-by-user-id)
        + [Get total unread notifs](#markdown-header-get-total-unread-notifs)
    - [Messages](#markdown-header-messages)
        + [Get all message previews](#markdown-header-messages)
        + [Get conversation with another person](#markdown-get-conversation-with-another-person)
        + [Send a message](#markdown-header-send-a-message)
        + [Inquire about a product or service](#markdown-header-inquire-about-a-product-or-service)
        + [Send message to all followers](#markdown-header-send-message-to-all-followers)
        + [Get total unread messages](#markdown-header-get-total-unread-messages)
    - [Affiliates](#markdown-header-affiliates)
        + [Get all Affiliates](#markdown-header-get-all-affiliates)
        + [Create affiliates](#markdown-header-create-affiliates)
    - [Redeem Requests](#markdown-header-redeem-requests)
        + [Get all Redeem requests](#markdown-header-get-all-redeem-requests)
        + [Create Redeem requests](#markdown-header-create-redeem-requests)
        + [Accept or Decline a request](#markdown-header-accept-or-decline-a-request)
    - [Presents](#markdown-header-presents)
        + [Get all Presents](#markdown-header-get-all-presents)
        + [Get all presents of the user](#markdown-header-get-all-presents-of-the-user)
        + [Get all Presents by present request id](#markdown-header-get-all-presents-by-present-request-id)
        + [Create Present](#markdown-header-create-present)
    - [Present Requests](#markdown-header-present-requests)
        + [Get all Present Requests](#markdown-header-present-requests)
        + [Create Present Request](#markdown-header-create-present-request)
        + [Accept or Decline a Request](#markdown-header-accept-or-decline-a-request)
    - [Settings](#markdown-header-settings)
        + [Update Settings](#markdown-header-update-settings)
        + [Get Settings](#markdown-header-get-settings)
    - [Company Info](#markdown-header-company-info)
        + [Privacy Policy](#markdown-header-privacy-policy)
        + [Terms and Conditions](#markdown-header-terms-and-conditions)
        + [About Us](#markdown-header-about-us)
        + [Contact Us](#markdown-header-contact-us)
    - [Country Search](#markdown-header-country-search)
    - [City Search](#markdown-header-city-search)
    - [Add ons payment](#markdown-header-add-ons-payment)
    - [Check Paynamics Payment](#markdown-header-check-paynamics-payment)
    - [Check DOKU Payment](#markdown-header-check-doku-payment)
    - [Doku Payment Process](#markdown-header-doku-payment-process)
    - [Add Membership on success payment](#markdown-header-add-membership-on-success-payment) **(08/13: Added)**
    - [Add Add-ons on success payment](#markdown-header-add-add-ons-on-success-payment) **(08/13: Added)**

---

## API Documentation
Development: `https://coloryourlife.betaprojex.com/app/api`

---

### Authentication
#### Basic Auth

Authorization: `Basic YWRtaW46MTIzNA==`
x-api-key: `bWCxzkqRY+qqQkPiNkV2ww====`
---

### Pagination
`?page=1&date_order=desc`

---
## API
#### POSTS

##### Create Post
POST `/posts/`
##### Parameters
| Name            | Type   | Description           | Sample Data |
| --------------- | ------ | --------------------- | ----------- |
| name            | string | -                     | Test Posts  |
| files[]         | array  | -                     |             |
| market_place_id | int    | 1-Products/2-Services | 1           |
| price           | string | -                     | 4500        |
| description     | string | -                     |             |
| category_id     | int    | -                     | -           |
| user_id         | int    | -                     | -           |
| thumbnail       | string | -                     | -           |
| sub_title        | string | -                     | -           |
| country_id        | int | -                     | 176           |
| city_id        | int | -                     | 78406           |

##### Response
```json
200 OK
{
    "data": {
        "post_id": "69",
        "market_place_id": "1",
        "user_id": "1",
        "name": "Product Testing country/city 4",
        "sub_title": "Size: available in Large and XL",
        "price": "4500",
        "thumbnail": "http://coloryourlifeapp.com/app/uploads/posts/products/1563178275_bored_panda2.jpg",
        "description": "Hello",
        "category_id": "1",
        "status": "1",
        "created_at": "2019-07-15 01:11:15",
        "updated_at": "0000-00-00 00:00:00",
        "country_id": "176",
        "city_id": "78406",
        "country_name": "Philippines",
        "city_name": "Marikina",
        "thumbnail_url": "http://coloryourlifeapp.com/app/uploads/posts/products/1563178275_bored_panda2.jpg",
        "price_formatted": "PHP 4,500.00",
        "category_name": "All Departments",
        "likes": 0,
        "is_liked": false,
        "user": {
            "user_id": "1",
            "username": "jc",
            "email": "jcandres@myoptimind.com",
            "fname": "Jc",
            "lname": "Andres",
            "mname": "",
            "password": "$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6",
            "profile_pic": "http://coloryourlifeapp.com/app/uploads/user_profile_pic/default.png",
            "membership_plan_id": "3",
            "market_place_id": "0",
            "market_place_name": "",
            "rank": "4",
            "status": "1",
            "first_log": "no",
            "country": "PH",
            "referral_code": "7777777",
            "referred_by": "0",
            "referrer_rank": "0",
            "points": "109.5",
            "bonus": "0",
            "referrals": "251",
            "rank_up_referrals": "199",
            "forgot_token": "",
            "login_type": "form",
            "sns_token": null,
            "stripe_customer_id": "cus_FOzu9YDAkXCfZa",
            "created_at": "2019-07-03 18:22:16",
            "updated_at": "2019-07-12 01:26:19"
        },
        "files": [
            {
                "posts_file_id": "149",
                "post_id": "69",
                "filename": "http://coloryourlifeapp.com/app/uploads/posts/products/1563178275_bored panda.jpg",
                "status": "1",
                "created_at": "2019-07-15 01:11:15",
                "updated_at": "0000-00-00 00:00:00"
            },
            {
                "posts_file_id": "150",
                "post_id": "69",
                "filename": "http://coloryourlifeapp.com/app/uploads/posts/products/1563178275_bored panda.jpg",
                "status": "1",
                "created_at": "2019-07-15 01:11:15",
                "updated_at": "0000-00-00 00:00:00"
            }
        ]
    },
    "message": "Post successfully created",
    "status": 201
}
```

---

##### Get Posts  
**_(Added 'likes' and 'is_liked' in response)_**  
All posts: GET `/posts/all/`  
All posts with viewer id: GET `/posts/all/{logged_in_user_id}/`  
All posts by products/services: GET `/posts/all/{logged_in_user_id}/{products/services/all}/`  
All posts by products/services by categories: GET `/posts/all/{logged_in_user_id}/{products/services/all}/{category_id/all}/`  
All posts by products/services by categories by country & city: GET `/posts/all/{logged_in_user_id}/{products/services/all}/{category_id/all}/{country_id/all}/{city_id/all}`  

##### Response
```json
200 Ok
{
    "data": [
        {
            "post_id": "3",
            "market_place_id": "2",
            "user_id": "1",
            "name": "Service Testing country/city 1",
            "sub_title": "Size: available in Large and XL",
            "price": "4500",
            "thumbnail": "http://localhost/coloryourlife/uploads/posts/services/1563182001_bored_panda1.jpg",
            "description": "Hello",
            "category_id": "40",
            "status": "1",
            "created_at": "2019-07-15 17:13:21",
            "updated_at": "0000-00-00 00:00:00",
            "country_id": "176",
            "city_id": "78406",
            "country_name": "Philippines",
            "city_name": "Marikina",
            "likes": 0,
            "is_liked": false,
            "price_formatted": "Php 4,500.00",
            "user_details": {
                "user_id": "1",
                "username": "jc",
                "fullname": "Jc Andres",
                "email": "jcandres@myoptimind.com",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            }
        },
        {
            "post_id": "1",
            "market_place_id": "1",
            "user_id": "1",
            "name": "Product Updated",
            "sub_title": "Size: available in Large and XL",
            "price": "4500",
            "thumbnail": "http://localhost/coloryourlife/uploads/posts/products/1563180690_bored_panda.jpg",
            "description": "Hello",
            "category_id": "1",
            "status": "1",
            "created_at": "2019-07-15 15:38:36",
            "updated_at": "2019-07-15 16:51:30",
            "country_id": "176",
            "city_id": "78406",
            "country_name": "Philippines",
            "city_name": "Marikina",
            "likes": 0,
            "is_liked": false,
            "price_formatted": "Php 4,500.00",
            "user_details": {
                "user_id": "1",
                "username": "jc",
                "fullname": "Jc Andres",
                "email": "jcandres@myoptimind.com",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            }
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

Single post: GET `/posts/{post_id}/{logged_in_user_id}`  
**_Add 'likes' and 'is_liked'_**  
##### Response
```json
{
    "data": {
        "post_id": "1",
        "market_place_id": "1",
        "user_id": "1",
        "name": "testing",
        "sub_title": "Size: available in Large and XL",
        "price": "4500",
        "thumbnail": "http://localhost/coloryourlife/uploads/posts/products/1552296091_default.png",
        "description": "Hello",
        "category_id": "1",
        "status": "1",
        "created_at": "2019-03-11 17:21:31",
        "updated_at": "0000-00-00 00:00:00",
        "country_id": "3",
        "city_id": "275",
        "country_name": "Afghanistan",
        "city_name": "Farah",
        "country_code": "AF",
        "thumbnail_url": "http://localhost/coloryourlife/uploads/posts/products/1552296091_default.png",
        "price_formatted": "PHP 4,500.00",
        "category_name": "Prod Cat 1",
        "likes": 1,
        "is_liked": true,
        "user": {
            "user_id": "1",
            "username": "jc",
            "email": "jcandres@myoptimind.com",
            "fname": "Jc",
            "lname": "Andres",
            "mname": "",
            "password": "$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6",
            "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
            "membership_plan_id": "4",
            "market_place_id": "0",
            "market_place_name": "",
            "rank": "4",
            "status": "1",
            "first_log": "no",
            "referral_code": "7777777",
            "referred_by": "0",
            "referrer_rank": "0",
            "points": "40",
            "bonus": "0",
            "referrals": "251",
            "rank_up_referrals": "198",
            "forgot_token": "",
            "login_type": "form",
            "sns_token": null,
            "stripe_customer_id": "",
            "created_at": "2019-02-28 15:27:18",
            "updated_at": "2019-02-28 15:34:34"
        },
        "files": [
            {
                "posts_file_id": "1",
                "post_id": "1",
                "filename": "http://localhost/coloryourlife/uploads/posts/products/1552296091_bored panda.jpg",
                "status": "1",
                "created_at": "2019-03-11 17:21:31",
                "updated_at": "0000-00-00 00:00:00"
            },
            {
                "posts_file_id": "2",
                "post_id": "1",
                "filename": "http://localhost/coloryourlife/uploads/posts/products/1552296091_ccc_19898.jpg",
                "status": "1",
                "created_at": "2019-03-11 17:21:31",
                "updated_at": "0000-00-00 00:00:00"
            }
        ]
    },
    "message": "Successfully found data",
    "status": 200
}
```

Get All posts of followed users: GET `/posts/follow/{user_id}/`  
Get All posts of followed users per products/services: GET `/posts/follow/{user_id}/{products/services/all}`  
Get All posts of followed users per products/services/all per category: GET `/posts/follow/{user_id}/{products/services/all}/{category_id/all}`  
Get All posts of followed users per products/services/all per category per country: GET `/posts/follow/{user_id}/{products/services/all}/{category_id/all}/{country_id/all}/{city_id/all}`  

**_Add 'likes' and 'is_liked'_**  
##### Response
```json
200 OK
{
    "data": {
        "posts": [
            {
                "post_id": "69",
                "market_place_id": "1",
                "user_id": "1",
                "name": "Product Testing country/city 4",
                "sub_title": "Size: available in Large and XL",
                "price": "4500",
                "thumbnail": "http://coloryourlifeapp.com/app/uploads/posts/products/1563178275_bored_panda2.jpg",
                "description": "Hello",
                "category_id": "1",
                "status": "1",
                "created_at": "2019-07-15 01:11:15",
                "updated_at": "0000-00-00 00:00:00",
                "country_id": "176",
                "city_id": "78406",
                "country_name": "Philippines",
                "city_name": "Marikina",
                "user_details": {
                    "user_id": "1",
                    "username": "jc",
                    "fullname": "Jc Andres",
                    "email": "jcandres@myoptimind.com",
                    "profile_pic": "http://coloryourlifeapp.com/app/uploads/user_profile_pic/default.png"
                },
                "price_formatted": "Php 4,500.00",
                "likes": 0,
                "is_liked": false
            },
            {
                "post_id": "64",
                "market_place_id": "1",
                "user_id": "1",
                "name": "Product Testing country/city",
                "sub_title": "Size: available in Large and XL",
                "price": "4500",
                "thumbnail": "http://coloryourlifeapp.com/app/uploads/posts/products/1563177767_bored_panda2.jpg",
                "description": "Hello",
                "category_id": "1",
                "status": "1",
                "created_at": "2019-07-15 01:02:47",
                "updated_at": "0000-00-00 00:00:00",
                "country_id": "176",
                "city_id": "78406",
                "country_name": "Philippines",
                "city_name": "Marikina",
                "user_details": {
                    "user_id": "1",
                    "username": "jc",
                    "fullname": "Jc Andres",
                    "email": "jcandres@myoptimind.com",
                    "profile_pic": "http://coloryourlifeapp.com/app/uploads/user_profile_pic/default.png"
                },
                "price_formatted": "Php 4,500.00",
                "likes": 0,
                "is_liked": false
            },
            {
                "post_id": "47",
                "market_place_id": "1",
                "user_id": "1",
                "name": "Product Updated Test 1",
                "sub_title": "Size: available in Large and XL",
                "price": "4500",
                "thumbnail": "http://coloryourlifeapp.com/app/uploads/posts/products/1563181694_bored_panda.jpg",
                "description": "Hello",
                "category_id": "1",
                "status": "1",
                "created_at": "2019-07-09 00:17:43",
                "updated_at": "2019-07-15 02:08:14",
                "country_id": "176",
                "city_id": "78406",
                "country_name": "Philippines",
                "city_name": "Marikina",
                "user_details": {
                    "user_id": "1",
                    "username": "jc",
                    "fullname": "Jc Andres",
                    "email": "jcandres@myoptimind.com",
                    "profile_pic": "http://coloryourlifeapp.com/app/uploads/user_profile_pic/default.png"
                },
                "price_formatted": "Php 4,500.00",
                "likes": 1,
                "is_liked": true
            }
        ],
        "ads": [
            {
                "ad_id": "2",
                "market_place_id": "1",
                "post_id": "2",
                "user_id": "4",
                "image": "http://coloryourlifeapp.com/app/uploads/featured_ads/1562220522_14976788_1645805929051995_6550184814850987741_o.jpg",
                "status": "active",
                "created_at": "2019-07-03 23:08:42",
                "updated_at": "0000-00-00 00:00:00"
            },
            {
                "ad_id": "1",
                "market_place_id": "1",
                "post_id": "2",
                "user_id": "4",
                "image": "http://coloryourlifeapp.com/app/uploads/featured_ads/1562220013_14976788_1645805929051995_6550184814850987741_o.jpg",
                "status": "active",
                "created_at": "2019-07-03 23:00:13",
                "updated_at": "0000-00-00 00:00:00"
            },
            {
                "ad_id": "3",
                "market_place_id": "1",
                "post_id": "2",
                "user_id": "4",
                "image": "http://coloryourlifeapp.com/app/uploads/featured_ads/1562220526_14976788_1645805929051995_6550184814850987741_o.jpg",
                "status": "active",
                "created_at": "2019-07-03 23:08:46",
                "updated_at": "0000-00-00 00:00:00"
            },
            {
                "ad_id": "9",
                "market_place_id": "1",
                "post_id": "12",
                "user_id": "4",
                "image": "http://coloryourlifeapp.com/app/uploads/featured_ads/1562739125_cropped2108720624649147659.jpg",
                "status": "active",
                "created_at": "2019-07-09 23:12:05",
                "updated_at": "0000-00-00 00:00:00"
            }
        ]
    },
    "message": "Successfully found all data",
    "status": 200
}
```
Get posts by user: GET `/posts/user/{user_id}/{viewer_id}/`  
Get posts by user per products/services: GET `/posts/user/{user_id}/{viewer_id}/{products/services/}`  
Get posts by user per products/services/all per category: GET `/posts/user/{user_id}/{viewer_id}/{products/services/all}/{category_id}`  
**_Add 'likes' and 'is_liked'_**  

##### Response
```json
200 OK
{
    "data": {
        "market_place": {
            "user": {
                "fullname": "Jc Andres",
                "username": "jc",
                "market_place_name": "testy",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
                "membership_plan": "Business"
            },
            "ratings": {
                "satisfied": 1,
                "meh": 1,
                "not-satisfied": 0
            },
            "subscribers": 2,
            "is_following": 0
        },
        "posts": [
            {
                "post_id": "1",
                "market_place_id": "2",
                "user_id": "1",
                "name": "testing",
                "sub_title": "Size: available in Large and XL",
                "price": "4500",
                "thumbnail": "http://localhost/coloryourlife/uploads/posts/services/1545115253_Screenshot_35.png",
                "description": "Hello",
                "category_id": "4",
                "status": "1",
                "created_at": "2018-12-18 14:40:53",
                "updated_at": "0000-00-00 00:00:00",
                "likes": 1,
                "is_liked": true,
                "price_formatted": "Php 4,500.00"
            }
        ]
    },
    "message": "Successfully found all data",
    "status": 200
}
```

---

##### Like/Unlike Posts
POST `/posts/like/`

##### Parameters
| Name    | Type | Description | Sample Data |
| ------- | ---- | ----------- | ----------- |
| user_id | int  | -           | 1           |
| post_id | int  | -           | 1           |

##### Response
```json
200 OK
{
    "data": {
        "post": {
            "post_id": "1",
            "market_place_id": "2",
            "user_id": "1",
            "name": "testing",
            "sub_title": "Size: available in Large and XL",
            "price": "4500",
            "thumbnail": "http://localhost/coloryourlife/uploads/posts/services/1530589980_IMG_4189.jpg",
            "description": "Hello",
            "category_id": "4",
            "status": "1",
            "created_at": "2018-07-03 11:53:00",
            "updated_at": "0000-00-00 00:00:00",
            "price_formatted": "PHP 4,500.00",
            "category_name": "Serv Cat 1",
            "user": {
                "user_id": "1",
                "username": "van",
                "email": "svboquiren@myoptimind.com",
                "fname": "Sir Vanleor",
                "lname": "Boquiren",
                "mname": "Castillo",
                "password": "$2y$10$Y9ley5ji7X92Q94pz2EixOeAJyI8AUDajq4pkLnngV0aFVQ53oGGa",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
                "membership_plan_id": "1",
                "market_place_id": "1",
                "status": "1",
                "first_log": "",
                "created_at": "2018-07-03 11:52:13",
                "updated_at": "0000-00-00 00:00:00"
            },
            "files": [
                {
                    "posts_file_id": "1",
                    "post_id": "1",
                    "filename": "http://localhost/coloryourlife/uploads/posts/services/1530589980_IMG_5145.JPG",
                    "status": "1",
                    "created_at": "2018-07-03 11:53:00",
                    "updated_at": "0000-00-00 00:00:00"
                },
                {
                    "posts_file_id": "2",
                    "post_id": "1",
                    "filename": "http://localhost/coloryourlife/uploads/posts/services/1530589980_IMG_5149.JPG",
                    "status": "1",
                    "created_at": "2018-07-03 11:53:00",
                    "updated_at": "0000-00-00 00:00:00"
                }
            ]
        }
    },
    "message": "Post liked",
    "status": 200
}
```

---

##### Get Liked Posts
GET `/posts/like/user/{user_id}/category_id/{category_id}?order=DESC` (if category id is not provided, will return **all**. if order is not specified, will default to **descending**)

##### Response
```
200 OK
{
    "data": [
        {
            "post_id": "3",
            "market_place_id": "1",
            "user_id": "1",
            "name": "testing 2",
            "sub_title": "Size: available in Large and XL",
            "price": "4500",
            "thumbnail": "http://localhost/coloryourlife/uploads/posts//1530598931_IMG_4189.jpg",
            "description": "Hello",
            "category_id": "2",
            "status": "1",
            "created_at": "2018-07-03 14:22:11",
            "updated_at": "0000-00-00 00:00:00",
            "price_formatted": "Php 4,500.00",
            "date_posted_f": "02/26/19 | 05:21 PM",
            "date_liked_f": "02/26/19 | 05:33 PM",
            "user_details": {
                "user_id": "1",
                "username": "van",
                "fullname": "Sir Vanleor Boquiren",
                "email": "svboquiren@myoptimind.com",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            }
        },
        {
            "post_id": "2",
            "market_place_id": "2",
            "user_id": "1",
            "name": "testing 2",
            "sub_title": "Size: available in Large and XL",
            "price": "4500",
            "thumbnail": "http://localhost/coloryourlife/uploads/posts//1530598566_IMG_4189.jpg",
            "description": "Hello",
            "category_id": "4",
            "status": "1",
            "created_at": "2018-07-03 14:16:06",
            "updated_at": "0000-00-00 00:00:00",
            "price_formatted": "Php 4,500.00",
            "date_posted_f": "02/26/19 | 05:21 PM",
            "date_liked_f": "02/26/19 | 05:33 PM",
            "user_details": {
                "user_id": "1",
                "username": "van",
                "fullname": "Sir Vanleor Boquiren",
                "email": "svboquiren@myoptimind.com",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            }
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

##### Update Posts
POST `/posts/{post_id}/update/`

##### Parameters
| Name            | Type   | Description           | Sample Data |
| --------------- | ------ | --------------------- | ----------- |
| name            | string | -                     | Test Posts  |
| files[]         | array  | -                     |             |
| market_place_id | int    | 1-Products/2-Services | 1           |
| price           | string | -                     | 4500        |
| description     | string | -                     |             |
| category_id     | int    | -                     | -           |
| user_id         | int    | -                     | -           |
| thumbnail       | string | -                     | -           |
| sub_title        | string | -                     | -           |
| country_id        | int | -                     | 176           |
| city_id        | int | -                     | 78406           |

##### Response
```json
200 OK
{
    "data": {
        "post_id": "1",
        "market_place_id": "1",
        "user_id": "1",
        "name": "Product Updated",
        "sub_title": "Size: available in Large and XL",
        "price": "4500",
        "thumbnail": "http://localhost/coloryourlife/uploads/posts/products/1563180690_bored_panda.jpg",
        "description": "Hello",
        "category_id": "1",
        "status": "1",
        "created_at": "2019-07-15 15:38:36",
        "updated_at": "2019-07-15 16:51:30",
        "country_id": "176",
        "city_id": "78406",
        "country_name": "Philippines",
        "city_name": "Marikina",
        "thumbnail_url": "http://localhost/coloryourlife/uploads/posts/products/1563180690_bored_panda.jpg",
        "price_formatted": "PHP 4,500.00",
        "category_name": "All Departments",
        "likes": 0,
        "is_liked": false,
        "user": {
            "user_id": "1",
            "username": "jc",
            "email": "jcandres@myoptimind.com",
            "fname": "Jc",
            "lname": "Andres",
            "mname": "",
            "password": "$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6",
            "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
            "membership_plan_id": "4",
            "market_place_id": "0",
            "market_place_name": "",
            "rank": "4",
            "status": "1",
            "first_log": "no",
            "country": "PH",
            "referral_code": "7777777",
            "referred_by": "0",
            "referrer_rank": "0",
            "points": "40",
            "bonus": "0",
            "referrals": "250",
            "rank_up_referrals": "198",
            "forgot_token": "",
            "login_type": "form",
            "sns_token": null,
            "stripe_customer_id": null,
            "created_at": "2019-07-09 16:19:10",
            "updated_at": "0000-00-00 00:00:00"
        },
        "files": [
            {
                "posts_file_id": "21",
                "post_id": "1",
                "filename": "http://localhost/coloryourlife/uploads/posts/products/1563180690_default.png",
                "status": "1",
                "created_at": "2019-07-15 16:51:30",
                "updated_at": "0000-00-00 00:00:00"
            },
            {
                "posts_file_id": "22",
                "post_id": "1",
                "filename": "http://localhost/coloryourlife/uploads/posts/products/1563180690_characters_glitch.gif",
                "status": "1",
                "created_at": "2019-07-15 16:51:30",
                "updated_at": "0000-00-00 00:00:00"
            }
        ]
    },
    "message": "Post successfully updated",
    "status": 200
}
```

---

##### Delete File
DELETE `/posts/delete-file`  
`x-www-form-urlencoded`

##### Parameters
| Name            | Type   | Description           | Sample Data |
| --------------- | ------ | --------------------- | ----------- |
| posts_file_id | int    | -  | 1           |

##### Response
```json
200 Ok
{
    "data": [],
    "message": "File successfully deleted",
    "status": 200
}
```

---

#### APP USERS

##### Register User
POST `/user/`

##### Parameters
| Name             | Type   | Description | Sample Data               |
| ---------------- | ------ | ----------- | ------------------------- |
| username         | string | -           | sel                       |
| email            | string | -           | cvalerio@myoptimind.com   |
| fname            | string | -           | Sel                       |
| lname            | string | -           | Valerio                   |
| password         | string | -           | Password123!              |
| confirm_password | string | -           | Password123!              |
| referral_code    | string | -           | 7777777                   |
| profile_pic      | file   | -           | -                         |
| country          | string | -           | PH / ID                   |

##### Response
```json
200 OK
{
    "data": {
        "user_id": "9",
        "username": "sel",
        "email": "cvalerio@myoptimind.com",
        "fname": "Sel",
        "lname": "Valerio",
        "mname": "",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
        "membership_plan_id": "0",
        "market_place_id": "0",
        "market_place_name": "",
        "rank": "1",
        "status": "1",
        "first_log": "yes",
        "referral_code": "PH6KMWCCP",
        "referred_by": "1",
        "referrer_rank": "4",
        "points": "0",
        "bonus": "0",
        "referrals": "0",
        "rank_up_referrals": "0",
        "login_type": "form",
        "sns_token": "",
        "created_at": "2019-02-28 20:07:05",
        "updated_at": "0000-00-00 00:00:00"
    },
    "message": "User successfully created",
    "status": 201
}
```
##### Register User via SNS (social networking sites)  
Call this endpoint when registering an account via sns  
POST `/user/sns`

##### Parameters
| Name             | Type   | Description | Sample Data               |
| ---------------- | ------ | ----------- | ------------------------- |
| username         | string | -           | jc2                       |
| email            | string | -           | jcandres2@myoptimind.com |
| fname            | string | -           | Jc               |
| lname            | string | -           | Andres                  |
| referral_code    | string | -           | 7777777                   |
| profile_pic      | file   | -           | -                         |
| login_type       | string | form (default), facebook, google  | facebook |
| sns_token        | string | somesecrettoken | - |
| country          | string | -           | PH / ID                   |

##### Response
```json
200 OK
{
    "data": {
        "user_id": "10",
        "username": "jc2",
        "email": "jcandres2@myoptimind.com",
        "fname": "Jc",
        "lname": "Andres",
        "mname": "",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
        "membership_plan_id": "0",
        "market_place_id": "0",
        "market_place_name": "",
        "rank": "1",
        "status": "1",
        "first_log": "yes",
        "referral_code": "PHZJ1BFW8",
        "referred_by": "1",
        "referrer_rank": "4",
        "points": "0",
        "bonus": "0",
        "referrals": "0",
        "rank_up_referrals": "0",
        "login_type": "facebook",
        "sns_token": "somesecrettoken",
        "created_at": "2019-02-28 20:17:58",
        "updated_at": "0000-00-00 00:00:00"
    },
    "message": "User successfully created",
    "status": 201
}
```

---

##### Get Users
All users: `/users/`  
##### Response
```json
{
    "data": [
        {
            "user_id": "3",
            "username": "edocampo",
            "email": "edocampo@myoptimind.com",
            "fname": "Elline",
            "lname": "Ocampo",
            "fullname": "Elline Ocampo",
            "membership_plan_id": "1",
            "market_place_id": "1",
            "created_at": "2019-02-19 20:38:46",
            "updated_at": "2019-03-14 03:17:43",
            "password": "$2y$10$iWzPTJ/E7gfHxYPOA2ddkerS/gsq.xxscmMoOeAHGDB.RLeurPnPG",
            "first_log": "yes",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
            "referral_code": "Boxszsd",
            "rank_id": "1",
            "rank_type": "White Member"
        },
        {
            "user_id": "4",
            "username": "kitcathe",
            "email": "cathcalipay@myoptimind.com",
            "fname": "Catherine",
            "lname": "Calipay",
            "fullname": "Catherine Calipay",
            "membership_plan_id": "2",
            "market_place_id": "0",
            "created_at": "2019-02-25 19:51:28",
            "updated_at": "2019-03-04 02:43:48",
            "password": "$2y$10$m19Y5x1osrjZv05cvs/YyO2bj5k6EB.1q.cDQzDebkKNfGWMZYu2y",
            "first_log": "no",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1551145887_cropped5637868124644485538.jpg",
            "referral_code": "UGUGMPB",
            "rank_id": "1",
            "rank_type": "White Member"
        },
        {
            "user_id": "7",
            "username": "boquirs",
            "email": "svboquiren@gmail.com",
            "fname": "Vanleor",
            "lname": "Boquirs",
            "fullname": "Vanleor Boquirs",
            "membership_plan_id": "2",
            "market_place_id": "0",
            "created_at": "2019-02-26 01:25:57",
            "updated_at": "2019-02-26 03:30:53",
            "password": "$2y$10$mWTuxX/6EmkNTYjzzV9kMuiliwnt/E0VYGUI/kYIMaTuuc1euN.Am",
            "first_log": "no",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1551165957_cropped7636635944667421395.jpg",
            "referral_code": "L1E8QXC",
            "rank_id": "1",
            "rank_type": "White Member"
        },
        ...
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

Single user: `/users/{user_id}`  
##### Response
```json
200 Ok
{
    "data": {
        "user_id": "6",
        "username": "sel9",
        "email": "cvalerio9@myoptimind.com",
        "login_type": "form",
        "fname": "Sel",
        "lname": "Valerio",
        "mname": null,
        "fullname": "Sel Valerio",
        "referral_code": "8Y2XSJG",
        "market_place_id": "0",
        "referred_by": "1",
        "points": "0",
        "bonus": "0",
        "referrals": "0",
        "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
        "market_place_name": "",
        "rank_id": "1",
        "rank_type": "White Member",
        "created_at": "2019-03-14 17:07:23",
        "updated_at": "0000-00-00 00:00:00",
        "rank_photo": "",
        "plan_name": null,
        "membership_plan_id": null,
        "membership_plan_name": "Not yet a member",
        "membership_expiry": "",
        "privileges": {
            "market_place_access": [],
            "chat_function": false,
            "color_bonus_access": false,
            "color_presents_access": false,
            "remaining_advertisement": 0,
            "remaining_video": 0,
            "remaining_group_message": 0,
            "remaining_live_webinar": 0
        },
        "referrals_rank": {
            "White Member": 0,
            "Bronze Member": 0,
            "Silver Member": 0,
            "Gold Member": 0,
            "Platinum Member": 0
        },
        "points_selection": []
    },
    "message": "Successfully found data",
    "status": 200
}
```

---

##### Update User
POST `/user/{user_id}/update`

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| fname    | string | -                                                   | Sel                     |
| lname    | string | -                                                   | Valerio                                    |
| profile_pic    | file | -                                                   | file.jpg                                    |
| market_place_name    | string | -                                                   | SkyeSophie                                    |

##### Response
```json
{
    "data": {
        "user_id": "9",
        "username": "sel",
        "email": "cvalerio@myoptimind.com",
        "fname": "Sel",
        "lname": "Valerio",
        "mname": "",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1551412348_bored_panda.jpg",
        "membership_plan_id": "0",
        "market_place_id": "0",
        "market_place_name": "SkyeSophie",
        "rank": "1",
        "status": "1",
        "first_log": "yes",
        "referral_code": "6KMWCCP",
        "referred_by": "1",
        "referrer_rank": "4",
        "points": "0",
        "bonus": "0",
        "referrals": "0",
        "rank_up_referrals": "0",
        "login_type": "form",
        "sns_token": "",
        "created_at": "2019-02-28 20:07:05",
        "updated_at": "2019-02-28 21:52:28"
    },
    "message": "Info successfully updated",
    "status": 200
}
```

##### Change password
POST `/user/{user_id}/update/password`

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| new_password    | string | -                                                   | newpass                     |
| old_password    | string | -                                                   | oldpass                     |


##### Response
```json
{
    "data": {
        "user_id": "3",
        "username": "edocampo",
        "email": "edocampo@myoptimind.com",
        "fname": "Elline",
        "lname": "Ocampo",
        "mname": "",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
        "membership_plan_id": "1",
        "market_place_id": "1",
        "market_place_name": "",
        "rank": "1",
        "status": "1",
        "first_log": "yes",
        "referral_code": "Boxszsd",
        "referred_by": "0",
        "referrer_rank": "0",
        "points": "0",
        "bonus": "0",
        "referrals": "0",
        "rank_up_referrals": "0",
        "login_type": "form",
        "sns_token": "",
        "created_at": "2019-02-19 20:38:46",
        "updated_at": "2019-02-28 20:36:41"
    },
    "message": "Info successfully updated",
    "status": 200
}
```



---

##### Login User
POST `/users/login/`

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| username    | string | -                                                   | jcandres@myoptimind.com                     |
| password    | string | -                                                   | Password123!                                    |
| device_id   | string | The device id of current device making this request | e1w6hEbZn-8:APA91bEUIb2JewYCIiApsMu5JfI5Ak... |
| firebase_id | string | Firebase ID                                         | somesupersecretid                             |

##### Response
```json
200 Ok
{
    "data": {
        "user_id": "1",
        "username": "jc",
        "email": "jcandres@myoptimind.com",
        "fname": "Jc",
        "lname": "Andres",
        "fullname": "Jc Andres",
        "membership_plan_id": "4",
        "created_at": "2019-02-19 20:38:46",
        "updated_at": "0000-00-00 00:00:00",
        "password": "$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6",
        "first_log": "yes",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
        "referral_code": "7777777",
        "rank_id": "4",
        "rank_type": "Gold Member",
        "membership_plan_name": "Business",
        "mp_access_limit": "-1", (-1 For all market place)
        "membership_expiry": "2019-03-31 00:00:00",
        "privileges": {
            "market_place_access": {
                "Products": true,
                "Services": true
            },
            "chat_function": true,
            "color_bonus_access": true,
            "color_presents_access": true,
            "remaining_advertisement": "4",
            "remaining_video": "3",
            "remaining_group_message": "3",
            "remaining_live_webinar": "3"
        },
        "with_pending_payment": "Yes"
    },
    "message": "User login successful",
    "status": "200"
}
```

---

##### Login User via SNS
POST `/users/login/sns`

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| sns_token    | string | -                                                   | somesecrettoken                                    |
| device_id   | string | The device id of current device making this request | e1w6hEbZn-8:APA91bEUIb2JewYCIiApsMu5JfI5Ak... |
| firebase_id | string | Firebase ID                                         | somesupersecretid                             |

##### Response
```json
200 Ok
{
    "data": {
        "user_id": "10",
        "username": "jc2",
        "email": "jcandres2@myoptimind.com",
        "fname": "Jc",
        "lname": "Andres",
        "fullname": "Jc Andres",
        "membership_plan_id": "0",
        "created_at": "2019-02-28 20:17:58",
        "updated_at": "0000-00-00 00:00:00",
        "password": "",
        "sns_token": "somesecrettoken",
        "first_log": "yes",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
        "referral_code": "ZJ1BFW8",
        "rank_id": "1",
        "rank_type": "White Member",
        "membership_plan_name": "Not yet a member",
        "mp_access_limit": "Not yet a member", (-1 For all market place)
        "membership_expiry": "",
        "privileges": {
            "market_place_access": [],
            "chat_function": false,
            "color_bonus_access": false,
            "color_presents_access": false,
            "remaining_advertisement": 0,
            "remaining_video": 0,
            "remaining_group_message": 0,
            "remaining_live_webinar": 0
        },
        "with_pending_payment": "Yes"
    },
    "message": "User login successful",
    "status": "200"
}
```

---

##### Logout User
POST `/users/logout/`

##### Parameters
| Name      | Type   | Description         | Sample Data                                   |
| --------- | ------ | ------------------- | --------------------------------------------- |
| device_id | string | Device id to delete | e1w6hEbZn-8:APA91bEUIb2JewYCIiApsMu5JfI5Ak... |

##### Response
```json
200 Ok
{
    "data": null,
    "message": "Logout success. Device notifications set to inactive state.",
    "status": 200
}
```
---

##### Upload Video
POST `/users/video/`
##### Parameters
| Name    | Type   | Description | Sample Data                                 |
| ------- | ------ | ----------- | ------------------------------------------- |
| title   | string | -           | Trys                                        |
| user_id | int    | -           | 1                                           |
| video   | string | -           | https://www.youtube.com/watch?v=zcGOoDThC1E |
##### Response
```json
200 Ok
{
    "data": {
        "video_id": "1",
        "user_id": "1",
        "title": "Trys",
        "video": "https://www.youtube.com/watch?v=zcGOoDThC1E",
        "created_at": "2018-06-22 10:50:31",
        "updated_at": "0000-00-00 00:00:00",
        "remaining_video": "0"
    },
    "message": "Successfully saved user's video",
    "status": "201"
}
```

---

##### Get Video
GET `/users/video/{user_id}/`

##### Response
```json
200 Ok
{
    "data": [
        {
            "video_id": "1",
            "user_id": "1",
            "title": "Trys",
            "video": "https://www.youtube.com/watch?v=zcGOoDThC1E",
            "created_at": "2018-06-22 10:50:31",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully found user videos",
    "status": "200"
}
```

---

##### Follow User
POST `/users/follow/`

##### Parameters
| Name         | Type | Description | Sample Data |
| ------------ | ---- | ----------- | ----------- |
| follower_id  | int  | user_id     | 2           |
| following_id | int  | user_id     | 1           |

##### Response
```json
200 Ok
{
    "data": {
        "follower": {
            "user_id": "2",
            "username": "jc",
            "email": "jcandres@myoptimind.com",
            "fname": "Jc",
            "lname": "Andres",
            "mname": "Santos",
            "membership_plan_id": "1",
            "market_place_id": "1",
            "profile_pic": "",
            "status": "1",
            "created_at": "2018-06-20 15:35:49",
            "updated_at": "0000-00-00 00:00:00"
        },
        "following": {
            "user_id": "1",
            "username": "van",
            "email": "svboquiren@myoptimind.com",
            "fname": "Sir Vanleor",
            "lname": "Boquiren",
            "mname": "Castillo",
            "membership_plan_id": "1",
            "market_place_id": "1",
            "profile_pic": "",
            "status": "1",
            "created_at": "2018-06-13 11:49:06",
            "updated_at": "0000-00-00 00:00:00"
        }
    },
    "message": "User successfully unfollowed",
    "status": 200
}
```

---

##### Get Following List
GET `/user/{viewer_id}/following/{user_id}`

##### Response
```json
200 Ok
{
    "data": [
        {
            "user_id": "4",
            "username": "sir_van",
            "email": "svboquiren@myoptimind.com",
            "fname": "Van",
            "lname": "Boquiren",
            "fullname": "Van Boquiren",
            "referral_code": "A7LC5BG",
            "profile_pic": ""
        },
        {
            "user_id": "5",
            "username": "sir_van",
            "email": "svboquiren@myoptimind.com",
            "fname": "Van",
            "lname": "Boquiren",
            "fullname": "Van Boquiren",
            "referral_code": "A7LC5BG",
            "profile_pic": ""
        }
    ],
    "message": "Successfully found all the data",
    "status": 200
}
```

---

##### Get Followers
GET `/user/{viewer_id}/followers/{user_id}`

##### Response
```json
200 Ok
{
    "data": [
        {
            "user_id": "1",
            "username": "jc",
            "email": "jcandres@myoptimind.com",
            "fname": "Jc",
            "lname": "Andres",
            "mname": "",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
            "membership_plan_id": "4",
            "market_place_id": "0",
            "market_place_name": "",
            "rank": "4",
            "status": "1",
            "first_log": "no",
            "referral_code": "7777777",
            "referred_by": "0",
            "referrer_rank": "0",
            "points": "92.5",
            "bonus": "0",
            "referrals": "251",
            "rank_up_referrals": "199",
            "forgot_token": "",
            "login_type": "form",
            "sns_token": "",
            "stripe_customer_id": "",
            "created_at": "2019-05-28 00:36:12",
            "updated_at": "2019-05-30 04:42:50",
            "fullname": "Jc Andres"
        },
        {
            "user_id": "2",
            "username": "jp_ang",
            "email": "jpang@myoptimind.com",
            "fname": "Patrick",
            "lname": "Ang",
            "mname": "",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
            "membership_plan_id": "1",
            "market_place_id": "1",
            "market_place_name": "",
            "rank": "1",
            "status": "1",
            "first_log": "no",
            "referral_code": "1234567",
            "referred_by": "0",
            "referrer_rank": "0",
            "points": "0",
            "bonus": "0",
            "referrals": "0",
            "rank_up_referrals": "0",
            "forgot_token": "",
            "login_type": "form",
            "sns_token": "",
            "stripe_customer_id": "",
            "created_at": "2019-05-28 00:36:12",
            "updated_at": "0000-00-00 00:00:00",
            "fullname": "Patrick Ang"
        }
    ],
    "message": "Successfully found all the data",
    "status": 200
}
```

---

##### Get Bonus Points
GET `/user/{user_id}/bonus/`

##### Response
```json
200 Ok
{
    "data": [
        {
            "user_id": "1",
            "points": "100",
            "free": "101",
            "challenger": "0",
            "economy": "0",
            "business": "1",
            "rank_type": "Gold Member"
        }
    ],
    "message": "Successfully found data",
    "status": 200
}
```

#### Get presents referral
GET `/user/{user_id}/presents/referral`

##### Response
```json
200 Ok
{
    "data": [
        {
            "user_id": "1",
            "points": "100",
            "free": "101",
            "challenger": "0",
            "economy": "0",
            "business": "1",
            "rank_type": "Gold Member"
        }
    ],
    "message": "Successfully found data",
    "status": 200
}
```
---

##### Send Referral Code
POST `/users/send-referral-code`
##### Parameters
| Name     | Type   | Description | Sample Data    |
| -------- | ------ | ----------- | -------------- |
| user_id  | int    | -           | 1              |
| emails[] | string | -           | email@mail.com |
| emails[] | string | -           | email@mail.com |
##### Response
```json
200 Ok
{
    "data": {
        "user_id": "9",
        "emails": [
            "jcandres@myoptimind.com",
            "cvalerio@myoptimind.com"
        ]
    },
    "message": "Successfully sent mails",
    "status": 200
}
```

---

##### Get Referrals
GET `/user/{user_id}/referrals/`


##### Response
```json
200 Ok
{
    "data": {
        "total_referrals": "251",
        "referrals": [
            {
                "username": "jc2",
                "fname": "Jc",
                "lname": "Andres",
                "membership_plan": "Free",
                "total_points": "4",
                "rank_type": "White Member"
            },
            {
                "username": "jc3",
                "fname": "Jc",
                "lname": "Andres",
                "membership_plan": "Challenger",
                "total_points": "44",
                "rank_type": "White Member"
            },
            {
                "username": "jc4",
                "fname": "Jc",
                "lname": "Andres",
                "membership_plan": "Economy",
                "total_points": "24",
                "rank_type": "White Member"
            },
            {
                "username": "jc5",
                "fname": "Jc",
                "lname": "Andres",
                "membership_plan": "Business",
                "total_points": "40",
                "rank_type": "White Member"
            }
        ]
    },
    "message": "Successfully found data",
    "status": 200
}
```

---

##### Forgot password
POST `/user/forgot-password/`

##### Parameters
| Name  | Type    | Description | Sample Data             |
| ----- | ------- | ----------- | ----------------------- |
| email | varchar | -           | edocampo@myoptimind.com |

##### Response
```json
200 Ok
{
    "data": [],
    "message": "Please check your email. We have sent a verification code to edocampo@myoptimind.com",
    "status": 200
}
```

```json
404 Not found
{
    "data": [],
    "message": "Email does not exists.",
    "status": 404
}
```


##### Forgot username
POST `/user/forgot-username/`

##### Parameters
| Name     | Type    | Description | Sample Data             |
| -------- | ------- | ----------- | ----------------------- |
| email    | varchar | -           | edocampo@myoptimind.com |
| password | varchar | -           | 123                     |
##### Response
```json
200 Ok
{
    "data": [],
    "message": "Please check your email. We have sent a verification code to edocampo@myoptimind.com",
    "status": 200
}
```

```json
404 Not found
{
    "data": [],
    "message": "Email does not exists.",
    "status": 404
}
```

##### Get Color Points
GET `/user/{user_id}/points/`

##### Response
```json
{
    "data": {
        "user_id": "1",
        "rank_type": "Platinum Member",
        "points": "40",
        "rank_photo": "http://localhost/coloryourlife/frontend/img/rank_logo/Gold_logo.png",
        "points_web_link": "http://localhost/coloryourlife/points"
    },
    "message": "Successfully found data",
    "status": 200
}
```

##### Get My Market Place
GET `/user/{user_id}/marketplace/`  
GET `/user/{user_id}/viewer/{viewer_id}/marketplace/`

##### Response
```json
{
    "data": {
        "user": {
            "fullname": "Jc Andres",
            "username": "jc",
            "market_place_name": "testy",
            "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
        },
        "ratings": {
            "satisfied": 1,
            "meh": 1,
            "not-satisfied": 0
        },
        "subscribers": 2,
        "is_following": 0
    },
    "message": "Successfully found data",
    "status": 200
}
```

---

##### Select Market Place
POST `/users/marketplace/`  
##### Parameters
| Name            | Type | Description | Sample Data |
| --------------- | ---- | ----------- | ----------- |
| user_id | int  | -           | 4           |
| market_place_id         | int  | -           | 2           |
##### Response
```json
200 Ok
{
    "data": {
        "market_place_access": {
            "Services": true
        }
    },
    "message": "Successfully updated market place",
    "status": 200
}
```
---

##### Get Plan Info
GET `/users/plan-info/{user_id}/`  
##### Response
```json
200 Ok
{
    "data": {
        "user_id": "4",
        "username": "sel",
        "email": "cvalerio@myoptimind.com",
        "fname": "Sel",
        "lname": "Valerio",
        "fullname": "Sel Valerio",
        "membership_plan_id": "4",
        "created_at": "2019-04-29 22:14:27",
        "updated_at": "2019-04-29 22:42:11",
        "password": "$2y$10$A9j5G5FUWi1nrWx4eExeyeDzEflgUy3hmKA4MOGoWWZ1APYsvsnVy",
        "first_log": "no",
        "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
        "referral_code": "PHTWQYEF7",
        "rank_id": "1",
        "rank_type": "White Member",
        "membership_plan_name": "Business",
        "mp_access_limit": "-1",
        "membership_expiry": "2019-08-30 11:33:36",
        "privileges": {
            "market_place_access": {},
            "chat_function": true,
            "color_bonus_access": false,
            "color_presents_access": false,
            "remaining_advertisement": "0",
            "remaining_video": "0",
            "remaining_group_message": "0",
            "remaining_live_webinar": "0"
        },
        "with_pending_payment": "Yes"
    },
    "message": "Successfully found data",
    "status": 200
}
```
---

---

#### ADS
##### Create Ads
POST `/ads/`
##### Parameters
| Name            | Type | Description | Sample Data |
| --------------- | ---- | ----------- | ----------- |
| market_place_id | int  | -           | 1           |
| post_id         | int  | -           | 1           |
| user_id         | int  | -           | 1           |
| files           | file | -           | -           |
##### Response
```json
200 Ok
{
    "data": {
        "ad_id": "1",
        "market_place_id": "1",
        "post_id": "1",
        "user_id": "1",
        "image": "http://localhost/coloryourlife/uploads/featured_ads/1529636649_bored_panda.jpg",
        "created_at": "2018-06-22 11:04:09",
        "updated_at": "0000-00-00 00:00:00",
        "remaining_advertisement": "2"
    },
    "message": "Ads successfully created",
    "status": 201
}
```

---

##### Get Ads
Get all ads: GET `/ads/`  
Get all featured ads: GET `/ads/featured/`  
Get all featured ads by products/services: GET `/ads/featured/{products/services}/`  
##### Response
```json
200 Ok
{
    "data": [
        {
            "ad_id": "1",
            "market_place_id": "1",
            "post_id": "1",
            "user_id": "1",
            "image": "1529636649_bored_panda.jpg",
            "created_at": "2018-06-22 11:04:09",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

#### CATEGORIES
##### Get Categories
Get all categories: GET `/categories/`  
Get all categories by product/services: GET `/categories/{products/services}/`  
##### Response
```json
200 Ok
{
    "data": [
        {
            "category_id": "1",
            "name": "Prod Cat 1",
            "market_place_id": "1"
        },
        {
            "category_id": "2",
            "name": "Prod Cat 2",
            "market_place_id": "1"
        },
        {
            "category_id": "3",
            "name": "Prod Cat 3",
            "market_place_id": "1"
        },
        {
            "category_id": "4",
            "name": "Serv Cat 1",
            "market_place_id": "2"
        },
        {
            "category_id": "5",
            "name": "Serv Cat 2",
            "market_place_id": "2"
        },
        {
            "category_id": "6",
            "name": "Serv Cat 3",
            "market_place_id": "2"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

#### POST REVIEWS
##### Create post reviews
POST `/reviews/post`
##### Parameters
| Name    | Type   | Description               | Sample Data                     |
| ------- | ------ | ------------------------- | ------------------------------- |
| user_id | int    | user_id of the *reviewer* | 1                               |
| post_id | int    | -                         | 2                               |
| review  | string | -                         | Test Review                     |
| rating  | string | -                         | Satisfied / Meh / Not satisfied |
##### Response
```json
200 Ok
{
    "data": [
        {
            "review_id": "3",
            "user_id": "1",
            "post_id": "2",
            "review": "Test Review",
            "rating": "Satisfied",
            "created_at": "2018-06-22 11:13:28",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully created new data",
    "status": 201
}
```

---

##### Get post reviews
Get all post reviews: GET `/reviews/post`  
Get all post reviews by post_id: GET `/reviews/post/{post_id}/`  
##### Response
```json
200 Ok
{
    "data": [
        {
            "post_review_id": "1",
            "user_id": "1",
            "post_id": "1",
            "review": "Omaewa mou shindeiru",
            "rating": "Meh",
            "created_at": "2018-07-17 17:45:14",
            "updated_at": "0000-00-00 00:00:00",
            "user": {
                "user_id": "1",
                "username": "jc",
                "email": "jcandres@myoptimind.com",
                "fname": "Jc",
                "lname": "Andres",
                "mname": null,
                "fullname": "Jc Andres",
                "referral_code": "JPsJTjX",
                "referred_by": "0",
                "points": "0",
                "bonus": "0",
                "referrals": "0",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            },
            "created_at_formatted": "07/17/18 | 05:45 PM"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

#### USER REVIEWS
##### Create user reviews
POST `/reviews/user`
##### Parameters
| Name        | Type   | Description                        | Sample Data                     |
| ----------- | ------ | ---------------------------------- | ------------------------------- |
| reviewee_id | int    | One being reviewed                 | 1                               |
| reviewer_id | int    | One who's going to give the review | 2                               |
| review      | string | -                                  | Test Review                     |
| rating      | string | -                                  | Satisfied / Meh / Not satisfied |
##### Response
```json
200 Ok
{
    "data": [
        {
            "user_review_id": "3",
            "reviewee_id": "1",
            "reviewer_id": "2",
            "review": "Test Review",
            "rating": "Satisfied",
            "created_at": "2018-06-22 11:13:28",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully created new data",
    "status": 201
}
```

---

##### Get user reviews
Get all user reviews: GET `/reviews/user`  
Get all user reviews by reviewee_id (the one who's getting reviewed) : GET `/reviews/user/{reviewee_id}/`  
##### Response
```json
200 Ok
{
    "data": [
        {
            "user_review_id": "1",
            "reviewee_id": "1",
            "reviewer_id": "1",
            "review": "Omaewa mou shindeiru",
            "rating": "Meh",
            "created_at": "2018-07-17 17:45:14",
            "updated_at": "0000-00-00 00:00:00",
            "user": {
                "user_id": "1",
                "username": "jc",
                "email": "jcandres@myoptimind.com",
                "fname": "Jc",
                "lname": "Andres",
                "mname": null,
                "fullname": "Jc Andres",
                "referral_code": "JPsJTjX",
                "referred_by": "0",
                "points": "0",
                "bonus": "0",
                "referrals": "0",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            },
            "created_at_formatted": "07/17/18 | 05:45 PM"
        },
        {
            "user_review_id": "2",
            "reviewee_id": "1",
            "reviewer_id": "2",
            "review": "Nani?!",
            "rating": "Satisfied",
            "created_at": "2018-07-17 17:45:14",
            "updated_at": "0000-00-00 00:00:00",
            "user": {
                "user_id": "2",
                "username": "jp_ang",
                "email": "jpang@myoptimind.com",
                "fname": "Patrick",
                "lname": "Ang",
                "mname": "",
                "fullname": "Patrick Ang",
                "referral_code": "0T4Mjm7",
                "referred_by": "0",
                "points": "0",
                "bonus": "0",
                "referrals": "0",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            },
            "created_at_formatted": "07/17/18 | 05:45 PM"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

#### MEMBERSHIP PLANS
##### List With Details

GET `/plans/details/`

##### Response
```json
200 Ok
{
    "data": [
        {
            "name": "Free",
            "rate": "Free for 3 MOS",
            "items": [
                {
                    "type": "normal",
                    "text": "Access to one market place only (Free for 3 mos. only)"
                },
                {
                    "type": "styled",
                    "text": "*If member wants to access the other market places:"
                },
                {
                    "type": "normal",
                    "text": "$1 per month / marketplace"
                },
                {
                    "type": "normal",
                    "text": "Chat function"
                }
            ]
        },
        {
            "name": "Challenger",
            "rate": "$12 / yearly",
            "items": [
                {
                    "type": "normal",
                    "text": "Access to one market place only (Free for 3 mos. only)"
                },
                {
                    "type": "styled",
                    "text": "*If member wants to access the other market places:"
                },
                {
                    "type": "normal",
                    "text": "$1 per month / marketplace"
                },
                {
                    "type": "normal",
                    "text": "Chat function"
                }
            ]
        },
        {
            "name": "Economy",
            "rate": "$10 / monthly",
            "items": [
                {
                    "type": "normal",
                    "text": "Access to one market place only (Free for 3 mos. only)"
                },
                {
                    "type": "styled",
                    "text": "*If member wants to access the other market places:"
                },
                {
                    "type": "normal",
                    "text": "$1 per month / marketplace"
                },
                {
                    "type": "normal",
                    "text": "Chat function"
                }
            ]
        },
        {
            "name": "Business",
            "rate": "$50 / monthly",
            "items": [
                {
                    "type": "normal",
                    "text": "$50 per month"
                },
                {
                    "type": "normal",
                    "text": "Access to all marketplace"
                },
                {
                    "type": "normal",
                    "text": "Chat function"
                },
                {
                    "type": "normal",
                    "text": "Advertisement display free for the first 3 ads (*$5/ad after 3 ads)"
                }
            ]
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

##### Select Membership Plan

Note: `If membership plan is free url will be blank`  

POST `/plans/select/`  

Success url: `/membership/success/`  
Success url for paynamics: `/membership/success/?paynamics_status=pending`  

##### Parameters
| Name               | Type | Description | Sample Data |
| ------------------ | ---- | ----------- | ----------- |
| user_id            | int  | -           | 1           |
| membership_plan_id | int  | -           | 3           |

##### Response (Free)
```json
200 Ok
{
    "data": {
        "url": "",
        "membership_name": "Free",
        "membership_expiry": "2019-05-28 14:21:26",
        "privileges": {
            "market_place_access": {
                "Products": true
            },
            "chat_function": true,
            "color_bonus_access": false,
            "color_presents_access": false,
            "remaining_advertisement": "0",
            "remaining_video": "0",
            "remaining_group_message": "0",
            "remaining_live_webinar": "0"
        }
    },
    "message": "Membership verified",
    "status": 200
}
```

##### Response
```json
200 Ok
{
    "data": {
        "url": "http://coloryourlife.betaprojex.com/app/membership/73",
        "ios_url": "http://coloryourlife.betaprojex.com/app/membership/73/ios",
        "android_url": "http://coloryourlife.betaprojex.com/app/membership/73/android",
        "membership_name": "Economy",
        "plan_rate": "10.00",
        "sessionid": "yg20Y19HhDuMzxQPDsUV",
        "payment_logs_id": 66
    },
    "message": "Membership verified",
    "status": 200
}
```

---

##### Apply Plan Now

POST `/plans/apply/`  

##### Parameters
| Name               | Type | Description | Sample Data |
| ------------------ | ---- | ----------- | ----------- |
| user_id            | int  | -           | 1           |

##### Response
```json
200 Ok
{
    "data": {
        "cancelled_plans": [
            {
                "membership_plan_id": "2",
                "membership_name": "Challenger",
                "expiration_date": "2021-04-17 14:11:07"
            }
        ],
        "user": {
            "user_id": "4",
            "username": "sel",
            "email": "cvalerio@myoptimind.com",
            "login_type": "form",
            "fname": "Sel",
            "lname": "Valerio",
            "mname": null,
            "fullname": "Sel Valerio",
            "referral_code": "LPSTKV8",
            "market_place_id": "0",
            "referred_by": "1",
            "points": "0",
            "bonus": "0",
            "referrals": "0",
            "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
            "market_place_name": "",
            "rank_id": "1",
            "rank_type": "White Member",
            "created_at": "2019-01-17 13:35:53",
            "updated_at": "2019-02-13 10:43:40",
            "rank_photo": "",
            "plan_name": "Economy",
            "membership_plan_id": "3",
            "membership_plan_name": "Economy",
            "membership_expiry": "2019-03-26 15:28:34",
            "privileges": {
                "market_place_access": {
                    "Products": true,
                    "Services": true
                },
                "chat_function": true,
                "color_bonus_access": true,
                "color_presents_access": true,
                "remaining_advertisement": "3",
                "remaining_video": "3",
                "remaining_group_message": "3",
                "remaining_live_webinar": "3"
            }
        }
    },
    "message": "Membership applied",
    "status": 200
}
```

---

#### Notifications
##### Get all notifs by user id

GET `/notifications/user/:user_id`

##### Response
```json
200 OK
{
    "data": [
        {
            "notification_id": "382",
            "user_id": "79",
            "type": "new_post",
            "payload": "a:2:{s:7:\"post_id\";i:62;s:13:\"post_owner_id\";s:3:\"104\";}",
            "is_read": "0",
            "created_at": "2019-06-24 17:47:19",
            "updated_at": "0000-00-00 00:00:00",
            "message": "Brian12 Griffin posted a new Product.",
            "message_hl": "Brian12 Griffin",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
            "unserialized_payload": {
                "post_id": 62,
                "post_owner_id": "104"
            },
            "relative_time": "2 minutes ago"
        },
        {
            "notification_id": "20",
            "user_id": "1",
            "type": "new_user_review",
            "payload": "a:1:{s:11:\"reviewer_id\";s:1:\"3\";}",
            "is_read": "0",
            "created_at": "2019-05-21 16:33:56",
            "updated_at": "0000-00-00 00:00:00",
            "message": "edocampo written you a new review",
            "message_hl": "edocampo",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1544075085_cropped-605674427.jpg",
            "unserialized_payload": {
                "reviewer_id": "3"
            },
            "relative_time": "12 seconds ago"
        },
        {
            "notification_id": "17",
            "user_id": "1",
            "type": "new_post_review",
            "payload": "a:2:{s:7:\"post_id\";s:1:\"1\";s:11:\"reviewer_id\";s:1:\"3\";}",
            "is_read": "0",
            "created_at": "2019-05-21 16:09:47",
            "updated_at": "0000-00-00 00:00:00",
            "message": "edocampo wrote a review in your testing post",
            "message_hl": "edocampo",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1544075085_cropped-605674427.jpg",
            "unserialized_payload": {
                "post_id": "1",
                "reviewer_id": "3"
            },
            "relative_time": "9 seconds ago"
        },
        {
            "notification_id": "3",
            "user_id": "1",
            "type": "like",
            "payload": "a:2:{s:8:\"liker_id\";s:1:\"1\";s:7:\"post_id\";s:1:\"1\";}",
            "is_read": "1",
            "created_at": "2018-08-03 15:09:39",
            "updated_at": "2018-08-03 15:14:06",
            "message": "jc has liked liked your testing post",
            "message_hl": "jc",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1544075085_cropped-605674427.jpg",
            "unserialized_payload": {
                "liker_id": "1",
                "post_id": "1"
            },
            "relative_time": "32 minutes ago"
        },
        {
            "notification_id": "2",
            "user_id": "1",
            "type": "user_follow",
            "payload": "a:1:{s:11:\"follower_id\";s:1:\"1\";}",
            "is_read": "1",
            "created_at": "2018-08-03 14:53:09",
            "updated_at": "2018-08-03 15:14:06",
            "message": "jp_ang has liked liked your testing post",
            "message_hl": "jp_ang",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/1544075085_cropped-605674427.jpg",
            "unserialized_payload": {
                "follower_id": "1"
            },
            "relative_time": "48 minutes ago"
        },
        {
            "notification_id": "1",
            "user_id": "4",
            "type": "referral",
            "payload": "a:2:{s:6:\"status\";s:7:\"success\";s:11:\"referred_id\";s:1:\"8\";}",
            "is_read": "1",
            "created_at": "2019-05-20 11:40:29",
            "updated_at": "2019-05-20 03:52:53",
            "message": "You have successful referral from Sel Valerio!",
            "message_hl": "Sel Valerio",
            "profile_pic": "http://coloryourlife.betaprojex.com/app/uploads/user_profile_pic/default.png",
            "unserialized_payload": {
                "status": "success",
                "referred_id": "8"
            },
            "relative_time": "6 hours ago"
        },
    ],
    "message": "Got all notifications",
    "status": 200
}
```

---

##### Get total unread notifs
GET `/notifications/count/:user_id`

##### Response
```json
200 Ok
{
    "data": {
        "notif_count": 2
    },
    "message": "Got the total number of unread notifications",
    "status": 200
}
```

---

#### Messages

##### Get all inbox previews (like in messages landing/home page)
GET `/messages/user/:user_id/previews`
##### Response
```json
200 OK
{
    "data": [
        {
            "body": "You: Whaddup yo!!😅",
            "user": {
                "id": "1",
                "name": "Jc Andres",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            },
            "is_read": "0",
            "relative_time": "3 days ago"
        },
        {
            "body": "🤣😂 I'm so funny",
            "user": {
                "id": "3",
                "name": "Elline Ocampo",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png"
            },
            "is_read": "1",
            "relative_time": "49 seconds ago"
        }
    ],
    "message": "Got all message previews",
    "status": 200
}
```

---

##### Get conversation with another person
#### NOTE: Use the parameter `?page=:(int)pagenumber:` for pagination (defaults to 1)

GET `/messages/user/:conversation_owner_id/conversation/:conversation_partner_id`
##### Response
```json
200 OK
{
    "data": [
        {
            "body": "Hahahah! 🤣😂",
            "thumbnail_url": "http://localhost/coloryourlife/uploads/message_attachments/1546401142_Screenshot_35.png",
            "thumbnail_orientation": "landscape",
            "user": {
                "id": "3",
                "name": "Elline Ocampo",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
                "is_you": false
            },
            "created_at_formatted": "September 14, 2018 04:32:24PM",
            "relative_time": "3 days ago"
        },
        {
            "body": "🤣😂 So funney hahahahah",
            "thumbnail_url": "http://localhost/coloryourlife/uploads/message_attachments/1546401142_Screenshot_35.png",
            "thumbnail_orientation": "landscape",
            "user": {
                "id": "2",
                "name": "Patrick Ang",
                "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
                "is_you": true
            },
            "created_at_formatted": "September 14, 2018 04:33:23PM",
            "relative_time": "3 days ago"
        }
    ],
    "message": "Got all messages",
    "status": 200,
    "page": 1
}
```

---

##### Send a message
POST `/messages`

##### Parameters
| Name        | Type | Description             | Sample Data       |
| ----------- | ---- | ----------------------- | ----------------- |
| sender_id   | int  | The id of the sender    | 2                 |
| receiver_id | int  | The id of the recipient | 3                 |
| body        | text | The message body        | Lorem ipsum dolor |
| thumbnail   | file | The attachment          | -                 |

##### Response
```json
201 Created
{
    "data": {
        "body": "lorem ipsum dolor",
        "thumbnail_url": "http://localhost/coloryourlife/uploads/message_attachments/1546401142_Screenshot_35.png",
        "thumbnail_orientation": "landscape",
        "user": {
            "id": "2",
            "name": "Patrick Ang",
            "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
            "is_you": true
         }
    },
    "message": "Message sent",
    "status": 201
}
```

---

##### Get total unread messages
GET `/messages/count/:user_id`

##### Response
```json
200 Ok
{
    "data": {
        "message_count": 2
    },
    "message": "Got the total number of unread messages",
    "status": 200
}
```

---

##### Inquire about a product or service
POST `/messages/posts`  
Sends a direct message with the name of the post interested in

##### Parameters
| Name        | Type | Description                      | Sample Data       |
| ----------- | ---- | -------------------------------- | ----------------- |
| sender_id   | int  | The id of the sender             | 2   
| post_id     | int  | The id of the post interested in | 3                 

##### Response
```json
{
    "data": {
        "body": "Hi! Is 'testing' available?",
        "thumbnail_url": "http://localhost/coloryourlife/uploads/posts/services/1546414427_Screenshot_20180407-150421.png",
        "thumbnail_orientation": "landscape",
        "user": {
            "id": "1",
            "name": "Jc Andres",
            "profile_pic": "http://localhost/coloryourlife/uploads/user_profile_pic/default.png",
            "is_you": true
        }
    },
    "message": "Message sent",
    "status": 201
}
```

---

#### Send message to all followers
POST `/messages/user/:user_id/followers`

##### Parameters
| Name | Type | Description      | Sample Data       |
| ---- | ---- | ---------------- | ----------------- |
| body | text | The message body | Lorem ipsum dolor |

##### Response
```json
201 Created
{
    "data": {
        "body": "Lorem ipsum dolor",
        "remaining_group_message": "2"
    },
    "message": "Message sent to all followers",
    "status": 201
}
```

---

### Affiliates

#### Get all affiliates
GET `/affiliates`


##### Response
```json
200 OK
{
    "data": [
        {
            "affiliate_id": "1",
            "image_path": "http://localhost/coloryourlife/public/affiliates_images/1_110pixels.jpg",
            "image_name": "110pixels.jpg",
            "title": "Affiliate 1",
            "created_at": "2018-09-24 14:40:36",
            "updated_at": "2018-09-24 14:40:36"
        },
        {
            "affiliate_id": "2",
            "image_path": "http://localhost/coloryourlife/public/affiliates_images/2_samplethumb.jpeg",
            "image_name": "samplethumb.jpeg",
            "title": "Affiliate 2",
            "created_at": "2018-09-24 14:40:46",
            "updated_at": "2018-09-24 14:40:46"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

##### Create Affiliate

POST `/affiliates/create`

##### Parameters
| Name            | Type | Description                           | Sample Data       |
| --------------- | ---- | ------------------------------------- | ----------------- |
| affiliate_image | file | image of the affiliate (jpg,jpeg,png) | Lazada_logo.jpeg |
| title | text | Title of the affiliate | Lazada, Amazon

##### Response

```json
201 Created
{
    "data": {
        "title": "Lazada"
    },
    "message": "Successfully created.",
    "status": 201
}

```

---

### Redeem Requests

#### get all requests
GET `/redeem-requests`

##### Response

```json
200 OK
{
    "data": [
        {
            "redeem_request_id": "1",
            "user_id": "4",
            "fname": "Jhondee",
            "lname": "Diaz",
            "mobile_no": "0913123",
            "address": "quezon city",
            "points_to_redeem": "10",
            "affiliate_id": "2",
            "url": "www.hello.com",
            "notes": "elow",
            "status": "accepted",
            "created_at": "2018-09-24 11:42:22",
            "updated_at": "2018-09-24 11:50:26"
        },
        {
            "redeem_request_id": "2",
            "user_id": "4",
            "fname": "Jhondee",
            "lname": "Diaz",
            "mobile_no": "0913123",
            "address": "quezon city",
            "points_to_redeem": "10",
            "affiliate_id": "2",
            "url": "www.hello.com",
            "notes": "elow",
            "status": "pending",
            "created_at": "2018-09-24 14:20:29",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

#### create redeem request

POST `/color-points/redeem`

##### Parameters

| Name            | Type | Description                           | Sample Data       |
| --------------- | ---- | ------------------------------------- | ----------------- |
|user_id| int | Id of the requester|
| affiliate_id | int | Id of the selected affiliate |
| fname | text | First name | Brendan |
| lname | text | Last name | Coco |
| mobile_no | text | Mobile number | 0918312331 |
| address | text | | 157 P. tuazon, Cubao, Quezon City |
| points_to_redeem | int | Number of points to redeem | 10,15,20 |
| url | text | | |
| notes | text | | |

##### Response

```json
{
    201 Created
    "data": [
        {
            "redeem_request_id": "3",
            "user_id": "4",
            "fname": "Jhondee",
            "lname": "Diaz",
            "mobile_no": "0913123",
            "address": "quezon city",
            "points_to_redeem": "10",
            "affiliate_id": "2",
            "url": "www.hello.com",
            "notes": "elow",
            "status": "pending",
            "created_at": "2018-09-24 15:08:16",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully created new data",
    "status": 201
}
```

---

#### accept or decline a request

POST `/redeem-request/{request_id}`

##### Parameters
| Name            | Type | Description                           | Sample Data       |
| --------------- | ---- | ------------------------------------- | ----------------- |
|action| text | action to be performed to the request | 'accept','decline' |


### Presents

#### get presents

##### get all presents
GET `/presents`

##### get all presents of the user
GET `/presents/user/{:user_id}`

##### Response
```json
200 OK
{
    "data": {
        "all": [
            {
                "present_id": "2",
                "image_path": "http://localhost/coloryourlife/public/presents_images/2_110pixels.jpg",
                "image_name": "110pixels.jpg",
                "title": "Present 1",
                "is_featured": "0",
                "type": "silver",
                "created_at": "2018-09-24 13:41:32",
                "updated_at": "2018-09-24 13:41:32"
            }
        ],
        "featured": [],
        "user_stats": {
            "Bronze Member": 1,
            "Silver Member": 1,
            "Gold Member": 1,
            "Platinum Member": 0,
            "points": "40",
            "rank_type": "Gold Member",
            "referrals": "250",
            "rank_photo": "http://coloryourlife.betaprojex.com/app/frontend/img/rank_logo/Gold_logo.png"
        }
    },
    "message": "Got all data",
    "status": 200
}
```

---

##### get all presents by present request id

GET `/api/presents/request/{present_request_id}`

##### Response
```json
200 OK
{
    "data": {
        "all": [
            {
                "present_id": "2",
                "image_path": "http://localhost/coloryourlife/public/presents_images/2_110pixels.jpg",
                "image_name": "110pixels.jpg",
                "title": "Present 1",
                "is_featured": "0",
                "type": "silver",
                "created_at": "2018-09-24 13:41:32",
                "updated_at": "2018-09-24 13:41:32"
            }
        ],
        "featured": []
    },
    "message": "Got all data",
    "status": 200
}
```

---

#### create present

POST `/presents/create`

##### Parameters

| Name            | Type | Description                           | Sample Data       |
| --------------- | ---- | ------------------------------------- | ----------------- |
| title | text | title of the present | free ticket to japan|
| presents_image | file | image of the present | |
| type | text | | bronze |

### Present Requests

##### get all present requests

GET `/present-requests/`

##### Response

```json
{
    "data": [
        {
            "present_request_id": "6",
            "user_id": "4",
            "fname": "jhondee",
            "lname": "diaz",
            "email": "fjanib@myoptimind.com",
            "mobile_no": "213131",
            "status": "pending",
            "created_at": "2018-09-24 13:42:20",
            "updated_at": "0000-00-00 00:00:00"
        },
        {
            "present_request_id": "7",
            "user_id": "4",
            "fname": "jhondee",
            "lname": "diaz",
            "email": "fjanib@myoptimind.com",
            "mobile_no": "213131",
            "status": "pending",
            "created_at": "2018-09-24 15:22:48",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

##### create present request

POST `/present-requests/apply`

##### Parameters

| Name            | Type | Description                           | Sample Data       |
| --------------- | ---- | ------------------------------------- | ----------------- |
| user_id | int | User id of the requester | |
|fname| text | First name | Brendan |
| lname | text | Last name | Coco |
| email | text | Email address | brendancoco@gmail.com |
| mobile_no | text | Mobile number | 092132312 |
| presents[] | int | Present Ids selected by the user | |

##### Response
```json
201 Created
{
    "data": {
        "fname": "jhondee",
        "lname": "diaz",
        "email": "fjanib@myoptimind.com",
        "mobile_no": "213131",
        "presents": [
            "1",
            "2"
        ],
        "user_id": "4"
    },
    "status": 201,
    "message": "Request Successfully sent!"
}
```

---

##### Accept or decline a request

POST `/present-request/{present_request_id}`

##### Parameters
| Name            | Type | Description                           | Sample Data       |
| --------------- | ---- | ------------------------------------- | ----------------- |
|action| text | action to be performed to the request | 'accept','decline' |


##### Response

```json
200 OK
{
    "status": 200,
    "data": {
        "present_request_id": "6",
        "user_id": "4",
        "fname": "jhondee",
        "lname": "diaz",
        "email": "fjanib@myoptimind.com",
        "mobile_no": "213131",
        "status": "pending",
        "created_at": "2018-09-24 13:42:20",
        "updated_at": "0000-00-00 00:00:00"
    },
    "message": "Request successfully accepted!"
}
```

---

#### Update Settings
POST `/settings/`

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| user_id    | int | -                                                   | 1                     |
| push_notification    | tinyint | -                                                   | 1                     |
| email_notification    | tinyint | -                                                   | 1                     |

##### Response
```json
{
    "data": [
        {
            "settings_id": "1",
            "user_id": "1",
            "push_notification": "1",
            "email_notification": "1",
            "created_at": "2019-01-09 10:25:04",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Settings saved",
    "status": 200
}
```

---

##### Get Settings
GET `/settings/user/{user_id}/`

##### Response
```json
200 Ok
{
    "data": [
        {
            "settings_id": "2",
            "user_id": "2",
            "push_notification": "1",
            "email_notification": "1",
            "created_at": "2019-01-09 10:25:30",
            "updated_at": "0000-00-00 00:00:00"
        }
    ],
    "message": "Found all data",
    "status": 200
}
```

---

#### Company Info

##### Privacy Policy
GET `/company-info/privacy-policy/`

##### Response
```json
200 Ok
{
    "data": {
        "content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    "message": "Successfully found data",
    "status": "200"
}
```

---

##### Terms and Conditions
GET `/company-info/terms-conditions/`

##### Response
```json
200 Ok
{
    "data": {
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sit amet consectetur adipiscing elit. Cursus turpis massa tincidunt dui. Id interdum velit laoreet id donec. Eleifend mi in nulla posuere. Euismod elementum nisi quis eleifend quam. Non enim praesent elementum facilisis leo vel fringilla. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Quam viverra orci sagittis eu. Convallis aenean et tortor at risus viverra. Fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate sapien nec. Elementum curabitur vitae nunc sed velit dignissim sodales ut eu. Phasellus faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis. In hendrerit gravida rutrum quisque non tellus orci. Morbi tristique senectus et netus et malesuada. Commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit.\r\n\r\nAenean et tortor at risus viverra adipiscing at in. Facilisis sed odio morbi quis. Ut tristique et egestas quis. Interdum varius sit amet mattis vulputate enim nulla. Rhoncus est pellentesque elit ullamcorper dignissim cras tincidunt lobortis. Facilisi etiam dignissim diam quis enim lobortis. Netus et malesuada fames ac turpis. Dictum fusce ut placerat orci nulla pellentesque dignissim enim. Ut ornare lectus sit amet est placerat. In hac habitasse platea dictumst quisque sagittis purus sit. Nulla posuere sollicitudin aliquam ultrices sagittis. Fermentum et sollicitudin ac orci phasellus. Duis ultricies lacus sed turpis. Fames ac turpis egestas integer. Commodo odio aenean sed adipiscing diam donec adipiscing tristique risus. Leo vel orci porta non pulvinar. Augue interdum velit euismod in pellentesque massa."
    },
    "message": "Successfully found data",
    "status": "200"
}
```

---

##### About Us
GET `/company-info/about-us/`

##### Response
```json
200 Ok
{
    "data": {
        "company_logo": "http://localhost/coloryourlife/frontend/img/cyl_logo.png",
        "about_the_company": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
        "ceo_message": "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?",
        "address": ""
    },
    "message": "Successfully found data",
    "status": "200"
}
```

---

##### Contact Us
GET `/company-info/contact-us/`

##### Response
```json
200 Ok
{
    "data": {
        "mobile": {
            "label": "2938761",
            "link": "9834757"
        },
        "email": {
            "label": "info@coloryourlifeaapp.com",
            "link": "info@coloryourlifeaapp.com"
        },
        "website": {
            "label": "coloryourlifeapp.com",
            "link": "http://coloryourlifeapp.com"
        },
        "facebook": {
            "label": "/COLOR-YOUR-LIFE",
            "link": "/COLOR-YOUR-LIFE-245571476014593/"
        },
        "twitter": {
            "label": "@COLORS_MEMBER",
            "link": "@COLORS_MEMBER"
        },
        "google_plus": {
            "label": "",
            "link": ""
        },
        "instagram": {
            "label": "@wemakeyourlifemorecolorful",
            "link": "@wemakeyourlifemorecolorful"
        },
        "youtube": {
            "label": "/coloryourlife",
            "link": "https://www.youtube.com/channel/UC63PN3Qp32LwH9551oPgWeA"
        },
        "youku": {
            "label": "youku.com/coloryourlife",
            "link": "http://v.youku.com/v_show/id_XMzg3MjU1NjgxMg==.html?spm=a2h0j.11185381.listitem_page1.5!2~A"
        }
    },
    "message": "Successfully found data",
    "status": "200"
}
```

---

#### Country Search
GET `/location/countries/:search_string`

##### Response
```json
{
    "data": [
        {
            "id": "176",
            "country_code": "PH",
            "country": "Philippines",
            "type": "CO"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

#### City Search
GET `/location/cities/:country_code/:search_string

##### Response
```json
{
    "data": [
        {
            "id": "78406",
            "iso": "PH-53-173778",
            "city": "Marikina",
            "type": "CI"
        }
    ],
    "message": "Successfully found all data",
    "status": 200
}
```

---

#### Add ons payment
POST `/payment/:type/`  
`type: ads/videos/webinars/group_messages`  

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| user_id    | int | -                                                   | 1                     |
| quantity    | int | -                                                   | 1                     |

##### Response
```json
200 Ok
{
    "data": {
        "url": "http://coloryourlife.betaprojex.com/app/payment/add-ons/ads/72",
        "ios_url": "http://coloryourlife.betaprojex.com/app/payment/add-ons/ads/72/ios",
        "android_url": "http://coloryourlife.betaprojex.com/app/payment/add-ons/ads/72/android"
    },
    "message": "Ready for payment",
    "status": 200
}
```

---

#### Check Paynamics Payment
POST `/payment/check/`  

##### Parameters
| Name        | Type   | Description                                         | Sample Data                                   |
| ----------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| user_id    | int | -                                                   | 1                     |


##### Response (Membership Plan)
```json
{
    "data": {
        "paynamic_request_id": "6",
        "user_id": "4",
        "membership_plan_id": "4",
        "payment_logs_id": "0",
        "requestid": "NWNlZmEzMGZhODFkNQ==",
        "responseid": "MjY0MDQ5NDMwODc5MjQ0OTMzNjE=",
        "status": "paid",
        "created_at": "2019-05-30 04:32:17",
        "updated_at": "2019-05-30 04:42:46",
        "membership_applied": "no"
    },
    "message": "Membership plan added",
    "status": 201
}
```

##### Response (Add-ons)
```json
{
    "data": {
        "paynamic_request_id": "7",
        "user_id": "4",
        "membership_plan_id": "0",
        "payment_logs_id": "113",
        "requestid": "NWNlZmE1MGYwNWQwMA==",
        "responseid": "NDU1NzU5MTIxNjA0MTE2OTU0ODI=",
        "status": "paid",
        "created_at": "2019-05-30 04:40:47",
        "updated_at": "2019-05-30 04:42:42",
        "membership_applied": "yes"
    },
    "message": "Add-ons added",
    "status": 201
}
```

---

#### Check DOKU Payment
GET `/payment/check-doku-payment/:sessionid`  
Get sessionid from [Select Membership Plan](#markdown-header-select-membership-plan)

##### Response (Success)
```json
{
    "data": {
        "payment_logs_id": "382",
        "user_id": "4",
        "type": "",
        "quantity": "0",
        "user_membership_plan_id": "0",
        "status": "applied",
        "membership_id": "4",
        "sessionid": "yg20Y19HhDuMzxQPDsUV",
        "payload": "serialized json",
        "created_at": "2019-06-11 00:36:59",
        "updated_at": "2019-06-11 00:38:12"
    },
    "message": "Payment successful.",
    "status": 200
}
```

##### Response (Success - Add-ons)
```json
{
    "data": {
        "payment_logs_id": "384",
        "user_id": "4",
        "type": "ads",
        "quantity": "1",
        "user_membership_plan_id": "174",
        "status": "applied",
        "membership_id": "0",
        "sessionid": "DT24L3Dm0aR6t1gPCAd1",
        "payload": "serialized json",
        "created_at": "2019-06-11 01:15:48",
        "updated_at": "2019-06-11 01:17:10"
    },
    "message": "Payment successful.",
    "status": 200
}
```

##### Response (Fail)
```json
{
    "data": [],
    "message": "No payments made.",
    "status": 404
}
```

---

#### DOKU Payment Process

+ Get sessionid in response of [Select Membership Plan](#markdown-header-select-membership-plan)
```json
{
    "data": {
        "url": "http://coloryourlife.betaprojex.com/app/membership/73",
        "ios_url": "http://coloryourlife.betaprojex.com/app/membership/73/ios",
        "android_url": "http://coloryourlife.betaprojex.com/app/membership/73/android",
        "membership_name": "Economy",
        "plan_rate": "10.00",
        "sessionid": "yg20Y19HhDuMzxQPDsUV"
    },
    "message": "Membership verified",
    "status": 200
}
```
+ Pay using DOKU
+ Success url of DOKU: `/app/membership/success_doku/`
+ Call [Check DOKU Payment](#markdown-header-check-doku-payment) API
##### Success Payment Response
```json
{
    "data": {
        "payment_logs_id": "382",
        "user_id": "4",
        "type": "",
        "quantity": "0",
        "user_membership_plan_id": "0",
        "status": "applied",
        "membership_id": "4",
        "sessionid": "yg20Y19HhDuMzxQPDsUV",
        "payload": "serialized json",
        "created_at": "2019-06-11 00:36:59",
        "updated_at": "2019-06-11 00:38:12"
    },
    "message": "Payment successful.",
    "status": 200
}
```
##### Fail Payment Response
```json
{
    "data": [],
    "message": "No payments made.",
    "status": 404
}
```

+ If Payment is success and is not add-ons payment, call [Apply Plan Now](#markdown-header-apply-plan-now) API

---

#### Add Membership on success payment
POST `payment/add/membership-plan`  

##### Parameters
| Name            | Type   | Description                                         | Sample Data                                   |
| --------------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| payment_logs_id | int    | from select membership plan api                     | 1                                             |

##### Response
```json
{
    "data": {
        "user_id": "4",
        "username": "leon",
        "email": "leon@mail.com",
        "fname": "Leon",
        "lname": "Unknown",
        "fullname": "Leon Unknown",
        "membership_plan_id": "2",
        "created_at": "2019-08-04 19:18:59",
        "updated_at": "2019-08-04 20:33:01",
        "first_log": "no",
        "profile_pic": "https://coloryourlifeapp.com/app/uploads/user_profile_pic/1564971538_cropped7521808440619141708.jpg",
        "referral_code": "USEXYUTN1",
        "rank_id": "1",
        "rank_type": "White Member",
        "membership_plan_name": "Challenger",
        "mp_access_limit": "1",
        "membership_expiry": "2020-09-05 11:33:06",
        "privileges": {
            "market_place_access": {},
            "chat_function": true,
            "color_bonus_access": false,
            "color_presents_access": false,
            "remaining_advertisement": "0",
            "remaining_video": "0",
            "remaining_group_message": "0",
            "remaining_live_webinar": "0"
        },
        "with_pending_payment": "No"
    },
    "message": "Membership successfully applied",
    "status": 200
}
```

+ If Membership is successfully applied, call [Apply Plan Now](#markdown-header-apply-plan-now) API

---

#### Add Add-ons on success payment
POST `payment/add/add-ons`  

##### Parameters
| Name            | Type   | Description                                         | Sample Data                                   |
| --------------- | ------ | --------------------------------------------------- | --------------------------------------------- |
| payment_logs_id | int    | from select membership plan api                     | 1                                             |

##### Response
```json
{
    "data": {
        "user_id": "6",
        "username": "john",
        "email": "john@mail.com",
        "fname": "John",
        "lname": "Watson",
        "fullname": "John Watson",
        "membership_plan_id": "4",
        "created_at": "2019-08-04 19:42:32",
        "updated_at": "2019-08-04 20:35:02",
        "first_log": "no",
        "profile_pic": "https://coloryourlifeapp.com/app/uploads/user_profile_pic/1564972952_cropped3554179227136076162.jpg",
        "referral_code": "USPK4VKTH",
        "rank_id": "1",
        "rank_type": "White Member",
        "membership_plan_name": "Business",
        "mp_access_limit": "-1",
        "membership_expiry": "2019-09-05 11:35:08",
        "privileges": {
            "market_place_access": {
                "Products": true,
                "Services": true
            },
            "chat_function": true,
            "color_bonus_access": true,
            "color_presents_access": true,
            "remaining_advertisement": "5",
            "remaining_video": "3",
            "remaining_group_message": "3",
            "remaining_live_webinar": "3"
        },
        "with_pending_payment": "No"
    },
    "message": "Add-ons successfully applied",
    "status": 200
}
```

---