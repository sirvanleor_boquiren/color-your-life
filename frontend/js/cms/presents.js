$('#editPresent').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var payload = button.data('payload');
    console.log(payload);
    var modal = $(this);
    modal.find('#imageHolder').html('<img width="250" src="'+payload.image_name+'" />');
    modal.find('input[name="title"]').val(payload.title);
    modal.find('input[name="no_of_members"]').val(payload.no_of_members);
    modal.find('select[name="type"]').val(payload.type);
    if(payload.is_featured == 1){
        modal.find('input[name="is_featured"]').prop('checked',true);
    }
    modal.find('#editForm').attr('action',payload.base_url + 'cms/Presents/edit/' + payload.id)

})

$('#deletePresent').on('show.bs.modal', function (event) {    
    var button = $(event.relatedTarget);
    var payload = button.data('payload');
    console.log(payload);
    var modal = $(this);
    
    $(modal.find('#deleteButton')).on('click',function(){
        var deleteString = modal.find('#deleteInput').val();
        if(deleteString.toLowerCase() === 'delete'){
            modal.find('#deleteForm').attr('action',payload.base_url + 'cms/Presents/delete/' + payload.id)
            modal.find('#deleteForm').submit();
        }
    })
})

$('#selectType').on('change',function(e){
    var base_url = $(this).data('baseurl');
    var val = $(this).val();
    if(val != '')
    {
        $.ajax({
            method: "POST",
            url: base_url + 'cms/Presents/checkIfCanBeFeatured',
            data: { type: val }
          })
        .done(function( msg ) {
            var data = JSON.parse(msg);
            if(data.result == true){
                $('#featurable').html(`
                <div class="form-group">
			        			<label>Include in Featured<span class="required"></span></label>
			        			<div class="checkbox">
                                              <label>
                                                  <input type="checkbox" name="is_featured" value="1">
                                              </label>
                                </div>
                            </div> 
                `)
            }else{
                $('#featurable').empty();
            }
        });
    }else{
        $('#featurable').empty();
    }
})

