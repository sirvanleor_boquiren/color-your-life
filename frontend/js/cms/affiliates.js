$('#editAffiliate').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var modal = $(this);
    modal.find('#imageHolder').html('<img height="200" width="200" src="'+button.data('image')+'" />');
    modal.find('input[name="title"]').val(button.data('title'));
    modal.find('#editForm').attr('action',button.data('url') + 'cms/affiliates/edit/' + button.data('id'));

});

$('#deleteAffiliate').on('show.bs.modal', function (event) {    
    var button = $(event.relatedTarget);
    var payload = button.data('payload');
    console.log(payload);
    var modal = $(this);
    
    $(modal.find('#deleteButton')).on('click',function(){
        var deleteString = modal.find('#deleteInput').val();
        if(deleteString.toLowerCase() === 'delete'){
            modal.find('#deleteForm').attr('action',payload.base_url + 'cms/affiliates/delete/' + payload.id)
            modal.find('#deleteForm').submit();
        }
    })
})