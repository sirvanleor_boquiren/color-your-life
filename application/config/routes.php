<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['migrate/(:any)'] = 'migrate/index/$1';
// $route['membership/(:any)/(:any)'] = 'membership/index/$1/$2';
$route['membership/(:num)'] = 'membership/index/$1';
$route['membership/(:num)/(:any)'] = 'membership/index/$1/$2';

# Users
$route['api/users/(:num)']				= "api/users/single/$1";
$route['api/user/(:num)']				= "api/users/single/$1";
$route['api/user/(:num)/update']		= "api/users/update/$1";
$route['api/user/(:num)/update/password']		= "api/users/update_password/$1";
$route['api/user/(:num)/bonus']			= "api/users/bonus/$1";
$route['api/user/(:num)/presents/referral']			= "api/users/presents_referral/$1";
$route['api/user/(:num)/marketplace']			= "api/users/marketplace/$1";
$route['api/user/(:num)/viewer/(:num)/marketplace']			= "api/users/marketplace/$1/$2";
$route['api/user/(:num)/referrals']		= "api/users/referrals/$1";
$route['api/user']						= "api/users"; # POST
$route['api/users/login/sns']						= "api/users/login_sns"; # POST
$route['api/user/sns']						= "api/users/sns"; # POST
$route['api/users/send-referral-code']	= "api/users/send_referral_code"; # POST
$route['api/user/forgot-password']	= "api/users/forgot_password"; # POST
$route['user/reset/(:any)']	= "users/reset/$1"; # GET
$route['api/user/forgot-username']	= "api/users/forgot_username"; # POST
$route['api/user/(:num)/following'] = "api/users/following/$1"; # GET
$route['api/user/(:num)/followers'] = "api/users/followers/$1"; # GET
$route['api/users/plan-info/(:num)'] = "api/users/plan_info/$1";

# Post reviews
$route['api/reviews/post']						= "api/post_reviews";
$route['api/reviews/post/(:num)']			= "api/post_reviews/post/$1";

# User reviews
$route['api/reviews/user']						= "api/user_reviews";
$route['api/reviews/user/(:num)']			= "api/user_reviews/user/$1";
$route['api/user/(:num)/points'] = "api/users/points/$1";

$route['api/ads/(:num)']				= "api/ads/single/$1";
// $route['api/ads/featured/products']		= "api/ads/featured/1";
// $route['api/ads/featured/services']		= "api/ads/featured/1";

# Posts
$route['api/posts']						= "api/posts/all";
$route['api/posts/(:num)/(:num)']		= "api/posts/single_post/$1/$2";
$route['api/posts/like/user/(:num)/category_id/(:num)']	= "api/posts/like/$1/$2";
$route['api/posts/like/user/(:num)']	= "api/posts/like/$1";
$route['api/posts/(:num)/update'] = "api/posts/update/$1";
$route['api/posts/delete-file'] = "api/posts/delete_file";

# MEssages
$route['api/messages/user/(:num)/conversation/(:num)']						= "api/messages/conversation/$1/$2";
$route['api/messages/user/(:num)/followers']                    	= "api/messages/followers/$1"; # message previews
$route['api/messages/user/(:num)/previews']					              = "api/messages/message_previews/$1"; # message previews
$route['api/messages']					                                 	= "api/messages/index";
$route['api/messages/posts']					                            = "api/messages/product_message/";

# Color Points
$route['api/color-points/redeem'] = "api/color_points/redeem";

# Redeem requests

$route['api/redeem-requests'] = 'api/redeem_requests';
$route['api/redeem-request/(:any)'] = 'api/redeem_requests/redeem_request/$1';

# Present requests

$route['api/present-requests'] = 'api/present_requests';
$route['api/present-requests/apply'] = 'api/present_requests/apply';
$route['api/present-request/(:any)'] = 'api/present_requests/present_request/$1';

# Presents

$route['api/presents/request/(:any)'] = 'api/presents/get_by_present_request/$1';


# Company Info
$route['api/company-info/privacy-policy'] = 'api/company_info/privacy_policy';
$route['api/company-info/terms-conditions'] = 'api/company_info/terms_conditions';
$route['api/company-info/about-us'] = 'api/company_info/about_us';
$route['api/company-info/contact-us'] = 'api/company_info/contact_us';

# Payment
$route['api/payment/add/membership-plan'] = 'api/payment/add_membership_plan';
$route['api/payment/add/add-ons'] = 'api/payment/add_add_ons';

$route['api/payment/check'] = 'api/payment/check';
$route['api/payment/(:any)'] = 'api/payment/add_ons/$1';
$route['api/payment/check-pending-payment/(:num)'] = 'api/payment/check_pending_payment/$1';
$route['api/payment/check-doku-payment/(:any)'] = 'api/payment/check_doku_payment/$1';

$route['payment/add-ons/(:any)/(:num)'] = 'payment/add_ons/$1/$2';
$route['payment/add-ons/(:any)/(:num)/(:any)'] = 'payment/add_ons/$1/$2/$3';

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/

// $route['api']
$route['api/example/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
