<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_user_device_ids_table extends CI_Migration {
  public function create_user_device_ids()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`udi_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'Fk app users'
      ),
      'device_id' => array(
        'type' => 'TEXT',
        'comment' => 'Device id'
      ),
      'firebase_id' => array(
        'type' => 'TEXT',
        'comment' => 'Firebase id that should be used in fcm'
      ),
      'is_active' => array(
        'type' => 'TINYINT',
        'default' => 0,
        'comment' => 'Flagged to 1 if we gonna send notif'
      ),
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if($this->dbforge->create_table('user_device_ids'))
    {
      $table = "user_device_ids";

      $data = array(
        'user_id' => '2',
        'device_id' => '695EA49B-960B-402A-AC94-10E5D66BE42B',
        'firebase_id' => 'e6q84WBWmzo:APA91bHZz9LoUuNrJVjdp-12-wnelba70Q5ol8Z-kEo6bqshIkeWNmVCmqzOfVneyX6vxmSBas-Fd5jyKuUXqhK9s3kJcD1m3ZWrZ5D77pVH9zKHD4-T-s7G_ui1KH_9LzFeyMekhIi5VQ4b2FlIM0xmZ8vKPXwDZw',
        'is_active' => 0,
      );
      $this->db->insert($table, $data);

      $data = array(
        'user_id' => '2',
        'device_id' => '27100ECF-C0A0-4355-A7C3-11E959C6F481',
        'firebase_id' => 'cJ2JiJ4MnvQ:APA91bGXFBKa8itklnFR6d0CnUtXp4gBwqK_Nl5IwpVuspHZBMsmBqgREoehOodUpbJxI7PqDQld3Un6s1VD3aBp24KUj6VpQQ71HomON544_z_zKYMqI4ekLPPVGMEDcdc5L7PrUUhrS405f0OTfT7sCGiljtOcrQ',
        'is_active' => 1,
      );
      $this->db->insert($table, $data);

      $data = array(
        'user_id' => '1',
        'device_id' => '27100ECF-C0A0-4355-A7C3-11E959C6F481',
        'firebase_id' => 'cJ2JiJ4MnvQ:APA91bGXFBKa8itklnFR6d0CnUtXp4gBwqK_Nl5IwpVuspHZBMsmBqgREoehOodUpbJxI7PqDQld3Un6s1VD3aBp24KUj6VpQQ71HomON544_z_zKYMqI4ekLPPVGMEDcdc5L7PrUUhrS405f0OTfT7sCGiljtOcrQ',
        'is_active' => 1,
      );
      $this->db->insert($table, $data);

      $data = array(
        'user_id' => '3',
        'device_id' => '27100ECF-C0A0-4355-A7C3-11E959C6F481',
        'firebase_id' => 'cJ2JiJ4MnvQ:APA91bGXFBKa8itklnFR6d0CnUtXp4gBwqK_Nl5IwpVuspHZBMsmBqgREoehOodUpbJxI7PqDQld3Un6s1VD3aBp24KUj6VpQQ71HomON544_z_zKYMqI4ekLPPVGMEDcdc5L7PrUUhrS405f0OTfT7sCGiljtOcrQ',
        'is_active' => 1,
      );
      $this->db->insert($table, $data);

    }
  }
  public function up()
  {
    $this->create_user_device_ids();
  }
  public function down()
  {
    $this->dbforge->drop_table('user_device_ids', true);
  }
}
