<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_payment_logs_table extends CI_Migration {
  public function create_payment_logs()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`payment_logs_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => 200
      ),
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => 100,
        'comment' => 'ads/videos/webinars/group_messages'
      ),
      'quantity' => array(
        'type' => 'INT',
        'constraint' => 100
      ),
      'user_membership_plan_id' => array(
        'type' => 'INT',
        'constraint' => 100
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => 100,
        'comment' => 'pending/paid/applied'
      ),
      'membership_id' => array(
        'type' => 'INT',
        'constraint' => 100,
        'default' => 0,
        'comment' => 'for doku in membership plan payment'
      ),
      'sessionid' => array(
        'type' => 'VARCHAR',
        'constraint' => 100,
        'comment' => 'for doku only'
      ),
      'payload' => array(
        'type' => 'TEXT',
        'comment' => 'PHP serialized string of the payload array'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('payment_logs');

  }
  public function up()
  {
    $this->create_payment_logs();
  }
  public function down()
  {
    $this->dbforge->drop_table('payment_logs', true);
  }
}
