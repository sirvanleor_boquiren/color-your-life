<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_notifications_table extends CI_Migration {
  public function create_notifications()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`notification_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'Fk app users. The one who received the notif.'
      ),
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => 200,
        'comment' => 'This is the notification type. (reply, like, follow, webinar, post_review, user_review, etc.)'
      ),
      'payload' => array(
        'type' => 'TEXT',
        'comment' => 'PHP serialized string of the payload array'
      ),
      'is_read' => array(
        'type' => 'TINYINT',
        'default' => 0,
        'comment' => 'If the notification has been read'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('notifications');

  }
  public function up()
  {
    $this->create_notifications();
  }
  public function down()
  {
    $this->dbforge->drop_table('notifications', true);
  }
}
