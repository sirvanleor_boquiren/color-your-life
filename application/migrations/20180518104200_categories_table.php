<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_categories_table extends CI_Migration {
  public function create_categories()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`category_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'market_place_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from market_place table'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    
    if($this->dbforge->create_table('categories'))
    {

      $table = "categories";

      $this->db->insert($table, ['name' => 'All Departments', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Arts & Crafts', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Automotive', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Baby', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Beauty & Personal Care', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Books', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Computers', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Digital Music', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Electronics', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Kindle Store', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Prime Video', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Women\'s Fashion', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Men\'s Fashion', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Girls\' Fashion', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Boys\' Fashion', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Deals', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Health & Household', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Home & Kitchen', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Industrial & Scientific', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Luggage', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Movies & TV', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Music, CDs & Vinyl', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Pet Supplies', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Software', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Sports & Outdoors', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Tools & Home Improvement', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Toys & Games', 'market_place_id' => '1']);
      $this->db->insert($table, ['name' => 'Video Games', 'market_place_id' => '1']);


      $this->db->insert($table, ['name' => 'Web, Mobile & Software Dev', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'IT & Networking', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Data Science & Analytics', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Engineering & Architecture', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Design & Creative', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Writing & Entry', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Translation', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Admin Support', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Accounting & Consulting', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Sales & Marketing', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Customer Service', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Legal', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Teaching & Education', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Delivery', 'market_place_id' => '2']);
      $this->db->insert($table, ['name' => 'Special work', 'market_place_id' => '2']);
    }
  }
  public function up()
  {
    $this->create_categories();
  }
  public function down()
  {
    $this->dbforge->drop_table('categories', true);
  }
}