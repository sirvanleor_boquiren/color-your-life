<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_app_users_table extends CI_Migration {
  public function create_app_users()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`user_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'username' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'email' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'fname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'lname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'mname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200',
        'null' => TRUE
      ),
      'password' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'profile_pic' => array(
        'type' => 'VARCHAR',
        'constraint' => '400',
        'null' => TRUE
      ),
      'membership_plan_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from membership_plans'
      ),
      'market_place_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from market_place; 0 = none, -1 = all'
      ),
      'market_place_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '250',
      ),
      'rank' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'rank of user from color_rank table',
        'default' => '1'
      ),
      'status' => array(
        'type' => 'TINYINT',
        'constraint' => '5',
        'default' => '1',
        'comment' => '0 - inactive, 1 - active'
      ),
      'first_log' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'default' => 'yes',
        'comment' => 'yes / no'
      ),
      'country' => array(
        'type' => 'VARCHAR',
        'constraint' => '100'
      ),
      'referral_code' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'user\'s own referral code'
      ),
      'referred_by' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'user_id'
      ),
      'referrer_rank' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'rank of the referrer from color_rank table'
      ),
      'points' => array(
        'type' => 'FLOAT',
        'comment' => 'points from referral'
      ),
      'bonus' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'bonus points'
      ),
      'referrals' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'total number of referrals'
      ),
      'rank_up_referrals' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'total number of referrals for ranking up'
      ),
      'forgot_token' => array(
        'type' => 'varchar',
        'constraint' => '100',
        'comment' => 'token sent in user\'s email'
      ),
      'login_type' => array(
        'type' => 'ENUM("form","facebook", "google")',
        'default' => 'form',
        'comment' => 'Login type in social media'
      ),
      'sns_token' => array(
        'type' => 'varchar',
        'constraint' => '300',
        'default' => null,
        'null' => true,
        'comment' => 'Social networking website tokens'
      ),
      'stripe_customer_id' => array(
        'type' => 'varchar',
        'constraint' => '300',
        'default' => null,
        'null' => true,
        'comment' => 'For auto debit'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if($this->dbforge->create_table('app_users')){
      $table = "app_users";

      $data = array(
        'username' => 'jc',
        'email' => 'jcandres@myoptimind.com',
        'fname' => 'Jc',
        'lname' => 'Andres',
        'mname' => '',
        'password' => '$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6', # Password123!
        'profile_pic' => null,
        'membership_plan_id' => 4,
        'market_place_id' => 0,
        'status' => 1,
        'first_log' => 'no',
        'country' => 'PH',
        'referral_code' => '7777777',
        'referred_by' => 0,
        'points' => 40,
        'bonus' => 0,
        'referrals' => 250,
        'rank_up_referrals' => 198,
        'rank' => 4
      );
      $this->db->insert($table, $data);

      $data = array(
        'username' => 'jp_ang',
        'email' => 'jpang@myoptimind.com',
        'fname' => 'Patrick',
        'lname' => 'Ang',
        'mname' => '',
        'password' => '$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6', # Password123!
        'profile_pic' => null,
        'membership_plan_id' => 1,
        'market_place_id' => 1,
        'status' => 1,
        'first_log' => 'no',
        'country' => 'PH',
        'referral_code' => '1234567',
        'referred_by' => 0,
        'points' => 0,
        'bonus' => 0,
        'referrals' => 0,
        'rank_up_referrals' => 0
      );
      $this->db->insert($table, $data);

      $data = array(
        'username' => 'edocampo',
        'email' => 'edocampo@myoptimind.com',
        'fname' => 'Elline',
        'lname' => 'Ocampo',
        'mname' => '',
        'password' => '$2y$10$Xx9EisUWWqGrOU0o8nSAUOxWDEwZSypAr883hRygRpIU5NIdIC1f6', # Password123!
        'profile_pic' => null,
        'membership_plan_id' => 1,
        'market_place_id' => 1,
        'status' => 1,
        'first_log' => 'no',
        'country' => 'PH',
        'referral_code' => 'Boxszsd',
        'referred_by' => 0,
        'points' => 0,
        'bonus' => 0,
        'referrals' => 0,
        'rank_up_referrals' => 0
      );
      $this->db->insert($table, $data);

    }
  }
  public function up()
  {
    $this->create_app_users();
  }
  public function down()
  {
    $this->dbforge->drop_table('app_users', true);
  }
}
