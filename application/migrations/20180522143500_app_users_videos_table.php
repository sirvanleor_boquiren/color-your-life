<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_app_users_videos_table extends CI_Migration {
  public function create_app_users_videos()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`video_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id in app_users'
      ),
      'title' => array(
        'type' => 'VARCHAR',
        'constraint' => '300',
      ),
      'video' => array(
        'type' => 'VARCHAR',
        'constraint' => '300',
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('app_users_videos');
  }
  public function up()
  {
    $this->create_app_users_videos();
  }
  public function down()
  {
    $this->dbforge->drop_table('app_users_videos', true);
  }
}