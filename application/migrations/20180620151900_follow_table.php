<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_follow_table extends CI_Migration {
  public function create_follow()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`follow_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'follower_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from app_users table'
      ),
      'following_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from app_users table'
      )
      
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('follow');
  }
  public function up()
  {
    $this->create_follow();
  }
  public function down()
  {
    $this->dbforge->drop_table('follow', true);
  }
}