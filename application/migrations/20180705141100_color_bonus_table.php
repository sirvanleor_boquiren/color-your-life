<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_color_bonus_table extends CI_Migration {
  public function create_color_bonus()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`color_bonus_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
      ),
      'free' => array(
        'type' => 'INT',
        'constraint' => '100',
      ),
      'challenger' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'economy' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'business' => array(
        'type' => 'INT',
        'constraint' => '100'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if ( $this->dbforge->create_table('color_bonus') ) {
      $table = "color_bonus";
      $data[0] = array(
        'user_id' => 1,
        'free' => 0,
        'challenger' => 0,
        'economy' => 0,
        'business' => 0
      );
      $data[1] = array(
        'user_id' => 2,
        'free' => 0,
        'challenger' => 0,
        'economy' => 0,
        'business' => 0
      );
      $data[2] = array(
        'user_id' => 3,
        'free' => 0,
        'challenger' => 0,
        'economy' => 0,
        'business' => 0
      );
      $this->db->insert($table, $data[0]);
      $this->db->insert($table, $data[1]);
      $this->db->insert($table, $data[2]);
    }
  }
  public function up()
  {
    $this->create_color_bonus();
  }
  public function down()
  {
    $this->dbforge->drop_table('color_bonus', true);
  }
}