<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_posts_files_table extends CI_Migration {
  public function create_posts_files()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`posts_file_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'post_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from posts table'
      ),
      'filename' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'status' => array(
        'type' => 'TINYINT',
        'constrait' => '5',
        'default' => '1',
        'comment' => '0 - inactive, 1 - active'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('posts_files');
  }
  public function up()
  {
    $this->create_posts_files();
  }
  public function down()
  {
    $this->dbforge->drop_table('posts_files', true);
  }
}