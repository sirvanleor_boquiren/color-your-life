<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_market_place_table extends CI_Migration {
  public function create_market_place()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`market_place_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");

    if($this->dbforge->create_table('market_place'))
    {
      
      $table = "market_place";
      $data = array(
        "name" => "Products"
      );
      $this->db->insert($table, $data);

      $data = array(
        "name" => "Services"
      );
      $this->db->insert($table, $data);

    }
  }
  public function up()
  {
    $this->create_market_place();
  }
  public function down()
  {
    $this->dbforge->drop_table('market_place', true);
  }
}