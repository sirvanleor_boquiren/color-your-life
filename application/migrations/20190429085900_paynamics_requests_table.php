<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_paynamics_requests_table extends CI_Migration {
  public function create_paynamics_requests()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`paynamic_request_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'membership_plan_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'default' => 0
      ),
      'payment_logs_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'default' => 0
      ),
      'requestid' => array(
        'type' => 'VARCHAR',
        'constraint' => 200
      ),
      'responseid' => array(
        'type' => 'VARCHAR',
        'constraint' => 200
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => 200,
        'default' => 'pending',
        'comment' => 'pending/paid/applied'
      ),
      'apply_now' => array(
        'type' => 'TINYINT',
        'default' => 0,
        'comment' => 'if plan should be applied immediately, for first time membership'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('paynamics_requests');

  }
  public function up()
  {
    $this->create_paynamics_requests();
  }
  public function down()
  {
    $this->dbforge->drop_table('paynamics_requests', true);
  }
}
