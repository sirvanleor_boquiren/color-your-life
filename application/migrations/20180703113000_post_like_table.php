<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_post_like_table extends CI_Migration {
  public function create_post_like()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`post_like_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'FK, liker ID'
      ),
      'post_id' => array(
        'type' => 'INT',
        'constraint' => '100',
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('post_like');
  }
  public function up()
  {
    $this->create_post_like();
  }
  public function down()
  {
    $this->dbforge->drop_table('post_like', true);
  }
}
