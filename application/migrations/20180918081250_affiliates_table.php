<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_affiliates_table extends CI_Migration {
  public function create_affiliates()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`affiliate_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'image_path' => array(
          'type' => 'VARCHAR',
          'constraint' => 500
      ),
      'image_name' => array(
        'type' => 'VARCHAR',
        'constraint' => 500
      ),
      'title' => array(
          'type' => 'VARCHAR',
          'constraint' => 200
      ),
      'site_link' => array(
        'type' => 'VARCHAR',
        'constraint' => 500
      ),
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('affiliates');

  }
  public function up()
  {
    $this->create_affiliates();
  }
  public function down()
  {
    $this->dbforge->drop_table('affiliates', true);
  }
}
