<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_color_rank_table extends CI_Migration {
  public function create_color_rank()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`color_rank_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'rank_type' => array(
        'type' => 'VARCHAR',
        'constraint' => '100'
      ),
      'min_qualification' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'max_qualification' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'rate' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'rank_photo' => array(
        'type' => 'VARCHAR',
        'constraint' => '500'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if ( $this->dbforge->create_table('color_rank') ) {
      $table = "color_rank";
      $data[0] = array(
        'rank_type' => 'White Member',
        'min_qualification' => 1,
        'max_qualification' => 49,
        'rate' => 20
      );
      $data[1] = array(
        'rank_type' => 'Bronze Member',
        'min_qualification' => 50,
        'max_qualification' => 99,
        'rate' => 25
      );
      $data[2] = array(
        'rank_type' => 'Silver Member',
        'min_qualification' => 100,
        'max_qualification' => 149,
        'rate' => 30
      );
      $data[3] = array(
        'rank_type' => 'Gold Member',
        'min_qualification' => 150,
        'max_qualification' => 199,
        'rate' => 35
      );
      $data[4] = array(
        'rank_type' => 'Platinum Member',
        'min_qualification' => 200,
        'max_qualification' => 9999,
        'rate' => 40
      );
      $this->db->insert($table, $data[0]);
      $this->db->insert($table, $data[1]);
      $this->db->insert($table, $data[2]);
      $this->db->insert($table, $data[3]);
      $this->db->insert($table, $data[4]);
    }

  }
  public function up()
  {
    $this->create_color_rank();
  }
  public function down()
  {
    $this->dbforge->drop_table('color_rank', true);
  }
}
