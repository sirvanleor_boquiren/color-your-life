<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_user_reviews_table extends CI_Migration {
  public function create_user_reviews()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`user_review_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'reviewee_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'The one who is being reviewed. Fk app users'
      ),
      'reviewer_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'The one who is giving the review. Fk app users'
      ),
      'review' => array(
        'type' => 'TEXT',
        'comment' => 'The body of review'
      ),
      'rating' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'Satisfied, Meh, Not satisfied'
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'default' => 'active',
        'comment' => 'active / inactive'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if($this->dbforge->create_table('user_reviews'))
    {
      $table = "user_reviews";

      $data = array(
        'reviewee_id' => '1',
        'reviewer_id' => '1',
        'review' => 'Omaewa mou shindeiru',
        'rating' => 'Meh',
        'status' => 'active'
      );
      $this->db->insert($table, $data);

      $data = array(
        'reviewee_id' => '1',
        'reviewer_id' => '2',
        'review' => 'Nani?!',
        'rating' => 'Satisfied',
        'status' => 'active'
      );
      $this->db->insert($table, $data);

    }
  }
  public function up()
  {
    $this->create_user_reviews();
  }
  public function down()
  {
    $this->dbforge->drop_table('user_reviews', true);
  }
}
