<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_messages_table extends CI_Migration {
  public function create_messages()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`message_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'body' => array(
        'type' => 'TEXT',
        'comment' => 'The body of review'
      ),
      'conversation_owner_id' => array(
        'type' => 'INT',
        'comment' => 'Copy of the owner (ID)'
      ),
      'conversation_partner_id' => array(
        'type' => 'INT',
        'comment' => 'The one appearing in your inbox'
      ),
      'sender_id' => array(
        'type' => 'INT',
        'comment' => 'FK sender'
      ),
      'receiver_id' => array(
        'type' => 'INT',
        'comment' => 'FK receiver of message'
      ),
      'attachment_id' => array(
        'type' => 'INT',
        'null' => 'true',
        'default' => null,
        'comment' => 'FK receiver of message'
      ),
      'post_id' => array(
        'type' => 'INT',
        'null' => 'true',
        'default' => null,
        'comment' => 'Post id if the message is an inquiry about a certain post'
      ),
      'is_read' => array(
        'type' => 'TINYINT',
        'default' => 0,
        'comment' => 'If the message has been read'
      )

    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('messages');

  }
  public function up()
  {
    $this->create_messages();
  }
  public function down()
  {
    $this->dbforge->drop_table('messages', true);
  }
}
