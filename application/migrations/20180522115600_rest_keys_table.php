<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_rest_keys_table extends CI_Migration {
  public function create_rest_keys()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '11',
      ),
      'rest_key' => array(
        'type' => 'VARCHAR',
        'constraint' => '40',
      ),
      'level' => array(
        'type' => 'INT',
        'constraint' => '11',
      ),
      'ignore_limits' => array(
        'type' => 'TINYINT',
        'constraint' => '4',
      ),
      'is_private_key' => array(
        'type' => 'TINYINT',
        'constraint' => '4',
      ),
      'ip_addresses' => array(
        'type' => 'TEXT',
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if($this->dbforge->create_table('rest_keys'))
    {
      $table = "rest_keys";
      $data = array(
        'rest_key' => 'bWCxzkqRY+qqQkPiNkV2ww===='
      );
      $this->db->insert($table, $data);
    }
  }
  public function up()
  {
    $this->create_rest_keys();
  }
  public function down()
  {
    $this->dbforge->drop_table('rest_keys', true);
  }
}