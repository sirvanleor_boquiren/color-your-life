<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_points_logs_table extends CI_Migration {
  public function create_points_logs()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`points_log_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'referrer' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'referred' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'redeem_request_id' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'membership / bonus'
      ),
      'points_added' => array(
        'type' => 'FLOAT'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('points_logs');
  }
  public function up()
  {
    $this->create_points_logs();
  }
  public function down()
  {
    $this->dbforge->drop_table('points_logs', true);
  }
}