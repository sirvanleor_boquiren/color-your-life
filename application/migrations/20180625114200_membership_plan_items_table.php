<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_membership_plan_items_table extends CI_Migration {
  public function create_membership_plan_items()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`membership_plan_item_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'membership_plan_id' => array(
        'type' => 'INT',
        'constraint' => '100',
      ),
      'content' => array(
        'type' => 'TEXT'
      ),
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'styled / normal'
      )
      
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if ($this->dbforge->create_table('membership_plan_items')) {
      $table = 'membership_plan_items';
      $data = array(
          array(
            'membership_plan_id' => '1',
            'content' => 'Access to one market place only (Free for 3 mos. only)',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '1',
            'content' => '*If member wants to access the other market places:',
            'type' => 'styled'
          ),
          array(
            'membership_plan_id' => '1',
            'content' => '$1 per month / marketplace',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '1',
            'content' => 'Chat function',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '2',
            'content' => 'Access to one market place only (Free for 3 mos. only)',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '2',
            'content' => '*If member wants to access the other market places:',
            'type' => 'styled'
          ),
          array(
            'membership_plan_id' => '2',
            'content' => '$1 per month / marketplace',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '2',
            'content' => 'Chat function',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '3',
            'content' => 'Access to one market place only (Free for 3 mos. only)',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '3',
            'content' => '*If member wants to access the other market places:',
            'type' => 'styled'
          ),
          array(
            'membership_plan_id' => '3',
            'content' => '$1 per month / marketplace',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '3',
            'content' => 'Chat function',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '4',
            'content' => '$50 per month',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '4',
            'content' => 'Access to all marketplace',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '4',
            'content' => 'Chat function',
            'type' => 'normal'
          ),
          array(
            'membership_plan_id' => '4',
            'content' => 'Advertisement display free for the first 3 ads (*$5/ad after 3 ads)',
            'type' => 'normal'
          )
        );
      $this->db->insert_batch($table, $data);
    }
  }
  public function up()
  {
    $this->create_membership_plan_items();
  }
  public function down()
  {
    $this->dbforge->drop_table('membership_plan_items', true);
  }
}