<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_crud_table extends CI_Migration {
  public function create_crud()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`crud_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");

    $this->dbforge->create_table('crud');

  }
  public function up()
  {
    $this->create_crud();
  }
  public function down()
  {
    $this->dbforge->drop_table('crud', true);
  }
}