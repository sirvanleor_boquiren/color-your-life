<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_admin_users_table extends CI_Migration {
  public function create_admin_users()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`admin_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'fname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200',
      ),
      'lname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200',
      ),
      'username' => array(
        'type' => 'VARCHAR',
        'constraint' => '225',
      ),
      'email' => array(
        'type' => 'VARCHAR',
        'constraint' => '225',
      ),
      'password' => array(
        'type' => 'VARCHAR',
        'constraint' => '300',
      ),
      'status' => array(
        'type' => 'TINYINT',
        'constraint' => '5',
        'default' => '1',
        'comment' => '1 - active, 0 - inactive'
      ),
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => '300',
        'default' => 'Admin',
        'comment' => 'Super Admin / Admin'
      ),
      'contact' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if($this->dbforge->create_table('admin_users'))
    {
      $table = "admin_users";
      $data = array(
        'fname' => 'Admin',
        'lname' => 'Boquiren',
        'username' => 'admin',
        'email' => 'admin@admin.com',
        'password' => '$2y$10$.E0fx6eXHTBW8JWPOIaYI.ciU.S.ButF7HcmMV3oh6xVpoWGbmEIO',
        'status' => '1',
        'contact' => '09163873993',
      );
      $this->db->insert($table, $data);

      $data = array(
        'fname' => 'Jc',
        'lname' => 'Andres',
        'username' => 'jc',
        'email' => 'jcandres@myoptimind.com',
        'password' => '$2y$10$.E0fx6eXHTBW8JWPOIaYI.ciU.S.ButF7HcmMV3oh6xVpoWGbmEIO',
        'status' => '1',
        'contact' => 'no',
        'type' => 'Super Admin'
      );
      $this->db->insert($table, $data);

      $data = array(
        'fname' => 'Sel',
        'lname' => 'Valerio',
        'username' => 'sel',
        'email' => 'cvalerio@myoptimind.com',
        'password' => '$2y$10$.E0fx6eXHTBW8JWPOIaYI.ciU.S.ButF7HcmMV3oh6xVpoWGbmEIO',
        'status' => '1',
        'contact' => 'no',
        'type' => 'Special Admin'
      );
      $this->db->insert($table, $data);
    }
  }
  public function up()
  {
    $this->create_admin_users();
  }
  public function down()
  {
    $this->dbforge->drop_table('admin_users', true);
  }
}