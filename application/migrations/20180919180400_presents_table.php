<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_presents_table extends CI_Migration {
  public function create_presents()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`present_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
        'image_path' => array(
            'type' => 'VARCHAR',
            'constraint' => 500
        ),
        'image_name' => array(
          'type' => 'VARCHAR',
          'constraint' => 500
        ),
        'title' => array(
            'type' => 'VARCHAR',
            'constraint' => 200
        ),
        'no_of_members' => array(
            'type' => 'INT',
            'constraint' => 100
        ),
        'is_featured' => array(
            'type' => 'TINYINT',
            'default' => 0
        ),
        'type' => array(
            'type' => 'VARCHAR',
            'constraint' => 200
        )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('presents');

  }
  public function up()
  {
    $this->create_presents();
  }
  public function down()
  {
    $this->dbforge->drop_table('presents', true);
  }
}
