<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_request_present_table extends CI_Migration {
  public function create_request_present()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`request_present_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'request_id' => array(
        'type' => 'INT',
        'constraint' => '200'
      ),
      'present_id' => array(
        'type' => 'INT',
        'constraint' => '200'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('request_present');

  }
  public function up()
  {
    $this->create_request_present();
  }
  public function down()
  {
    $this->dbforge->drop_table('request_present', true);
  }
}
