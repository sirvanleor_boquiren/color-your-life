<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_posts_table extends CI_Migration {
  public function create_posts()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`post_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'market_place_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from market_place table | 1 - Products | 2 - Services'
      ),
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id in app_users'
      ),
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'sub_title' => array(
        'type' => 'VARCHAR',
        'constraint' => '300',
        'default' => null
      ),
      'price' => array(
        'type' => 'VARCHAR',
        'constraint' => '100'
      ),
      'thumbnail' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'description' => array(
        'type' => 'VARCHAR',
        'constraint' => '3000',
      ),
      'category_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from categories table'
      ),
      'status' => array(
        'type' => 'TINYINT',
        'constraint' => '5',
        'default' => '1',
        'comment' => '0 - inactive, 1 - active'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('posts');
  }
  public function up()
  {
    $this->create_posts();
  }
  public function down()
  {
    $this->dbforge->drop_table('posts', true);
  }
}