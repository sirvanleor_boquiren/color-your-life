<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_admin_logs_table extends CI_Migration {
  public function create_admin_logs()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`admin_log_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'admin_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'Fk app users. The one who received the notif.'
      ),
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => 200
      ),
      'payload' => array(
        'type' => 'TEXT',
        'comment' => 'PHP serialized string of the payload array'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('admin_logs');

  }
  public function up()
  {
    $this->create_admin_logs();
  }
  public function down()
  {
    $this->dbforge->drop_table('admin_logs', true);
  }
}
