<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_reset_query extends CI_Migration {
	public function reset_data()
	{
		$this->db->truncate("advertisements");
		$this->db->truncate("app_users");
		$this->db->truncate("app_users_videos");
		$this->db->truncate("ci_sessions");
		$this->db->truncate("color_bonus");
		$this->db->truncate("follow");
		$this->db->truncate("messages");
		$this->db->truncate("message_attachments");
		$this->db->truncate("notifications");
		$this->db->truncate("payment_logs");
		$this->db->truncate("paynamics_requests");
		$this->db->truncate("points_logs");
		$this->db->truncate("posts");
		$this->db->truncate("posts_files");
		$this->db->truncate("post_like");
		$this->db->truncate("post_location");
		$this->db->truncate("post_reviews");
		$this->db->truncate("present_requests");
		$this->db->truncate("redeem_requests");
		$this->db->truncate("request_present");
		$this->db->truncate("settings");
		$this->db->truncate("user_device_ids");
		$this->db->truncate("user_membership_plan");
		$this->db->truncate("user_reviews");
	}

	public function up()
	{
		$this->reset_data();
	}
}
?>