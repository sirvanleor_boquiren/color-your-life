<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_present_requests_table extends CI_Migration {
  public function create_present_requests()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`present_request_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '200'
      ),
      'fname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'lname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'email' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'mobile_no' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'default' => 'pending'
      )

    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('present_requests');

  }
  public function up()
  {
    $this->create_present_requests();
  }
  public function down()
  {
    $this->dbforge->drop_table('present_requests', true);
  }
}
