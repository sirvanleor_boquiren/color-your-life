<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_message_attachments_table extends CI_Migration {
  public function create_message_attachments()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`attachment_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'thumbnail' => array(
        'type' => 'TEXT',
        'comment' => 'The file name'
      ),
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('message_attachments');

  }
  public function up()
  {
    $this->create_message_attachments();
  }
  public function down()
  {
    $this->dbforge->drop_table('message_attachments', true);
  }
}
