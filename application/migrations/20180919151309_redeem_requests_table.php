<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_redeem_requests_table extends CI_Migration {
  public function create_redeem_requests()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`redeem_request_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '200'
      ),
      'fname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'lname' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'mobile_no' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'address' => array(
        'type' => 'TEXT'
      ),
      'points_to_redeem' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'affiliate_id' => array(
        'type' => 'INT',
        'constraint' => '200'
      ),
      'url' => array(
        'type' => 'VARCHAR',
        'constraint' => '200'
      ),
      'notes' => array(
        'type' => 'TEXT'
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => '200',
        'default' => 'pending'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('redeem_requests');

  }
  public function up()
  {
    $this->create_redeem_requests();
  }
  public function down()
  {
    $this->dbforge->drop_table('redeem_requests', true);
  }
}
