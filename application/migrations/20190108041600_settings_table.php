<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_settings_table extends CI_Migration {
  public function create_settings()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`settings_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'push_notification' => array(
        'type' => 'TINYINT'
      ),
      'email_notification' => array(
        'type' => 'TINYINT'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('settings');
  }
  public function up()
  {
    $this->create_settings();
  }
  public function down()
  {
    $this->dbforge->drop_table('settings', true);
  }
}
