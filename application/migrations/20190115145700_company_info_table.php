<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_company_info_table extends CI_Migration {
  public function create_company_info()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`company_info_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'type' => array(
        'type' => 'VARCHAR',
        'constraint' => 200,
        'comment' => 'This is the info type. (contact_us, about_us, terms_conditions, privacy_policy)'
      ),
      'payload' => array(
        'type' => 'TEXT',
        'comment' => 'PHP serialized string of the payload array'
      ),
      'created_by' => array(
        'type' => 'INT',
        'constraint' => 100,
        'comment' => 'ID of admin'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('company_info');

  }
  public function up()
  {
    $this->create_company_info();
  }
  public function down()
  {
    $this->dbforge->drop_table('company_info', true);
  }
}
