<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_user_membership_plan_table extends CI_Migration {
  public function create_user_membership_plan()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`user_membership_plan_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'membership_plan_id' => array(
        'type' => 'INT',
        'constraint' => '100',
      ),
      'expiration_date' => array(
        'type' => 'DATETIME',
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'active / expired / cancelled'
      ),
      'market_place_access' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 for all'
      ),
      'chat_function' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'allowed / not_allowed'
      ),
      'points_redeem_start' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'Starting points where user can redeem'
      ),
      'advertisement' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'video_recording_dist' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'group_message' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'live_webinar' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'special_bonuses' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'with / without'
      ),
      'presents' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'allowed / not_allowed'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if ($this->dbforge->create_table('user_membership_plan')) {
      $table = "user_membership_plan";
      
      $data = array(
        'user_id' => 1,
        'membership_plan_id' => 4,
        'status' => 'active',
        'market_place_access' => -1,
        'chat_function' => 'allowed',
        'points_redeem_start' => 50,
        'advertisement' => 3,
        'video_recording_dist' => 3,
        'group_message' => 3,
        'live_webinar' => 3,
        'special_bonuses' => 'with',
        'presents' => 'allowed'
      );
      $this->db->insert($table, $data);

      $data = array(
        'user_id' => 2,
        'membership_plan_id' => 1,
        'status' => 'active',
        'market_place_access' => 0,
        'chat_function' => 'allowed',
        'points_redeem_start' => 50,
        'advertisement' => 0,
        'video_recording_dist' => 0,
        'group_message' => 0,
        'live_webinar' => 0,
        'special_bonuses' => 'without',
        'presents' => 'not_allowed'
      );
      $this->db->insert($table, $data);

      $data = array(
        'user_id' => 3,
        'membership_plan_id' => 1,
        'status' => 'active',
        'market_place_access' => 0,
        'chat_function' => 'allowed',
        'points_redeem_start' => 50,
        'advertisement' => 0,
        'video_recording_dist' => 0,
        'group_message' => 0,
        'live_webinar' => 0,
        'special_bonuses' => 'without',
        'presents' => 'not_allowed'
      );
      $this->db->insert($table, $data);
    }
  }
  public function up()
  {
    $this->create_user_membership_plan();
  }
  public function down()
  {
    $this->dbforge->drop_table('user_membership_plan', true);
  }
}