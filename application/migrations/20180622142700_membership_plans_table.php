<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_membership_plans_table extends CI_Migration {
  public function create_membership_plans()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`membership_plan_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '100'
      ),
      'rate' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'amount per month'
      ),
      'rate_type' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'yearly / monthly'
      ),
      'market_place_access' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 for all'
      ),
      'chat_function' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'allowed / not_allowed'
      ),
      'points_redeem_start' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'Starting points where user can redeem'
      ),
      'advertisement' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'video_recording_dist' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'group_message' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'live_webinar' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => '-1 = infinite, 0 = none, 1 = 1 free'
      ),
      'special_bonuses' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'with / without'
      ),
      'presents' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'allowed / not_allowed'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if ( $this->dbforge->create_table('membership_plans') ) {
      $table = "membership_plans";
      $data[0] = array(
        'name' => 'Free',
        'rate' => '0',
        'rate_type' => 'monthly',
        'market_place_access' => '1',
        'chat_function' => 'allowed',
        'points_redeem_start' => '50',
        'advertisement' => '0',
        'video_recording_dist' => '0',
        'group_message' => '0',
        'live_webinar' => '0',
        'special_bonuses' => 'without',
        'presents' => 'not_allowed'
      );
      $data[1] = array(
        'name' => 'Challenger',
        'rate' => '12',
        'rate_type' => 'yearly',
        'market_place_access' => '1',
        'chat_function' => 'allowed',
        'points_redeem_start' => '50',
        'advertisement' => '0',
        'video_recording_dist' => '0',
        'group_message' => '0',
        'live_webinar' => '0',
        'special_bonuses' => 'without',
        'presents' => 'not_allowed'
      );
      $data[2] = array(
        'name' => 'Economy',
        'rate' => '10',
        'rate_type' => 'monthly',
        'market_place_access' => '1',
        'chat_function' => 'allowed',
        'points_redeem_start' => '50',
        'advertisement' => '0',
        'video_recording_dist' => '0',
        'group_message' => '1',
        'live_webinar' => '0',
        'special_bonuses' => 'with',
        'presents' => 'allowed'
      );
      $data[3] = array(
        'name' => 'Business',
        'rate' => '50',
        'rate_type' => 'monthly',
        'market_place_access' => '-1',
        'chat_function' => 'allowed',
        'points_redeem_start' => '50',
        'advertisement' => '3',
        'video_recording_dist' => '3',
        'group_message' => '3',
        'live_webinar' => '3',
        'special_bonuses' => 'with',
        'presents' => 'allowed'
      );
      $this->db->insert($table, $data[0]);
      $this->db->insert($table, $data[1]);
      $this->db->insert($table, $data[2]);
      $this->db->insert($table, $data[3]);
    }

  }
  public function up()
  {
    $this->create_membership_plans();
  }
  public function down()
  {
    $this->dbforge->drop_table('membership_plans', true);
  }
}