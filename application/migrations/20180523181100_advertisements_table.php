<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_advertisements_table extends CI_Migration {
  public function create_advertisements()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`ad_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'market_place_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from market_place table | 1 - Products | 2 - Services'
      ),
      'post_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id from posts'
      ),
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'id in app_users'
      ),
      'image' => array(
        'type' => 'VARCHAR',
        'constraint' => '300'
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'expired/etc',
        'default' => 'active'
      ),
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('advertisements');
  }
  public function up()
  {
    $this->create_advertisements();
  }
  public function down()
  {
    $this->dbforge->drop_table('advertisements', true);
  }
}