<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_post_reviews_table extends CI_Migration {
  public function create_post_reviews()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`post_review_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'Reviewer ID'
      ),
      'post_id' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'Post id to be reviewed'
      ),
      'review' => array(
        'type' => 'TEXT',
        'comment' => 'The body of review'
      ),
      'rating' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'comment' => 'Satisfied, Meh, Not satisfied'
      ),
      'status' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'default' => 'active',
        'comment' => 'active / inactive'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    if($this->dbforge->create_table('post_reviews'))
    {
      $table = "post_reviews";

      $data = array(
        'user_id' => '1',
        'post_id' => '1',
        'review' => 'Omaewa mou shindeiru',
        'rating' => 'Meh',
        'status' => 'active'
      );
      $this->db->insert($table, $data);

      $data = array(
        'user_id' => '1',
        'post_id' => '2',
        'review' => 'Nani?!',
        'rating' => 'Satisfied',
        'status' => 'active'
      );
      $this->db->insert($table, $data);

    }
  }
  public function up()
  {
    $this->create_post_reviews();
  }
  public function down()
  {
    $this->dbforge->drop_table('post_reviews', true);
  }
}
