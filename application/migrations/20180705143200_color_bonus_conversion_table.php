<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_color_bonus_conversion_table extends CI_Migration {
  public function create_color_bonus_conversion()
  {
    // $this->dbforge->add_field('id');
    $this->dbforge->add_field("`color_bonus_conversion_id` INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY");
    $this->dbforge->add_field(array(
      'membership_plan_id' => array(
        'type' => 'INT',
        'constraint' => '100'
      ),
      'members_joined' => array(
        'type' => 'INT',
        'constraint' => '100',
        'comment' => 'number of members joined'
      ),
      'points_equivalent' => array(
        'type' => 'INT',
        'constraint' => '100'
      )
    ));
    $this->dbforge->add_field("`created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP");
    $this->dbforge->add_field("`updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP");
    $this->dbforge->create_table('color_bonus_conversion');
  }
  public function up()
  {
    $this->create_color_bonus_conversion();
  }
  public function down()
  {
    $this->dbforge->drop_table('color_bonus_conversion', true);
  }
}