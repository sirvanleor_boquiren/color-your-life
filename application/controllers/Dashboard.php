<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('accounts_model');
    }

    public function wrapper($body, $data = NULL) 
	{
		// if (check_login($this->session)) 
		// {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		// }
		// else {
			// redirect(base_url());
		// }
	}

	public function index()
	{
		// $data["total_users"] = $this->accounts_model->getTotalUsers("0"); # 0 - for non admin/app users
		// var_dump($data);
		$this->wrapper("dashboard");
	}

}

?>