<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Points extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model("Rank_model", 'rank_model');
    }

    public function index()
    {
    	$data['color_rank'] = $this->rank_model->all();
    	$this->load->view('web_view/color_points', $data);
    }
}