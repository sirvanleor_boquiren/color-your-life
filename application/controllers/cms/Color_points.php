<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Color_points extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model("affiliate_model");
        $this->load->model("settings_model");
        $this->load->library("pagination");
    }

    public function wrapper($body, $data = NULL)
	{
		if (check_login($this->session)) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		}
        else {
            redirect(base_url());
        }
    }

    public function affiliates()
    {
        $search_keyword = @$_GET['search_keyword'];

        if (@$search_keyword) {
            $data['affiliates'] = $this->affiliate_model->all($search_keyword);
            $total_data = count($this->affiliate_model->getAllAffiliates($search_keyword));
            $data['search_keyword'] = $search_keyword;
        }
        else {
            $data['affiliates'] = $this->affiliate_model->all();
            $total_data = count($this->affiliate_model->getAllAffiliates());
        }
        
        # Init Pagination
        $page_conf['base_url'] = base_url("cms/color_points/affiliates");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = $total_data;
        $page_conf['per_page'] = 10;        
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();
        $this->wrapper('affiliates_management',$data);
    }

        
    public function getToken() 
    {
        if(isset($_SESSION['token'])) {
            return $_SESSION['token']; 
        } else {
            $length = 32;
            $token = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $length); 
            $_SESSION['token'] = $token;
            return $token;
        }
    }

    /** PENDING REDEEM POINTS **/
    public function pending() 
    {
        $length = 32;
        $_SESSION['token-expire'] = time() + 3600;

        $this->load->model('color_points_model');
        $search_keyword = @$_GET['search_keyword'];

        if (@$search_keyword) {
            $data['redeem_requests'] = $this->color_points_model->pending_requests(null, $search_keyword);
            $data['membership_plans'] = $this->color_points_model->membership_plans();
            $total_data = count($this->color_points_model->pending_requests(true, $search_keyword));
            $data['search_keyword'] = $search_keyword;
        }
        else {
            $data['redeem_requests'] = $this->color_points_model->pending_requests();
            $data['membership_plans'] = $this->color_points_model->membership_plans();
            $total_data = count($this->color_points_model->pending_requests(true));
        }
        
        # Init Pagination
        $page_conf['base_url'] = base_url("cms/color_points/pending");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = $total_data;
        $page_conf['per_page'] = 10;        
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();


        $data['token'] = $this->getToken();
        $this->wrapper('color_points_pending',$data);
    }

    public function create()
    {
        $post = $this->input->post();
        if($this->affiliate_model->add($post)){
            $msg_data['alert_msg'] = "Successfully added.";
            $msg_data['alert_class'] = "alert-success";
            if($errors = $this->affiliate_model->uploadImage()){
                $msg_data['alert_msg'] = $msg_data['alert_msg'].' | '.$errors;
                $msg_data['alert_class'] = "alert-block alert-danger";
            }
        }
        $this->session->set_flashdata($msg_data);
        redirect('cms/color_points/affiliates');
    }

    public function edit($id)
    {
        // update other data
        $affiliate = $this->affiliate_model->get($id);
        $post = $this->input->post();
        $changes = 0;
        // update only if there's a change
        if($this->affiliate_model->updateChanges($id,$post)){
            $changes++;
        }
        
        
        //if there's a file uploaded replace existing image
        if(!$_FILES['affiliate_image']['error'] == UPLOAD_ERR_NO_FILE){
            $errors = $this->affiliate_model->uploadImage($id);
            if($errors !== NULL){
                $msg_data['alert_msg'] = $errors;
                $msg_data['alert_class'] = "alert-block alert-danger";
            }else{
                $changes++;
                unlink('public/affiliates_images/'.$affiliate->affiliate_id."_".$affiliate->image_name);
            }
            
        }
        if($changes){
            $msg_data['alert_msg'] = "Data successfully updated.";
            $msg_data['alert_class'] = "alert-success";
        }
        $this->session->set_flashdata($msg_data);
        redirect('cms/color_points/affiliates');
    }

    public function delete($id)
    {
        $affiliate = $this->db->get_where('affiliates',[
            'affiliate_id' => $id
        ])->row();
        $affiliate->image_name = str_replace(" ", "_", $affiliate->image_name);
        if(unlink('public/affiliates_images/'.$affiliate->affiliate_id.'_'.$affiliate->image_name) && $this->affiliate_model->delete($id)){
            $msg_data['alert_msg'] = "Successfully Deleted.";
            $msg_data['alert_class'] = "alert-success";
            
        }else{
            //fail
            $msg_data['alert_msg'] = "Error Deleting data.";
            $msg_data['alert_class'] = "alert-block alert-info";
        }
        $this->session->set_flashdata($msg_data);

        redirect('cms/color_points/affiliates');
    }

    public function rejected()
    {
        $this->load->model('color_points_model');
        if ($this->input->post('action')) { 
            $data['is_updated'] = $this->color_points_model->reject_request($this->input->post('id'));
            if ($data['is_updated']) {

                $user = $this->color_points_model->get_requestor_details($this->input->post('id'));
                $notif = $this->settings_model->getSettings($user->user_id);
                /**
                 * Send email
                 */
                if (@$notif[0]->email_notification) {
                    $this->load->helper("email_template");
                    $recipient = $user->email;
                    $subject = "Rejected color points redeem";
                    $body = rejected_color_points_template($user->fname . " " . $user->lname);
                    send_email($this, $recipient, $subject, $body);
                }
            }
            $msg_data['alert_msg'] = "Redeem Request is successfully rejected.";
            $msg_data['alert_class'] = "alert-block alert-danger";
            $this->session->set_flashdata($msg_data);
        } else {
            $data['is_updated'] = null;
        }

        $search_keyword = @$_GET['search_keyword'];

        if (@$search_keyword) {
            $data['redeem_requests'] = $this->color_points_model->rejected_requests(null, $search_keyword);
            $total_data = count($this->color_points_model->rejected_requests(true, $search_keyword));
            $data['search_keyword'] = $search_keyword;
        }
        else {
            $data['redeem_requests'] = $this->color_points_model->rejected_requests();
            $total_data = count($this->color_points_model->rejected_requests(true));
        }
       
        # Init Pagination
        $page_conf['base_url'] = base_url("cms/color_points/rejected");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = $total_data;
        $page_conf['per_page'] = 10;
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();

        $this->wrapper('color_points_rejected',$data);

    }

    public function approved()
    {
        $this->load->model('color_points_model');
        if ($this->input->post('token') == $this->getToken()){
            if ($this->input->post('action')) {
               if ($this->color_points_model->approve_requests($this->input->post('id'),$this->input->post('points_to_deduct'))) {
                    
                    $user = $this->color_points_model->get_requestor_details($this->input->post('id'));
                    $notif = $this->settings_model->getSettings($user->user_id);
                    /**
                     * Send email
                     */
                    if (@$notif[0]->email_notification) {
                        $this->load->helper("email_template");
                        $recipient = $user->email;
                        $subject = "Approved color points redeem";
                        $body = approved_color_points_template($user->fname . " " . $user->lname);
                        send_email($this, $recipient, $subject, $body);
                    }

                   $msg_data['alert_msg'] = "Redeem Request is successfully approved.";
                   $msg_data['alert_class'] = "alert-block alert-info";
                   $this->session->set_flashdata($msg_data);
               }
               unset($_SESSION['token']);
           }     
       }

       $search_keyword = @$_GET['search_keyword'];

        if (@$search_keyword) {
            $data['redeem_requests'] = $this->color_points_model->approved_requests(null, $search_keyword);
            $total_data = count($this->color_points_model->approved_requests(true, $search_keyword));
            $data['search_keyword'] = $search_keyword;
        }
        else {
            $data['redeem_requests'] = $this->color_points_model->approved_requests();
            $total_data = count($this->color_points_model->approved_requests(true));
        }
        
        # Init Pagination
        $page_conf['base_url'] = base_url("cms/color_points/approved");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = $total_data;
        $page_conf['per_page'] = 10;
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();

        $this->wrapper('color_points_approved',$data);

    }

}

?>
