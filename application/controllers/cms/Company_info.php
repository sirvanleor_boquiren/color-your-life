<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_info extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('company_info_model', 'model');
    }

    public function wrapper($body, $data = NULL)
	{
		if (check_login($this->session)) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		}
        else {
            redirect(base_url());
        }
    }

    public function privacy_policy()
    {
    	$data['data'] = (array)$this->model->get('privacy_policy');
    	$this->wrapper('privacy_policy', $data);
    }

    public function contact_us()
    {
        $data['data'] = (array)$this->model->get('contact_us');
        $this->wrapper('contact_us', $data);
    }

    public function about_us()
    {
        $data['data'] = (array)$this->model->get('about_us');
        $this->wrapper('about_us', $data);
    }

    public function terms_conditions()
    {
        $data['data'] = (array)$this->model->get('terms_conditions');
        $this->wrapper('terms_conditions', $data);
    }

    public function add()
    {
    	$post = $this->input->post();
    	$res = $this->model->add($post);

    	$msg_data['alert_msg'] = "Failed to update";
    	$msg_data['alert_class'] = "alert-block alert-danger";
    	if ($res) {
    		$msg_data['alert_msg'] = "Data successfully updated.";
    		$msg_data['alert_class'] = "alert-success";
    	}
    	$this->session->set_flashdata($msg_data);

    	if (@$post['save_privacy_policy']) {
    		redirect('cms/company_info/privacy_policy');
    	}
        elseif (@$post['save_about_us']) {
            redirect('cms/company_info/about_us');
        }
        elseif (@$post['save_contact_us']) {
            redirect('cms/company_info/contact_us');
        }
        elseif (@$post['save_terms_conditions']) {
            redirect('cms/company_info/terms_conditions');
        }
    }
}

?>
