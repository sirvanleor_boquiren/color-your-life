<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presents extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model("admin_model");
        $this->load->model("user_model");
        $this->load->model("present_model");
        $this->load->model("present_model","model");
        $this->load->model("settings_model");

    }

    public function wrapper($body, $data = NULL)
	{
		// if (check_login($this->session))
		// {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		// }
		// else {
			// redirect(base_url());
		// }
    }
    
    public function index(){
		$data['presents'] = $this->present_model->all();

		# Init Pagination
        $page_conf['base_url'] = base_url("cms/presents");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = count($this->present_model->getAllPresents());
        $page_conf['per_page'] = 10;        
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();

		$this->wrapper('presents_management',$data);
	}

	public function create()
	{

		$post = $this->input->post();
		if($this->present_model->add($post)){
			$msg_data['alert_msg'] = "Successfully added.";
			$msg_data['alert_class'] = "alert-success";
			if($errors = $this->present_model->uploadImage()){
				$msg_data['alert_msg'] = $msg_data['alert_msg'].' | '.$errors;
				$msg_data['alert_class'] = "alert-block alert-danger";
			}
		}
		$this->session->set_flashdata($msg_data);
		redirect('cms/presents');
		
	}

	public function edit($id)
	{
        
		// update other data
		$present = $this->present_model->get($id);
        $post = $this->input->post();
        $feature_able = true;

        if(!isset($post['is_featured'])){
            $post['is_featured'] = 0;
        }
		$changes = 0;



        if($post['type'] != $present->type || $present->is_featured != $post['is_featured']){
            $featuredcount = $this->present_model->getFeaturedCountByColor($post['type']);
            if($post['is_featured'] == 1 && $featuredcount >= 10){
                $post['is_featured'] = 0;
                $feature_able = false;
            }
        }
		// update only if there's a change
		if($this->present_model->updateChanges($id,$post)){
            $changes++;
		}
		
		
		//if there's a file uploaded replace existing image
		if(!$_FILES['presents_image']['error'] == UPLOAD_ERR_NO_FILE){
			$errors = $this->present_model->uploadImage($id);
			if($errors !== NULL){
				$msg_data['alert_msg'] = $errors;
				$msg_data['alert_class'] = "alert-block alert-danger";
			}else{
				$changes++;
				//var_dump($present);
				unlink('public/presents_images/'.$present->present_id."_".$present->image_name);
			}
			
		}
		if($changes){
			$msg_data['alert_msg'] = "Data successfully updated. ";
			$msg_data['alert_class'] = "alert-success";
		}

        if(!$feature_able){
            $msg_data['alert_msg'] .= "Max limit of 10 for featured in type {$post['type']} is reached";
			$msg_data['alert_class'] = "alert-block alert-danger";
        }
		$this->session->set_flashdata($msg_data);
		redirect('cms/presents');
	}

	public function delete($id)
	{
		$present = $this->db->get_where('presents',[
			'present_id' => $id
		])->row();
    $present->image_name = str_replace(" ", "_", $present->image_name);
		if(unlink('public/presents_images/'.$present->present_id.'_'.$present->image_name) && $this->present_model->delete($id)){
			$msg_data['alert_msg'] = "Successfully Deleted.";
			$msg_data['alert_class'] = "alert-success";
			
		}else{
			//fail
			$msg_data['alert_msg'] = "Error Deleting data.";
			$msg_data['alert_class'] = "alert-block alert-danger";
		}
		$this->session->set_flashdata($msg_data);

		redirect('cms/presents');
	}
	
	public function checkIfCanBeFeatured()
	{
		$type = $this->input->post('type');

		$this->present_model->canBeFeatured($type,true);
	}

    public function  getToken() 
    {
        if(isset($_SESSION['token'])) {
            return $_SESSION['token']; 
        } else {
            $length = 32;
            $token = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $length); 
            $_SESSION['token'] = $token;
            return $token;
        }
    }

    /** PENDING PRESENTS POINTS **/
    public function pending() 
    {
        
        $data['presents_requests'] = $this->present_model->pending_requests();
        
        # Init Pagination
        $page_conf['base_url'] = base_url("cms/presents/pending");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = count($this->present_model->allPendingRequests());
        $page_conf['per_page'] = 10;        
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();

        $data['token'] = $this->getToken();
        $this->wrapper('presents_pending',$data);
    }

    public function rejected()
    {

         if ($this->input->post('token') == $this->getToken()){
            if ($this->input->post('action')) {
             $data['redeem_requests'] = $this->present_model->reject_requests($this->input->post('id'));
             if ($data['redeem_requests']) {
                $user = $this->present_model->get_requestor_details($this->input->post('id'));
                $notif = $this->settings_model->getSettings($user->user_id);

                /**
                 * Send email
                 */
                if (@$notif[0]->email_notification) {
                  $this->load->helper("email_template");
                  $recipient = $user->email;
                  $subject = "Rejected Color Presents Request";
                  $body = rejected_color_presents_template($user->fname . " " . $user->lname);
                  send_email($this, $recipient, $subject, $body);
                }
             }
             unset($_SESSION['token']);
            }     
        }
        
        
        $data['presents_requests'] = $this->present_model->rejected_requests();

        # Init Pagination
        $page_conf['base_url'] = base_url("cms/presents/rejected");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = count($this->present_model->allRejectedRequests());
        $page_conf['per_page'] = 10;
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();
        
        $this->wrapper('presents_rejected',$data);

    }

    public function approved()
    {
       
        if ($this->input->post('token') == $this->getToken()){
            if ($this->input->post('action')) {
             $data['redeem_requests'] = $this->present_model->approve_requests($this->input->post('id'));
             if ($data['redeem_requests']) {

                $user = $this->present_model->get_requestor_details($this->input->post('id'));
                $notif = $this->settings_model->getSettings($user->user_id);

                 /**
                  * Send email
                  */
                 if (@$notif[0]->email_notification) {
                   $this->load->helper("email_template");
                   $recipient = $user->email;
                   $subject = "Approved Color Presents Request";
                   $body = approved_color_presents_template($user->fname . " " . $user->lname);
                   send_email($this, $recipient, $subject, $body);
                 }
             }
             unset($_SESSION['token']);
            }     
        }
        
        
        $data['presents_requests'] = $this->present_model->approved_requests();

        # Init Pagination
        $page_conf['base_url'] = base_url("cms/presents/approved");
        $page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
        $page_conf['total_rows'] = count($this->present_model->allApprovedRequests());
        $page_conf['per_page'] = 10;
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
        $this->pagination->initialize($page_conf);
        # / Init Pagination
        
        $data['pagination'] = $this->pagination->create_links();
        
        $this->wrapper('presents_approved',$data);

    }

}

?>
