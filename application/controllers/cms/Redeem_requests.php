<?php

class Redeem_requests extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('redeem_requests_model');
        $this->load->model('user_model');
        $this->load->model('notification_model');
    }
    public function wrapper($body, $data = NULL)
	{
		// if (check_login($this->session))
		// {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		// }
		// else {
			// redirect(base_url());
		// }
    }
    public function index(){

        //get all redeem requests
        $data['redeem_requests'] = $this->redeem_requests_model->getAllWithUserAndAffiliate();

       // return index view of redeem requests


       $this->wrapper('redeem_requests_management',$data);	
    }

    public function process_redeem_request($id,$action)
    {
        $post = $this->input->post();
        $redeem_request = @$this->redeem_requests_model->get($id)[0];
        $response = [
            'status' => 200,
            'data' => $redeem_request
        ];
        $valid = true;

        if(!($valid = ($redeem_request != NULL))){
            $response['message'] = "Request not found!";
        }elseif($valid = substr($redeem_request->status,strlen($redeem_request->status)-2,2) == 'ed'){
            $response['message'] = 'This request has already been processed!';
            $valid = false;
        }elseif(!($valid = (in_array($action,['accept','decline'])) ) ){
            $response['message'] = "Action not recognized!";
        }
        
        if(!$valid){
            $response['data'] = (object)[];
            $response['status'] = 400;
        }
        if($valid && strtolower($action) === 'accept'){
            $response['message'] = 'Request successfully accepted!';
            $user = $this->user_model->getUserby('user_id', $redeem_request->user_id);
            if($user->points - $redeem_request->points_to_redeem < 0){
                $response = [
                    'message' => 'User doesn\'t have enough points!',
                    'status' => 400
                ];
            }else{

                $this->redeem_requests_model->update($id,[
                    'status' => 'accepted'
                ]);
                $this->user_model->update($user->user_id,[
                    'points' => ($user->points - $redeem_request->points_to_redeem)
                ]);

                /**
                 * Check if Settings if push notif is on
                 */
                $notif = $this->settings_model->getSettings($user->user_id);
                if ($notif[0]->push_notification) {
                    $this->user_model->sendMail($user->email,'Color your life - Your request is accepted!','your redeem request has been accepted!');
                }
                
                # Send in-app notification
                $this->sendNotification("Color Points", $user->user_id, "redeem_request", ['status' => 'accepted']);
            }
        }elseif($valid && strtolower($action == 'decline')){
            $this->redeem_requests_model->update($id,[
                'status' => 'declined'
            ]);
            $user = $this->user_model->getUserby('user_id', $redeem_request->user_id);

            /**
             * Check if Settings if push notif is on
             */
            $notif = $this->settings_model->getSettings($user->user_id);
            if ($notif[0]->push_notification) {
                $this->user_model->sendMail($user->email,'Color your life - Your request is declined!','your redeem request has been declined!');
            }

            # Send in-app notification
            $this->sendNotification("Color Points", $user->user_id, "redeem_request", ['status' => 'declined']);

            $response['message'] = 'Request successfully declined!';
        }
        $flashdata = [
            'alert_msg' => $response['message'],
            'alert_class' => $response['status'] == 200 ? "alert-success" : "alert-block alert-danger"
        ];
        $this->session->set_flashdata($flashdata);

        redirect(base_url('cms/redeem_requests'));


    }

    function sendNotification($title = "Color your life", $user_id, $notif_type, $payload)
    {
        $this->load->model('settings_model');

        /**
         * Check if Settings if push notif is on
         */
        $notif = $this->settings_model->getSettings($user_id);

        if ($notif[0]->push_notification) {
            $notification_message = $this->notification_model->storeNotif($user_id, $notif_type, $payload);
            $recipients = $this->udi_model->getDeviceIdsByUserId($user_id);
            if ($recipients) {
                foreach ($recipients as $to) {
                    $firebase_response = $this->firebase_model->sendMultiple(
                        $to,
                        ['data' => []],
                        ['title' => $title, 'body' => $notification_message]
                    );
                    log_message('error', $firebase_response);
                    log_message('error', $notification_message);
                }
            }
        }
    }

}
