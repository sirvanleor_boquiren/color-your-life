<?php

class Present_requests extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('present_requests_model');
        $this->load->model('user_model');
        $this->load->model('present_model');
    }
    public function wrapper($body, $data = NULL)
	{
		// if (check_login($this->session))
		// {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		// }
		// else {
			// redirect(base_url());
		// }
    }
    public function index(){

        //get all redeem requests
        $data['present_requests'] = $this->present_requests_model->getAllWithUser();
        foreach($data['present_requests'] as $present_request)
        {
            $present_request->present = [];
            $present_request->presents = $this->present_model->getByPresentRequestId($present_request->present_request_id);
        }
       // return index view of redeem requests


       $this->wrapper('present_requests_management',$data);	
    }

    public function present_request($id,$action)
    {
        $post = $this->input->post();
        $present_request = $this->present_requests_model->get($id)[0];
        $response = [
            'status' => 200,
            'data' => $present_request
        ];

        $valid = true;
        // $action = $this->input->post('action');

        if(!($valid = ($present_request != NULL))){
            $response['message'] = "Request not found!";
        }elseif($valid = substr($present_request->status,strlen($present_request->status)-2,2) == 'ed'){
            $response['message'] = 'This request has already been processed!';
            $valid = false;
        }elseif(!($valid = (in_array($action,['accept','decline'])) ) ){
            $response['message'] = "Action not recognized!";
        }
        
        if(!$valid){
            $response['data'] = (object)[];
            $response['status'] = 400;
        }
        if($valid && strtolower($action) === 'accept'){
            $this->present_requests_model->update($id,[
                'status' => 'accepted'
            ]);
            $response['message'] = 'Request successfully accepted!';
            $user = $this->user_model->getUserby('user_id', $present_request->user_id);

            /**
             * Check if Settings if push notif is on
             */
            $notif = $this->settings_model->getSettings($user->user_id);
            if ($notif[0]->push_notification) {
                $this->user_model->sendMail($user->email,'Color your life - Your request is accepted!','your present request has been accepted!');
            }

        }elseif($valid && strtolower($action == 'decline')){
            $this->present_requests_model->update($id,[
                'status' => 'declined'
            ]);
            $user = $this->user_model->getUserby('user_id', $present_request->user_id);

            /**
             * Check if Settings if push notif is on
             */
            $notif = $this->settings_model->getSettings($user->user_id);
            if ($notif[0]->push_notification) {
                $this->user_model->sendMail($user->email,'Color your life - Your request is declined!','your present request has been declined!');
            }

            $response['message'] = 'Request successfully declined!';
        }

        $flashdata = [
            'alert_msg' => $response['message'],
            'alert_class' => $response['status'] == 200 ? "alert-success" : "alert-block alert-danger"
        ];
        $this->session->set_flashdata($flashdata);

        redirect(base_url('cms/present_requests'));
    }

}
