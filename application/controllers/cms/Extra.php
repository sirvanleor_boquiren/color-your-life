<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is for Special Admin :)
 * Some cheat functions
 */
class Extra extends CI_Controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->model("user_model");
        $this->load->model("extra_model");
        $this->load->library("pagination");
    }

    public function wrapper($body, $data = NULL)
    {
    	if (check_login($this->session)) {
    		$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
    	}
    	else {
    		redirect(base_url());
    	}
    }

    public function index($id)
    {
    	$result = $this->user_model->get($id);
    	$data['details'] = $result;

    	$data['paynamics_payments'] = $this->extra_model->getPaynamicsPayments($id);
    	$data['privileges'] = $this->extra_model->getPrivileges($id);
        $data['referrals'] = $this->extra_model->getReferralCount($id);
        $data['color_bonus'] = $this->user_model->getBonus($id);

		$this->wrapper("zxc_extra", $data);
    }

    public function paynamics_request_status_change($user_id, $request_id, $status)
    {
    	$data['status'] = $status;
    	$res = $this->extra_model->updatePaynamicsRequests($request_id, $data);
    	if ($res) {
    		$return['alert_class'] = "alert-success";
    		$return['alert_msg'] = "Paid! :)";
    	}
    	else {
    		$return['alert_msg'] = "Failed to change! :(";
    		$return['alert_class'] = "alert-block alert-danger";
    	}
    	$msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
    	$this->session->set_flashdata($msg_data);
    	redirect(base_url('cms/extra/index/' . $user_id));
    }

    public function update_referrals($user_id)
    {
        $res = $this->user_model->updateUser($user_id, $_POST);
        if ($res) {
            $return['alert_class'] = "alert-success";
            $return['alert_msg'] = "Referrals updated! :)";
        }
        else {
            $return['alert_class'] = "alert-block alert-danger";
            $return['alert_msg'] = "Failed to update referrals";
        }
        $msg_data = array("alert_msg" => $return['alert_msg'], "alert_class" => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url("cms/extra/index/" . $user_id . "/#referrals"));
    }

    public function update_points($user_id)
    {
        $res = $this->user_model->updateUser($user_id, $_POST);
        if ($res) {
            $return['alert_class'] = "alert-success";
            $return['alert_msg'] = "Points updated! :)";
        }
        else {
            $return['alert_class'] = "alert-block alert-danger";
            $return['alert_msg'] = "Failed to update points";
        }
        $msg_data = array("alert_msg" => $return['alert_msg'], "alert_class" => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url("cms/extra/index/" . $user_id . "/#color_points"));
    }

    public function update_bonus($user_id)
    {
        $res = $this->extra_model->updateBonus($user_id, $_POST);
        if ($res) {
            $return['alert_class'] = "alert-success";
            $return['alert_msg'] = "Points updated! :)";
        }
        else {
            $return['alert_class'] = "alert-block alert-danger";
            $return['alert_msg'] = "Failed to update points";
        }
        $msg_data = array("alert_msg" => $return['alert_msg'], "alert_class" => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url("cms/extra/index/" . $user_id . "/#color_bonus"));
    }

    public function update_privileges($user_id)
    {
        $res = $this->extra_model->updatePrivileges($user_id, $_POST);
        if ($res) {
            $return['alert_class'] = "alert-success";
            $return['alert_msg'] = "Privileges updated! :)";
        }
        else {
            $return['alert_class'] = "alert-block alert-danger";
            $return['alert_msg'] = "Failed to update privileges";
        }
        $msg_data = array("alert_msg" => $return['alert_msg'], "alert_class" => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url("cms/extra/index/" . $user_id . "/#privileges"));
    }


}