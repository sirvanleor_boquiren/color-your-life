<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->model("user_model");
        $this->load->model("ajax_model");
        $this->load->model("plans_model");
        $this->load->library("pagination");
    }

    public function wrapper($body, $data = NULL)
    {
    	if (check_login($this->session)) {
    		$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
    	}
    	else {
    		redirect(base_url());
    	}
    }

    public function index()
    {
        $search_keyword = @$_GET['search_keyword'];
        
        if ($search_keyword) {
            $res = $this->user_model->allRaw($search_keyword); # get all searched app users
            $total_data = count($this->user_model->all($search_keyword));
            $data['search_keyword'] = $search_keyword;
        }
        else {
            $res = $this->user_model->allRaw(); # get all app users
            $total_data = count($this->user_model->all());
        }
        
    	$plans = $this->plans_model->all();
    	foreach ($plans as $plan) {
    		$arr_plans[$plan->membership_plan_id] = $plan->name;
    	}

    	# Init Pagination
    	$page_conf['base_url'] = base_url("cms/users");
    	$page_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
    	$page_conf['total_rows'] = $total_data;
    	$page_conf['per_page'] = 10;
        $page_conf['page_query_string'] = TRUE;
        $page_conf['use_page_numbers'] = TRUE;
        $page_conf['query_string_segment'] = "page";
    	$this->pagination->initialize($page_conf);
    	# / Init Pagination
    	
    	$data['pagination'] = $this->pagination->create_links();
    	$data['res'] = $res;
    	$data['plans'] = $arr_plans;
    	$data['edit_link'] = "cms/users/edit";
    	$this->wrapper("user_management", $data); 
    }

    public function details($id)
    {
    	$result = $this->user_model->get($id);
    	$data['details'] = $result;
    	$data['bonus'] = $this->user_model->getBonus($id);
    	$this->wrapper("user_details", $data);
    }

    /**
     * first name, last name, username, email
     */
    public function edit()
    {
    	$post = $this->input->post();
    	$msg = array();

    	# Check if username exists in the database
    	$usernameExists = $this->ajax_model->checkExists2($post, "app_users", "user_id", "username", "user_id");
    	if ($usernameExists) {
    		$msg[] = "Username already exists. Please enter a new valid username.";
    		$return['alert_class'] = "alert-block alert-danger";
    	}

    	# Check if email exists in the database
    	$emailExists = $this->ajax_model->checkExists2($post, "app_users", "user_id", "email", "user_id");
    	if ($emailExists) {
    		$msg[] = "Email already exists. Please enter a new valid email.";
    		$return['alert_class'] = "alert-block alert-danger";
    	}

    	# if empty, no error
    	if (empty($msg)) {
    		$res = $this->user_model->updateUser($post['user_id'], $post);
    		if (!$res) {
    			$msg[] = "Something went wrong when updating the admin. Please try again or contact system administrator.";
    			$return['alert_class'] = "alert-block alert-danger";
    		}
    		else {
    			$msg[] = "User successfully updated.";
    			$return['alert_class'] = "alert-success";
    		}
    	}
    	$return['alert_msg'] = implode("<br>", $msg);
    	$msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
    	$this->session->set_flashdata($msg_data);
    	redirect(base_url('cms/users'));
    }

    /**
     * For Super Admin only
     */
    public function delete()
    {

    }

    public function deactivate($user_id)
	{
		$this->user_model->changeStatus($user_id, 0);
		$msg_data = array(
			'alert_msg' => 'User Deactivated',
			'alert_class' => 'alert-success');
		$this->session->set_flashdata($msg_data);
		redirect('cms/users');
	}

	public function activate($user_id)
	{
		$this->user_model->changeStatus($user_id, 1);
		$msg_data = array(
			'alert_msg' => 'User Activated',
			'alert_class' => 'alert-success');
		$this->session->set_flashdata($msg_data);
		redirect('cms/users');
	}

	public function posts($user_id)
	{
		var_dump("Posts"); die();
	}

}