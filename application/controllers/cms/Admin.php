<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->library("pagination");
        $this->load->model("admin_model");
    }

    public function wrapper($body, $data = NULL)
    {
    	if (check_login($this->session)) {
    		$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
    	}
    	else {
    		redirect(base_url());
    	}
    }

    public function index()
    {
        if ($this->session->userdata['type'] == "Super Admin" || $this->session->userdata['type'] == "Special Admin") {
            $data = array();
            $data['all_users'] = $this->admin_model->getAllAdmin();
            $data['table_header'] = "Admin Management";
            $data['add_link'] = "cms/admin/add";
            $data['edit_link'] = "cms/admin/edit";
            $data['change_pass_link'] = "cms/admin/changePassword";

            # Get Pagination
            $pag_conf['base_url'] = base_url()."users/admin";
            $pag_conf['reuse_query_string'] = TRUE; # maintain get varibles if any
            $pag_conf['total_rows'] = $this->admin_model->getTotalAdmin();
            $pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
            $this->pagination->initialize($pag_conf);
            $data["pagination"] = $this->pagination->create_links();
            $this->wrapper("admin_management", $data);
        }
        else {
            redirect(base_url());
        }
    }

    public function add()
    {
        $return = $this->admin_model->addAdmin($this->input->post());
        $msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url('cms/admin'));
    }

    public function edit()
    {
        $post = $this->input->post();
        $return = $this->admin_model->editAdmin($post);
        $msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url('cms/admin'));
    }

    public function changePassword()
    {
        $post = $this->input->post();
        $return = $this->admin_model->changePassword($post);
        $msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
        $this->session->set_flashdata($msg_data);
        redirect(base_url('cms/admin'));
    }

}