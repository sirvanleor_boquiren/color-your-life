<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paynamics_beta extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->model('plans_model');
    }

    public function index()
    {

    }

    public function response()
    {
    	echo "Payment Success";
    }

    public function notification_debug()
    {
        $post['paymentresponse'] = "";
    }

    public function notification()
    {
    	try {
            $post = $this->input->post();
            if (@$post['paymentresponse']) {
                $body = $post['paymentresponse']; // initial data
                $base64 = str_replace(" ", "+", $body);
                $body = base64_decode($base64); // actual xml
                $data = simplexml_load_string($body); // readable decoded data

                $forSign = $data->application->merchantid . $data->application->request_id . $data->application->response_id . $data->responseStatus->response_code . $data->responseStatus->response_message . $data->responseStatus->response_advise . $data->application->timestamp . $data->application->rebill_id;
                $cert = PAYNAMICS_MERCHANT_KEY; //merchantkey
                $_sign = hash("sha512", $forSign . $cert);

                //check if signature verified and equal
                if ($data->application->signature == $_sign) {
                    $payment_data['requestid'] = base64_encode($data->application->request_id);
                    $payment_data['responseid'] = base64_encode($data->application->response_id);
                    $payment_data['payload'] = serialize($post);
                    // check if successful payment
                    if ($data->responseStatus->response_code == 'GR001' || $data->responseStatus->response_code == 'GR002') {
                        $payment_data['status'] = "paid";
                    }
                    // check if pending payment
                    else if ($data->responseStatus->response_code == 'GR033') {
                        $payment_data['status'] = "pending";
                    }
                    // check if payment was cancelled
                    else if ($data->responseStatus->response_code == 'GR053') {
                        $payment_data['status'] = "cancelled";
                    } 
                    //check if failed payment
                    else {
                        $payment_data['status'] = "failed";
                    }

                    $data_update = $this->payment_model->updatePaynamicsRequest($payment_data);
                    // send_email($this, "jcandres@myoptimind.com", "Data Update Result", "Payment Data: " . serialize($payment_data) . "<br> Is data updated? " . (string)$data_update);

                }
                else {
                    send_email($this, "jcandres@myoptimind.com", "Signatures don't match.", "Signatures don't match.");
                }
            }
            else {
                send_email($this, "jcandres@myoptimind.com", "No response data.", "No response data.");
            }
    	}
    	catch (Exception $ex) {
    		echo $ex->getMessage();
    	}
    }

    public function res_add_ons($user_id, $payment_logs_id)
    {
    	if (@$payment_logs_id && $_GET) {
    		$data['user_id'] = $user_id;
    		$data['payment_logs_id'] = $payment_logs_id;
    		$data['requestid'] = $_GET['requestid'];
    		$data['responseid'] = $_GET['responseid'];
    		$data['apply_now'] = 1;
    		$last_id = $this->payment_model->insertPaynamicsRequest($data);

    		redirect("payment/success/?paynamics_status=pending");
    	}
    	else {
    		echo "Invalid Request";
    	}
    }

    public function res($user_id, $membership_plan_id)
    {
    	if (@$membership_plan_id && $_GET) {
    		$data['user_id'] = $user_id;
    		$data['membership_plan_id'] = $membership_plan_id;
	    	$data['requestid'] = $_GET['requestid'];
	    	$data['responseid'] = $_GET['responseid'];
	    	$last_id = $this->payment_model->insertPaynamicsRequest($data);

	    	# if there's no current active plan
	    	# apply a free membership plan
	    	if (!$this->plans_model->getCurrentPlan($user_id)) {
	    		# update paynamics request flag to apply membership immediately if paid
	    		$data_request['requestid'] = $_GET['requestid'];
	    		$data_request['responseid'] = $_GET['responseid'];
	    		$data_request['apply_now'] = 1;
	    		$this->payment_model->updatePaynamicsRequest($data_request);
	    		$data_plan['user_id'] = $user_id;
	    		$data_plan['membership_plan_id'] = 1;
	    		$this->plans_model->verifyMembership($data_plan);
	    	}
	    	
	    	redirect("membership/success/?paynamics_status=pending");
    	}
    	else {
    		echo "Invalid Request";
    	}
    }

    public function not()
    {

    }

    public function cancel()
    {
    	echo "Payment Cancelled";
    }

    public function payment($status)
    {
    	echo "Payment " . $status;
    }

}