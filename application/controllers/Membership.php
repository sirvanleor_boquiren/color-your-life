<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model("plans_model", 'model');
        $this->load->model("user_model");
        $this->load->model("payment_model");
    }

    public function index($payment_logs_id, $device = null)
    {
        $logs_detail = $this->payment_model->getLogsDetail($payment_logs_id);
        if (@$logs_detail && @$logs_detail->status == "pending") {
            $user_id = $logs_detail->user_id;
            $membership_id = $logs_detail->membership_id;

            $data['user_id'] = $user_id;
            $data['membership_id'] = $membership_id;
            $data['membership_plan'] = $this->model->getPlan($membership_id);
            $data['user'] = $this->user_model->getUser($user_id);
            // $equivalent_json = file_get_contents("https://api.exchangeratesapi.io/latest?base=USD", false, stream_context_create(array('http' => array('max_redirects' => 101))));
            // $equivalent_json = json_decode($equivalent_json);
            
            $equivalent_json = new stdClass();
            $equivalent_json->rates = new stdClass();
            $equivalent_json->rates->HKD = 7;
            $equivalent_json->rates->PHP = 52;
            $equivalent_json->rates->IDR = 14196;

            ## HKD ##
            // if ($equivalent_json->rates->HKD) {
            //     $data['membership_plan']->hkd_rate = round($data['membership_plan']->rate * $equivalent_json->rates->HKD, 2);
            // }
            switch ($data['membership_plan']->membership_plan_id) {
                case 2:
                    $data['membership_plan']->hkd_rate = "80.00";
                    break;
                case 3:
                    $data['membership_plan']->hkd_rate = "80.00";
                    break;
                case 4:
                    $data['membership_plan']->hkd_rate = "400.00";
                    break;
                
                default:
                    $data['membership_plan']->hkd_rate = "400.00";
                    break;
            }
            ## HKD ##
            
            ## PHP ##
                // if ($equivalent_json->rates->PHP) {
                //     $data['membership_plan']->php_rate = (string)round($data['membership_plan']->rate * $equivalent_json->rates->PHP, 2);
                // }
            switch ($data['membership_plan']->membership_plan_id) {
                case 2:
                    $data['membership_plan']->php_rate = "500";
                    break;
                case 3:
                    $data['membership_plan']->php_rate = "500";
                    break;
                case 4:
                    $data['membership_plan']->php_rate = "2500";
                    break;
                
                default:
                    $data['membership_plan']->php_rate = "2500";
                    break;
            }
            ## PHP ##
            
            ## For Alipay ##
            $s['payment_type'] = "alipay";
            $s['amount'] = $data['membership_plan']->hkd_rate * 100;
            $s['currency'] = "hkd";
            $s['email'] = $data['user']->email;
            $s['membership_plan_name'] = $data['membership_plan']->name;
            $s['membership_plan_id'] = $data['membership_plan']->membership_plan_id;
            $s['user_id'] = $user_id;
            $s['payment_logs_id'] = $payment_logs_id;
            $alipay_source = $this->payment_model->createSourceForMembership($s);
            $data['alipay_url'] = $alipay_source->redirect->url;
            ## For Alipay ##
            
            ## For WeChat Pay ##
            // $s['payment_type'] = "wechat";
            // $wechat_source = $this->payment_model->createSourceForMembership_wechat($s);
            // $wechat_temp['url'] = $wechat_source->wechat->qr_code_url;
            // $wechat_temp['source'] = $wechat_source->id;
            // $wechat_temp['amount'] = $data['membership_plan']->hkd_rate * 100;
            // $wechat_temp['user_id'] = $user_id;
            // $wechat_temp['membership_plan_name'] = $data['membership_plan']->name;
            // $wechat_temp['membership_plan_id'] = $data['membership_plan']->membership_plan_id;
            // $wechat_temp['email'] = $data['user']->email;
            // $wechat_temp['payment_logs_id'] = $payment_logs_id;
            // $data['wechat'] = $wechat_temp;
            ## For WeChat Pay ##        
            
            ## IDR ##
            // if (@$idr_equivalent_json->rates->IDR) {
            //     $data['membership_plan']->idr_rate = round($data['membership_plan']->rate * $idr_equivalent_json->rates->IDR, 2);
            // }
            switch ($data['membership_plan']->membership_plan_id) {
                case 2:
                    $data['membership_plan']->idr_rate = "170000.00";
                    break;
                case 3:
                    $data['membership_plan']->idr_rate = "140000.00";
                    break;
                case 4:
                    $data['membership_plan']->idr_rate = "700000.00";
                    break;
                
                default:
                    $data['membership_plan']->idr_rate = "700000.00";
                    break;
            }
            ## IDR ##
            
            $data['sessionid'] = $logs_detail->sessionid;
            if ($device == "ios") {
                $this->load->view('web_view/payment_ios', $data);
            }
            elseif ($device == "android") {
                $this->load->view('web_view/payment_android', $data);
            }
            else {
                $this->load->view('web_view/payment', $data);
            }
        }
        else {
            $this->load->view('web_view/payment_invalid');
        }
    }

    public function notification()
    {
        $this->load->view('web_view/notification');
    }

    /**
     * For auto debit
     * - not yet applied but working
     */
    public function renewal_charge()
    {
        require_once('stripe/init.php');
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        $charge = \Stripe\Charge::create([
            'amount' => 10000, // $15.00 this time
            'currency' => 'hkd',
            'customer' => "cus_EYw3mR7VDg3Oby", // Previously stored, then retrieved
        ]);

        var_dump($charge); die();
    }

    /**
     * For Alipay and wechat pay
     */
    public function charge_membership()
    {
        require_once('stripe/init.php');
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_GET['source'];
        $email = $_GET['email'];

        try {
            $customer = \Stripe\Customer::create([
                'source' => $token,
                'email' => $email,
            ]);

            $this->user_model->updateUser($_GET['user_id'], array('stripe_customer_id' => $customer->id));

            $charge = \Stripe\Charge::create([
                'amount' => $_GET['amount'],
                'currency' => $_GET['currency'],
                'description' => 'Payment - (' . $_GET['membership_plan_name'] . ' Plan) (' . $_GET['user_id'] . ')',
                'customer' => $customer->id,
                'receipt_email' => $email,
            ]);
        } catch (Exception $e) {
            redirect("membership/fail");
        }

        if ($charge->outcome->seller_message == 'Payment complete.') {
            $res = $this->model->applyMembership($_GET['user_id'], $_GET['membership_plan_id']);
            if ($res) {
                # change status in payment logs
                $data['status'] = "applied";
                $this->payment_model->updatePaymentLogs($_GET['payment_logs_id'], $data);
                redirect("membership/success");
            }
            else {
                redirect("membership/fail");
            }
        }
        else {
            redirect("membership/fail");
        }
    }

    public function charge()
    {
        require_once('stripe/init.php');
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $email = $_POST['stripeEmail'];

        $customer = \Stripe\Customer::create([
            'source' => $token,
            'email' => $email,
        ]);

        $this->user_model->updateUser($_POST['user_id'], array('stripe_customer_id' => $customer->id));

        $charge = \Stripe\Charge::create([
            'amount' => $_POST['amount'],
            'currency' => $_POST['currency'],
            'description' => 'Payment - (' . $_POST['plan_name'] . ' Plan) (' . $_POST['user_id'] . ')',
            'customer' => $customer->id,
            'receipt_email' => $email,
        ]);

        if ($charge->outcome->seller_message == 'Payment complete.') {
            $res = $this->model->applyMembership($_POST['user_id'], $_POST['membership_id']);
            if ($res) {
                redirect("membership/success");
            }
            else {
                redirect("membership/fail");
            }
        }
        else {
            redirect("membership/fail");
        }
    }
    
    public function success()
    {
        if ($paynamics_status = $this->input->get('paynamics_status')) {
            echo "Payment " . $paynamics_status . ".<br><br>";
            echo "You are subscribed to Free Membership until payment is done(For first time user only). Thank you!";
        }
        else {
            $this->load->view('web_view/payment_success');
        }
    }

    public function success_doku()
    {
        // $this->load->view('web_view/payment_success');
    }

    public function notify_url()
    {
        if ($_POST['RESULTMSG'] == "SUCCESS") {
            $data['payload'] = serialize($_POST);
            $data['status'] = "applied";
            $this->payment_model->updatesPaymentlogsBySessionid($_POST['SESSIONID'], $data);
        }
        else {
            $data['payload'] = serialize($_POST);
            $data['status'] = "failed";
            $this->payment_model->updatePaymentLogs($_POST['SESSIONID'], $data);
        }
    }

    public function identify_url()
    {
    }

    public function fail()
    {
        $this->load->view('web_view/fail');
    }

    public function plans()
    {
        if (isset($this->session->userdata['logged_in'])){
            $this->load->view('membership_plans');
        }
        else {
            $this->load->view('login');
        }
    }
}