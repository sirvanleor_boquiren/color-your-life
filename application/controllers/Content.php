<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model("admin_model");
        $this->load->model("user_model", 'u_model');
    }

    public function wrapper($body, $data = NULL) 
	{
		// if (check_login($this->session)) 
		// {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		// }
		// else {
			// redirect(base_url());
		// }
	}

	public function About()
	{
		// $data["total_users"] = $this->accounts_model->getTotalUsers("0"); # 0 - for non admin/app users
		// var_dump($data);
		$page_data['summernote_wysiwyg'] = true;
		$page_data['header_title'] = "About Us Content";
		$this->wrapper("aboutus_management", $page_data);
	}

	public function countperCountry()
	{
		$res = $this->u_model->getCountUserperCountry();
		header('Content-Type: application/json');
		echo json_encode($res);
	}

}

?>