<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->model('plans_model');
    }

    /**
     * For Alipay and wechat pay
     */
    public function charge_add_ons()
    {
        require_once('stripe/init.php');
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_GET['source'];
        $email = $_GET['email'];

        try {
            $charge = \Stripe\Charge::create([
                'amount' => $_GET['amount'],
                'currency' => $_GET['currency'],
                'description' => "Add-ons(user_id:" . $_GET['user_id'] . ", type:" . $_GET['type'] . ", id:" . $_GET['payment_logs_id'] . ")",
                'source' => $token,
            ]);   
        } catch (Exception $e) {
            redirect("payment/fail");
        }

        if ($charge->outcome->seller_message == "Payment complete.") {
            # add count in user_membership_logs
            $this->plans_model->updateAddonsPlan($_GET['payment_logs_id']);

            # update status in logs
            $this->payment_model->updatePaymentLogs($_GET['payment_logs_id'], array('status' => 'paid'));

            redirect("payment/success");
        }
        else {
            redirect("payment/fail");
        }
    }

    public function charge()
    {
    	require_once('stripe/init.php');
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $email = $_POST['stripeEmail'];

        $charge = \Stripe\Charge::create([
            'amount' => $_POST['amount'],
            'currency' => $_POST['currency'],
            'description' => "Add-ons(user_id:" . $_POST['user_id'] . ", type:" . $_POST['type'] . ", id:" . $_POST['payment_logs_id'] . ")",
            'source' => $token,
        ]);

        if ($charge->outcome->seller_message == 'Payment complete.') {
        	# add count in user_membership_logs
        	$this->plans_model->updateAddonsPlan($_POST['payment_logs_id']);
        	
        	# update status in logs
        	$this->payment_model->updatePaymentLogs($_POST['payment_logs_id'], array('status' => 'paid'));
        	redirect("payment/success");
        }
        else {
        	redirect("payment/fail");
        }
    }

    public function add_ons($type, $payment_logs_id, $device = null)
    {
        $equivalent_json = new stdClass();
        $equivalent_json->rates = new stdClass();
        $equivalent_json->rates->HKD = 7;
        $equivalent_json->rates->PHP = 52;
        $equivalent_json->rates->IDR = 14196;

    	if ($type == "ads" || $type == "videos") {
    		$price = 5;
    	}
    	else {
    		$price = 10;
    	}
    	
    	$logs_detail = $this->payment_model->getLogsDetail($payment_logs_id);
    	if ($logs_detail && $logs_detail->status == 'pending' && $logs_detail->type == $type) {
    		$data['amount'] = $logs_detail->quantity * $price;
            $data['user'] = $this->user_model->getUser($logs_detail->user_id);

            ## HKD ##
            // if ($equivalent_json->rates->HKD) {
            //     $data['amount_hk'] = round( ($logs_detail->quantity * $price) * $equivalent_json->rates->HKD , 2);
            // }
            switch ($type) {
                case 'ads':
                    $data['amount_hk'] = 40;
                    break;
                case 'videos':
                    $data['amount_hk'] = 40;
                    break;
                case 'webinars':
                    $data['amount_hk'] = 80;
                    break;
                case 'group_messages':
                    $data['amount_hk'] = 80;
                    break;
                
                default:
                    $data['amount_hk'] = 80;
                    break;
            }
            ## HKD ##
            

            ## IDR ##
            switch ($type) {
                case 'ads':
                    $data['amount_idr'] = "75000.00";
                    break;
                case 'videos':
                    $data['amount_idr'] = "75000.00";
                    break;
                case 'webinars':
                    $data['amount_idr'] = "145000.00";
                    break;
                case 'group_messages':
                    $data['amount_idr'] = "145000.00";
                    break;
                
                default:
                    $data['amount_idr'] = "145000.00";
                    break;
            }
            ## IDR ##
            

            ## PHP ##
            // if ($equivalent_json->rates->PHP) {
            //     $data['amount_php'] = (string)round( ($logs_detail->quantity * $price) * $equivalent_json->rates->PHP , 2);
            // }
            switch ($type) {
                case 'ads':
                    $data['amount_php'] = "250";
                    break;
                case 'videos':
                    $data['amount_php'] = "250";
                    break;
                case 'webinars':
                    $data['amount_php'] = "500";
                    break;
                case 'group_messages':
                    $data['amount_php'] = "500";
                    break;
                
                default:
                    $data['amount_php'] = "500";
                    break;
            }
            ## PHP ##
            
            ## For Alipay ##
            $s['payment_type'] = "alipay";
            $s['amount'] = $data['amount_hk'] * 100;
            $s['currency'] = "hkd";
            $s['email'] = $data['user']->email;
            $s['type'] = $logs_detail->type;
            $s['payment_logs_id'] = $logs_detail->payment_logs_id;
            $s['user_id'] = $logs_detail->user_id;
            $alipay_source = $this->payment_model->createSourceForAddOns($s);
            $data['alipay_url'] = $alipay_source->redirect->url;
            ## For Alipay ##
            
            ## For WeChat Pay ##
            // $s['payment_type'] = "wechat";
            // $wechat_source = $this->payment_model->createSourceForAddOns($s);
            // $wechat_temp['url'] = $wechat_source->wechat->qr_code_url;
            // $wechat_temp['source'] = $wechat_source->id;
            // $wechat_temp['amount'] = $data['amount_hk'] * 100;
            // $wechat_temp['user_id'] = $logs_detail->user_id;
            // $wechat_temp['type'] = $logs_detail->type;
            // $wechat_temp['payment_logs_id'] = $logs_detail->payment_logs_id;
            // $wechat_temp['email'] = $data['user']->email;
            // $data['wechat'] = $wechat_temp;
            ## For WeChat Pay ##

    		$data['payment_logs_id'] = $logs_detail->payment_logs_id;
    		$data['user_id'] = $logs_detail->user_id;
    		$data['type'] = $logs_detail->type;
            $data['sessionid'] = $logs_detail->sessionid;
            
            if ($device == "ios") {
                $this->load->view('web_view/add_ons_payment_ios', $data);
            }
            elseif ($device == "android") {
                $this->load->view('web_view/add_ons_payment_android', $data);
            }
            else {
                $this->load->view('web_view/add_ons_payment', $data);
            }
    	}
    	else {
    		$this->load->view('web_view/payment_invalid');
    	}
    }

    public function success()
    {
    	$this->load->view('web_view/payment_success');
    }

    public function success_doku()
    {
    	$this->load->view('web_view/payment_success');
    }

    public function notify_url()
    {
        if ($_POST['RESULTMSG'] == "SUCCESS") {
            $data['payload'] = serialize($_POST);
            $data['status'] = "applied";
            $this->payment_model->updatesPaymentlogsBySessionid($_POST['SESSIONID'], $data);
        }
        else {
            $data['payload'] = serialize($_POST);
            $data['status'] = "failed";
            $this->payment_model->updatePaymentLogs($_POST['SESSIONID'], $data);
        }
    }

    public function identify_url()
    {
    	
    }

    public function fail()
    {
    	$this->load->view('web_view/payment_fail');
    }
}