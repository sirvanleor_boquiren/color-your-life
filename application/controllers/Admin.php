<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('admin_model');
    }

    public function wrapper($body, $data = NULL) 
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		}
		else {
			redirect(base_url());
		}
	}

	public function index()
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->wrapper("dashboard");
		}
		else {
			$this->load->view('login');
		}
	}

	public function login()
	{
		$post = $this->input->post();
		$data['admin'] = $this->admin_model->login($post);
		// $data['admin'] = true;
		if ($data['admin']) {
			if ($data['admin']->status == 1) {
				$this->session->userdata['logged_in'] = 1;
				$this->session->userdata['admin_id'] = $data['admin']->admin_id;
				$this->session->userdata['username'] = $data['admin']->username;
				$this->session->userdata['email'] = $data['admin']->email;
				$this->session->userdata['fname'] = $data['admin']->fname;
				$this->session->userdata['lname'] = $data['admin']->lname;
				$this->session->userdata['status'] = $data['admin']->status;
				$this->session->userdata['contact'] = $data['admin']->contact;
				$this->session->userdata['type'] = $data['admin']->type;
				$this->session->userdata['created_at'] = $data['admin']->created_at;
				redirect(base_url());
			}
			else {
				$msg_data = array('alert_msg' => 'Account Deactivated', 'alert_color' => 'red');
				$this->session->set_flashdata($msg_data);
				$this->load->view('login');
			}
		}
		else {
			$msg_data = array('alert_msg' => 'Incorrect Username/Password', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
			$this->load->view('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

}