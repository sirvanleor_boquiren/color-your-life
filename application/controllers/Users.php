<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model("admin_model");
        $this->load->model("user_model");
    }

    public function wrapper($body, $data = NULL)
	{
		// if (check_login($this->session))
		// {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			$this->load->view('partials/footer');
		// }
		// else {
			// redirect(base_url());
		// }
	}

	public function index()
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->wrapper("dashboard");
		}
		else {
			$this->load->view('login');
		}
	}

	public function admin()
	{
		if (isset($this->session->userdata['logged_in'])) {
			$data = array();
			$data['all_users'] = $this->admin_model->getAllAdmin();
			$data['table_header'] = "Admin Management";
			$data['add_link'] = "users/addAdmin";
			$data['edit_link'] = "users/editAdmin";
			$data['change_pass_link'] = "users/changePassword";
			# Get Pagination
			$pag_conf['base_url'] = base_url()."users/admin";
			$pag_conf['reuse_query_string'] = TRUE;	# maintain get varibles if any
			$pag_conf['total_rows'] = $this->admin_model->getTotalAdmin();
			$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
			$this->pagination->initialize($pag_conf);
			$data["pagination"] = $this->pagination->create_links();
			$this->wrapper("admin_management", $data);
		}
		else {
			$this->load->view('login');
		}
	}

	# POST Functions
	public function addAdmin()
	{
		$return = $this->admin_model->addAdmin($this->input->post());
		$msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
		$this->session->set_flashdata($msg_data);
		redirect(base_url('users/admin'));
	}

	public function editAdmin()
	{
		$post = $this->input->post();
		$return = $this->admin_model->editAdmin($post);
		$msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $resturn['alert_class']);
		$this->session->set_flashdata($msg_data);
		redirect(base_url('users/admin'));
	}

	public function changePassword()
	{
		$post = $this->input->post();
		$return = $this->admin_model->changePassword($post);
		$msg_data = array('alert_msg' => $return['alert_msg'], 'alert_class' => $return['alert_class']);
		$this->session->set_flashdata($msg_data);
		redirect(base_url('users/admin'));
	}

	public function deactivateAdmin($admin_id)
	{
		$this->admin_model->changeStatus($admin_id, 0);
		$this->session->set_flashdata('status_change', 'User deactivated');
		redirect('users/admin');
	}

	public function activateAdmin($admin_id)
	{
		$this->admin_model->changeStatus($admin_id, 1);
		$this->session->set_flashdata('status_change', 'User activated');
		redirect('users/admin');
	}

	########################

	public function app_users()
	{
		if (isset($this->session->userdata['logged_in'])) {
			$res = $this->user_model->allRaw();  # get all app users

			# Init Pagination
			$pag_conf['base_url'] = base_url("users/app_users");
			$pag_conf['reuse_query_string'] = TRUE;	# maintain get varibles if any
			$pag_conf['total_rows'] = count($res);
			$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
			$this->pagination->initialize($pag_conf);
			# / Init Pagination

			$data["pagination"] = $this->pagination->create_links();
			$data['res'] = $res;

			$this->wrapper('user_management', $data);
		}
		else {
			$this->load->view('login');
		}
	}

	public function deactivate($user_id)
	{
		$this->user_model->changeStatus($user_id, 0);
		$msg_data = array(
			'alert_msg' => 'User Deactivated',
			'alert_class' => 'alert-success');
		$this->session->set_flashdata($msg_data);
		redirect('users/app_users');
	}

	public function activate($user_id)
	{
		$this->user_model->changeStatus($user_id, 1);
		$msg_data = array(
			'alert_msg' => 'User Activated',
			'alert_class' => 'alert-success');
		$this->session->set_flashdata($msg_data);
		redirect('users/app_users');
	}

	public function reset()
    {
		$this->load->view('reset');
    }

    public function validate_reset()
    {	
    	$input = $this->input->post();
    	$msg_data = array(
				'alert_msg' => 'Something went wrong. Please try again', 
				'alert_class' => 'alert-block alert-danger');

    	if ($input['forgot-token'] != '') {
	    	if (($user = $this->user_model->getUserby('forgot_token', $input['forgot-token']))) {
	    		$msg_data = array(
					'alert_msg' => 'Password changed successfully', 
					'alert_class' => 'alert-success');

	    		if (!$this->user_model->updatePass($user->user_id, $input['password'])) {
	    			$msg_data = array(
						'alert_msg' => 'Something went wrong. Please try again', 
						'alert_class' => 'alert-block alert-danger');
	    		}else{
	    			$this->user_model->clearToken($user->user_id);
	    		}

	    	}
    	}

        $this->session->set_flashdata($msg_data);
		redirect('users/reset/'.$input['forgot-token']);

    }

}

?>
