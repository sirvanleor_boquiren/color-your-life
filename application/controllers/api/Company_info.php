<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Company_info extends Crud_controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("company_info_model", "model");
    }

    function privacy_policy_get()
    {
      $r_status = "200";
      $res = $this->model->get("privacy_policy");
      if ($res->content == "") {
        $res = [];
      }
      $msg = "Successfully found data";
      $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
      $this->response($r_return, $r_status);
    }

    function about_us_get()
    {
      $r_status = "200";
      $res = $this->model->get("about_us");
      if ($res && $res['about_the_company'] != "") {
        $res['company_logo'] = base_url("frontend/img/cyl_logo.png");
      }
      else {
        $res = [];
      }
      $msg = "Successfully found data";
      $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
      $this->response($r_return, $r_status);
    }

    function terms_conditions_get()
    {
      $r_status = "200";
      $res = $this->model->get("terms_conditions");
      if ($res->content == "") {
        $res = [];
      }
      $msg = "Successfully found data";
      $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
      $this->response($r_return, $r_status);
    }

    function contact_us_get()
    {
      $r_status = "200";
      $res = $this->model->get("contact_us");
      $msg = "Successfully found data";
      $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
      $this->response($r_return, $r_status);
    }

}
