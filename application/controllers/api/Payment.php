<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

class Payment extends Crud_controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('payment_model', 'model');
        $this->load->model('plans_model');
        $this->load->model('user_model');
    }

    public function add_membership_plan_post()
    {
        $post = $this->post();
        $payment_logs_details = $this->model->getLogsDetail($post['payment_logs_id']);
        if ($payment_logs_details->status == "applied") {
            $res = [];
            $msg = "Membership already applied";
            $status = 400;
        }
        elseif ($payment_logs_details->membership_id != 0) {
            $applied = $this->plans_model->applyMembership($payment_logs_details->user_id, $payment_logs_details->membership_id);
            if ($applied) {
                $data['status'] = "applied";
                $this->model->updatePaymentLogs($post['payment_logs_id'], $data);
                $res = $this->user_model->getPlanInfo($payment_logs_details->user_id);
                unset($res->password);
                $msg = "Membership successfully applied";
                $status = 200;
            }
            else {
                $res = [];
                $msg = "Failed to apply membership";
                $status = 400;
            }
        }
        else {
            $res = [];
            $msg = "Invalid data";
            $status = 400;
        }
        $this->response(['data' => $res, 'message' => $msg, 'status' => $status], $status);
    }

    public function add_add_ons_post()
    {
        $post = $this->post();
        $payment_logs_details = $this->model->getLogsDetail($post['payment_logs_id']);
        if ($payment_logs_details->status == "applied") {
            $res = [];
            $msg = "Add-ons already applied";
            $status = 400;
        }
        elseif ($payment_logs_details->type != "" && $payment_logs_details->user_membership_plan_id != 0) {
            # add count in user_membership_logs
            $this->plans_model->updateAddonsPlan($post['payment_logs_id']);
            
            # update status in logs
            $data['status'] = "applied";
            $this->model->updatePaymentLogs($post['payment_logs_id'], $data);

            $res = $this->user_model->getPlanInfo($payment_logs_details->user_id);
            unset($res->password);
            $msg = "Add-ons successfully applied";
            $status = 200;
        }
        else {
            $res = [];
            $msg = "Invalid data";
            $status = 400;
        }
        $this->response(['data' => $res, 'message' => $msg, 'status' => $status], $status);
    }

    public function add_ons_post($type)
    {
    	if (in_array($type, ['ads', 'videos', 'webinars', 'group_messages'])) {
    		$post = $this->post();
	    	$current_plan = $this->plans_model->getActivePlan($post['user_id']);
	    	if ( !$current_plan || (@$current_plan->membership_plan_id != 3 && @$current_plan->membership_plan_id != 4) ) {
	    		$msg = "Action is not applicable to this plan.";
	    		$status = 400;
	    		$this->response(['data' => [], 'message' => $msg, 'status' => $status], $status);
	    	}
	    	else {
	    		$data['user_id'] = $post['user_id'];
	    		$data['type'] = $type;
	    		$data['quantity'] = $post['quantity'];
	    		$data['user_membership_plan_id'] = $current_plan->user_membership_plan_id;
	    		$data['status'] = "pending";
                $data['sessionid'] = $this->randomString(20);

	    		$res = $this->model->getPaymentLink($data);
	    		$msg = "Ready for payment";
	    		$status = 200;
	    		$this->response(['data' => $res, 'message' => $msg, 'status' => $status], $status);
	    	}
    	}
    	else {
    		$msg = "Invalid add-ons";
    		$status = 400;
    		$this->response(['data' => [], 'message' => $msg, 'status' => $status], $status);
    	}
    }

    public function check_post()
    {
    	$post = $this->post();
    	$res = $this->payment_model->checkPaynamicsRequests($post['user_id']);
    	if ($res) {
            if ($res->membership_plan_id == 0) {
                # add count in user_membership_logs
                $this->plans_model->updateAddonsPlan($res->payment_logs_id);

                # update status in logs
                $this->model->updatePaymentLogs($res->payment_logs_id, array('status' => 'applied'));

                $data['requestid'] = $res->requestid;
                $data['responseid'] = $res->responseid;
                $data['status'] = "applied";
                $this->payment_model->updatePaynamicsRequest($data);
                $this->response(['data' => $res, 'message' => "Add-ons added", 'status' => 201], 201);
            }
            else {
                if ($this->plans_model->applyMembership($res->user_id, $res->membership_plan_id)) {
                    # update status in logs
                    $this->model->updatePaymentLogs($res->payment_logs_id, array('status' => 'applied'));
                    
                    if ($res->membership_applied == "yes") {
                        $this->plans_model->applyNow($res->user_id);
                    }
                    $data['requestid'] = $res->requestid;
                    $data['responseid'] = $res->responseid;
                    $data['status'] = "applied";
                    $this->payment_model->updatePaynamicsRequest($data);
                    $this->response(['data' => $res, 'message' => "Membership plan added", 'status' => 201], 201);
                }
                else {
                    $this->response(['data' => $res, 'message' => "Failed to add plan", 'status' => 400], 400);
                }
            }
    	}
    	else {
    		$this->response(['data' => [], 'message' => "No data found", 'status' => 404], 404);
    	}
    }

    public function check_pending_payment_get($user_id)
    {
        $where_arr = array("user_id" => $user_id, "status" => "paid");
        $payment_logs = $this->model->getPaymentlogs($where_arr);
        if ($payment_logs) {
            foreach ($payment_logs as $payment) {
                if ($payment->membership_id != 0) {
                    $res = $this->plans_model->applyMembership($payment->user_id, $payment->membership_id);
                    $temp_msg = " and membership plan added";
                }
                else {
                    $res = $this->plans_model->updateAddonsPlan($payment->payment_logs_id);
                    $temp_msg = " and add-ons is applied";
                }
                if ($res) {
                    # change status in payment logs
                    $data['status'] = "applied";
                    $this->model->updatePaymentLogs($payment->payment_logs_id, $data);
                }
                $msg = "Payment done" . $temp_msg;
                $status = 200;
            }
        }
        else {
            $msg = "No data found";
            $status = 404;
        }
        $this->response(['data' => [], 'message' => $msg, 'status' => $status], $status);
    }

    public function check_doku_payment_get($sessionid)
    {
        $res = $this->payment_model->getPaymentlogsBySessionid($sessionid, 'applied');
        if ($res) {
            $msg = "Payment successful.";
            $status = 200;
        }
        else {
            $res = [];
            $msg = "No payments made.";
            $status = 404;
        }
        $this->response(['data' => $res, 'message' => $msg, 'status' => $status], $status);
    }

    function randomString($len = 10)
    {
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}