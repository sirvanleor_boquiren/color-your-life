<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Settings extends Crud_controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("settings_model", 'model');
    }

    public function index_post()
    {
        $post = $this->post();
        if (@$post['user_id']) {
            $user_id = $post['user_id'];
            $res = $this->model->updateSettings($user_id, $post);
            if ($res) {
                $res = $this->model->getSettings($user_id);
                $message = "Settings saved";
            }
            else {
                $res = array();
                $message = "Something went wrong.";    
            }
        }
        else {
            $res = array();
            $message = "Something went wrong.";
        }
        $this->response(['data' => $res, 'message' => $message, 'status' => 200], 200);
    }

    public function user_get($user_id)
    {
        $res = $this->model->getSettings($user_id);
        $message = "Found all data";
        $this->response(['data' => $res, 'message' => $message, 'status' => 200], 200);  
    }

    
}
