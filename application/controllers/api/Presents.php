<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;


class Presents extends Crud_controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('present_model');
        $this->load->model('user_model');
        $this->load->model('present_model','model');
        $this->table = "presents";

    }

    public function create_post()
	{
        $post = $this->input->post();


		if($this->present_model->add($post)){
            $message = "Successfully created.";
            $status = 200;
			if($errors = $this->present_model->uploadImage()){
				$message = $message . ' | ' . $errors;
			}
		}else{
            $post = (object)[];
            $status = 400;
        }
        $this->response(['data' => $post, 'message' => $message, 'status' => $status],$status);
    }

    public function user_get($user_id)
    {
        $user = $this->user_model->get($user_id);
        $type = strtolower(explode(" ", $user->rank_type)[0]); # get the "bronze" from "Bronze Member"
        $data = (object)[];
        $type = array();
        // $data->all = $this->model->getByColor($type);
        //$data->featured = $this->model->getByColor($type,true);
        $data->user_stats = $this->user_model->getUserPresentStats($user_id);

        #all presents
        $allpresents = $this->model->getAllPresents();
        #loop and get all rank
        foreach ($data->user_stats as $key => $value) {
            if (strpos($key, 'Member') !== false) {
                $type[strtolower(trim(substr($key,0,strlen($key)-6)))] = $value;
            }
        }
        
        // loop all presents
        foreach ($allpresents as $key => $value) {
            // loop rank referral count
            foreach ($type as $typekey => $typevalue) {
                // check all present if it is included by same rank and referral count
                 if ($value->type === $typekey && $value->no_of_members <= $typevalue) {
                 $data->all[] = $value;
                     if ($value->is_featured == 1) {
                        $data->featured[] = $value;      
                     }
                 }
            }
            
        }
        
        $status = 200;
        $message = "Got all data";
        // if(!in_array($type,['bronze','silver','platinum','gold', 'white'])){ # remove white?
        //     $data = (object)[];
        //     $message = "Type not recognized";
        //     $status = 400;
        // }


        $this->response(['data' => $data, 'message' => $message, 'status' => $status],$status);
    }

    public function get_by_present_request_get($request_id)
    {
        $this->load->model('present_requests_model');
        $request = $this->present_requests_model->get($request_id);
        $presents = $this->db->select('presents.*')
                            ->from('presents')
                             ->join('request_present','request_present.present_id = presents.present_id')
                             ->where('request_present.request_id',$request_id)
                             ->get()->result();
        $this->response($presents,200);
    }



}
