<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Post_reviews extends Crud_controller {

    #GET api/reviews/posts
    #POST api/reviews/posts
    #GET api/reviews/posts/:id

    public function __construct()
    {
        parent::__construct();
        $this->load->model("post_reviews_model", 'model');
        $this->load->model("firebase_model");
        $this->load->model("posts_model");
        $this->load->model("user_model");

    }

    /**
     * Get all reviews by post
     * @param  [int] $post_id [unique id of post]
     * @return [array] [display all reviews under specific post]
     */
    public function post_get($post_id)
    {
    	$res = $this->model->allByPost($post_id);

      # We attach the username and profile pic to the response
      # Also format the created at
      foreach ($res as $key => $value) {
        $user_id = $value->user_id;

        $user = @$this->user_model->get($user_id);
        $res[$key]->user = $user; #user object

        $res[$key]->created_at_formatted = date('m/d/y | h:i A', strtotime($value->created_at)); #format the date
      }

    	$this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }

    public function index_post()
    {
      $data = $this->input->post();
      if($last_id = $this->model->add($data)){ # Try to add and get the last id
        $res = $this->model->get($last_id); # Get the last entry data
        $post_owner = $this->posts_model->getPostOwner($data['post_id']);
        $post_owner_id = $post_owner->user_id;

        $reviewer_info = $this->user_model->getUser($data['user_id']);
        $reviewer_uname = $reviewer_info->username;
        
        $post_info = $this->posts_model->get($data['post_id'])[0];
        $post_name = $post_info->name;

        $notif_type = 'new_post_review';
        $notification_message = $this->notification_model->storeNotif($post_owner_id, $notif_type, ['post_id' => $data['post_id'], 'reviewer_id' => $data['user_id']]);
        /**
         * Check if Settings if push notif is on
         */
        $notif = $this->settings_model->getSettings($post_owner_id);
        if (@$notif[0]->push_notification) {
          ##### FIREBASE NOTIFICATION BLOCK ####
          $recipients = $this->udi_model->getDeviceIdsByUserId($post_owner_id);
          foreach ($recipients as $to) {
            $firebase_response = $this->firebase_model->sendMultiple($to,
             ['data' => 
                (object)[
                    'id' => $data['post_id'],
                    'type' => 'new_post_review'
                ]
             ],
             ['title' => 'New post review', 'body' => $reviewer_uname . ' wrote a review in your ' . $post_name . ' post']
            );
            log_message('error', $firebase_response);
            log_message('error', $notification_message);
          }
          ##### / FIREBASE NOTIFICATION BLOCK ####
        }

        /**
         * email notification
         */
        if (@$notif[0]->email_notification) {
          $this->load->helper("email_template");
          $recipient = $post_owner->email;
          $subject = $reviewer_info->username . " reviewed your post in Color Your Life";
          $body = reviewed_post_template(['post_name' => $post_info->name, 'reviewer_profile_pic' => $reviewer_info->profile_pic, 'reviewer_fullname' => $reviewer_info->fullname, 'review' => $data['review'], 'fname' => $post_owner->fname]);
          send_email($this, $recipient, $subject, $body);
        }

        $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
        $this->response(['data' => $res, 'message' => 'Successfully created new data', 'status' => 201], 201);
      }else{
        $this->response(['data' => [], 'message' => 'Malformed syntax', 'status' => 400], 400);
      }
    }

}
