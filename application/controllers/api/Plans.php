<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Plans extends Crud_controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("plans_model", 'model');
		$this->load->model("user_model");
		$this->load->model("payment_model");
	}

	public function details_get()
	{
		$res = $this->model->details();
        $this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
	}

	public function select_post()
	{
		$post = $this->input->post();
		if ($post['membership_plan_id'] == 1) {
			$res = $this->model->verifyMembership($post);
		}
		else {
			$data['user_id'] = $post['user_id'];
			$data['membership_id'] = $post['membership_plan_id'];
			$data['status'] = "pending";
			$data['sessionid'] = randomString(20);
			$res = $this->payment_model->getPaymentLink($data);
		}
		
		if ($res) {
			$this->response(['data' => $res, 'message' => 'Membership verified', 'status' => 200], 200);
		}
		else {
			$this->response(['data' => [], 'message' => 'Something went wrong.', 'status' => 400], 400);
		}
	}

	public function apply_post()
	{
		$post = $this->post();
		$res['cancelled_plans'] = $this->model->applyNow($post['user_id']);
		$res['user'] = $this->user_model->get($post['user_id']);
		$this->response(['data' => $res, 'message' => 'Membership applied', 'status' => 200], 200);
	}
}

