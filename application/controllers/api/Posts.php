<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Posts extends Crud_controller {

    #GET api/posts/
    #POST api/posts/
    #GET api/posts/{id}

    #GET api/posts/{user_id}/{type}
    #GET api/posts/{type}/{category_id}
    #GET api/posts/{user_id}

    public function __construct()
    {
        parent::__construct();
        $this->load->model("posts_model", 'model');
        $this->load->model("user_model");
        $this->load->model("ads_model");
        $this->load->model("settings_model");
        $this->load->model("notification_model");
        $this->load->model("follow_model");
    }

    /**
     * get single post
     * @param  [type] $post_id [description]
     * @param  [type] $user_id [logged-in user]
     */
    function single_post_get($post_id, $user_id)
    {
        $res = $this->model->get($post_id, $user_id)[0];
        if(isset($res->password)) {
            unset($res->password);
        }
        if($res || $res !== []) { # Respond with 404 when the resource is not found
            $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Data not found', 'status' => 404], 404);
        }
    }

    function all_get($loggedin_user_id = NULL, $type = NULL, $category_id = NULL, $country_id = NULL, $city_id = NULL)
    {
        $res = $this->model->all_posts($loggedin_user_id, getMarketIdByName($type), $category_id, $country_id, $city_id);
        $this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }

    function user_get($user_id, $viewer_id = NULL, $type = NULL, $category_id = NULL)
    {
        $res = [];
        $res['market_place'] = $this->user_model->getMyMarketplace($user_id, $viewer_id);
        $res['posts'] = $this->model->allByUser($user_id, $viewer_id, getMarketIdByName($type), $category_id);
        $this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }

    function category_get($category_id)
    {
        $res = $this->model->all_posts(NULL, $category_id);
        $this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }

    public function index_post()
    {
        $final = array();
        $status = 400;
        $post = $this->input->post();
        $user = $this->user_model->get($post['user_id']);
        $market_place_access = array();
        foreach ((array)$user->privileges->market_place_access as $key => $value) {
            $market_place_access[] = getMarketIdByName(strtolower($key));
        }

        if (empty($market_place_access)) {
            $msg = "Please select market place";
        }
        elseif (in_array($post['market_place_id'], $market_place_access)) {
            $msg = "Files to upload missing";
            if(isset($_FILES['files']))
            {
                $msg = "Malformed Syntax";
                $files = $this->model->batch_upload($_FILES['files'], getDirPerMarketPlace($post['market_place_id'])); # found in helpers custom
                $thumbnail = $this->model->single_upload("thumbnail", getDirPerMarketPlace($post['market_place_id']));
                $msg = $thumbnail['message'];
                if($thumbnail['status'])
                {
                    $post['thumbnail'] = $thumbnail['thumbnail'];
                    if($this->model->check_category($post['category_id'], $post['market_place_id']))
                    {
                        // save and unset country and city ids
                        $data['city_id'] = @$post['city_id'];
                        $data['country_id'] = @$post['country_id'];
                        unset($post['city_id']);
                        unset($post['country_id']);
                        // save and unset country and city ids
                        
                        if($last_id = $this->model->add($post)){ # Try to add and get the last id
                            $data['post_id'] = $last_id;
                            if ($data['city_id'] != NULL && $data['country_id'] != NULL) {
                                $this->model->add_post_location($data);
                            }

                            ##### FIREBASE NOTIFICATION BLOCK ####
                            ##### @author: @jjjjcccjjf ###########
                            $followers = $this->follow_model->getFollowers($post['user_id']);
                            foreach ($followers as $follower) {
                                $notif_type = "new_post";
                                $notification_message = $this->notification_model->storeNotif($follower, $notif_type, ['post_id' => $last_id, 'post_owner_id' => $post['user_id']]);
                                /**
                                 * Check if Settings if push notif is on
                                 */
                                $notif = $this->settings_model->getSettings($follower);
                                if (@$notif[0]->push_notification) {
                                    $recipients = $this->udi_model->getDeviceIdsByUserId($follower);
                                    if ($recipients) {
                                        foreach ($recipients as $to) { # send multiple bruh
                                            $firebase_response = $this->firebase_model->sendMultiple($to,
                                                ['data' => 
                                                    (object) [
                                                        'badge' => $this->notification_model->getNumberOfUnreadNotifs($follower),
                                                        'post_id' => $last_id,
                                                        'type' => 'new_post'
                                                    ]
                                                ],
                                                ['title' => 'New Post', 'body' => $notification_message]
                                            );
                                            log_message('error', $firebase_response);
                                            log_message('error', $notification_message);
                                        } # end foreach recipients
                                    } # end if recipients
                                } # end if settings

                                /**
                                 * email notification
                                 */
                                if (@$notif[0]->email_notification) {
                                  $this->load->helper("email_template");
                                  $follower_info = $this->user_model->getUser($follower);
                                  $recipient = $follower_info->email;
                                  $subject = $user->username . " has a new post";
                                  $body = new_post_template(['fullname' => $follower_info->fullname, 'username' => $user->username, 'post_name' => $post['name']]);
                                  send_email($this, $recipient, $subject, $body);
                                }

                            } # end foreach followers
                            ##### / FIREBASE NOTIFICATION BLOCK ####
                            ########################################

                            if($this->model->save_files($files['files'], $last_id))
                            {
                                $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
                                $status = 201;
                                $msg = 'Post successfully created';
                                $final = $this->model->get($last_id)[0]; # Get the last entry data
                            }
                        }
                    }
                    else
                    {
                        $msg = "Category ID and Market Place ID does not match";
                    }

                }
            }
        }
        else {
            $msg = "You can't post in this market place";
        }

        $this->response(['data' => $final, 'message' => $msg, 'status' => $status], $status);
    }

    public function like_post()
    {
        $post = $this->input->post();
        $res = $this->model->likePost($post);
        $post_owner_id = @$this->posts_model->getPostOwner($post['post_id'])->user_id;

        ##### FIREBASE NOTIFICATION BLOCK ####
        ##### @author: @jjjjcccjjf ###########
        if ($res['msg'] == 'Post liked') {
            $notif_type = 'like';
            $notification_message = $this->notification_model->storeNotif($post_owner_id, $notif_type, ['liker_id' => $post['user_id'], 'post_id' => $post['post_id']]); # User who liked and the post s/he liked

            /**
             * Check if Settings if push notif is on
             */
            $notif = $this->settings_model->getSettings($post_owner_id);
            if (@$notif[0]->push_notification) {
                $recipients = $this->udi_model->getDeviceIdsByUserId($post_owner_id);
                if ($recipients) {
                    foreach ($recipients as $to) { # send multiple bruh
                        $firebase_response = $this->firebase_model->sendMultiple($to,
                            ['data' => 
                                (object) [
                                    'badge' => $this->notification_model->getNumberOfUnreadNotifs($post_owner_id),
                                    'post_id' => $post['post_id'],
                                    'type' => 'liked_post'
                                ]
                            ],
                            ['title' => 'New like', 'body' => $notification_message]
                        );
                        log_message('error', $firebase_response);
                        log_message('error', $notification_message);
                    } # end foreach
                } # end if recipients
            } # end if settings

            /**
             * email notification
             */
            if (@$notif[0]->email_notification) {
                $this->load->helper("email_template");
                $post_owner = $this->user_model->getUser($post_owner_id);
                $liker = $this->user_model->getUser($post['user_id']);
                $recipient = $post_owner->email;
                $subject = $liker->username . " liked your post in Color Your Life";
                $body = liked_post_template(['fname' => $post_owner->fname, 'liker_profile_pic' => $liker->profile_pic, 'liker_fullname' => $liker->fullname, 'post_name' => $this->model->get($post['post_id'])[0]->name]);
                send_email($this, $recipient, $subject, $body);
            }

        }
        ##### / FIREBASE NOTIFICATION BLOCK ####
        ########################################

        $this->response(["data" => $res['data'], "message" => $res['msg'], "status" => $res['status']], $res['status']);
    }

   public function follow_get($user_id, $type = null, $category_id = null, $country_id = NULL, $city_id = NULL)
   {
       $res['posts'] = $this->model->getPostFollow($user_id, getMarketIdByName($type), $category_id, $country_id, $city_id);
       $res['ads'] = $this->ads_model->getAdsFollow($user_id);
       $this->response(["data" => $res, "message" => "Successfully found all data", "status" => 200], 200);
   }

    public function like_get($user_id, $category_id = null)
    {
        $order = $this->input->get('order') ?: 'DESC';
        $res = $this->model->getLikedPosts($user_id, $category_id, $order);
        $this->response(["data" => $res, "message" => "Successfully found all data", "status" => 200], 200);
    }

    public function update_post($post_id)
    {
        $final = array();
        $post = $this->post();

        if (@$_FILES) {
            $files = $this->model->batch_upload($_FILES['files'], getDirPerMarketPlace($post['market_place_id']));
            $thumbnail = $this->model->single_upload("thumbnail", getDirPerMarketPlace($post['market_place_id']));
            if ($thumbnail['status']) {
                $post['thumbnail'] = $thumbnail['thumbnail'];
            }
        }
        else {
            $this->response(['data' => $final, 'message' => 'Files to upload missing', 'status' => 400], 400);
        }


        if (!$this->model->check_category($post['category_id'], $post['market_place_id'])) {
            $this->response(['data' => $final, 'message' => 'Category ID and Market Place ID does not match', 'status' => 400], 400);
        }

        if ($this->model->updatePost($post_id, $post)) {
            if ($this->model->save_files($files['files'], $post_id)) {
                $final = $this->model->get($post_id)[0];
                $this->response(['data' => $final, 'message' => 'Post successfully updated', 'status' => 200], 200);
            }
        }
        else {
            $this->response(['data' => $final, 'message' => 'Failed to update post', 'status' => 400], 400);
        }
    }

    public function delete_file_delete()
    {
        $arr = array();
        $data = $this->delete();
        $res = $this->model->delete_file($data['posts_file_id']);
        if ($res) {
            $this->response(['data' => $arr, 'message' => 'File successfully deleted', 'status' => 200], 200);
        }
        else {
            $this->response(['data' => $arr, 'message' => 'Failed to delete file', 'status' => 200], 200);
        }
    }

    public function delete_file_post()
    {
        $arr = array();
        $data = $this->post();
        $res = $this->model->delete_file($data['posts_file_id']);
        if ($res) {
            $this->response(['data' => $arr, 'message' => 'File successfully deleted', 'status' => 200], 200);
        }
        else {
            $arr = $this->model->getPostsFile($data['posts_file_id']);
            $this->response(['data' => $arr, 'message' => 'Failed to delete file', 'status' => 200], 200);
        }
    }
}
