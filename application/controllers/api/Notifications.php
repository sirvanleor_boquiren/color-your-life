<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use Coduo\PHPHumanizer\DateTimeHumanizer;

class Notifications extends Crud_controller {

    #GET api/reviews/user
    #POST api/reviews/user
    #GET api/reviews/user/:id

    public function __construct()
    {
        parent::__construct();
        $this->load->model("notification_model");

    }

    function user_get($user_id){
      $res = $this->notification_model->allNotifs($user_id);

      $pks_to_mark_read = []; # array of primary keys we will mark as read

      if ($res) { # if there are notifs
        # format our response too
        foreach ($res as $key => $obj) {
          $res[$key]->unserialized_payload = unserialize($obj->payload);
          $pks_to_mark_read[] = $obj->notification_id;

          /**
           * Fix for inaccurate timestamp in beta :)
           */
          if (in_array(base_url(), ["https://coloryourlife.betaprojex.com/app/"])) {
            $obj->created_at = date("Y-m-d H:i:s", strtotime($obj->created_at . " + 13 hours"));
          }
          elseif (in_array(base_url(), ["https://coloryourlifeapp.com/app/"])) {
            $obj->created_at = date("Y-m-d H:i:s", strtotime($obj->created_at . " + 15 hours"));
          }

          $res[$key]->relative_time = DateTimeHumanizer::difference(
            new \DateTime(date("Y-m-d H:i:s")),
            new \DateTime($obj->created_at)
          );
        }
        $this->notification_model->markAsRead($pks_to_mark_read); # We mark as read on first retrieve
      }

      return $this->response(
        ['data' => $res, 'message' => 'Got all notifications', 'status' => 200], 200
      );
    }

  function count_get($user_id)
  {
    $res = $this->notification_model->getNumberOfUnreadNotifs($user_id);
    return $this->response(
      ['data' => ['notif_count' => $res], 'message' => 'Got the total number of unread notifications', 'status' => 200], 200
    );
  }

}
