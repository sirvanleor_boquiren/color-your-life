<?php

defined('BASEPATH') OR exit('No direct script access allowed');


use Restserver\Libraries\REST_Controller;

class Present_requests extends Crud_controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('present_requests_model','model');
        $this->table = "present_requests";
        $this->table_pk = "present_request_id";
        $this->load->model('user_model');
    }

    public function apply_post()
    {
        $post = $this->input->post();
        $response = [
            'data' => $post,
            'status' => 201,
            'message' => 'Request Successfully sent!'
        ];
        if(!$this->model->add($post)){
            $response = [
                'status' => 400,
                'message' => 'Something went wrong.'
            ];
        }
        else {
            $notif = $this->settings_model->getSettings($post['user_id']);
            if (@$notif[0]->email_notification) {
                /**
                 * Send email
                 */
                $this->load->helper("email_template");
                $user_info = $this->user_model->getUser($post['user_id']);
                $body = apply_for_color_presents_template($user_info->fullname);
                $recipient = $user_info->email;
                $subject = "Application for Color Presents";
                send_email($this, $recipient, $subject, $body);
            }
        }
        $this->response($response,$response['status']);
    }

    public function present_request_post($id)
    {
        $post = $this->input->post();
        $present_request = @$this->model->get($id)[0];
        $response = [
            'status' => 200,
            'data' => $present_request
        ];

        $valid = true;
        $action = $this->input->post('action');

        if(!($valid = ($present_request != NULL))){
            $response['message'] = "Request not found!";
        }elseif($valid = substr($present_request->status,strlen($present_request->status)-2,2) == 'ed'){
            $response['message'] = 'This request has already been processed!';
            $valid = false;
        }elseif(!($valid = (in_array($action,['accept','decline'])) ) ){
            $response['message'] = "Action not recognized!";
        }
        
        if(!$valid){
            $response['data'] = (object)[];
            $response['status'] = 400;
        }
        if($valid && strtolower($action) === 'accept'){
            $this->model->update($id,[
                'status' => 'accepted'
            ]);
            $response['message'] = 'Request successfully accepted!';
            $user = $this->user_model->getUserby('user_id', $present_request->user_id);
            $this->user_model->sendMail($user->email,'Color your life - Your request is accepted!','your present request has been accepted!');
        }elseif($valid && strtolower($action == 'decline')){
            $this->model->update($id,[
                'status' => 'declined'
            ]);
            $user = $this->user_model->getUserby('user_id', $present_request->user_id);
            $this->user_model->sendMail($user->email,'Color your life - Your request is declined!','your present request has been declined!');

            $response['message'] = 'Request successfully declined!';
        }

        $this->response($response,$response['status']);
    }
}
