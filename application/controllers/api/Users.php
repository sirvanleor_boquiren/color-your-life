<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Users extends Crud_controller {

    #GET api/users/
    #POST api/users/
    #GET api/users/{id}

    public function __construct()
    {
        parent::__construct();
        $this->load->model("user_model", 'model');
        $this->load->model("posts_model");
        $this->load->model("rank_model");
        $this->load->model("settings_model");
        $this->load->model("follow_model");
    }

    public function video_get($user_id)
    {
        $r_status = "200";
        $res = $this->model->allVideos($user_id);
        $msg = "Successfully found user videos";
        $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
        $this->response($r_return, $r_status);
    }

    public function video_post()
    {
        $r_status = "400";
        $res = array();
        $msg = "Uploading video failed";
        $data = $this->post();

        $remaining_video = $this->model->getRemainingOf($data['user_id'], "video_recording_dist");
        if ($remaining_video <= 0 && $remaining_video != -1) {
            $msg = "Can't post anymore videos";
            $temp->remaining_video = $this->model->getRemainingOf($data['user_id'], "video_recording_dist");
            $res = $temp;
        }
        else {
            $res = $this->model->addVideo($data);
            if($res)
            {
                $this->user_model->updateRemainingOf($data['user_id'], "video_recording_dist", "-1"); # subtract 1 count to the remaining count of videos of a user
                $res[0]->remaining_video = $this->user_model->getRemainingOf($data['user_id'], "video_recording_dist");
                $r_status = "201";
                $msg = "Successfully saved user's video";
            }
        }

        $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
        $this->response($r_return, $r_status);

    }

    public function login_post()
    {
        # Set Defaults
        $r_status = "400";
        $res = array();
        $msg = "Invalid Username/Password.";

        $res = $this->model->login($this->post());
        if($res)
        {
            $r_status = "200";
            $msg = "User login successful";

                    // ### We update or add device id
                    $this->udi_model->loginDevice($res->user_id, $this->input->post('device_id'), $this->input->post('firebase_id'));
                    // ### / We update or add device id
        }

        $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
        $this->response($r_return, $r_status);
    }

    public function login_sns_post()
    {
        # Set Defaults
        $r_status = "400";
        $res = array();
        $msg = "User credentials invalid";

        $res = $this->model->loginSns($this->post());
        if($res)
        {
            $r_status = "200";
            $msg = "User login successful";

                    // ### We update or add device id
                    $this->udi_model->loginDevice($res->user_id, $this->input->post('device_id'), $this->input->post('firebase_id'));
                    // ### / We update or add device id
        }

        $r_return = array('data' => $res, 'message' => $msg, 'status' => $r_status);
        $this->response($r_return, $r_status);
    }

    public function logout_post()
    {
      # Set Defaults
      $this->udi_model->logoutDevice($this->input->post('device_id'));

      #Bruh
      $r_return = array('data' => null, 'message' => 'Logout success. Device notifications set to inactive state.', 'status' => 200);
      $this->response($r_return, 200);
    }

    public function update_post($user_id)
    {
        $post = $this->post();
        $file = $_FILES;

        # upload ID photo
        if (@$file) {
            $photo = $this->model->upload('profile_pic');
            $post['profile_pic'] = $photo['profile_pic'];
        }

        $res = $this->model->updateUser($user_id, $post);
        if ($res) {
            $user = $this->model->get($user_id);
            $this->response(["data" => $user, "message" => "Info successfully updated", "status" => 200], 200);
        }
        else {
            $this->response(['message' => 'Update Failed', 'status' => 404], 404);
        }
    }

    public function update_password_post($user_id)
    {
        $res = [];

        if (password_verify($this->input->post('old_password'), $this->model->getPassword($user_id))) {
          $res = $this->model->updatePass($user_id, $this->input->post('new_password'));
        }

        if ($res) {
            $user = $this->model->get($user_id);
            $this->response(["data" => $user, "message" => "Info successfully updated", "status" => 200], 200);
        }
        else {
            $this->response(['message' => 'Update Failed', 'status' => 404], 404);
        }
    }

    public function index_post()
    {
        $res = array();
        $msg = "Email already in use.";
        $status = "400";

        # Check Email
        $post = $this->input->post();

        # upload ID photo
        if (@$_FILES['profile_pic']) {
            $photo = $this->model->upload('profile_pic');
            $post['profile_pic'] = $photo['profile_pic'];
        }

        if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            $msg = "Invalid email";
        }

        if($this->model->verifyEmail($post['email']))
        {
            # Check Password
            $checkPass = check_password($post['password'], $post['confirm_password']);
            if($checkPass['passed'])
            {
                $msg = "Username already taken.";
                # Check Username
                if($this->model->verifyUsername($post['username']))
                {
                    # Check Referral Code
                    # If referral_code is not blank AND valid
                    # OR
                    # referral_code is blank
                    if ( (@$post['referral_code'] != "" && $this->model->verifyReferralCode($post['referral_code'])) || @$post['referral_code'] == "" ) {
                        unset($post['confirm_password']);
                        $post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
                        if($last_id = $this->model->add($post))
                        {
                            $res = $this->model->getUser($last_id); # Get the last entry data
                            unset($res->password);

                            /**
                             * Send welcome email to user
                             */
                            $this->load->helper("email_template");
                            $recipient = $res->email;
                            $subject = "Welcome to Color Your Life, " . $res->fname . " " . $res->lname;
                            $body = registration_template();
                            send_email($this, $recipient, $subject, $body);

                            $msg = "User successfully created";
                            $status = 201;
                            $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
                        }
                        else
                        {
                            $msg = "Array input error";
                            $status = 400;

                        }
                    }
                    else {
                        $msg = "Invalid Referral Code";
                    }
                }
            }
            else
            {
                $msg = "Password must contain at least 10 characters with number, capital letter and special character.";
            }
        }

        $this->response(["data" => $res, "message" => $msg, "status" => $status], $status);
    }

    public function sns_post()
    {
      $res = array();
      $msg = "Email or SNS Token invalid.";
      $status = "400";

      # Check Email
      $post = $this->input->post();

      # upload ID photo
      if (@$_FILES['profile_pic']) {
        $photo = $this->model->upload('profile_pic');
        $post['profile_pic'] = $photo['profile_pic'];
      }

      if($this->model->verifyEmail($post['email']) && $this->model->verifySnsToken($post['sns_token']))
      {

        $msg = "Username invalid.";
        # Check Username
        if($this->model->verifyUsername($post['username']))
        {
          if ( (@$post['referral_code'] != "" && $this->model->verifyReferralCode($post['referral_code'])) || @$post['referral_code'] == "" ) {
              if($last_id = $this->model->add($post))
              {
                  $res = $this->model->getUser($last_id); # Get the last entry data
                  unset($res->password);
                  $msg = "User successfully created";
                  $status = 201;
                  $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
              }
              else
              {
                  $msg = "Array input error";
                  $status = 400;
              }
          }
          else {
              $msg = "Invalid Referral Code";
          }

        }

      }

      $this->response(["data" => $res, "message" => $msg, "status" => $status], $status);
    }

    public function follow_post()
    {
        $post = $this->input->post();
        $res = $this->model->follow($post);

        if ($res['msg'] == 'User successfully followed') {
            $notif_type = 'user_follow';
            $notification_message = $this->notification_model->storeNotif($post['following_id'], $notif_type, ['follower_id' => $post['follower_id']]);

            /**
             * Check if Settings if push notif is on
             */
            $notif = $this->settings_model->getSettings($post['following_id']);
            if ($notif[0]->push_notification) {

                ##### FIREBASE NOTIFICATION BLOCK ####
                ##### @author: @jjjjcccjjf ###########
                $recipients =  $this->udi_model->getDeviceIdsByUserId($post['following_id']);
                if ($recipients) {
                    foreach ($recipients as $to) { # send multiple bruh
                        $firebase_response = $this->firebase_model->sendMultiple($to,
                            ['data' => 
                                (object)[
                                    'badge' => $this->notification_model->getNumberOfUnreadNotifs($post['following_id']),
                                    'subscriber_id' => $post['follower_id'],
                                    'type' => 'new_subscriber'
                                ]
                            ],
                            ['title' => 'New follower', 'body' => $notification_message]
                        );
                        log_message('error', $firebase_response);
                        log_message('error', $notification_message);
                    } # end foreach
                } # end if recipients
                ##### / FIREBASE NOTIFICATION BLOCK ####
                ########################################
                
            } # end if settings

            /**
             * Email notification
             */
            if (@$notif[0]->email_notification) {
                $this->load->helper("email_template");
                $follower_info = $this->model->getUser($post['follower_id']);
                $following_info = $this->model->getUser($post['following_id']);
                $recipient = $following_info->email;
                $subject = "A new follower!";
                $body = user_follow_template(['fname' => $following_info->fname, 'username' => $follower_info->username, 'follower_fullname' => $follower_info->fullname, 'follower_profile_pic' => $follower_info->profile_pic, 'follower_marketplace_name' => $follower_info->market_place_name]);
                send_email($this, $recipient, $subject, $body);
            }

        }

        $this->response(["data" => $res['data'], "message" => $res['msg'], "status" => 200], 200);
    }

    public function following_get($user_id)
    {
        $res = $this->model->getFollowing($user_id);
        $this->response(['data' => $res, 'message' => 'Successfully found all the data', 'status' => 200], 200);
    }

    public function followers_get($user_id)
    {
        $res = $this->follow_model->getFollowers($user_id);
        $final = array();
        foreach ($res as $user_id) {
            $final[] = $this->model->getUser($user_id);
        }
        $this->response(['data' => $final, 'message' => 'Successfully found all the data', 'status' => 200], 200);
    }

    public function single_get($user_id)
    {
        $res = $this->model->get($user_id);
        
        if ($res || $res !== []) { # Respond with 404 when the resource is not found
            $res->referrals_rank = $this->model->getEachReferralCount($user_id);
            $points_selection = [];

            $totalpoints = $res->points;
            $index = 0;
            while($totalpoints >= 50 && count($points_selection) < 20){
                $totalpoints -= 50;
                $points_selection[] = 50 * ($index + 1);
                $index++;
            }

            $res->points_selection = $points_selection;$res->referrals_rank = $this->model->getEachReferralCount($user_id);
            $points_selection = [];

            $totalpoints = $res->points;
            $index = 0;
            while($totalpoints >= 50 && count($points_selection) < 20){
                $totalpoints -= 50;
                $points_selection[] = 50 * ($index + 1);
                $index++;
            }

            $res->points_selection = $points_selection;
            $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Data not found', 'status' => 404], 404);
        }
    }

    public function bonus_get($user_id)
    {
        $user = $this->model->get($user_id);
        $res = $this->model->getBonus($user_id);
        $res->rank_type = str_replace(" Member", "", $user->rank_type);
        if ($res || $res !== []) { # Respond with 404 when the resource is not found
            $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Data not found', 'status' => 404], 404);
        }
    }

    public function marketplace_post()
    {
        $post = $this->post();
        $market_place_access = $this->model->getMarketPlaceAccess($post['user_id']);
        if (empty($market_place_access)) {
            $res = $this->model->updateMarketPlace($post['user_id'], $post['market_place_id']);
            if ($res) {
                $market_place_access = $this->model->getMarketPlaceAccess($post['user_id']);
                $this->response(['data' => ['market_place_access' => $market_place_access], 'message' => 'Successfully updated market place', 'status' => 200], 200);
            }
        }
        else {
            $this->response(['data' => ['market_place_access' => $market_place_access], 'message' => 'Market place already selected', 'status' => 200], 200);
        }
    }

    public function marketplace_get($user_id, $viewer_id = NULL)
    {
        $res = $this->model->getMyMarketplace($user_id, $viewer_id);
        if ($res || $res !== []) { # Respond with 404 when the resource is not found
            $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Data not found', 'status' => 404], 404);
        }
    }

    public function presents_referral_get($user_id)
    {
        $user = $this->model->get($user_id);
        $upper_part = ['points' => $user->points, 'rank_type' => $user->rank_type];
        $res = array_merge($this->model->countPresentsReferral($user_id), $upper_part); # refer to 15-Color-Presents.png screen
        if ($res || $res !== []) { # Respond with 404 when the resource is not found
            $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Data not found', 'status' => 404], 404);
        }
    }

    public function referrals_get($user_id)
    {
        $res = new stdClass();
        $res->total_referrals = $this->model->getUser($user_id)->referrals;
        $res->referrals = $this->model->getReferrals($user_id);
        if ($res || $res !== []) { # Respond with 404 when the resource is not found
            $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Data not found', 'status' => 404], 404);
        }
    }

    public function send_referral_code_post()
    {
        $post = $this->input->post();
        $post['emails'] = array_map('trim', $post['emails']);
        $referral_code = $this->model->getReferralCode($post['user_id'])->referral_code;
        $subject = "Color Your Life";
        $message = "Referral Code: <b><u>" . $referral_code . "</u></b>";
        if (send_email($this, $post['emails'], $subject, $message)) {
            $this->response(['data' => $post, 'message' => 'Successfully sent mails', 'status' => 200], 200);
        }
        else {
            $this->response(['message' => 'Failed to send mails', 'status' => 404], 404);
        }
    }

    public function forgot_password_post()
    {

        if ( ( $user = $this->model->getUserby('email', $this->input->post('email')) ) &&
            $ft = $this->model->setForgotToken(@$user->user_id)) {
            $user = $this->model->getUser($user->user_id);

            $subject = "Color Your Life";
            $link = "<a href='".base_url('user/reset/').$user->forgot_token."'>Click Here</a>";
            $message = '';
            $message .= 'You have requested a password reset, please follow the link below to reset your password.<br>';
            $message .= "Password Reset Link: <b><u>" . $link . "</u></b><br>";
            $message .= "Please ignore this email if you did not request a password change.";

            if (send_email($this, $user->email, $subject, $message)) {
                $this->response(['data' => (object)[], 'message' => 'Once verified by our system, please check your email. We have sent a reset link to '.$user->email, 'status' => 200], 200);
            }else{
                $this->response(['data' => (object)[], 'message' => 'There was a failure in sending mail', 'status' => 400], 400);
            }
        }else{
            $this->response(['data' => (object)[], 'message' => 'Once verified by our system, please check your email. We have sent a reset link to '.$this->input->post('email'), 'status' => 404], 404);
        }

    }

    public function forgot_username_post()
    {
        $post = $this->input->post();
        $check = $this->model->checkUser($post);

        if ($check) {

            # Email block
            $subject = "Color Your Life";
            $message = '';
            $message .= "Hi ".$check->fname.", <br>";
            $message .= "Your username is <b><u>" . $check->username . "</u></b>.";
            # /Email block

            if (send_email($this, $check->email, $subject, $message)) {
                 $this->response(['data' => [], 'message' => 'Please check your email. We have sent your username to '.$check->email, 'status' => 200], 200);
            }else{
                $this->response(['data' => [], 'message' => 'There was a failure in sending mail', 'status' => 400], 400);
            }


        }else{
            $this->response(['data' => [], 'message' => 'Invalid email/password', 'status' => 404], 404);
        }
    }

    public function points_get($user_id)
    {
        $res = $this->model->getColorPoints($user_id);
        $res->points_web_link = base_url('points');
        $this->response(['data' => $res, 'message' => 'Successfully found data', 'status' => 200], 200);
    }

    public function plan_info_get($user_id)
    {
        $res = $this->model->getPlanInfo($user_id);
        if ($res) {
            $msg = "Successfully found data";
            $status = 200;
        }
        else {
            $msg = "No data found";
            $status = 400;
            $res = [];
        }
        $this->response(['data' => $res, 'message' => $msg, 'status' => $status], $status);
    }
}
