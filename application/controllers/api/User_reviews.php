<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class User_reviews extends Crud_controller {

    #GET api/reviews/user
    #POST api/reviews/user
    #GET api/reviews/user/:id

    public function __construct()
    {
        parent::__construct();
        $this->load->model("user_reviews_model", 'model');
        $this->load->model("firebase_model");
        $this->load->model("user_model");
        $this->load->model("settings_model");
        $this->load->model("notification_model");
    }

    /**
     * Get all reviews by reviewee_id
     * @param  [int] $reviewee_id [unique id of post]
     * @return [array] [display all reviews under specific post]
     */
    public function user_get($reviewee_id)
    {
    	$res = $this->model->allByRevieweeId($reviewee_id);

      # We attach the username and profile pic to the response
      # Also format the created at
      foreach ($res as $key => $value) {
        $reviewer_id = $value->reviewer_id;

        $user = @$this->user_model->get($reviewer_id);
        $res[$key]->user = $user; #user object

        $res[$key]->created_at_formatted = date('m/d/y | h:i A', strtotime($value->created_at)); #format the date
      }

    	$this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }

    public function index_post()
    {
      $data = $this->input->post();
      if($last_id = $this->model->add($data)){ # Try to add and get the last id
        $res = $this->model->get($last_id); # Get the last entry data

        $notif_type = 'new_user_review';
        $notification_message = $this->notification_model->storeNotif($data['reviewee_id'], $notif_type, ['reviewer_id' => $data['reviewer_id']]);

        /**
         * Check if Settings if push notif is on
         */
        $notif = $this->settings_model->getSettings($data['reviewee_id']);
        if (@$notif[0]->push_notification) {
          ##### FIREBASE NOTIFICATION BLOCK ####
          # get device ids via user_id
          $recipients = $this->udi_model->getDeviceIdsByUserId($data['reviewee_id']);
          foreach ($recipients as $to) {
            $firebase_response = $this->firebase_model->sendMultiple($to,
                ['data' => 
                    (object) [
                        'id' => $data['reviewee_id'],
                        'type' => 'new_user_review'
                    ]
                ],
             ['title' => 'New review', 'body' => 'Someone written you a new review']
             );
            log_message('error', $firebase_response);
            log_message('error', $notification_message);
          }
          ##### / FIREBASE NOTIFICATION BLOCK ####
        }

        /**
         * email notification
         */
        if (@$notif[0]->email_notification) {
          $this->load->helper("email_template");
          $reviewer_info = $this->user_model->getUser($data['reviewer_id']);
          $reviewee_info = $this->user_model->getUser($data['reviewee_id']);
          $recipient = $reviewee_info->email;
          $subject = $reviewer_info->username . " reviewed you in Color Your Life";
          $body = reviewed_user_template(['reviewer_profile_pic' => $reviewer_info->profile_pic, 'reviewer_fullname' => $reviewer_info->fullname, 'review' => $data['review'], 'fname' => $reviewee_info->fname]);
          send_email($this, $recipient, $subject, $body);
        }

        $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
        $this->response(['data' => $res, 'message' => 'Successfully created new data', 'status' => 201], 201);
      }else{
        $this->response(['data' => [], 'message' => 'Malformed syntax', 'status' => 400], 400);
      }
    }

}
