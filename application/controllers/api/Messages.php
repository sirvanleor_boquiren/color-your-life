<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use Coduo\PHPHumanizer\DateTimeHumanizer;

class Messages extends Crud_controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('messages_model', 'model');
        $this->load->model('message_attachments_model');
        $this->load->model('user_model');
    }

    /**
     * sending a message one to one
     * @return [type] [description]
     */
    public function index_post()
    {
      $attachment_id = null;
      $upload_res = $this->model->upload('thumbnail');

      if ($upload_res['status']) {
        $attachment_id = $this->message_attachments_model->add(['thumbnail' => $upload_res['thumbnail']]);
      }

      $res = $this->model->sendMessage(array_merge($this->input->post(), ['attachment_id' => $attachment_id]));
      $this->response(['data' => $res, 'message' => 'Message sent', 'status' => 201], 201);
    }

    /**
     * get conversation between two people
     * @param  [type] $id                      [description]
     * @param  [type] $conversation_partner_id [description]
     * @return [type]                          [description]
     */
    public function conversation_get($conversation_owner_id, $conversation_partner_id)
    {
      $res = $this->model->getThread($conversation_owner_id, $conversation_partner_id);
      $message = count($res) ? 'Got all messages' : 'No messages found';

      if ($res) {
        $this->model->markAsRead($conversation_owner_id, $conversation_partner_id); # mark as read on retrieve of message thread
      }

      $this->response(['data' => $res, 'message' => $message, 'status' => 200, 'page' => @$this->input->get('page') ?: 1], 200);
    }

    public function followers_post($conversation_owner_id)
    {
      $remaining_group_message = $this->user_model->getRemainingOf($conversation_owner_id, "group_message");
      if ($remaining_group_message <= 0 && $remaining_group_message != -1) {
        $this->response(['data' => (object)['remaining_group_message' => "0"], 'message' => "Can't send anymore group message", 'status' => 200], 200);
      }
      else {
        $res = $this->model->sendToFollowers($conversation_owner_id, $this->input->post('body'));
        if ($res) {
          $this->user_model->updateRemainingOf($conversation_owner_id, "group_message", "-1"); # subtract 1 count to the remaining count of group_message in the user's plan
          $remaining_group_message = $this->user_model->getRemainingOf($conversation_owner_id, "group_message");
          $this->response(['data' => (object)['body' => $this->input->post('body'), 'remaining_group_message' => $remaining_group_message], 'message' => 'Message sent to all followers', 'status' => 200], 200);
        } 
        else {
          $this->response(['data' => (object)[], 'message' => 'You have no followers', 'status' => 200], 200);
        }
      }

    }

    public function product_message_post()
    {
      $res = $this->model->sendToPostOwner($this->input->post());

      if ($res) {
        $this->response(['data' => $res, 'message' => 'Message sent', 'status' => 201], 201);
      } else {
        $this->response(['data' => (object)[], 'message' => 'Bad request; nonexistent post or malformed syntax', 'status' => 400], 400);
      }
    }

    public function message_previews_get($user_id)
    {
      $res = $this->model->getMessagePreviews($user_id);
      $message = count($res) ? 'Got all message previews' : 'Your inbox is empty';

      $this->response(['data' => $res, 'message' => $message, 'status' => 200], 200);
    }

    public function count_get($user_id)
    {
      $res = $this->model->getNumberOfUnreadMessages($user_id);
      return $this->response(
        ['data' => ['message_count' => $res], 'message' => 'Got the total number of unread messages', 'status' => 200], 200
      );
    }

}
