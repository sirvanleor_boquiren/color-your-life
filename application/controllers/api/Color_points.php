<?php

defined('BASEPATH') OR exit('No direct script access allowed');


use Restserver\Libraries\REST_Controller;

class Color_points extends Crud_controller {
	public function __construct()
	{
        parent::__construct();
        $this->load->model('color_points_model', 'model');
        $this->load->model('user_model');
        $this->load->model('affiliate_model');
        $this->load->model("settings_model");
    }

    public function redeem_post()
    {
    	$data = $this->input->post();
    	$user_info = $this->user_model->getUser($data['user_id']);
        
    	if ($user_info) {
    		if ($user_info->points >= $data['points_to_redeem']) {
    			$affiliate = $this->affiliate_model->get($data['affiliate_id']);
    			if ($affiliate) {
    				if ($last_id = $this->model->add($data)) {
                        /**
                         * Check if Settings if push notif is on
                         */
                        $notif = $this->settings_model->getSettings($data['user_id']);
                        if (@$notif[0]->email_notification) {
                            /**
                             * Send email here
                             */
                            $this->load->helper("email_template");
                            $body = redeem_color_points_template($affiliate->title, $data['url']);
                            $message = "You have successfully requested to redeem points.<br> (This body of message is just a sample. This must be edited later).";
                            send_email($this, $user_info->email, "Redeem Request from " . $user_info->fullname, $body);
                        }

			            $res = $this->model->get($last_id); # Get the last entry data
			            $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
			            $this->response(['data' => $res, 'message' => 'Successfully created new data', 'status' => 201], 201);
    				}
    				else {
    					$this->response(['data' => [], 'message' => 'Malformed syntax', 'status' => 400], 400);
    				}
    			}
    			else {
    				$this->response(['data' => [], 'message' => 'Affiliate does not exists',  'status' => 400], 400);
    			}
    		}
    		else {
    			$this->response(['data' => [], 'message' => 'Not enough points', 'status' => 400], 400);
    		}
    	}
    	else {
    		$this->response(['data' => [], 'message' => 'User does not exists', 'status' => 400], 400);
    	}
    }
}