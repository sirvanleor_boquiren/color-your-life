<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Categories extends Crud_controller {
    
    #GET api/categories/

    public function __construct()
    {
        parent::__construct();
        $this->load->model("categories_model", 'model');
    }

    function index_get($type = NULL)
    {
    	$market_place_id = ($type) ? getMarketIdByName($type) : $type;

    	$res = $this->model->all_categories($market_place_id);
        $this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }
}
