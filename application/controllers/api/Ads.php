<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
// require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Ads extends Crud_controller {
    
    #GET api/ads/
    #POST api/ads/
    #GET api/ads/{id}

    public function __construct()
    {
        parent::__construct();
        $this->load->model("ads_model", 'model');
        $this->load->model("user_model");
    }

    public function index_post()
    {
        $final = array();
        $status = 400;
        $msg = "Files to upload missing";
        $data = $this->input->post();

        $remaining_ads = $this->user_model->getRemainingOf($data['user_id'], "advertisement");
        if ($remaining_ads <= 0 && $remaining_ads != -1) {
            $msg = "Can't post anymore ads";
            $res->remaining_advertisement = $this->user_model->getRemainingOf($data['user_id'], "advertisement");
            $final = $res;
        }
        else {
            if(isset($_FILES['files']))
            {
                $files = $this->model->upload('files');
                if($files['status'])
                {
                    $data['image'] = $files['files'];
                    $msg = "Malformed Syntax";
                    if($last_id = $this->model->add($data)){ # Try to add and get the last id
                        $this->user_model->updateRemainingOf($data['user_id'], "advertisement", "-1"); # subtract 1 count to the remaining count of advertisement of a user
                        $res = $this->model->get($last_id)[0]; # Get the last entry data
                        $res->remaining_advertisement = $this->user_model->getRemainingOf($data['user_id'], "advertisement");
                        $res->image = base_url("uploads/featured_ads/".$res->image);
                        $this->response_header('Location', api_url($this) .  $last_id); # Set the header location
                        $status = 201;
                        $msg = 'Ads successfully created';
                        $final = $res;
                    }
                }
                else
                {
                    $msg = $files['message'];
                }
                
            }
        }

        $this->response(['data' => $final, 'message' => $msg, 'status' => $status], $status);
    }

    public function featured_get($type = NULL)
    {
        # $type [ 1 - Products, 2 - Services ]
        $final = array();
        $status = 200;
        $msg = "Successfully found featured ads";
        $type = ($type) ? getMarketIdByName($type) : $type;
        $final = $this->model->getFeatured($this->input->post(), $type);
        $this->response(['data' => $final, 'message' => $msg, 'status' => $status], $status);
    }

    public function try_post()
    {
        $this->response(['data' => $_FILES]);
    }

    
}
