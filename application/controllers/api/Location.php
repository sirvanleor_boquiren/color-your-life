<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

class Location extends Crud_controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model("location_model", "model");
    }

    public function index_get() { echo "No!"; }
    public function index_post() { echo "No!"; }

    public function countries_get($search_string = "")
    {
    	$res = $this->model->getCountries($search_string);
    	$this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    }

    public function cities_get($country_code = "", $search_string = "")
    {
    	if ($country_code != "") {
    		$res = $this->model->getCities($country_code, $search_string);
    		$this->response(['data' => $res, 'message' => 'Successfully found all data', 'status' => 200], 200);
    	}
    	else {
    		$this->response(['data' => [], 'message' => 'Country code is required', 'status' => 400], 400);	
    	}
    }
    
}