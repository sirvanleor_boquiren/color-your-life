<?php
class Notification_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "notifications";
        $this->table_pk = "notification_id";

        $this->load->model('user_model');
        $this->load->model('posts_model');
    }

    /**
     * store nontification to the database
     * @param  [type] $user_id  the user id of the one receiving the notif
     * @param  [type] $type     type of notif (reply, like, follow, webinar, post_review, user_review, etc.)
     * @param  [type] $payload  array of data we plan to serialize
     * @return [type]           [description]
     */
    function storeNotif($user_id, $type, $payload) {

      $payload_str = serialize($payload);

      $data['user_id'] = $user_id;
      $data['type'] = $type;
      $data['payload'] = $payload_str; # so we can store arrays in the database

			$this->deleteIfExists($data);
      $this->add($data);

      return $this->constructMessage($type, $payload_str);
    }

		public function deleteIfExists($data)
		{
			$this->db->where([
				'user_id' => $data['user_id'],
				'type' => $data['type'],
			 	'payload' => $data['payload']
		  ]);
			return $this->db->delete($this->table);
		}

    /**
     * get all notifs of certain user
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function allNotifs($user_id)
    {
			$this->paginate();
			$this->db->order_by($this->table_pk, 'DESC');
      $notifs = $this->db->get_where($this->table, ['user_id' => $user_id])->result();
      $res = $this->formatNotifs($notifs);
      return $res;
    }

    public function getNumberOfUnreadNotifs($user_id)
    {
      $this->db->where("is_read", 0);
      $this->db->where("user_id", $user_id);
      $this->db->from($this->table);
      return $this->db->count_all_results();
    }

    /**
     * format notifs so they return the correct text
     * @param  [type] $notifs [description]
     * @return [type]         [description]
     */
    public function formatNotifs($notifs)
    {
      $res = [];
      foreach ($notifs as $key => $value) {
        $value->message = $this->constructMessage($value->type, $value->payload);
        $value->message_hl = $this->getHighlightedMessage($value->type, $value->payload);
        $value->profile_pic = $this->getProfilePic($value->type, $value->payload);
        $res[] = $value;
      }

      return $res;
    }

		public function markAsRead($pks_arr)
		{
			$this->db->where_in($this->table_pk, $pks_arr);
			return $this->db->update($this->table, ['is_read' => 1]);
		}

    public function getHighlightedMessage($type, $payload)
    {
      $p = unserialize($payload);
      $str = '';
      switch ($type) {
        case 'like':
          $str = $this->user_model->get($p['liker_id'])->username;
          break;
        case 'user_follow':
          $str = $this->user_model->get($p['follower_id'])->username; 
          break;
        case 'message':
          $str = $this->user_model->getUser($p['sender_id'])->fullname;
          break;
        case 'redeem_request':
          $str = $p['status'];
          break;
        case 'referral':
          $str = $this->user_model->get($p['referred_id'])->username;
          break;
        case 'new_post_review':
          $str = $this->user_model->get($p['reviewer_id'])->username;
          break;
        case 'new_user_review':
          $str = $this->user_model->get($p['reviewer_id'])->username;
          break;
        case 'new_post':
          $str = $this->user_model->get($p['post_owner_id'])->username;
          break;
      }

      return $str;
    }

    public function constructMessage($type, $payload)
    {
      $p = unserialize($payload); # p for payload arr
      $str = '';
      switch ($type) {
        case 'like':
          $str = $this->t_like($p);# p for payload arr
          break;
        case 'user_follow':
          $str = $this->t_userFollow($p);# p for payload arr
          break;
        case 'message':
          $str = $this->t_message($p);# p for payload arr
          break;
        case 'redeem_request':
          $str = $this->t_request($p);
          break;
        case 'referral':
          $str = $this->t_referral($p);
          break;
        case 'new_post_review':
          $str = $this->t_postReview($p);
          break;
        case 'new_user_review':
          $str = $this->t_userReview($p);
          break;
        case 'new_post':
          $str = $this->t_newPost($p);
          break;

        default:
          $str = 'You have a notification';
          break;
      }

      return $str;
    }

    public function t_newPost($p)
    {
      $post_owner = $this->user_model->get($p['post_owner_id']);
      // $post_owner_name = $post_owner->fname . " " . $post_owner->lname;
      $post_owner_name = $post_owner->username;
      $post = $this->posts_model->get($p['post_id'])[0];
      $post_type = ($post->market_place_id == 1) ? "Product" : "Service";
      return "$post_owner_name posted a new $post_type.";
    }

    public function t_postReview($p)
    {
      $post_name = $this->posts_model->get($p['post_id'])[0]->name;
      $reviewer_name = $this->user_model->get($p['reviewer_id'])->username;
      return "$reviewer_name wrote a review in your $post_name post";
    }

    public function t_userReview($p)
    {
      $reviewer_name = $this->user_model->get($p['reviewer_id'])->username;
      return "$reviewer_name written you a new review";
    }

    public function t_like($p)
    {
      $liker_name = $this->user_model->get($p['liker_id'])->username;
      $post_name  = $this->posts_model->get($p['post_id'])[0]->name;
      return "$liker_name has liked your $post_name post";
    }

		public function t_userFollow($p)
		{
			$follower_name = $this->user_model->get($p['follower_id'])->username;
			return "$follower_name followed you";
		}

		public function t_message($p)
		{
			$sender_name = $this->user_model->getUser($p['sender_id'])->fullname . ": ";
			return "{$sender_name}{$p['body']}";
		}

    public function t_request($p)
    {
      return "Your request has been {$p['status']}";
    }

    public function t_referral($p)
    {
      $referred_user = $this->user_model->get($p['referred_id'])->username;
      return "You have successful referral from {$referred_user}!";
    }

    /**
     * get profile of user that liked/followed/etc
     */
     public function getProfilePic($type, $payload)   
     {
      $p = unserialize($payload);
      switch ($type) {
        case 'like':
          $profile_pic = $this->user_model->get($p['liker_id'])->profile_pic;
          break;

        case 'user_follow':
        $profile_pic = $this->user_model->get($p['follower_id'])->profile_pic;
          break;

        case 'new_post':
          $profile_pic = $this->user_model->get($p['post_owner_id'])->profile_pic;
          break;
        
        default:
          $profile_pic = base_url("uploads/user_profile_pic/default.png");
          break;
      }
      return $profile_pic;
     }

}
