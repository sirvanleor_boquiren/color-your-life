<?php

class Accounts_model extends CI_model
{
	public function __construct()
	{
		# ...
	}

	#Login 
	public function verifyLogin($post)
	{		
		$this->db->where("email = '".$post['email']."' OR username ='".$post['username']."'");
		$db_results = $this->db->get("admin_users");
		if($db_results->num_rows() >= 1) 
		{
			$db_data = $db_results->result()[0];
			if($db_data->status == 1) 
			{
				if(password_verify($post['password'], $db_data->password))
				{
					return $db_data;
				}
				else
				{
					return array("error"=>"401");
				}
			}
			else
			{
				return array("error"=>"401");
			}
		}
		else
		{
			return array("error"=>"false");
		}
	}

	#Number of users per type and status
	public function getTotalUsers($user_type = null, $is_active = "1")
	{
		$type = "";
		if($user_type != null)
		{
			$type = " AND u_is_admin = ".$user_type;
		}
		$this->db->select("u_id");
		$this->db->from("users");
		$this->db->where("u_is_active = ".$is_active.$type);
		$db_results = $this->db->get();
		return $db_results->num_rows();
	}

	#All users details per type
	public function getAllUsers($user_type = null)
	{
		$where = $user_type != null ? "u_is_admin = ".$user_type : "1";
		$this->db->select("u_id, u_fname, u_lname, u_minitial, pos_desc, u_mobile, u_email, u_is_active");
		$this->db->from("users");
		$this->db->join("positions", "users.pos_id = positions.pos_id");
		$this->db->order_by("u_id", "desc");
		$this->db->where($where);
		$limit = 0;
		if($this->uri->segment(3) !== FALSE)
		{
			$limit = $this->uri->segment(3);
		}
		$this->db->limit(TABLE_DATA_PER_PAGE, $limit); # number_per_page, start row
		$db_results = $this->db->get();

		if($db_results->num_rows() > 0)
		{
			$db_data = array();
			foreach ($db_results->result() as $user) {				
				$firstname = $user->u_minitial != "" ? $user->u_fname." ".$user->u_minitial."." : $user->u_fname;
				$user->fullname = $firstname." ".$user->u_lname;
				$user->status = $user->u_is_active == 1 ? "Active" : "Inactive";
				$db_data[] = $user;
			}
			return $db_data;
		}
		else
		{
			return null;
		}

	}

	# POST edit user
	public function editUser($post)
	{
		$this->db->where("u_id", $post['u_id']);
		unset($post["u_id"]); # Prevent Conflict

		if(isset($post["u_password"]))
		{
			if($post["u_password"] != "")
			{
				$post["u_password"] = password_hash($post["u_password"], PASSWORD_DEFAULT);
			}
			else
			{
				unset($post["u_password"]); # Prevent clearing password
			}
		}
		return $this->db->update("users", $post);
	}

	# POST add user
	public function addUser($post)
	{
		$this->load->model("ajax_model");
		if(!$this->ajax_model->checkExists($post, "users", "u_id", "u_email"))
		{
			$p = $post["u_fname"]."%G8123".time();
			$post['u_password'] = password_hash($p, PASSWORD_DEFAULT);

			$this->load->library('email');
			// $config_mail['protocol']='smtp';
	        // $config_mail['smtp_host']='mail.smtp2go.com';
	        // $config_mail['smtp_port']='80';
	        // $config_mail['smtp_timeout']='30';
	        // $config_mail['smtp_user']='betamail@optimindsolutions.com';
	        // $config_mail['smtp_pass']='MDFldDJ5a3lkbWk3';
	        $config_mail['charset']='utf-8';
	        $config_mail['newline']="\r\n";
	        $config_mail['wordwrap'] = TRUE;
	        $config_mail['mailtype'] = 'html';
	        $this->email->initialize($config_mail);
			$this->email->from('noreply@g8Marketing.com', 'G8 Marketing Admin');
			$this->email->to($post['u_email']); #$post['u_email']
			$this->email->subject('New User Registered');
			$message = "Hi ".$post['u_fname']."!<br><br>";
			$message .= "You are now registered in the G8Marketing App.<br> To access your account here is your credentials:<br>";
			$message .= "Username: ".$post["u_email"]."<br>";
			$message .= "Password: ".$p."<br>";
			$message .= "Thank you!";

			$this->email->message($message);
			if($this->email->send())
			{
				if($this->db->insert("users", $post))
				{
					return array("alert_msg" => "Successfully added new User", "alert_color" => "green");
				}
				else
				{
					return array("alert_msg" => "Adding of users failed. Please contact maintenance or try again.", "alert_color" => "red");
				}
			}
			else
			{
				return array("alert_msg" => "Sending of email and adding of user failed. Please enter a valid email.", "alert_color" => "red");
			}
		}
		else
		{
			return array("alert_msg" => "Email already exists. Please enter a new valid email.", "alert_color" => "red");
		}
	}

	# POST Check User
	public function checkEmail($post)
	{
		$match = false;
		if(isset($post["u_email"]))
		{
			$this->db->select("u_id");
			$this->db->from("users");
			$this->db->where("u_email = '".$post["u_email"]."'");
			if($this->db->get()->num_rows() != 0)
			{
				$match = true;
			}
		}

		return $match;
		
	}
}

?>