<?php
class User_reviews_model extends Crud_model {
	public function __construct()
	{
		parent::__construct();
		$this->table = "user_reviews";
		$this->table_pk = "user_review_id";
	}

	public function add($data)
    {
        $user_review_id = $this->checkUserReview($data['reviewee_id'], $data['reviewer_id']);
        if ($user_review_id) {
            $this->updateUserReview($user_review_id, ["status" => "inactive"]);
        }

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function deleteUserReview($user_review_id)
    {
        $this->db->where("user_review_id", $user_review_id);
        return $this->db->delete($this->table);
    }

    public function updateUserReview($user_review_id, $data)
    {
        $this->db->where("user_review_id", $user_review_id);
        return $this->db->update($this->table, $data);
    }

    public function checkUserReview($reviewee_id, $reviewer_id)
    {
        $this->db->where("reviewee_id", $reviewee_id);
        $this->db->where("reviewer_id", $reviewer_id);
        $this->db->where("status", "active");
        $res = $this->db->get($this->table)->result();
        if ($res) {
            return $res[0]->user_review_id;
        }
        else {
            return false;
        }
    }

	public function allByRevieweeId($reviewee_id)
	{
		$final = array();

		$this->db->where("reviewee_id", $reviewee_id);
        $this->db->where("status", "active");
		$this->db->order_by("created_at", "DESC");
		$res = $this->db->get($this->table)->result();

		foreach ($res as $obj) {
			$final[] = $obj;
		}

		return $final;
	}

	public function countReviews($user_id, $type = "Satisfied")
	{
		$this->db->where('reviewee_id', $user_id);
		$this->db->where('rating', $type);
        $this->db->where("status", "active");
		return $this->db->count_all_results($this->table);
	}

}
