<?php
class Posts_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "posts";
        $this->table_pk = "post_id";
        $this->upload_dir = "posts";

        $this->load->model('firebase_model');
        $this->load->model('notification_model');
        $this->load->model('user_device_ids_model', 'udi_model');
        $this->load->model('user_model', 'user_model');
    }

    public function get_currency($post_id)
    {
        // $equivalent_currency_json = file_get_contents("http://country.io/currency.json", false, stream_context_create(array('http' => array('max_redirects' => 101))));
        // $equivalent_currency_json = (array)json_decode($equivalent_currency_json);
        $equivalent_currency_json = ["BD"=>"BDT","BE"=>"EUR","BF"=>"XOF","BG"=>"BGN","BA"=>"BAM","BB"=>"BBD","WF"=>"XPF","BL"=>"EUR","BM"=>"BMD","BN"=>"BND","BO"=>"BOB","BH"=>"BHD","BI"=>"BIF","BJ"=>"XOF","BT"=>"BTN","JM"=>"JMD","BV"=>"NOK","BW"=>"BWP","WS"=>"WST","BQ"=>"USD","BR"=>"BRL","BS"=>"BSD","JE"=>"GBP","BY"=>"BYR","BZ"=>"BZD","RU"=>"RUB","RW"=>"RWF","RS"=>"RSD","TL"=>"USD","RE"=>"EUR","TM"=>"TMT","TJ"=>"TJS","RO"=>"RON","TK"=>"NZD","GW"=>"XOF","GU"=>"USD","GT"=>"GTQ","GS"=>"GBP","GR"=>"EUR","GQ"=>"XAF","GP"=>"EUR","JP"=>"JPY","GY"=>"GYD","GG"=>"GBP","GF"=>"EUR","GE"=>"GEL","GD"=>"XCD","GB"=>"GBP","GA"=>"XAF","SV"=>"USD","GN"=>"GNF","GM"=>"GMD","GL"=>"DKK","GI"=>"GIP","GH"=>"GHS","OM"=>"OMR","TN"=>"TND","JO"=>"JOD","HR"=>"HRK","HT"=>"HTG","HU"=>"HUF","HK"=>"HKD","HN"=>"HNL","HM"=>"AUD","VE"=>"VEF","PR"=>"USD","PS"=>"ILS","PW"=>"USD","PT"=>"EUR","SJ"=>"NOK","PY"=>"PYG","IQ"=>"IQD","PA"=>"PAB","PF"=>"XPF","PG"=>"PGK","PE"=>"PEN","PK"=>"PKR","PH"=>"PHP","PN"=>"NZD","PL"=>"PLN","PM"=>"EUR","ZM"=>"ZMK","EH"=>"MAD","EE"=>"EUR","EG"=>"EGP","ZA"=>"ZAR","EC"=>"USD","IT"=>"EUR","VN"=>"VND","SB"=>"SBD","ET"=>"ETB","SO"=>"SOS","ZW"=>"ZWL","SA"=>"SAR","ES"=>"EUR","ER"=>"ERN","ME"=>"EUR","MD"=>"MDL","MG"=>"MGA","MF"=>"EUR","MA"=>"MAD","MC"=>"EUR","UZ"=>"UZS","MM"=>"MMK","ML"=>"XOF","MO"=>"MOP","MN"=>"MNT","MH"=>"USD","MK"=>"MKD","MU"=>"MUR","MT"=>"EUR","MW"=>"MWK","MV"=>"MVR","MQ"=>"EUR","MP"=>"USD","MS"=>"XCD","MR"=>"MRO","IM"=>"GBP","UG"=>"UGX","TZ"=>"TZS","MY"=>"MYR","MX"=>"MXN","IL"=>"ILS","FR"=>"EUR","IO"=>"USD","SH"=>"SHP","FI"=>"EUR","FJ"=>"FJD","FK"=>"FKP","FM"=>"USD","FO"=>"DKK","NI"=>"NIO","NL"=>"EUR","NO"=>"NOK","NA"=>"NAD","VU"=>"VUV","NC"=>"XPF","NE"=>"XOF","NF"=>"AUD","NG"=>"NGN","NZ"=>"NZD","NP"=>"NPR","NR"=>"AUD","NU"=>"NZD","CK"=>"NZD","XK"=>"EUR","CI"=>"XOF","CH"=>"CHF","CO"=>"COP","CN"=>"CNY","CM"=>"XAF","CL"=>"CLP","CC"=>"AUD","CA"=>"CAD","CG"=>"XAF","CF"=>"XAF","CD"=>"CDF","CZ"=>"CZK","CY"=>"EUR","CX"=>"AUD","CR"=>"CRC","CW"=>"ANG","CV"=>"CVE","CU"=>"CUP","SZ"=>"SZL","SY"=>"SYP","SX"=>"ANG","KG"=>"KGS","KE"=>"KES","SS"=>"SSP","SR"=>"SRD","KI"=>"AUD","KH"=>"KHR","KN"=>"XCD","KM"=>"KMF","ST"=>"STD","SK"=>"EUR","KR"=>"KRW","SI"=>"EUR","KP"=>"KPW","KW"=>"KWD","SN"=>"XOF","SM"=>"EUR","SL"=>"SLL","SC"=>"SCR","KZ"=>"KZT","KY"=>"KYD","SG"=>"SGD","SE"=>"SEK","SD"=>"SDG","DO"=>"DOP","DM"=>"XCD","DJ"=>"DJF","DK"=>"DKK","VG"=>"USD","DE"=>"EUR","YE"=>"YER","DZ"=>"DZD","US"=>"USD","UY"=>"UYU","YT"=>"EUR","UM"=>"USD","LB"=>"LBP","LC"=>"XCD","LA"=>"LAK","TV"=>"AUD","TW"=>"TWD","TT"=>"TTD","TR"=>"TRY","LK"=>"LKR","LI"=>"CHF","LV"=>"EUR","TO"=>"TOP","LT"=>"LTL","LU"=>"EUR","LR"=>"LRD","LS"=>"LSL","TH"=>"THB","TF"=>"EUR","TG"=>"XOF","TD"=>"XAF","TC"=>"USD","LY"=>"LYD","VA"=>"EUR","VC"=>"XCD","AE"=>"AED","AD"=>"EUR","AG"=>"XCD","AF"=>"AFN","AI"=>"XCD","VI"=>"USD","IS"=>"ISK","IR"=>"IRR","AM"=>"AMD","AL"=>"ALL","AO"=>"AOA","AQ"=>"","AS"=>"USD","AR"=>"ARS","AU"=>"AUD","AT"=>"EUR","AW"=>"AWG","IN"=>"INR","AX"=>"EUR","AZ"=>"AZN","IE"=>"EUR","ID"=>"IDR","UA"=>"UAH","QA"=>"QAR","MZ"=>"MZN"];

        $this->db->select("meta_location.*");
        $this->db->from("posts");
        $this->db->join("post_location", "post_location.post_id = posts.post_id");
        $this->db->join("meta_location", "meta_location.id = post_location.country_id");
        $this->db->where("posts.post_id = " . $post_id);
        $res = $this->db->get()->result();
        if ($res) {
            $res = $res[0];
            return $equivalent_currency_json[$res->iso];
        }
        else {
            return "USD";
        }
        
    }

    // # API
    public function check_category($cat_id, $market_place_id)
    {
        $this->db->select("COUNT(category_id) as total");
        $this->db->where("category_id", $cat_id);
        $this->db->where("market_place_id", $market_place_id);
        $res = $this->db->get("categories")->result()[0]->total;
        if($res >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public function save_files($files, $post_id)
    {
        # Delete all files connected to this post then save new
        $this->db->select("posts_file_id");
        $this->db->from("posts_files");
        $this->db->where("post_id", $post_id);
        $res = $this->db->get()->result();
        foreach ($res as $file) {
            $this->delete_file($file->posts_file_id);
        }

        $return = true;
        foreach ($files as $filename) {
            $this->table = "posts_files";
            $data = array();
            $data['filename'] = $filename;
            $data['post_id'] = $post_id;
            if(!$this->add($data))
            {
                $return = false;
            }
        }
        return $return;
    }

    public function delete_file($posts_file_id)
    {
        $this->db->select("posts_files.filename, posts.market_place_id");
        $this->db->from("posts_files");
        $this->db->join("posts", "posts.post_id = posts_files.post_id");
        $this->db->where("posts_file_id", $posts_file_id);
        $query = $this->db->get();
        $file = $query->result();
        $res = false;
        if ($file) {
            $file = $file[0];
            if (is_file('uploads/' . getDirPerMarketPlace($file->market_place_id) . str_replace(" ", "_", $file->filename))) {
                unlink('uploads/' . getDirPerMarketPlace($file->market_place_id) . str_replace(" ", "_", $file->filename));
            }
            $this->db->where("posts_file_id", $posts_file_id);
            $res = $this->db->delete("posts_files");
        }

        return $res;
    }

    public function getPostsFile($posts_file_id)
    {
        $this->db->from("posts_files");
        $this->db->where("posts_file_id", $posts_file_id);
        $query = $this->db->get();
        $file = $query->result();
        $res = [];
        if ($file) {
            $res = $file[0];
        }
        return $res;
    }

    public function batch_upload($files = [], $subfolder = "posts")
    {
        if($files == [] || $files == null ) return []; # Immediately returns an empty array if a parameter is not provided or key is not existing with the help of @ operator. Example @$_FILES['nonexistent_key']
        # Defaults
        // $k = key($files); # Gets the `key` of the uplaoded thing on your form
        $k = "files";
        $uploaded_files = []; # Initialize empty return array
        $upload_path = 'uploads/' . $subfolder;
        # Configs
        $config['upload_path'] = $upload_path; # Set upload path
        #$config['allowed_types'] = 'gif|jpg|jpeg|png'; # NOTE: Change this as needed
        $config['allowed_types'] = '*'; # Van change for android fix
        # Folder creation
        if (!is_dir($upload_path) && !mkdir($upload_path, DEFAULT_FOLDER_PERMISSIONS, true)){
          mkdir($upload_path, DEFAULT_FOLDER_PERMISSIONS, true); # You can set DEFAULT_FOLDER_PERMISSIONS constant in application/config/constants.php
        }

        foreach ($files['name'] as $key => $image) {
          $_FILES[$k]['name'] = $files['name'][$key];
          $_FILES[$k]['type'] = $files['type'][$key];
          $_FILES[$k]['tmp_name'] = $files['tmp_name'][$key];
          $_FILES[$k]['error'] = $files['error'][$key];
          $_FILES[$k]['size'] = $files['size'][$key];
          $filename = time() . "_" . $files['name'][$key]; # Renames the filename into timestamp_filename
          $images[] = $uploaded_files[$k][] = $filename; # Appends all filenames to our return array with the key
          $config['file_name'] = $filename;
          $this->upload->initialize($config);
          $this->upload->do_upload($k);
        //   if (!$this->upload->do_upload($k))
        //   {
        //       $error = array('error' => $this->upload->display_errors());
              
        //       var_dump($error);
        //       var_dump($this->upload->data()); die();
        //   }
        //   else
        //   {
        //       var_dump($this->upload->data()); die();
        //   } VAN - fixed a bug from android that is causing error
        }
        return $uploaded_files;
    }

    public function single_upload($file_key, $dir)
    {
        $this->upload_dir = $dir;
        return $this->model->upload("thumbnail");
    }

    public function all_posts($loggedin_user_id, $market_place_id, $category_id, $country_id, $city_id)
    {
        $final = array();

        if ($category_id && $category_id != "all") {
            $this->db->where("posts.category_id", $category_id);
        }

        if ($market_place_id || $market_place_id == "all") {
            $this->db->where("posts.market_place_id", $market_place_id);
        }

        if ($country_id && $country_id != "all") {
            $this->db->group_start();
            $this->db->where("post_location.country_id", $country_id);
            $this->db->or_where("post_location.country_id IS NULL");
            $this->db->or_where("post_location.country_id", 0);
            $this->db->group_end();
        }

        if ($city_id && $city_id != "all") {
            $this->db->group_start();
            $this->db->where("post_location.city_id", $city_id);
            $this->db->or_where("post_location.city_id IS NULL");
            $this->db->or_where("post_location.city_id", 0);
            $this->db->group_end();
        }

        $this->db->select('posts.*, post_location.country_id, post_location.city_id, loc_country.local_name AS country_name, loc_city.local_name AS city_name');
        $this->db->from('posts');
        $this->db->join('post_location', 'post_location.post_id = posts.post_id', 'left');
        $this->db->join('meta_location loc_country', 'loc_country.id = post_location.country_id', 'left');
        $this->db->join('meta_location loc_city', 'loc_city.id = post_location.city_id', 'left');;
        $this->db->order_by('posts.post_id', 'DESC');
        $res = $this->db->get()->result();
        foreach ($res as $obj) {
            # Remove null values
            $obj->country_id = ($obj->country_id) ? $obj->country_id : "";
            $obj->city_id = ($obj->city_id) ? $obj->city_id : "";
            $obj->country_name = ($obj->country_name) ? $obj->country_name : "";
            $obj->city_name = ($obj->city_name) ? $obj->city_name : "";
            # / Remove null values
            
            $obj->likes = $this->getNumberOfLikes($obj->post_id);
            if ($loggedin_user_id) {
                $obj->is_liked = ($this->is_liked(array("user_id" => $loggedin_user_id, "post_id" => $obj->post_id))) ? true : false;
            }
            $currency = $this->get_currency($obj->post_id);
            $obj->price_formatted = $currency . " ".number_format($obj->price, 2);
            $obj->thumbnail = base_url("uploads/".getDirPerMarketPlace($obj->market_place_id).$obj->thumbnail);
            $this->db->select("user_id, username, CONCAT(fname, ' ', lname) as fullname, email, profile_pic");
            $this->db->where("user_id", $obj->user_id);
            $obj->user_details = (@$this->db->get("app_users")->result()[0])?: (object)[];
            if(@$obj->user_details->profile_pic == null)
            {
                $obj->user_details->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $obj->user_details->profile_pic = base_url("uploads/user_profile_pic/" . $obj->user_details->profile_pic);
            }

            $final[] = $obj;
        }
        // var_dump($final); die();
        return $final;
    }

    public function allByUser($user_id, $viewer_id, $market_place_id, $category_id)
    {
        $this->paginate();
        $final = array();
        $this->db->where("user_id", $user_id);
        $this->db->order_by('post_id', 'DESC');

        if ($market_place_id || $market_place_id == "all") {
            $this->db->where("market_place_id", $market_place_id);
        }

        if ($category_id) {
            $this->db->where("category_id", $category_id);
        }

        $res = $this->db->get($this->table)->result();
        foreach ($res as $obj) {
            $obj->likes = $this->getNumberOfLikes($obj->post_id);
            if ($viewer_id) {
                $obj->is_liked = ($this->is_liked(array("user_id" => $viewer_id, "post_id" => $obj->post_id))) ? true : false;
            }
            $currency = $this->get_currency($obj->post_id);
            $obj->price_formatted = $currency . " " . number_format($obj->price, 2);
            $obj->thumbnail = base_url("uploads/".getDirPerMarketPlace($obj->market_place_id).$obj->thumbnail);
            $final[] = $obj;
        }

        return $final;
    }

    public function get($id, $user_id = 0)
    {
        $this->db->select('posts.*, post_location.country_id, post_location.city_id, loc_country.local_name AS country_name, loc_city.local_name AS city_name, loc_country.iso AS country_code');
        $this->db->from('posts');
        $this->db->join('post_location', 'post_location.post_id = posts.post_id', 'left');
        $this->db->join('meta_location loc_country', 'loc_country.id = post_location.country_id', 'left');
        $this->db->join('meta_location loc_city', 'loc_city.id = post_location.city_id', 'left');
        $this->db->where('posts.post_id', $id);
        $this->db->order_by('posts.post_id', 'DESC');
        $obj = $this->db->get()->result()[0];
        $obj->thumbnail = base_url("uploads/".getDirPerMarketPlace($obj->market_place_id).$obj->thumbnail);
        $obj->thumbnail_url = $obj->thumbnail;
        $currency = $this->get_currency($obj->post_id);
        $obj->price_formatted = $currency . " " . number_format($obj->price, 2);

        /* Get category name */
        $this->db->where("category_id", $obj->category_id);
        $obj->category_name = $this->db->get("categories")->result()[0]->name;

        # Remove null values
        $obj->country_id = ($obj->country_id) ? $obj->country_id : "";
        $obj->city_id = ($obj->city_id) ? $obj->city_id : "";
        $obj->country_name = ($obj->country_name) ? $obj->country_name : "";
        $obj->city_name = ($obj->city_name) ? $obj->city_name : "";
        $obj->country_code = ($obj->country_code) ? $obj->country_code : "";
        # / Remove null values

        /**
         * get no. of likes
         */
        $obj->likes = $this->getNumberOfLikes($id);

        /* Check if current user like this post */
        $obj->is_liked = ($this->is_liked(array("user_id" => $user_id, "post_id" => $id))) ? true : false;

        /* Get user details */
        $this->db->where('user_id', $obj->user_id);
        $obj->user = $this->db->get('app_users')->result()[0];
        if($obj->user->profile_pic == null || $obj->user->profile_pic == "")
        {
            $obj->user->profile_pic = base_url("uploads/user_profile_pic/default.png");
        }
        else {
            $obj->user->profile_pic = base_url("uploads/user_profile_pic/" . $obj->user->profile_pic);   
        }

        $this->db->where("post_id", $obj->post_id);
        $obj->files = array();
        $res_files = $this->db->get("posts_files")->result();

        foreach ($res_files as $file_obj) {
            $file_obj->filename = base_url("uploads/".getDirPerMarketPlace($obj->market_place_id).$file_obj->filename);
            $obj->files[] = $file_obj;
        }
        // var_dump($obj); die();
        return array("0" => $obj);
    }

    public function verifyPost($data)
    {
        $this->db->where($this->table_pk, $data['post_id']);
        $this->db->where("user_id !=", $data['user_id']);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    /**
     * [Like a post]
     * @param  [array] $data [user_id, post_id]
     * @return [array]       [data, msg, status]
     */
    public function likePost($data)
    {
        if ($this->verifyPost($data)) {
            if ($this->is_liked($data)) {
                # unlike
                if ($this->unlike($data)) {
                    $res['data']['post'] = $this->get($data['post_id'])[0];
                    $res['msg'] = "Post unliked";
                    $res['status'] = 200;
                }
                else {
                    $res['data']['post'] = "";
                    $res['msg'] = "Unlike failed";
                    $res['status'] = 400;
                }
            }
            else {
                # like
                if ($this->like($data)) {
                    $res['data']['post'] = $this->get($data['post_id'])[0];
                    $res['msg'] = "Post liked";
                    $res['status'] = 200;
                }
                else {
                    $res['data']['post'] = "";
                    $res['msg'] = "Like failed";
                    $res['status'] = 400;
                }
            }
        }
        else {
            $res['data']['post'] = "";
            $res['msg'] = "Invalid post";
            $res['status'] = 400;
        }

        return $res;
    }

    public function is_liked($data)
    {
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('post_id', $data['post_id']);
        $res = $this->db->get('post_like')->result();
        return $res;
    }

    public function like($data)
    {
        return $this->db->insert("post_like", $data);
    }

    public function unlike($data)
    {
        $this->db->where('user_id', $data['user_id']);
        $this->db->where('post_id', $data['post_id']);
        return $this->db->delete("post_like");
    }

    public function getLikedPosts($user_id, $category_id = null, $order = 'DESC')
    {
        $final = array();
        $this->db->select('posts.*, post_like.created_at AS date_liked');
        $this->db->from('post_like');
        $this->db->join('posts', 'posts.post_id = post_like.post_id');
				if ($category_id) {
					# add this to filter by category id
					$this->db->where('posts.category_id', $category_id);
				}
        $this->db->where('post_like.user_id', $user_id);
        $this->db->order_by('post_like.created_at', $order);
        $query = $this->db->get();
        $res = $query->result();

        foreach ($res as $obj) {
            $currency = $this->get_currency($obj->post_id);
            $obj->price_formatted = $currency . " " . number_format($obj->price, 2);
            $obj->thumbnail = base_url("uploads/" . getDirPerMarketPlace($obj->market_place_id) . $obj->thumbnail);
            $obj->date_posted_f = date("m/d/y | h:i A", strtotime($obj->created_at));
            $obj->date_liked_f = date("m/d/y | h:i A", strtotime($obj->date_liked));

            $user = $this->user_model->getUser($obj->user_id);
            $obj->user_details = new stdClass;
            $obj->user_details->user_id = $user->user_id;
            $obj->user_details->username = $user->username;
            $obj->user_details->fullname = $user->fname . " " . $user->lname;
            $obj->user_details->email = $user->email;
            $obj->user_details->profile_pic = ($user->profile_pic == null) ? base_url("uploads/user_profile_pic/default.png") : $user->profile_pic;

            $final[] = $obj;

        }

        return $final;
    }

		/**
		 * @author @jjjjcccjjf
		 */
		public function getPostOwner($post_id)
		{
			$user_id = @$this->db->get_where($this->table, ['post_id' => $post_id])->row()->user_id;
			return $this->db->get_where('app_users', ['user_id' => $user_id])->row();
		}

    /**
     * Get all posts of all followed users
     * Order by date of posts
     */
    public function getPostFollow($user_id, $market_place_id, $category_id, $country_id, $city_id)
    {
        $this->db->reset_query();
        $this->paginate();
        $final = array();

        if ($category_id && $category_id != "all") {
            $this->db->where("posts.category_id", $category_id);
        }

        if ($market_place_id || $market_place_id == "all") {
            $this->db->where("posts.market_place_id", $market_place_id);
        }

        if ($country_id && $country_id != "all") {
            $this->db->group_start();
            $this->db->where("post_location.country_id", $country_id);
            $this->db->or_where("post_location.country_id IS NULL");
            $this->db->or_where("post_location.country_id", 0);
            $this->db->group_end();
        }

        if ($city_id && $city_id != "all") {
            $this->db->group_start();
            $this->db->where("post_location.city_id", $city_id);
            $this->db->or_where("post_location.city_id IS NULL");
            $this->db->or_where("post_location.city_id", 0);
            $this->db->group_end();
        }

        $this->db->select('posts.*, post_location.country_id, post_location.city_id, loc_country.local_name AS country_name, loc_city.local_name AS city_name');
        $this->db->from('posts');
        $this->db->join('follow', 'follow.following_id = posts.user_id');
        $this->db->join('post_location', 'post_location.post_id = posts.post_id', 'left');
        $this->db->join('meta_location loc_country', 'loc_country.id = post_location.country_id', 'left');
        $this->db->join('meta_location loc_city', 'loc_city.id = post_location.city_id', 'left');;
        $this->db->where('follow.follower_id = ' . $user_id);
        $this->db->order_by('posts.post_id', 'DESC');
        $result = $this->db->get()->result();
        foreach ($result as $post) {
            # Remove null values
            $post->country_id = ($post->country_id) ? $post->country_id : "";
            $post->city_id = ($post->city_id) ? $post->city_id : "";
            $post->country_name = ($post->country_name) ? $post->country_name : "";
            $post->city_name = ($post->city_name) ? $post->city_name : "";
            # / Remove null values

            $this->db->select("user_id, username, CONCAT(fname, ' ', lname) as fullname, email, profile_pic");
            $this->db->where("user_id", $post->user_id);
            $post->user_details = $this->db->get("app_users")->result()[0];
            if($post->user_details->profile_pic == null)
            {
                $post->user_details->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $post->user_details->profile_pic = base_url("uploads/user_profile_pic/" . $post->user_details->profile_pic);
            }
            $currency = $this->get_currency($post->post_id);
            $post->price_formatted = $currency . " " . number_format($post->price, 2);
            $post->thumbnail = base_url("uploads/".getDirPerMarketPlace($post->market_place_id).$post->thumbnail);

            /**
             * Get number of likes
             */
            $post->likes = $this->getNumberOfLikes($post->post_id);

            /**
             * get if is liked
             */
            $post->is_liked = ($this->is_liked(array("user_id" => $user_id, "post_id" => $post->post_id))) ? true : false;

            $final[] = $post;
        }
        return $final;
    }

    public function paginate($table = "posts")
    {
        if (@$this->input->get('page') ?: $_GET['page'] = 1){
            $per_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : $this->num_per_page; # Make 10 default $per_page if $per_page is not set
            $offset = ($_GET['page'] - 1) * $per_page;
            $this->db->limit($per_page, $offset);
        }
        if($this->input->get('date_order'))
        {
            $this->db->order_by($table.".created_at", $this->input->get('date_order'));
        }
    }

    public function getNumberOfLikes($post_id)
    {
        $this->db->where('post_id', $post_id);
        $this->db->from('post_like');
        return $this->db->count_all_results();
    }

    function cmp($a, $b)
    {
        return strcmp($b->created_at, $a->created_at);
    }

    public function updatePost($post_id, $data)
    {
        if (($data['country_id'] || $data['country_id'] == 0) && ($data['city_id'] || $data['city_id'] == 0)) {
            $post_loc_data['post_id'] = $post_id;
            $post_loc_data['country_id'] = $data['country_id'];
            $post_loc_data['city_id'] = $data['city_id'];
            unset($data['country_id']);
            unset($data['city_id']);
            if ($this->get_post_location($post_id)) {
                $this->update_post_location($post_loc_data);
            }
            else {
                $this->add_post_location($post_loc_data);
            }
        }
        
        return $this->db->update($this->table, $data, array($this->table_pk => $post_id));
    }

    /**
     * $post_id, $country_id, $city_id
     */
    public function add_post_location($data)
    {
        return $this->db->insert("post_location", $data);
    }

    /**
     * $post_id, $country_id, $city_id
     */
    public function update_post_location($data)
    {
        return $this->db->update("post_location", $data, array("post_id" => $data['post_id']));
    }

    public function get_post_location($post_id)
    {
        $this->db->where("post_id", $post_id);
        $res = $this->db->get("post_location")->result();
        return $res;
    }
}
?>
