<?php

class Settings_model extends CI_model
{
  public function __construct()
  {
    parent::__construct();
    $this->table = "settings";
    $this->table_pk = "settings_id";
  }

  public function updateSettings($user_id, $data)
  {
    if ($this->getSettings($user_id)) {
      # update
      $this->db->where('user_id', $user_id);
      return $this->db->update('settings', $data);
    }
    else {
      # insert
      return $this->db->insert('settings', $data);
    }
  }

  public function getSettings($user_id)
  {
    $this->db->where("user_id", $user_id);
    $res = $this->db->get($this->table)->result();
    if (!count($res)) {
      $this->insertSettings(array('user_id' => $user_id, 'push_notification' => 1, 'email_notification' => 1));
      $res = $this->getSettings($user_id);
    }
    return $res;
  }

  public function insertSettings($data)
  {
    $this->db->insert('settings', $data);
    return $this->db->insert_id();
  }

}

?>
