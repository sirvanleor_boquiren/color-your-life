<?php
class Plans_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "membership_plans";
        $this->table_pk = "membership_plan_id";

        $this->points = array();
        $this->points[1] = 0; # Free membership = 0 points
        $this->points[2] = 20; # Challenger membership = 20 points
        $this->points[3] = 2; # Economy membership = 2 points
        $this->points[4] = 10; # Business membership = 10 points

        $this->plan = array();
        $this->plan[1] = 'free';
        $this->plan[2] = 'challenger';
        $this->plan[3] = 'economy';
        $this->plan[4] = 'business';

        $this->load->model('settings_model');
        $this->load->model('user_model', 'user_model');
        $this->load->model('payment_model');
    }

    public function all()
    {
    	$query = $this->db->get($this->table, 4);
    	$result = $query->result();
    	return $result;
    }

    public function details()
    {
    	$plans = $this->all();
    	$ctr = 0;
    	foreach ($plans as $plan) {
    		$data[$ctr] = new stdClass();
    		$data[$ctr]->name = $plan->name;
    		if ($plan->rate == 0) {
    			$data[$ctr]->rate = "Free for 3 MOS";
    		}
    		else {
    			$data[$ctr]->rate = "$" . $plan->rate . " / " . $plan->rate_type;	
    		}
    		$items = $this->itemsPerPlan($plan->membership_plan_id);
    		foreach ($items as $item) {
    			$temp_item = new stdClass();
    			$temp_item->type = $item->type;
    			$temp_item->text = $item->content;
    			$data[$ctr]->items[] = $temp_item;
    		}
    		
    		$ctr++;
    	}
    	return $data;
    }

    public function itemsPerPlan($membership_plan_id)
    {
    	$this->db->where("membership_plan_id", $membership_plan_id);
    	$query = $this->db->get("membership_plan_items");
    	$result = $query->result();
    	return $result;
    }

    public function getPlan($membership_plan_id)
    {
    	$this->db->where("membership_plan_id", $membership_plan_id);
    	$query = $this->db->get("membership_plans");
    	$result = $query->result()[0];
    	return $result;
    }

    public function getRank($rank_id)
    {
        $this->db->where("color_rank_id", $rank_id);
        $query = $this->db->get("color_rank");
        $result = $query->result()[0];
        return $result;
    }

    public function getActivePlan($user_id)
    {
        $this->db->where("user_id", $user_id);
        $this->db->where("status", "active");
        $this->db->where("expiration_date > ", date("Y-m-d"));
        $this->db->order_by("created_at", "ASC");
        $this->db->limit(1);
        $query = $this->db->get("user_membership_plan");
        $result = $query->result();
        if ($result) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    public function getCurrentPlan($user_id)
    {
        $this->db->where("user_id", $user_id);
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get("user_membership_plan");
        $result = $query->result();
        if ($result) {
            return $result[0];
        }
        else {
            return false;
        }
    }

    /**
     * if 0 rate return blank url
     * else return membership payment url
     */
    public function verifyMembership($data)
    {
    	$membership_plan = $this->getPlan($data['membership_plan_id']);

    	
    	# check if user exists
    	if (!$this->user_model->getUser($data['user_id']))
    	{
    		return false;
    	}

        /**
         * check if there's prev plan for this user
         * because user can't renew the plan as free member
         */
        $prev_plan = $this->getCurrentPlan($data['user_id']);
        if ($prev_plan && $data['membership_plan_id'] == 1) {
            return false;
        }

    	if ($membership_plan->rate == 0) {
    		$res = $this->applyMembership($data['user_id'], $data['membership_plan_id']);
    		if ($res) {
    			$result['url'] = "";
    			$result['membership_name'] = $membership_plan->name;

                $current_plan = $this->getCurrentPlan($data['user_id']);
                $result['membership_expiry'] = $current_plan->expiration_date;
                if ($current_plan->expiration_date == "0000-00-00 00:00:00") {
                    $result['membership_expiry'] = date("Y-m-d H:i:s", strtotime(date("Y-m-d") . "+30 days"));
                }

                $temp = new stdClass();
                $temp->market_place_access = $this->user_model->getMarketPlaceAccess($data['user_id']); # Get market place access
                $temp->chat_function = $this->user_model->chatFunction($data['user_id']); # Get chat function access
                $temp->color_bonus_access = $this->user_model->colorBonusAccess($data['user_id']); # Get color bonus access
                $temp->color_presents_access = $this->user_model->colorPresentsAccess($data['user_id']); # Get color presents access
                $temp->remaining_advertisement = $this->user_model->getRemainingOf($data['user_id'], 'advertisement'); # Get remaining advertisements
                $temp->remaining_video = $this->user_model->getRemainingOf($data['user_id'], 'video_recording_dist'); # Get remaining video recording distribution
                $temp->remaining_group_message = $this->user_model->getRemainingOf($data['user_id'], 'group_message'); # Get remaining group message
                $temp->remaining_live_webinar = $this->user_model->getRemainingOf($data['user_id'], 'live_webinar'); # Get remaining live webinar

                $result['privileges'] = $temp;

    		}
    		else {
    			$result = false;
    		}
    	}
    	else {
    		$result['url'] = base_url("membership/" . $data['user_id'] . "/" . $membership_plan->membership_plan_id);
    		$result['membership_name'] = $membership_plan->name;
            $result['plan_rate'] = number_format($membership_plan->rate, 2);
    	}
    	return $result;
    }

    public function applyNow($user_id)
    {
        $current_plan = $this->getCurrentPlan($user_id);
        $membership_plan = $this->getPlan($current_plan->membership_plan_id);

        $this->db->where("user_id = '" . $user_id . "' AND user_membership_plan_id != '" . $current_plan->user_membership_plan_id . "' AND (status = 'active' OR status = '')");
        $query = $this->db->get('user_membership_plan');
        $result = $query->result();
        $final = array();
        if ($result) {
            foreach ($result as $key => $val) {
                $final[$key]['membership_plan_id'] = $val->membership_plan_id;
                $final[$key]['membership_name'] = $this->getPlan($val->membership_plan_id)->name;
                $final[$key]['expiration_date'] = $val->expiration_date;
            }
        }

        # change status of prev plans (expired)
        $this->db->where("user_id = '" . $user_id . "' AND user_membership_plan_id != '" . $current_plan->user_membership_plan_id . "' AND (status = 'active' OR status = '') AND expiration_date < '" . date("Y-m-d H:i:s") . "'");
        $this->db->update('user_membership_plan', ["status" => "expired"]);
        $update1 = $this->db->affected_rows();

        # change status of prev plans
        $this->db->where("user_id = '" . $user_id . "' AND user_membership_plan_id != '" . $current_plan->user_membership_plan_id . "' AND (status = 'active' OR status = '')");
        $this->db->update('user_membership_plan', ["status" => "cancelled"]);
        $update2 = $this->db->affected_rows();
        
        if ($update1 || $update2) {
            # update expiration date of current plan in user_membership_plan table
            $start_date = date("Y-m-d H:i:s");
            if ($membership_plan->rate_type == "monthly") {
                $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($start_date)));
            }
            elseif ($membership_plan->rate_type == "yearly") {
                $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+12 months", strtotime($start_date)));
            }
            else { # something should catch if no coditions are true
                $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($start_date)));
            }
            $this->db->where("user_membership_plan_id = '" . $current_plan->user_membership_plan_id . "'");
            $this->db->update('user_membership_plan', $data);

            # change membership_plan_id in app_users table
            $this->db->update('app_users', ['membership_plan_id' => $current_plan->membership_plan_id], ['user_id' => $user_id]);
        }

        return $final;
    }

    public function firstMembershipFromPaynamics($user_id)
    {
        $this->db->where("user_id", $user_id);
        $this->db->limit(1);
        $res = $this->db->get("paynamics_requests")->result();
        if ($res) {
            $res = $res[0];
            if ($res->status == "paid" && $res->apply_now == "1") {
                $res = true;
            }
            else {
                $res = false;
            }
        }

        return $res;
    }

    /**
     * Apply membership to the user
     */
    public function applyMembership($user_id, $membership_plan_id)
    {
        $membership_plan = $this->getPlan($membership_plan_id);
        $prev_plan = $this->getCurrentPlan($user_id);      

        $data['user_id'] = $user_id;
        $data['membership_plan_id'] = $membership_plan->membership_plan_id;
        $data_users['membership_plan_id'] = $membership_plan->membership_plan_id; # for app_users

        # if membership plan is 0 / Free
        if ($membership_plan->rate == 0) {
            $this->db->update('app_users', ['membership_plan_id' => $membership_plan_id], ['user_id' => $user_id]);
            $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+3 months"));
        }
        else {
            $start_date = date("Y-m-d H:i:s");
            if (@$prev_plan) {
                # check current plan
                if ($prev_plan->status == "active" || $prev_plan->expiration_date >= date("Y-m-d H:i:s")) {
                    $start_date = $prev_plan->expiration_date;
                }
            }

            if ($membership_plan->rate_type == "monthly") {
                $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($start_date)));
            }
            elseif ($membership_plan->rate_type == "yearly") {
                $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+12 months", strtotime($start_date)));
            }
            else { # something should catch if no coditions are true
                $data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($start_date)));
            }
        }

        # prepare data
        $data['status'] = "active";
        $data['market_place_access'] = ($membership_plan->market_place_access == "-1") ? "-1" : "0";
        $data['chat_function'] = $membership_plan->chat_function;
        $data['points_redeem_start'] = $membership_plan->points_redeem_start;
        $data['advertisement'] = $membership_plan->advertisement;
        $data['video_recording_dist'] = $membership_plan->video_recording_dist;
        $data['group_message'] = $membership_plan->group_message;
        $data['live_webinar'] = $membership_plan->live_webinar;
        $data['special_bonuses'] = $membership_plan->special_bonuses;
        $data['presents'] = $membership_plan->presents;

        # insert into user_membership_plan
        if ($this->db->insert("user_membership_plan", $data)) {
            
            /**
             * add points to the user with referral code
             * if user register or renew his membership
             * if referred by is not 0
             */
            $user = $this->user_model->getUser($user_id);
            if ($user->referred_by != 0) {
                /**
                 * Calculate points by getting the plan of the referral
                 * and the rank of the referrer when the referral is referred XD
                 */
                $referrer_plan = $this->getRank($user->referrer_rank);
                /*        user plan rate           referrer rank rate          */
                $points = $membership_plan->rate * $referrer_plan->rate * 0.01;

                if ($this->addPoints($user->referred_by, $points)) {
                    # Send in-app notification
                    $this->sendNotification("Referral", $user->referred_by, "referral", ['status' => 'success', 'referred_id' => $user_id]);

                    # Color points logs
                    $this->logPoints($user->referred_by, $user_id, $points);
                }
            }
            
            /**
             * add bonus/referral count in color_bonus table
             */
            $first_log_paynamics = $this->firstMembershipFromPaynamics($user_id);
            if ($user->first_log == "yes" || $first_log_paynamics) {
                $this->user_model->update($user_id, ['first_log' => 'no', 'membership_plan_id' => $membership_plan_id]);
                if ($user->referred_by != 0) {

                    if ($first_log_paynamics) { # deduct 1 points from temporary free
                        $this->db->where('user_id', $user->referred_by);
                        $this->db->set("free", "free - 1", FALSE);
                        if ($this->db->update('color_bonus')) {
                            # check if user is qualified for bonus reward then deduct that points
                            $this->reverseColorBonus($user->referred_by, "1", $user_id);
                        }
                    }

                    $plan = $this->plan[$membership_plan_id];
                    $this->db->where('user_id', $user->referred_by);
                    $this->db->set($plan, "$plan + 1", FALSE);
                    if ($this->db->update('color_bonus')) {
                        # check if user is qualified for bonus reward
                        $this->colorBonus($user->referred_by, $membership_plan_id, $user_id);
                    }

                    if (!$first_log_paynamics) { # na dodoble kasi :(
                        $this->addReferralCount($user->referred_by);
                    }
                }
            }

            /**
             * Check if membership is not paid and
             * count if the paid membership in user_membership table is only 1 (recently added)
             */
            if ($membership_plan_id != 1 && $this->countPaidMembership($user_id) == 1) {
                if ($user->referred_by != 0) {
                    # add rank up referral
                    $this->addRankUpReferralCount($user->referred_by);

                    # check if user is qualified to rank up
                    $this->colorRank($user->referred_by);
                }
            }

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Apply membership to the user
     */
    public function applyMembership_backupcode($user_id, $membership_plan_id)
    {
    	$membership_plan = $this->getPlan($membership_plan_id);
        $prev_plan = $this->getCurrentPlan($user_id);
        
        /**
         * Check if upgrade/downgrade/same
         * upgrade = cut current plan then apply selected plan
         * downgrade = if current plan is active, add selected plan after the current plan
         * same = if current plan is active, add selected plan after the current plan
         */
        $renewal_type = "new";
        if ($prev_plan) {
            $renewal_type = "same";
            if ($prev_plan->membership_plan_id < $membership_plan_id) {
                # upgrade plan
                $renewal_type = "upgrade";
            }
            elseif ($prev_plan->membership_plan_id > $membership_plan_id) {
                # downgrade plan
                $renewal_type = "downgrade";
            }
        }

    	$data['user_id'] = $user_id;
    	$data['membership_plan_id'] = $membership_plan->membership_plan_id;
        $data_users['membership_plan_id'] = $membership_plan->membership_plan_id; # for app_users

    	# if membership plan is 0 / Free
    	if ($membership_plan->rate == 0) {
    		$data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+3 months"));
    	}
    	# else
    	else {
            $start_date = date("Y-m-d H:i:s");
            if ($renewal_type == "downgrade" || $renewal_type == "same") {
                # check current plan
                if ($prev_plan->status == "active" || $prev_plan->expiration_date >= date("Y-m-d H:i:s")) {
                    $start_date = $prev_plan->expiration_date;
                }
            }
            elseif ($renewal_type == "upgrade") {
                # change status of current plan
                $this->db->where("user_id = '" . $user_id . "' AND (status = 'active' OR status = '')");
                $this->db->update('user_membership_plan', ["status" => "cancelled"]);
            }

    		if ($membership_plan->rate_type == "monthly") {
    			$data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($start_date)));
    		}
    		elseif ($membership_plan->rate_type == "yearly") {
    			$data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+12 months", strtotime($start_date)));
    		}
    		else { # something should catch if no coditions are true
    			$data['expiration_date'] = date("Y-m-d H:i:s", strtotime("+1 months", strtotime($start_date)));
    		}
    	}

        $data['status'] = "active";
    	$data['market_place_access'] = $membership_plan->market_place_access;
    	$data['chat_function'] = $membership_plan->chat_function;
    	$data['points_redeem_start'] = $membership_plan->points_redeem_start;
    	$data['advertisement'] = $membership_plan->advertisement;
    	$data['video_recording_dist'] = $membership_plan->video_recording_dist;
    	$data['group_message'] = $membership_plan->group_message;
    	$data['live_webinar'] = $membership_plan->live_webinar;
    	$data['special_bonuses'] = $membership_plan->special_bonuses;
    	$data['presents'] = $membership_plan->presents;

        # update app_users
        # if type is downgrade, do not update membership plan id in app_users table
        if ($renewal_type != "downgrade") {
            $this->user_model->update($user_id, ['membership_plan_id' => $membership_plan_id]);
        }

    	# insert into user_membership_plan
    	if ($this->db->insert("user_membership_plan", $data)) {
    		
	    	/**
	    	 * add points to the user with referral code
	    	 * if user register or renew his membership
	    	 * if referred by is not 0
	    	 */
	    	$user = $this->user_model->getUser($user_id);
	    	if ($user->referred_by != 0) {
                /**
                 * Calculate points by getting the plan of the referral
                 * and the rank of the referrer when the referral is referred XD
                 */
                $referrer_plan = $this->getRank($user->referrer_rank);
                /*        user plan rate           referrer rank rate          */
                $points = $membership_plan->rate * $referrer_plan->rate * 0.01;

	    		if ($this->addPoints($user->referred_by, $points)) {
                    # Send in-app notification
                    $this->sendNotification("Referral", $user->referred_by, "referral", ['status' => 'success', 'referred_id' => $user_id]);

	    			# Color points logs
	    			$this->logPoints($user->referred_by, $user_id, $points);
	    		}
	    	}
	    	
	    	# update app_users - first_log
    		$update_user['first_log'] = "no";
	    	/**
	    	 * if first_log is already 'no', it will return 0
	    	 * add bonus/referral count in color_bonus table
	    	 */
	    	if ($this->user_model->update($user_id, $update_user))
	    	{
                if ($user->referred_by != 0) {
                    $plan = $this->plan[$membership_plan_id];
                    $this->db->where('user_id', $user->referred_by);
                    $this->db->set($plan, "$plan + 1", FALSE);
                    if ($this->db->update('color_bonus'))
                    {
                        # check if user is qualified for bonus reward
                        $this->colorBonus($user->referred_by, $membership_plan_id, $user_id);
                    }
                    $this->addReferralCount($user->referred_by);
                }
	    	}

            /**
             * Check if membership is not paid and
             * count if the paid membership in user_membership table is only 1 (recently added)
             */
            if ($membership_plan_id != 1 && $this->countPaidMembership($user_id) == 1) {
                if ($user->referred_by != 0) {
                    # add rank up referral
                    $this->addRankUpReferralCount($user->referred_by);

                    # check if user is qualified to rank up
                    $this->colorRank($user->referred_by);
                }
            }

	    	return true;
    	}
    	else {
    		return false;
    	}
    }

    /**
     * Get user
     * including rank of referrer and membership plan
     */
    public function getUser($id)
    {
        $this->db->select('app_users.*, color_rank.color_rank_id, color_rank.rank_type, color_rank.rate AS rank_rate, membership_plans.membership_plan_id, membership_plans.name, membership_plans.rate AS plan_rate');
        $this->db->from('app_users');
        $this->db->join('color_rank', 'color_rank.color_rank_id = app_users.referrer_rank');
        $this->db->join('membership_plans', 'membership_plans.membership_plan_id = app_users.membership_plan_id');
        $this->db->where('app_users.user_id', $id);
        $res = $this->db->get()->result();
        if (!$res) {
            return false;
        }
        else {
            $res = $res[0];
        }
        unset($res->password);
        unset($res->forgot_token);
        if(@$res->profile_pic == null)
        {
            $res->profile_pic = base_url("uploads/user_profile_pic/default.png");
        }
        else {
            $res->profile_pic = base_url("uploads/user_profile_pic/" . $res->profile_pic);
        }

        /**
         * Change NULL values to empty string
         */
        foreach ($res as $key => $value) {
            if ($value == NULL) {
                $res->$key = "";
            }
        }
        
        return $res;
    }

    public function colorRank($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('app_users');
        $result = $query->result()[0];

        # Get rank
        $this->db->where('min_qualification <=', $result->rank_up_referrals);
        $this->db->where('max_qualification >=', $result->rank_up_referrals);
        $query_rank = $this->db->get("color_rank");
        $result_rank = $query_rank->result()[0];

        # Update rank
        $this->db->set('rank', $result_rank->color_rank_id);
        $this->db->where('user_id', $user_id);
        $this->db->update('app_users');
    }

    public function reverseColorBonus($user_id, $membership_plan_id, $referral_id = 0)
    {
        /**
         * get number of referral of user of last updated plan (+1, nag deduct kasi ng 1 referral; see where this functions is used above)
         */
        $num_ref = $this->getReferralsByPlan($user_id, $membership_plan_id) + 1;

        /**
         * Check color_bonus_conversion for points
         */
        if ($points = $this->getEquivalentPoints($num_ref, $membership_plan_id))
        {
            /**
             * Deduct bonus points (reverse nga eh)
             */
            if ($this->deductBonus($user_id, $points)) {
                # Bonus points logs
                $this->logPoints($user_id, $referral_id, "-" . $points, 'bonus (temp free)');
            }
        }
    }

    public function colorBonus($user_id, $membership_plan_id, $referral_id = 0)
    {
    	/**
    	 * get number of referral of user of last updated plan
    	 */
    	$num_ref = $this->getReferralsByPlan($user_id, $membership_plan_id);
    	
    	/**
    	 * Check color_bonus_conversion for points
    	 */
    	if ($points = $this->getEquivalentPoints($num_ref, $membership_plan_id))
    	{
    		/**
	    	 * Add bonus points
	    	 */
	    	if ($this->addBonus($user_id, $points)) {
	    		# Bonus points logs
	    		$this->logPoints($user_id, $referral_id, $points, 'bonus');
	    	}
    	}
    }

    public function getEquivalentPoints($num_ref, $membership_plan_id)
    {
        $result = 0;
        if ($num_ref != 0) {
            switch ($membership_plan_id) {
                case 1: # Free
                    if ($num_ref % 50 == 0) {
                        $result = 10;
                    }
                    return $result;
                    break;

                case 2: # Challenger
                    if ($num_ref % 50 == 0) {
                        $result = 25;
                    }
                    return $result;
                    break;

                case 3: # Economy
                    if ($num_ref % 50 == 0) {
                        $temp_a = $num_ref / 50;
                        if ($temp_a < 3) {
                            $result = 80;
                        }
                        else {
                            $temp_b = $temp_a - 2;
                            $result = 80;
                            for ($i=0; $i < $temp_b; $i++) { 
                                $result *= 2;
                            }
                        }
                    }
                    return $result;
                    break;

                case 4: # Business
                    if ($num_ref % 50 == 0) {
                        $temp_a = $num_ref / 50;
                        $result = 50 + (100 * $temp_a);
                    }
                    return $result;
                    break;
                
                default:
                    return $result;
                    break;
            }
        }
        else {
            return $result; # 0
        }
    }

    public function getReferralsByPlan($user_id, $membership_plan_id)
    {
    	$plan = $this->plan[$membership_plan_id];
    	$this->db->select($plan);
    	$this->db->where('user_id', $user_id);
    	$query = $this->db->get('color_bonus');
        $result = $query->result();
        if ($result) {
            return $result[0]->$plan;
        }
        else {
            return 0;
        }
    }

    public function addPoints($user_id, $points)
    {
    	$this->db->where('user_id', $user_id);
    	$this->db->set('points', "points + $points", FALSE);
    	return $this->db->update('app_users');
    }

    public function addBonus($user_id, $points)
    {
    	$this->db->where('user_id', $user_id);
    	$this->db->set('points', "points + $points", FALSE);
    	$this->db->set('bonus', "bonus + $points", FALSE);
    	return $this->db->update('app_users');
    }

    public function deductBonus($user_id, $points)
    {
        $this->db->where('user_id', $user_id);
        $this->db->set('points', "points - $points", FALSE);
        $this->db->set('bonus', "bonus - $points", FALSE);
        return $this->db->update('app_users');
    }

    public function countPaidMembership($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('membership_plan_id !=', 1);
        $this->db->from('user_membership_plan');
        return $this->db->count_all_results();
    }

    /**
     * add only 1
     */
    public function addReferralCount($user_id)
    {
    	$this->db->where('user_id', $user_id);
    	$this->db->set('referrals', 'referrals + 1', FALSE);
    	return $this->db->update('app_users');
    }

    public function addRankUpReferralCount($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->set('rank_up_referrals', 'rank_up_referrals + 1', FALSE);
        return $this->db->update('app_users');
    }

    /**
     * Logs
     */
    public function logPoints($referrer, $referred, $points_added, $type = 'membership')
    {
    	$data['referrer'] = $referrer;
    	$data['referred'] = $referred;
        $data['type'] = $type;
    	$data['points_added'] = $points_added;
    	return $this->db->insert('points_logs', $data);
    }

    /**
     * Cancel membership of user
     */
    public function cancelMembership()
    {

    }

    /**
     * Update Add-ons in the plan
     */
    public function updateAddonsPlan($payment_logs_id)
    {
        $addons_array = array('ads' => 'advertisement', 'videos' => 'video_recording_dist', 'group_messages' => 'group_message', 'webinars' => 'live_webinar');
        $payment_logs_detail = $this->payment_model->getLogsDetail($payment_logs_id);
        $type = $addons_array[$payment_logs_detail->type];
        $quantity = $payment_logs_detail->quantity;

        $this->db->where('user_membership_plan_id', $payment_logs_detail->user_membership_plan_id);
        $this->db->set($type, "$type + $quantity", FALSE);
        return $this->db->update('user_membership_plan');
    }

    function sendNotification($title = "Color your life", $user_id, $notif_type, $payload)
    {
        /**
         * Check if Settings if push notif is on
         */
        $notif = $this->settings_model->getSettings($user_id);

        if (!@$notif) {
            $sendNotif = 1;
        }
        else {
            $sendNotif = ($notif[0]->push_notification) ? 1 : 0;
        }

        $this->load->model('notification_model');
        $notification_message = $this->notification_model->storeNotif($user_id, $notif_type, $payload);
        if ($sendNotif) {
            $recipients = $this->udi_model->getDeviceIdsByUserId($user_id);
            if ($recipients) {
                foreach ($recipients as $to) {
                    $firebase_response = $this->firebase_model->sendMultiple(
                        $to,
                        ['data' => 
                            (object)[
                                'referral_id' => $payload['referred_id'],
                                'type' => 'new_referral'
                            ]
                        ],
                        ['title' => $title, 'body' => $notification_message]
                    );
                    log_message('error', $firebase_response);
                    log_message('error', $notification_message);
                }
            }
        }

        if (@$notif[0]->email_notification) {
            $this->load->helper("email_template");
            $user_info = $this->user_model->getUser($user_id);
            $referral_info = $this->user_model->getUser($payload['referred_id']);
            $recipient = $user_info->email;
            $subject = "Successful Referral";
            $body = new_referral_template(['fname' => $user_info->fname, 'referral_profile_pic' => $referral_info->profile_pic, 'referral_fullname' => $referral_info->fullname]);
            send_email($this, $recipient, $subject, $body);
        }
    }

}