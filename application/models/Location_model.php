<?php
class Location_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "meta_location";
        $this->table_pk = "id";
    }

    public function getCountries($search_string)
    {
        $this->db->select("id, iso AS country_code, local_name AS country, type");

        if ($search_string != "") {
            $this->db->where("(iso LIKE '%" . $search_string . "%' OR local_name LIKE '%" . $search_string . "%')");
        }

        $this->db->where("type", "CO");
        $this->db->limit(0);
        $this->db->order_by("local_name", "ASC");
        $res = $this->db->get($this->table)->result();
        
        # Add all countries option at the beginning of res
        if ($search_string == "") {
            $temp_obj = new stdClass();
            $temp_obj->id = "0";
            $temp_obj->country_code = "all";
            $temp_obj->country = "All Countries";
            $temp_obj->type = "";
            array_unshift($res, $temp_obj);
        }
        # / Add all countries option at the beginning of res

        return $res;
    }

    public function getCities($country_code, $search_string)
    {
        $this->db->select("id, iso, local_name AS city, type");

        if ($search_string != "") {
            $this->db->where("(local_name LIKE '%" . $search_string . "%')");
        }

        $this->db->where("(iso LIKE '" . $country_code . "%')");
        $this->db->where("(type = 'CI' OR type = 'RE')");
        $this->db->order_by("local_name", "ASC");
        $this->db->group_by("local_name");
        $this->db->limit(0);
        $res = $this->db->get($this->table)->result();

        # Add all cities option at the beginning of res
        if ($search_string == "") {
            $temp_obj = new stdClass();
            $temp_obj->id = "0";
            $temp_obj->iso = "";
            $temp_obj->city = "All Cities";
            $temp_obj->type = "";
            array_unshift($res, $temp_obj);
        }
        # / Add all cities option at the beginning of res

        return $res;
    }
}
?>