<?php
class Extra_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("user_model");
    }

    public function getPaynamicsPayments($user_id)
    {
    	$this->db->where("user_id", $user_id);
    	$res = $this->db->get("paynamics_requests")->result();
    	return $res;
    }

    public function updatePaynamicsRequests($request_id, $data)
    {
    	$this->db->where("paynamic_request_id", $request_id);
    	return $this->db->update('paynamics_requests', $data);
    }

    public function getPrivileges($user_id)
    {
    	$temp = new stdClass();
    	$temp->remaining_advertisement = $this->user_model->getRemainingOf($user_id, 'advertisement');
    	$temp->remaining_video = $this->user_model->getRemainingOf($user_id, 'video_recording_dist');
    	$temp->remaining_group_message = $this->user_model->getRemainingOf($user_id, 'group_message');
    	$temp->remaining_webinar = $this->user_model->getRemainingOf($user_id, 'live_webinar');
    	return $temp;
    }

    public function getReferralCount($user_id)
    {
        $temp = new stdClass();
        $user = $this->user_model->getUser($user_id);
        $temp->all = $user->referrals;
        $temp->rank_up = $user->rank_up_referrals;
        return $temp;
    }

    public function updateBonus($user_id, $data)
    {
        $this->db->where("user_id", $user_id);
        return $this->db->update("color_bonus", $data);
    }

    public function updatePrivileges($user_id, $data)
    {
        // var_dump($data); die();
        $res_ads = $this->user_model->updateRemainingOf($user_id, "advertisement", $data['advertisement'] - $this->user_model->getRemainingOf($user_id, 'advertisement'));

        var_dump($res_ads);
        die();

        return $res_ads;
    }

}