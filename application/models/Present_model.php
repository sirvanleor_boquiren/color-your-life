<?php
class Present_model extends Crud_model2 {

    public function __construct(){
        parent::__construct();
        $this->table = "presents";
        $this->table_pk = "present_id";
    }

    public function getFeaturedCountByColor($type)
    {
        return $this->db->get_where('presents',[
            'type' => $type,
            'is_featured' => '1'
        ])->num_rows();
    }

    public function getByColor($type,$only_featured = false)
    {
        $this->db->from('presents');
        $this->db->where('type',$type);
        if($only_featured){
            $this->db->where('is_featured',1);
        }
        return $this->db->get()->result();
    }
    
    #LES
    public function getAllPresents() {
        $this->db->from('presents');
        $this->db->order_by('title', 'ASC');
        return $this->db->get()->result();
    }

    public function getByPresentRequestId($id)
    {
        $this->db->select('presents.*')
                 ->from('presents')
                 ->join('request_present','presents.present_id = request_present.present_id')
                 ->where('request_present.request_id',$id);
        
        return $this->db->get()->result();
    }

    public function canBeFeatured($color,$json = false)
    {
        $presents = $this->getByColor($color,true);
        $canBeFeatured =  count($presents) >= 10 ? false : true;
        if($json){
            die(json_encode(['result' => $canBeFeatured]));
        }

        return $canBeFeatured;

    }

    public function uploadImage($id = NULL)
  {
    $config['upload_path']          = 'public/presents_images/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        // $config['max_size']             = 1000;
        // $config['max_width']            = 1024;
    // $config['max_height']           = 768;
    $image_id = ($id == NULL ? $this->db->insert_id() : $id);
    $filename = $_FILES['presents_image']['name'];
        $config['file_name'] = $image_id.'_'.$filename;
        // $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('presents_image'))
        {
      //return error file upload
      return $this->upload->display_errors();

        }
        else
        {
            $imagepath = $this->upload->data()['file_name'];
            $this->present_model->update($image_id,[
        'image_name' => $filename,
        'image_path' => base_url().$config['upload_path'].$imagepath
            ]);
        }
  }

    public function allPendingRequests()
    {
        $this->db->select('request_present.*,group_concat(presents.title) as `present_titles` , present_requests.*')
                 ->from('present_requests')
                 ->join('request_present','request_present.request_id = present_requests.present_request_id')
                 ->join('presents', 'request_present.present_id = presents.present_id')
                 ->where('present_requests.status =', 'pending')
                 ->group_by('request_present.request_id');
        return $this->db->get()->result();
    }

    public function allApprovedRequests()
    {
        $this->db->select('request_present.*,group_concat(presents.title) as `present_titles` , present_requests.*')
                 ->from('present_requests')
                 ->join('request_present','request_present.request_id = present_requests.present_request_id')
                 ->join('presents', 'request_present.present_id = presents.present_id')
                 ->where('present_requests.status =', 'approved')
                 ->group_by('request_present.request_id');  
        return $this->db->get()->result();
    }

    public function allRejectedRequests()
    {
        $this->db->select('request_present.*,group_concat(presents.title) as `present_titles` , present_requests.*')
                 ->from('present_requests')
                 ->join('request_present','request_present.request_id = present_requests.present_request_id')
                 ->join('presents', 'request_present.present_id = presents.present_id')
                 ->where('present_requests.status =', 'rejected')
                 ->group_by('request_present.request_id');
                 
        return $this->db->get()->result();
    }

    public function pending_requests()
    {
        $data = array();

        $this->db->select('request_present.*,group_concat(presents.title) as `present_titles` , present_requests.*')
                 ->from('present_requests')
                 ->join('request_present','request_present.request_id = present_requests.present_request_id')
                 ->join('presents', 'request_present.present_id = presents.present_id')
                 ->where('present_requests.status =', 'pending')
                 ->group_by('request_present.request_id')
                 ->order_by('present_requests.created_at', 'ASC');
                 
        $data['listing'] = $this->db->get()->result();
        
        $this->db->select('presents.*')
                ->from('presents')
                ->order_by("FIELD(type,'bronze','silver','gold','platinum'),no_of_members");
        $data['presents'] = $this->db->get()->result();
        
        foreach ($data['listing'] as $value) {
            foreach ($data['presents'] as $key) {
                if ($value->present_id == $key->present_id) 
                    $value->title = $key->title;
            }
        }

        $users = array();
        $this->load->model('user_model');
        foreach ($data['listing'] as $list) {
            $users[] = $this->user_model->getUserPresentStats($list->user_id);
        }

        $data['user_members'] = $users;

        return $data;
    }

    public function rejected_requests()
    {
        $data = array();

        $this->db->select('request_present.*,group_concat(presents.title) as `present_titles` , present_requests.*')
                 ->from('present_requests')
                 ->join('request_present','request_present.request_id = present_requests.present_request_id')
                 ->join('presents', 'request_present.present_id = presents.present_id')
                 ->where('present_requests.status =', 'rejected')
                 ->group_by('request_present.request_id')
                 ->order_by('present_requests.updated_at', 'DESC');
                 
        $data['listing'] = $this->db->get()->result();
        
        $this->db->select('presents.*')
                ->from('presents');
        $data['presents'] = $this->db->get()->result();
        
        foreach ($data['listing'] as $value) {
            foreach ($data['presents'] as $key) {
                if ($value->present_id == $key->present_id) 
                    $value->title = $key->title;
            }
        }

        $users = array();
        $this->load->model('user_model');
        foreach ($data['listing'] as $list) {
            $users[] = $this->user_model->getUserPresentStats($list->user_id);
        }

        $data['user_members'] = $users;

        return $data;
    }

    public function approved_requests()
    {

        $data = array();

        $this->db->select('request_present.*,group_concat(presents.title) as `present_titles` , present_requests.*')
                 ->from('present_requests')
                 ->join('request_present','request_present.request_id = present_requests.present_request_id')
                 ->join('presents', 'request_present.present_id = presents.present_id')
                 ->where('present_requests.status =', 'approved')
                 ->group_by('request_present.request_id')
                 ->order_by('present_requests.updated_at', 'DESC');
                 
        $data['listing'] = $this->db->get()->result();
        
        $this->db->select('presents.*')
                ->from('presents');
        $data['presents'] = $this->db->get()->result();
        
        foreach ($data['listing'] as $value) {
            foreach ($data['presents'] as $key) {
                if ($value->present_id == $key->present_id) 
                    $value->title = $key->title;
            }
        }

        $users = array();
        $this->load->model('user_model');
        foreach ($data['listing'] as $list) {
            $users[] = $this->user_model->getUserPresentStats($list->user_id);
        }

        $data['user_members'] = $users;

        return $data;
    }

    public function approve_requests($id)
    {
         $data = array(
               'status' => 'approved',
            );

        $this->db->where('present_request_id', $id);
        return $this->db->update( 'present_requests' , $data); 
    }

    public function reject_requests($id)
    {
        $data = array(
               'status' => 'rejected',
            );

        $this->db->where('present_request_id', $id);
        return $this->db->update( 'present_requests' , $data); 
    }

    public function get_requestor_details($present_request_id)
    {
        $this->db->select("app_users.*");
        $this->db->join("app_users", "app_users.user_id = present_requests.user_id");
        $this->db->where("present_request_id", $present_request_id);
        return @$this->db->get("present_requests")->result()[0];
    }

}
?>