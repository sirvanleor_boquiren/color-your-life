<?php

class Ajax_model extends CI_model
{
	public function __construct()
	{
		# ...
	}

	# POST Check if exists
	public function checkExists($post, $table, $select, $column)
	{
		$match = false;
		if(isset($post[$column]))
		{
			$this->db->select($select);
			$this->db->from($table);
			$this->db->where($column." = '".$post[$column]."'");
			if($this->db->get()->num_rows() != 0)
			{
				$match = true;
			}
		}

		return $match;
	}

	/**
	 * Check if there's an existing data except for the current id($column2)
	 */
	public function checkExists2($post, $table, $select, $column, $column2)
	{
		$match = false;
		if (isset($post[$column]) && isset($post[$column2])) {
			$this->db->select($select);
			$this->db->from($table);
			$this->db->where($column . " = '" . $post[$column] . "'");
			$this->db->where($column2 . " != '" . $post[$column2] . "'");
			if ($this->db->get()->num_rows() != 0) {
				$match = true;
			}
		}

		return $match;
	}
}

?>