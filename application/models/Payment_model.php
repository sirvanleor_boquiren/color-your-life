<?php
class Payment_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('plans_model');
    }

    public function getPaymentLink($data)
    {
    	$result = array();

    	if ($this->db->insert('payment_logs', $data)) {
    		$logs_id = $this->db->insert_id();
            if (@$data['membership_id']) { # membership plan payment
                $membership_plan = $this->plans_model->getPlan($data['membership_id']);
                $result['url'] = base_url("membership/" . $logs_id);
                $result['ios_url'] = base_url("membership/" . $logs_id . "/ios");
                $result['android_url'] = base_url("membership/" . $logs_id . "/android");
                $result['membership_name'] = $membership_plan->name;
                $result['plan_rate'] = number_format($membership_plan->rate, 2);
                $result['sessionid'] = $data['sessionid'];
                $result['payment_logs_id'] = $logs_id;
            }
            else { # add-ons payment
                $result['url'] = base_url("payment/add-ons/" . $data['type'] . "/" . $logs_id);
                $result['ios_url'] = base_url("payment/add-ons/" . $data['type'] . "/" . $logs_id . "/ios");
                $result['android_url'] = base_url("payment/add-ons/" . $data['type'] . "/" . $logs_id . "/android");
                $result['sessionid'] = $data['sessionid'];
                $result['payment_logs_id'] = $logs_id;
            }
    	}

    	return $result;
    }

    public function getLogsDetail($payment_logs_id)
    {
    	$this->db->where('payment_logs_id', $payment_logs_id);
    	$res = $this->db->get('payment_logs')->result();
    	if ($res) {
    		return $res[0];
    	}
    	else {
    		return false;
    	}
    }

    public function insertPaynamicsRequest($data)
    {
        if ($this->db->insert('paynamics_requests', $data)) {
            return $this->db->insert_id();
        }
        else {
            return false;
        }
    }

    public function updatePaynamicsRequest($data)
    {
        $this->db->where("requestid", $data['requestid']);
        $this->db->where("responseid", $data['responseid']);
        return $this->db->update('paynamics_requests', $data);
    }

    public function insertDataForTesting($data)
    {
        $ins_data['type'] = "for_testing";
        $ins_data['payload'] = serialize($data);
        $ins_data['created_by'] = 0;
        $this->db->insert("company_info", $ins_data);
    }

    public function updatePaymentLogs($payment_logs_id, $data)
    {
    	$this->db->where("payment_logs_id", $payment_logs_id);
    	return $this->db->update('payment_logs', $data);
    }

    /**
     * Used by doku notif url
     */
    public function updatesPaymentlogsBySessionid($sessionid, $data)
    {
        $payment_log = $this->getPaymentlogsBySessionid($sessionid, 'pending');
        if ($payment_log) {
            if ($payment_log->membership_id != 0) {
                $this->model->applyMembership($payment_log->user_id, $payment_log->membership_id);
            }
            else {
                $this->plans_model->updateAddonsPlan($payment_log->payment_logs_id);
            }
            
            $this->db->where("sessionid", $sessionid);
            $this->db->where("status", "pending");
            return $this->db->update("payment_logs", $data);
        }
        else {
            return false;
        }
    }

    public function getPaymentlogs($where_arr)
    {
        $this->db->where($where_arr);
        $res = $this->db->get("payment_logs")->result();
        if ($res) {
            return $res;
        }
        else {
            return false;
        }
    }

    public function getPaymentlogsBySessionid($sessionid, $status = "pending")
    {
        $this->db->where("sessionid", $sessionid);
        $this->db->where("status", $status);
        $res = $this->db->get("payment_logs")->result();
        if ($res) {
            return $res[0];
        }
        else {
            return false;
        }
    }

    public function checkPaynamicsRequests($user_id)
    {
        $this->db->where("user_id", $user_id);
        $this->db->where("status", "paid");
        $res = $this->db->get('paynamics_requests')->result();
        if ($res) {
            $res[0]->membership_applied = "no";
            if ($res[0]->apply_now) {
                $res[0]->membership_applied = "yes";
            }
            unset($res[0]->apply_now);
            return $res[0];
        }
        else {
            return false;
        }
    }

    public function checkPendingPaidPayment($user_id)
    {
        $this->db->where("user_id = " . $user_id . " AND (status = 'pending' OR status = 'paid') AND created_at >= '" . date("Y-m-d H:i:s", strtotime("-2 days")) . "'");
        $res = $this->db->get('paynamics_requests')->result();
        if ($res) {
            return $res[0];
        }
        else {
            return false;
        }
    }

    /**
     * Create source for alipay and wechat
     * $data['payment_type'], ['currency'], ['amount']
     */
    public function createSourceForAddOns($data)
    {
        require_once('stripe/init.php');
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        $source = \Stripe\Source::create([
            "type" => $data['payment_type'], 
            "currency" => $data['currency'], 
            "amount" => $data['amount'],
            "redirect" => [
                "return_url" => base_url("payment/charge_add_ons?amount=" . $data['amount'] . "&email=" . $data['email'] . "&currency=" . $data['currency'] . "&type=" . $data['type'] . "&payment_logs_id=" . $data['payment_logs_id'] . "&user_id=" . $data['user_id'])
            ]
        ]);
        return $source;
    }

    /**
     * Create source for alipay and wechat in membership module
     */
    public function createSourceForMembership($data)
    {
        require_once('stripe/init.php');
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        $source = \Stripe\Source::create([
            "type" => $data['payment_type'], 
            "currency" => $data['currency'], 
            "amount" => $data['amount'],
            "redirect" => [
                "return_url" => base_url("membership/charge_membership?amount=" . $data['amount'] . "&email=" . $data['email'] . "&currency=" . $data['currency'] . "&membership_plan_id=" . $data['membership_plan_id'] . "&user_id=" . $data['user_id'] . "&membership_plan_name=" . $data['membership_plan_name'] . "&payment_logs_id=" . $data['payment_logs_id'])
            ]
        ]);
        return $source;
    }

    /**
     * Create source for alipay and wechat in membership module
     */
    public function createSourceForMembership_wechat($data)
    {
        require_once('stripe/init.php');
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        $source = \Stripe\Source::create([
            "type" => $data['payment_type'], 
            "currency" => $data['currency'], 
            "amount" => $data['amount'],
            "redirect" => [
                "return_url" => base_url("membership/charge_membership?amount=" . $data['amount'] . "&email=" . $data['email'] . "&currency=" . $data['currency'] . "&membership_plan_id=" . $data['membership_plan_id'] . "&user_id=" . $data['user_id'] . "&membership_plan_name=" . $data['membership_plan_name'] . "&payment_logs_id=" . $data['payment_logs_id'])
            ]
        ]);
        return $source;
    }

}