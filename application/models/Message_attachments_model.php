<?php
use Coduo\PHPHumanizer\DateTimeHumanizer;
class Message_attachments_model extends Crud_model {
	public function __construct()
	{
		parent::__construct();
		$this->table = "message_attachments";
		$this->table_pk = "attachment_id";

		$this->upload_dir = "message_attachments";
	}


	/**
	* this is a multipurpose function for finding the attachment or post
	* @param  [type] $attachment_id [description]
	* @param  [type] $post_id       [description]
	* @return [type]                [description]
	*/
	public function getty($attachment_id, $post_id)
	{
		$ressy = $this->getAttachment($attachment_id);
		if ($ressy->thumbnail_url) { # meaning, if found an attachment
			return $ressy;
		}

		# else, look for post _id
		if ($post_id) {
			$posty = $this->getPostThumbnail($post_id); # wow wordpress?
			if ($posty->thumbnail_url) { # meaning, if found an attachment
				return $posty;
			}
		}

		return (object)['thumbnail_url' => ""]; # pag wala talga, return blank
	}

	public function getImageOrientation($file)
	{
		if (!$file) {
			return "";
		}

		list($width, $height) = @getimagesize($file);
		if ($width > $height) {
			return 'landscape';
		} else {
			return 'portrait';
		}
	}

	public function getPostThumbnail($post_id)
	{
		$owner_id = @$this->posts_model->getPostOwner($post_id)->user_id;
		return $this->posts_model->get($post_id, $owner_id)[0];
	}

	public function getAttachment($id)
	{
		$this->db->where($this->table_pk, $id);
		return $this->formatRow($this->db->get($this->table)->row());
	}

	public function formatRow($obj)
	{
		if (!$obj) { # if our obj is blank
			return false;
		}

		$obj->thumbnail_url = "";
		if ($obj->thumbnail) {
			$obj->thumbnail_url = base_url('uploads/')."message_attachments/".$obj->thumbnail;
		}
		return $obj;
	}

} # end class
