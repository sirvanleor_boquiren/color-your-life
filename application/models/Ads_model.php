<?php
class Ads_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "advertisements";
        $this->table_pk = "ad_id";
        $this->upload_dir = "featured_ads";

        // $this->load->database();
        $this->load->model('user_model');
    }


    # API
    public function getFeatured($post, $type)
    {
        $today = new DateTime();
        $final = array();
        if ($type) {
            $this->db->where("market_place_id", $type);
        }
        $this->db->where("(status = '' OR status = 'active')");
        $this->db->order_by("ad_id", "RANDOM");
        $this->db->limit("10");
        $res = $this->db->get($this->table)->result();
        foreach ($res as $obj) {
            $ads_created = new DateTime($obj->created_at);
            $interval = $today->diff($ads_created);
            if ($interval->days >= 14) {
                $data['status'] = "expired";
                $this->updateAds($obj->ad_id, $data);
                continue;
            }
            $obj->image = base_url("uploads/featured_ads/".$obj->image);
            $final[] = $obj;
        }
        return $final;
    }

    public function getAdsFollow($user_id)
    {
        $today = new DateTime();
        $final = array();
        $this->db->reset_query();
        $this->db->select("advertisements.*");
        $this->db->from("advertisements");
        $this->db->join("follow", "follow.following_id = advertisements.user_id");
        $this->db->where("follow.follower_id = " . $user_id);
        $this->db->where("(status = '' OR status = 'active')");
        $this->db->order_by("ad_id", "RANDOM");
        $this->db->limit("10");
        $query = $this->db->get();
        $result = $query->result();
        foreach ($result as $obj) {
            $ads_created = new DateTime($obj->created_at);
            $interval = $today->diff($ads_created);
            if ($interval->days >= 14) {
                $data['status'] = "expired";
                $this->updateAds($obj->ad_id, $data);
                continue;
            }
            $obj->image = base_url("uploads/featured_ads/".$obj->image);
            $final[] = $obj;
        }
        return $final;
    }

    public function updateAds($ad_id, $data)
    {
        return $this->db->update($this->table, $data, array("ad_id" => $ad_id));

    }

}
?>