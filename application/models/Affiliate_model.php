<?php
class Affiliate_model extends Crud_model {

    public function __construct(){
        parent::__construct();
        $this->table = "affiliates";
    }

  public function update($id, $data)
  {
    $this->db->where('affiliate_id', $id);
    return $this->db->update($this->table, $data);
  }

  public function get($id)
  {
    return $this->db->get_where('affiliates',[
      'affiliate_id' => $id 
    ])->row();
  }

  public function all($search_keyword = NULL)
  {
    if (@$search_keyword) {
      $this->db->or_like("LOWER(title)", strtolower($search_keyword), "both");
      $this->db->or_like("LOWER(site_link)", strtolower($search_keyword), "both");
    }
    $this->db->order_by("title", "ASC");
    return $this->db->get($this->table)->result();
  }

  public function getAllAffiliates($search_keyword = NULL) {
    if (@$search_keyword) {
      $this->db->or_like("LOWER(title)", strtolower($search_keyword), "both");
      $this->db->or_like("LOWER(site_link)", strtolower($search_keyword), "both");
    }
    $this->db->from($this->table);
    return $this->db->get()->result();
  }

  //use only if $data has the same field names in the database
  public function updateChanges($id,$data)
  {
    $affiliate = $this->get($id);
    $changes = 0;

    foreach($data as $key => $val)
    {
      if($affiliate->{$key} != $data[$key]){
        $changes++;
        $this->db->set($key,$val)
                 ->where('affiliate_id',$id)
                 ->update('affiliates');
      }
    }

    return $changes ? true : false;
  }

  public function uploadImage($id = NULL)
  {
    $config['upload_path']          = 'public/affiliates_images/';
		$config['allowed_types']        = 'jpg|png|jpeg';
		// $config['max_size']             = 1000;
		// $config['max_width']            = 1024;
    // $config['max_height']           = 768;
    $image_id = ($id == NULL ? $this->db->insert_id() : $id);
    $filename = $_FILES['affiliate_image']['name'];
		$config['file_name'] = $image_id.'_'.$filename;
		// $this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('affiliate_image'))
		{
      //return error file upload
      return $this->upload->display_errors();

		}
		else
		{
			$imagepath = $this->upload->data()['file_name'];
			$this->affiliate_model->update($image_id,[
        'image_name' => $filename,
        'image_path' => base_url().$config['upload_path'].$imagepath
			]);
		}
  }

  public function delete($id){
    return $this->db->delete('affiliates', ['affiliate_id' => $id]);
  }


}
?>