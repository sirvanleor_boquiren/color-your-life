<?php
class Company_info_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "company_info";
        $this->table_pk = "company_info_id";
    }

    public function add($data)
    {
        $final = array();
        $content = array();
        if (@$data['save_privacy_policy']) {
            $final['type'] = "privacy_policy";
            $content = ['content' => $data['content']];
        }

        if (@$data['save_terms_conditions']) {
            $final['type'] = "terms_conditions";
            $content = ['content' => $data['content']];
        }

        if (@$data['save_about_us']) {
            $final['type'] = "about_us";
            $content = ['about_the_company' => $data['about_the_company'], 
                        'ceo_message' => $data['ceo_message'], 
                        'address' => $data['address']];
        }

        if (@$data['save_contact_us']) {
            $final['type'] = "contact_us";
            $content = ['mobile' => ['label' => $data['mobile_label'], 'link' => $data['mobile']],
                        'email' => ['label' => $data['email_label'], 'link' => $data['email']],
                        'website' => ['label' => $data['website_label'], 'link' => $data['website']],
                        'facebook' => ['label' => $data['facebook_label'], 'link' => $data['facebook']],
                        'twitter' => ['label' => $data['twitter_label'], 'link' => $data['twitter']],
                        'instagram' => ['label' => $data['instagram_label'], 'link' => $data['instagram']],
                        'youtube' => ['label' => $data['youtube_label'], 'link' => $data['youtube']],
                        'youku' => ['label' => $data['youku_label'], 'link' => $data['youku']]];
        }

        $final['payload'] = serialize($content);
        $final['created_by'] = $_SESSION['admin_id'];
        return $this->db->insert($this->table, $final);
    }

    public function get($type)
    {
        $this->db->where('type', $type);
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(1);
        $res = $this->db->get($this->table)->result();
        if ($res) {
            return unserialize($res[0]->payload);
        }
        else {
            $temp = new stdClass();
            $temp->content = "";
            return $temp;
        }
    }

    public function edit()
    {

    }
}
?>
