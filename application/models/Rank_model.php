<?php

class Rank_model extends CI_model
{
  public function __construct()
  {
    parent::__construct();
    $this->table = "color_rank";
    $this->table_pk = "color_rank_id";
  }

  public function all()
  {
    $query = $this->db->get($this->table);
    $result = $query->result();
    return $result;
  }


}

?>
