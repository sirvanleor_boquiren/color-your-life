<?php

class Positions_model extends CI_model
{
	public function __construct()
	{
		# ...
	}	

	#All position details
	public function getAllPositions($is_admin = 0)
	{
		$this->db->select("pos_id, pos_desc");
		$this->db->from("positions");
		$this->db->where("pos_is_admin = ".$is_admin);
		$db_data = array();
		foreach ($this->db->get()->result() as $pos_arr) {
			$db_data[$pos_arr->pos_id] = $pos_arr->pos_desc;
		}
		return $db_data;
	}
}

?>