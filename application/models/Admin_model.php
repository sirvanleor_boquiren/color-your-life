<?php

class Admin_model extends CI_model
{
	public function __construct()
	{
		$this->load->library("pagination");
		$this->load->model('ajax_model');
	}

	public function login($data)
	{
		$this->db->where('username', $data['username']);
		$query = $this->db->get('admin_users');
		if ($query->num_rows() >= 1) {
			$result = $query->result()[0];
			if (password_verify($data['password'], $result->password)) {
				return $result;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	#Number of users per type and status
	public function getTotalAdmin()
	{
		$this->db->select("COUNT(admin_id) as total");
		$res = $this->db->get("admin_users");
		return $res->result()[0]->total;
	}

	#All users details per type
	public function getAllAdmin()
	{
		$this->db->where("type !=", "Special Admin");
		$this->db->order_by("created_at", "desc");
		// $this->db->where("status", 1); # Get inactive admin as well for activation
		$limit = 0;
		if($this->uri->segment(3) !== FALSE)
		{
			$limit = $this->uri->segment(3);
		}
		$this->db->limit(TABLE_DATA_PER_PAGE, $limit); # number_per_page, start row
		$db_results = $this->db->get("admin_users");

		if($db_results->num_rows() > 0)
		{
			$db_data = array();
			foreach ($db_results->result() as $user) {				
				$user->fullname = $user->fname." ".$user->lname;
				$user->status = $user->status == 1 ? "Active" : "Inactive";
				$user->contact = $user->contact == "" ? "N/A" : $user->contact;
				$db_data[] = $user;
			}
			return $db_data;
		}
		else
		{
			return null;
		}
	}

	public function addAdmin($post)
	{
		$final = array();
		$checkPass = check_password($post['password'], $post['confirm_password']);
		if($checkPass['passed'])
		{
			$final['alert_msg'] = "Username already exists. Please enter a new valid username.";
			$final['alert_class'] = "alert-block alert-danger";
			$userNameExists = $this->ajax_model->checkExists($post, "admin_users", "admin_id", "username");
			if(!$userNameExists)
			{
				$final['alert_msg'] = "Email already exists. Please enter a new valid email.";
				$final['alert_class'] = "alert-block alert-danger";
				$emailExists = $this->ajax_model->checkExists($post, "admin_users", "admin_id", "email");
				if(!$emailExists)
				{
					$post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
					unset($post['confirm_password']);
					$final['alert_msg'] = "Something went wrong when adding the admin. Please try again or contact system administrator.";
					$final['alert_class'] = "alert-block alert-danger";
					if($this->db->insert("admin_users", $post))
					{
						$final['alert_msg'] = "New admin user successfully added.";
						$final['alert_class'] = "alert-success";
					}
				}
			}
		}
		else
		{
			$final['alert_msg'] = implode("<br>", $checkPass['msg']);
			$final['alert_class'] = "alert-block alert-danger";
		}
		return $final;
	}

	public function changePassword($post)
	{
		$final = array();
		$checkPass = check_password($post['password'], $post['confirm_password']);
		if ($checkPass['passed']) {
			$post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
			unset($post['confirm_password']);
			$this->db->where("admin_id", $post['admin_id']);
			$final['alert_msg'] = "Something went wrong when adding the admin. Please try again or contact system administrator.";
			$final['alert_class'] = "alert-block alert-danger";
			$this->db->where("admin_id", $post['admin_id']);
			if ($this->db->update("admin_users", $post)) {
				$final['alert_msg'] = "Admin user successfully update.";
				$final['alert_class'] = "alert-success";
			}
		}
		else {
			$final['alert_msg'] = implode("<br>", $checkPass['msg']);
			$final['alert_class'] = "alert-block alert-danger";
		}
		return $final;
	}

	public function editAdmin($post)
	{
		$final = array();
		$final['alert_msg'] = "Username already exists. Please enter a new valid username.";
		$final['alert_class'] = "alert-block alert-danger";
		$userNameExists = $this->ajax_model->checkExists2($post, "admin_users", "admin_id", "username", "admin_id");
		if (!$userNameExists) {
			$final['alert_msg'] = "Email already exists. Please enter a new valid email.";
			$final['alert_class'] = "alert-block alert-danger";
			$emailExists = $this->ajax_model->checkExists2($post, "admin_users", "admin_id", "email", "admin_id");
			if (!$emailExists) {
				$final['alert_msg'] = "Something went wrong when updating the admin. Please try again or contact system administrator.";
				$final['alert_class'] = "alert-block alert-danger";
				$this->db->where("admin_id", $post['admin_id']);
				if ($this->db->update("admin_users", $post)) {
					$final['alert_msg'] = "Admin user successfully updated.";
					$final['alert_class'] = "alert-success";
				}
			}
		}

		return $final;
	}

	/**
     * Change status of the user to active (1) or inactive (0)
     */
    public function changeStatus($user_id, $status)
    {
        $this->db->where("admin_id", $user_id);
        return $this->db->update("admin_users", array("status" => $status));
    }
}

?>