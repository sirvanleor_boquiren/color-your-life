<?php
use Coduo\PHPHumanizer\DateTimeHumanizer;
class Messages_model extends Crud_model {
	public function __construct()
	{
		parent::__construct();
		$this->table = "messages";
		$this->table_pk = "message_id";

		$this->upload_dir = "message_attachments";

		$this->num_per_page = 10; # Pagination
		$this->load->model('follow_model');
		$this->load->model('user_model');
		$this->load->model('notification_model');
		$this->load->model('message_attachments_model');
		$this->load->model('settings_model');
	}

	public function sendToPostOwner($post)
	{
		$post_id = $post['post_id'];
		$receiver_id = @$this->posts_model->getPostOwner($post_id)->user_id;
		$sender = $this->user_model->getUser($post['sender_id']);
		$sender_fullname = $sender->fname . " " . $sender->lname;
		$sender_email = $sender->email;
		$posty = @$this->posts_model->get($post_id)[0];
		$posty_name = $posty->name;
		
		if ($posty->market_place_id == 1) { #Products
			return ($receiver_id && $posty_name) ? $this->sendMessage(
				[
					'sender_id' => $post['sender_id'],
					'receiver_id' => $receiver_id,
					'body' => "Hi! Is '$posty_name' available?",
					'post_id' => $post_id
				]
			) : false;
		}
		elseif ($posty->market_place_id == 2) { #Services
			return ($receiver_id && $posty_name) ? $this->sendMessage(
				[
					'sender_id' => $post['sender_id'],
					'receiver_id' => $receiver_id,
					'body' => "Hi I would like to apply for this job.\nJob Post: $posty_name\nFull Name: $sender_fullname\nEmail: $sender_email",
					'post_id' => $post_id
				]
			) : false;
		}
		else {
			return false;
		}

	}

	/**
	* this adds two rows for every message
	* one for the sender, and one for the receiver
	* @param  [type] $data [description]
	* @return [type]       [description]
	*/
	public function sendMessage($data)
	{
		$thumbnail_url = @$this->message_attachments_model->getty($data['attachment_id'], $data['post_id'])->thumbnail_url;
		$thumbnail_orientation = $this->message_attachments_model->getImageOrientation($thumbnail_url);

		$res = $this->add(array_merge(['conversation_owner_id' => $data['sender_id'], 'conversation_partner_id' => $data['receiver_id']], $data)) &&
		$this->add(array_merge(['conversation_owner_id' => $data['receiver_id'], 'conversation_partner_id' => $data['sender_id']], $data)) ?
		(object)['body' => $data['body'], 'thumbnail_url' => $thumbnail_url, 'thumbnail_orientation' => $thumbnail_orientation] : # return the message if successful
		(object)[]; # return empty if insert fails
		$user = $this->user_model->getUser($data['sender_id']);
		$recipient_user = $this->user_model->getUser($data['receiver_id']);
		$res->user = (object)[
			'id' => $data['sender_id'],
			'name' =>$user->fullname,
			'profile_pic' =>$user->profile_pic,
			'is_you' => true
		];
		

		if (@$res) {
			/**
			 * Check if Settings if push notif is on
			 */
			$notif = $this->settings_model->getSettings($data['receiver_id']);
			if (@$notif[0]->push_notification) {
				##### FIREBASE NOTIFICATION BLOCK ####
				##### @author: @jjjjcccjjf ###########
				# get device ids via user_id
				$notif_type = 'message';
				$recipients =  $this->udi_model->getDeviceIdsByUserId($data['receiver_id']);
				$notification_message = $this->notification_model->constructMessage(
					$notif_type,
					serialize($data)
				);
				foreach ($recipients as $to) { # send multiple bruh
					$firebase_response = $this->firebase_model->sendMultiple($to,
						['data' =>
							(object) [
								'sender_id' => $data['sender_id'],
								'body' => $res->body,
								'user' => (object)[
									'id' => $data['sender_id'],
									'name' =>$user->fullname,
									'profile_pic' =>$user->profile_pic,
								],
								'type' => 'new_message'
							]
						],
						['title' => 'New message', 'body' => $notification_message, 'priority' => 'high']
					);
					log_message('error', $to);
					log_message('error', $firebase_response);
					log_message('error', $notification_message);
				} # end foreach
				##### / FIREBASE NOTIFICATION BLOCK ####
				########################################
			}

			if (@$notif[0]->email_notification) {
				$this->load->helper("email_template");
				$recipient = $recipient_user->email;
				$subject = $user->fname . " sent you a message.";
				// $body = send_message_template(['profile_pic' => $user->profile_pic, 'fullname' => $user->fullname, 'body' => $res->body]);
				$body = send_message_template(['sender_profile_pic' => $user->profile_pic, 'sender_fullname' => $user->fullname, 'message' => $res->body]);
				$res->email_sent = send_email($this, $recipient, $subject, $body);
			}

		}
		return $res;
	}

/**
* get coversation WITH specific person
* @param  [type] $conversation_owner_id   [description]
* @param  [type] $conversation_partner_id [description]
* @return [type]                          [description]
*/
public function getThread($conversation_owner_id, $conversation_partner_id)
{
	$this->db->where('conversation_owner_id', $conversation_owner_id);
	$this->db->where('conversation_partner_id', $conversation_partner_id);
	$this->db->order_by($this->table_pk, 'DESC');
	$this->paginate();
	$data = $this->db->get($this->table)->result();
	return $this->formatData($data, $conversation_owner_id, $conversation_partner_id); # we array reverse so we follow the ordering a la messenger style
}

public function markAsRead($conversation_owner_id, $conversation_partner_id)
{
	$this->db->where('conversation_owner_id', $conversation_owner_id);
	$this->db->where('conversation_partner_id', $conversation_partner_id);
	$this->db->where('is_read', 0);
	return $this->db->update($this->table, ['is_read' => 1]);
}

public function sendToFollowers($conversation_owner_id, $body)
{
	$followers = $this->follow_model->getFollowers($conversation_owner_id);
	if (!$followers) {
		return false; # no followers
	}
	try {
		foreach ($followers as $follower_id) {
			$this->sendMessage(
				array(
					'sender_id' => $conversation_owner_id,
					'receiver_id' => $follower_id,
					'body' => $body
				) # end array
			); # end sendmessage
		} # end foreach
		return true; # Thank you PHP for being synchronous
	} catch (\Exception $e) {
		return false;
	}
} # end sendToFollowers
public function formatData($data, $conversation_owner_id, $conversation_partner_id)
{
	$users = [];
	$users[$conversation_owner_id] = $this->user_model->get($conversation_owner_id);
	$users[$conversation_partner_id] = $this->user_model->get($conversation_partner_id);
	$res = [];
	foreach ($data as $key => $value) {
		$thumbnail_url = @$this->message_attachments_model->getty($value->attachment_id, $value->post_id)->thumbnail_url;
		$thumbnail_orientation = $this->message_attachments_model->getImageOrientation($thumbnail_url);

		/**
         * Fix for inaccurate timestamp in beta :)
         */
		if (in_array(base_url(), ["https://coloryourlife.betaprojex.com/app/"])) {
			$value->created_at = date("Y-m-d H:i:s", strtotime($value->created_at . " + 13 hours"));
		}
		elseif (in_array(base_url(), ["https://coloryourlifeapp.com/app/"])) {
			$value->created_at = date("Y-m-d H:i:s", strtotime($value->created_at . " + 15 hours"));
		}

		$res[] = (object)[
			'body' => $value->body,
			'thumbnail_url' => $thumbnail_url,
			'thumbnail_orientation' => $thumbnail_orientation,
			'user' => (object)[
				'id' => $value->sender_id,
				'name' => $users[$value->sender_id]->fullname,
				'profile_pic' => $users[$value->sender_id]->profile_pic,
				'is_you' => $value->sender_id == $conversation_owner_id
			],
			'created_at_formatted' => date('F j, Y h:i:sA', strtotime($value->created_at)),
			'relative_time' => DateTimeHumanizer::difference(new \DateTime(date("Y-m-d H:i:s")), new \DateTime($value->created_at))
		];
	}
	return $res;
} # end formatData
public function getMessagePreviews($user_id)
{
	$partner_ids = $this->db->query("SELECT conversation_partner_id FROM messages WHERE conversation_owner_id = {$user_id} GROUP BY conversation_partner_id")->result();
	$res = [];
	foreach ($partner_ids as $key => $value) {
		$t_arr = [];
		$d = $this->db->query("SELECT * from messages where conversation_partner_id = {$value->conversation_partner_id} AND conversation_owner_id = {$user_id} ORDER BY created_at desc LIMIT 1")->row();
		$users[$d->conversation_partner_id] = $this->user_model->get($d->conversation_partner_id);
		if ($user_id == $d->sender_id) {
			$d->is_read = 1;
		}
		
		/**
         * Fix for inaccurate timestamp in beta :)
         */
		if (in_array(base_url(), ["https://coloryourlife.betaprojex.com/app/"])) {
			$d->created_at = date("Y-m-d H:i:s", strtotime($d->created_at . " + 13 hours"));
		}
		elseif (in_array(base_url(), ["https://coloryourlifeapp.com/app/"])) {
			$d->created_at = date("Y-m-d H:i:s", strtotime($d->created_at . " + 15 hours"));
		}

		$t_arr = (object)[
			'body' => ($d->sender_id == $user_id) ? "You: " . $d->body : $d->body,
			'user' => (object)[
				'badge' => $this->getNumberOfUnreadMessages($d->conversation_partner_id),
				'id' => $d->conversation_partner_id,
				'name' => $users[$d->conversation_partner_id]->fullname,
				'profile_pic' => $users[$d->conversation_partner_id]->profile_pic,
				// 'is_you' => $d->conversation_partner_id == $user_id
			],
			'is_read' => $d->is_read,
			'created_at' => $d->created_at,
			'created_at_formatted' => date('F j, Y h:i:sA', strtotime($d->created_at)),
			'relative_time' => DateTimeHumanizer::difference(new \DateTime(date("Y-m-d H:i:s")), new \DateTime($d->created_at))
		];
		$res[] = $t_arr;
	}
	usort($res, 'sortDateCreated');
	return $res;
}

public function getNumberOfUnreadMessages($user_id)
{
	$res = $this->db->query("SELECT message_id FROM messages WHERE receiver_id = {$user_id} AND conversation_owner_id = {$user_id} AND is_read = 0 GROUP BY conversation_partner_id")->result();
	return count($res);
}

} # end class
