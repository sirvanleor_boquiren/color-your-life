<?php
class Color_points_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "redeem_requests";
        $this->table_pk = "redeem_request_id";
    }

    public function get_requestor_details($redeem_request_id)
    {
        $this->db->select("app_users.*");
        $this->db->join("app_users", "app_users.user_id = redeem_requests.user_id");
        $this->db->where("redeem_request_id", $redeem_request_id);
        return @$this->db->get("redeem_requests")->result()[0];
    }

    public function pending_requests($total = null, $search_keyword = NULL)
    {
        if (@$search_keyword) {
            $this->db->group_start();
            $this->db->or_like("LOWER(app_users.points)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(membership_plans.name)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(affiliates.title)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(redeem_requests.fname, ' ', redeem_requests.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.mobile_no)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.address)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(app_users.fname, ' ', app_users.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.email)", strtolower($search_keyword), "both");
            $this->db->group_end();
        }

    	$this->db->select("redeem_requests.*, app_users.points, CONCAT(app_users.fname, ' ', app_users.lname) AS fullname, app_users.email, membership_plans.name, affiliates.title");
    	$this->db->join('app_users', 'redeem_requests.user_id = app_users.user_id');
    	$this->db->join('membership_plans', 'membership_plans.membership_plan_id = app_users.membership_plan_id');
    	$this->db->join('affiliates','redeem_requests.affiliate_id = affiliates.affiliate_id');
    	$this->db->where('redeem_requests.status =', 'pending');
        $this->db->order_by("created_at", "ASC");
         if ($total != null) {
            $this->db->limit("0");
        }
        return $this->db->get($this->table)->result();
    }
    
    public function membership_plans() {
    	$this->db->select("*");
    	return $this->db->get("membership_plans")->result();
    }

    public function rejected_requests($total = null, $search_keyword = null) {
    	$this->db->select("redeem_requests.*, app_users.points, CONCAT(app_users.fname, ' ', app_users.lname) AS fullname, app_users.email, membership_plans.name, affiliates.title");
    	$this->db->join('app_users', 'redeem_requests.user_id = app_users.user_id');
    	$this->db->join('membership_plans', 'membership_plans.membership_plan_id = app_users.membership_plan_id');
    	$this->db->join('affiliates','redeem_requests.affiliate_id = affiliates.affiliate_id');
        if (@$search_keyword) {
            $this->db->group_start();
            $this->db->or_like("LOWER(app_users.points)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(membership_plans.name)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(affiliates.title)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(redeem_requests.fname, ' ', redeem_requests.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.mobile_no)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.address)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(app_users.fname, ' ', app_users.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.email)", strtolower($search_keyword), "both");
            $this->db->group_end();
        }
    	$this->db->where('redeem_requests.status =', 'rejected');
        $this->db->order_by("redeem_requests.updated_at", "DESC");
        if ($total != null) {
            $this->db->limit("0");
        }
        return $this->db->get($this->table)->result();
    }

    public function approved_requests($total = null, $search_keyword = null) {
    	$this->db->select("redeem_requests.*, app_users.points, CONCAT(app_users.fname, ' ', app_users.lname) AS fullname, app_users.email, membership_plans.name, affiliates.title, points_logs.points_added");
    	$this->db->join('app_users', 'redeem_requests.user_id = app_users.user_id');
    	$this->db->join('membership_plans', 'membership_plans.membership_plan_id = app_users.membership_plan_id');
    	$this->db->join('affiliates','redeem_requests.affiliate_id = affiliates.affiliate_id');
        $this->db->join('points_logs', 'points_logs.redeem_request_id = redeem_requests.redeem_request_id');
        if (@$search_keyword) {
            $this->db->group_start();
            $this->db->or_like("LOWER(app_users.points)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(membership_plans.name)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(affiliates.title)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(redeem_requests.fname, ' ', redeem_requests.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.mobile_no)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(redeem_requests.address)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(app_users.fname, ' ', app_users.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.email)", strtolower($search_keyword), "both");
            $this->db->group_end();
        }
    	$this->db->where('redeem_requests.status =', 'approved');
        $this->db->order_by("redeem_requests.updated_at", "DESC");
        if ($total != null) {
            $this->db->limit("0");
        }
        return $this->db->get($this->table)->result();
    }

    public function reject_request($id)
    {
    	$data = array(
               'status' => 'rejected',
            );

		$this->db->where('redeem_request_id', $id);
		return $this->db->update( $this->table , $data); 

    }

    public function approve_requests($id,$points)
    {
    	$this->db->trans_begin();

    	$this->db->select('user_id,points_to_redeem');
    	$this->db->where('redeem_request_id = ',$id);
    	$result = $this->db->get($this->table)->row();

    	

    	$this->db->select('points');
    	$this->db->where('user_id = ',$result->user_id);
    	$curr_points = $this->db->get('app_users')->row();

    	$new_points = $curr_points->points - $points;
    	//var_dump($new_points); die();

    	
    	// update current points in app_users
    	$data = array (
    		'points' => $new_points
    	);

    	$this->db->where('user_id = ',$result->user_id);
    	$this->db->update('app_users', $data);

    	// update redeem requests status
		$data = array(
			'status' => 'approved'
		);
		$this->db->where('redeem_request_id', $id);
		$this->db->update( $this->table , $data); 

		// update points log
		$data = array(
			   'redeem_request_id' => $id,
			   'user_id' => $result->user_id,
			   'type' => 'redeem',
			   // 'points_added' => '-'.$result->points_to_redeem,
                'points_added' => '-'.$points,      
			);

		$this->db->insert('points_logs', $data);
		
		if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
            return false;
		}
		else
		{
		    $this->db->trans_commit();
            return true;
		}
    }
}