<?php
class User_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "app_users";
        $this->table_pk = "user_id";
        $this->upload_dir = "user_profile_pic";

				$this->load->model('user_device_ids_model', 'udi_model');
				$this->load->model('user_reviews_model');
				$this->load->model('categories_model');
				$this->load->model('follow_model');
				$this->load->model('rank_model');
                $this->load->model('plans_model');
        // $this->load->database();

        $this->load->library('email');
        // $config_mail['protocol']='smtp';
        // $config_mail['smtp_host']='mail.smtp2go.com';
        // $config_mail['smtp_timeout']='30';
        // $config_mail['smtp_port']='80';
        // $config_mail['smtp_user']='coloryourlife';
        // $config_mail['smtp_pass']='aDUybDhxcDB4cjAw';
        // $config_mail['charset']='utf-8';
        // $config_mail['newline']="\r\n";
        // $config_mail['wordwrap'] = TRUE;

        $config_mail['protocol']='smtp';
        $config_mail['smtp_host']='mail.smtp2go.com';
        $config_mail['smtp_port']='2525';
        $config_mail['smtp_user']='edocampo@myoptimind.com';
        $config_mail['smtp_pass']='OXJwdHhyYXN4aGcw';
        $config_mail['charset']='utf-8';
        $config_mail['newline']="\r\n";
        $config_mail['wordwrap'] = TRUE;
        $config_mail['mailtype'] = 'html';
        // $config_mail['mailtype'] = 'html';



        $this->email->initialize($config_mail);
    }

    # API

    public function add($post)
    {
        $temp_referred_by_code = "";
        if (@$post['referral_code']) {
            $temp_referred_by_code = $post['referral_code'];
        }

        # if there's a referral code
        if ($temp_referred_by_code != "") {
            $user = $this->getByReferralCode($temp_referred_by_code);
            $post['referred_by'] = $user->user_id;
            $post['referrer_rank'] = $user->rank;
        }

        $post['referral_code'] = strtoupper($post['country']) . $this->generateReferralCode();
        while ($this->verifyReferralCode($post['referral_code'])) {
            $post['referral_code'] = strtoupper($post['country']) . $this->generateReferralCode();
        }

        // unset($post['country']);

        $this->db->insert($this->table, $post);
        $last_id = $this->db->insert_id();

        $this->createBonus($last_id);

        return $last_id;
    }

    /**
     * Change status of the user to active (1) or inactive (0)
     */
    public function changeStatus($user_id, $status)
    {
        $this->db->where("user_id", $user_id);
        return $this->db->update("app_users", array("status" => $status));
    }

    /**
     * Insert row in color_bonus table
     */
    public function createBonus($user_id)
    {
        $data['user_id'] = $user_id;
        $this->db->insert('color_bonus', $data);
    }

    /**
     * Get user by referral code
     */
    public function getByReferralCode($referral_code)
    {
        $this->db->where('referral_code like binary', $referral_code);
        $query = $this->db->get($this->table);
        return $query->result()[0];
    }

    /**
     * Verify if referral code already exists
     */
    public function verifyReferralCode($referral_code)
    {
        $this->db->select("COUNT(*) AS codeCount");
        $this->db->where('referral_code like binary', $referral_code);
        $query = $this->db->get($this->table);
        return $query->result()[0]->codeCount;
    }

	public function getMyMarketplace($user_id, $viewer_id = NULL)
	{
		$user = $this->get($user_id);
		$subscribers = count($this->follow_model->getFollowers($user_id));
		$res = [
			'user' => [
				'fullname' => $user->fullname,
				'username' => $user->username,
				'market_place_name' => $user->market_place_name,
				'profile_pic' => $user->profile_pic,
                'membership_plan' => $user->plan_name
				],
			'ratings' => (object) [
				'satisfied' => $this->user_reviews_model->countReviews($user_id, 'Satisfied'),
				'meh' => $this->user_reviews_model->countReviews($user_id, 'Meh'),
				'not-satisfied' =>$this->user_reviews_model->countReviews($user_id, 'Not satisfied')
			],
			// 'categories' => $this->categories_model->all_categories($user->market_place_id),
			'subscribers' => $subscribers
		];

        if ($viewer_id) {
            $following = 0;
            if ($this->is_following(['follower_id' => $viewer_id, 'following_id' => $user_id])) {
                $following = 1;
            }
            $res['is_following'] = $following;
        }
        
		return $res;
	}

    public function generateReferralCode($length = 7)
    {
        $characters = '0123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return strtoupper($randomString);
    }

    public function getPlanInfo($user_id)
    {  
        $this->db->select("user_id, username, email, fname, lname, CONCAT(fname, ' ', lname) as fullname, membership_plan_id, app_users.created_at, app_users.updated_at, password, first_log,profile_pic, referral_code, rank AS rank_id, rank_type");
        $this->db->from("app_users");
        $this->db->join("color_rank", "color_rank.color_rank_id = app_users.rank");
        $this->db->where("app_users.user_id", $user_id);
        $user = $this->db->get()->result();
        // $user = $this->getUser($user_id);
        if ($user) {
            $user = $user[0];
            $final = new stdClass();
            $final = $user;
            # Get membership plan name
            if ($user->membership_plan_id != 0) {
                $plan = $this->plans_model->getPlan($user->membership_plan_id);
                $final->membership_plan_name = $plan->name;
                $final->mp_access_limit = ($plan->market_place_access == -1) ? "-1" : "1";
            }
            else {
                $final->membership_plan_name = "Not yet a member";
                $final->mp_access_limit = "Not yet a member";
            }

            # Get current membership plan details
            $current_plan = $this->plans_model->getCurrentPlan($user_id);
            if ($current_plan) {
                $final->membership_expiry = $current_plan->expiration_date;
            }
            else {
                $final->membership_expiry = "";
            }


            $temp = new stdClass();
            $temp->market_place_access = (object)$this->getMarketPlaceAccess($user_id); # Get market place access
            $temp->chat_function = $this->chatFunction($user_id); # Get chat function access
            $temp->color_bonus_access = $this->colorBonusAccess($user_id); # Get color bonus access
            $temp->color_presents_access = $this->colorPresentsAccess($user_id); # Get color presents access
            $temp->remaining_advertisement = $this->getRemainingOf($user_id, 'advertisement'); # Get remaining advertisements
            $temp->remaining_video = $this->getRemainingOf($user_id, 'video_recording_dist'); # Get remaining video recording distribution
            $temp->remaining_group_message = $this->getRemainingOf($user_id, 'group_message'); # Get remaining group message
            $temp->remaining_live_webinar = $this->getRemainingOf($user_id, 'live_webinar'); # Get remaining live webinar

            $final->privileges = $temp;

            if(@$final->profile_pic == null)
            {
                $final->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $final->profile_pic = base_url("uploads/user_profile_pic/" . $final->profile_pic);
            }

            /**
             * Check if user has pending payment(paynamics)
             */
            $pending_payment = $this->payment_model->checkPendingPaidPayment($final->user_id);
            if ($pending_payment) {
                $final->with_pending_payment = "Yes";
            }
            else {
                $final->with_pending_payment = "No";
            }

            return $final;

        }
        else {
            return false;
        }
    }

    public function login($post)
    {
        $return = null;

        $this->db->select("user_id, username, email, fname, lname, CONCAT(fname, ' ', lname) as fullname, membership_plan_id, app_users.created_at, app_users.updated_at, password, first_log,profile_pic, referral_code, rank AS rank_id, rank_type");
        $this->db->from("app_users");
        $this->db->join("color_rank", "color_rank.color_rank_id = app_users.rank");
        $this->db->where("email", $post['username']);
        $this->db->or_where("username", $post['username']);
        $res = $this->db->get()->result();
        
        if($res)
        {
            # Get membership plan name
            if ($res[0]->membership_plan_id != 0) {
                $plan = $this->plans_model->getPlan($res[0]->membership_plan_id);
                $res[0]->membership_plan_name = $plan->name;
                $res[0]->mp_access_limit = ($plan->market_place_access == -1) ? "-1" : "1";
            }
            else {
                $res[0]->membership_plan_name = "Not yet a member";
                $res[0]->mp_access_limit = "Not yet a member";
            }

            # Get current membership plan details
            $current_plan = $this->plans_model->getCurrentPlan($res[0]->user_id);
            if ($current_plan) {
                if ( date("Y-m-d H:i:s") > date("Y-m-d H:i:s", strtotime($current_plan->expiration_date)) ) { #expired

                    # paynamics request block                    
                    $paynamics_request = $this->payment_model->checkPaynamicsRequests($res[0]->user_id);
                    if ($paynamics_request) {
                        if ($paynamics_request->membership_plan_id != 0) {
                            $this->plans_model->applyMembership($paynamics_request->user_id, $paynamics_request->membership_plan_id); # add plans to user_membership_plan table
                            $this->payment_model->updatePaymentLogs($paynamics_request->payment_logs_id, array('status' => 'applied')); # update status in logs
                            $this->plans_model->applyNow($paynamics_request->user_id);
                            $this->payment_model->updatePaynamicsRequest(['requestid' => $paynamics_request->requestid, 'responseid' => $paynamics_request->responseid, 'status' => "applied"]);
                            $res[0] = $this->login($post);
                            $current_plan = $this->plans_model->getCurrentPlan($res[0]->user_id);
                        }
                    }
                    # / paynamics request block
                    
                }

                $res[0]->membership_expiry = $current_plan->expiration_date;
                if ($current_plan->expiration_date == "0000-00-00 00:00:00") {
                    $res[0]->membership_expiry = date("Y-m-d H:i:s", strtotime(date("Y-m-d") . "+30 days"));
                }
            }
            else {
                $res[0]->membership_expiry = "";
            }

            $temp = new stdClass();
            $temp->market_place_access = (object)$this->getMarketPlaceAccess($res[0]->user_id); # Get market place access
            $temp->chat_function = $this->chatFunction($res[0]->user_id); # Get chat function access
            $temp->color_bonus_access = $this->colorBonusAccess($res[0]->user_id); # Get color bonus access
            $temp->color_presents_access = $this->colorPresentsAccess($res[0]->user_id); # Get color presents access
            $temp->remaining_advertisement = $this->getRemainingOf($res[0]->user_id, 'advertisement'); # Get remaining advertisements
            $temp->remaining_video = $this->getRemainingOf($res[0]->user_id, 'video_recording_dist'); # Get remaining video recording distribution
            $temp->remaining_group_message = $this->getRemainingOf($res[0]->user_id, 'group_message'); # Get remaining group message
            $temp->remaining_live_webinar = $this->getRemainingOf($res[0]->user_id, 'live_webinar'); # Get remaining live webinar

            $res[0]->privileges = $temp;

            if(@$res[0]->profile_pic == null)
            {
                $res[0]->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $res[0]->profile_pic = base_url("uploads/user_profile_pic/" . $res[0]->profile_pic);
            }

            $res = $res[0];

            /**
             * Check if user has pending payment(paynamics)
             */
            $pending_payment = $this->payment_model->checkPendingPaidPayment($res->user_id);
            if ($pending_payment) {
                $res->with_pending_payment = "Yes";
            }
            else {
                $res->with_pending_payment = "No";
            }

            if(password_verify($post['password'], $res->password))
            {
                $return = $res;
            }
            
        }

        return $return;
    }

    public function loginSns($post)
    {
        $return = null;

        $this->db->select("user_id, username, email, fname, lname, CONCAT(fname, ' ', lname) as fullname, membership_plan_id, app_users.created_at, app_users.updated_at, password, sns_token, first_log,profile_pic, referral_code, rank AS rank_id, rank_type");
        $this->db->from("app_users");
        $this->db->join("color_rank", "color_rank.color_rank_id = app_users.rank");
        $this->db->where("sns_token", $post['sns_token']);
        $res = $this->db->get()->result();

        if($res)
        {
            # Get membership plan name
            if ($res[0]->membership_plan_id != 0) {
                $plan = $this->plans_model->getPlan($res[0]->membership_plan_id);
                $res[0]->membership_plan_name = $plan->name;
                $res[0]->mp_access_limit = ($plan->market_place_access == -1) ? "-1" : "1";
            }
            else {
                $res[0]->membership_plan_name = "Not yet a member";
                $res[0]->mp_access_limit = "Not yet a member";
            }

            # Get current membership plan details
            $current_plan = $this->plans_model->getCurrentPlan($res[0]->user_id);
            if ($current_plan) {
                $res[0]->membership_expiry = $current_plan->expiration_date;
                if ($current_plan->expiration_date == "0000-00-00 00:00:00") {
                    $res[0]->membership_expiry = date("Y-m-d H:i:s", strtotime(date("Y-m-d") . "+30 days"));
                }
                
            }
            else {
                $res[0]->membership_expiry = "";
            }
            
            $temp = new stdClass();
            $temp->market_place_access = (object)$this->getMarketPlaceAccess($res[0]->user_id); # Get market place access
            $temp->chat_function = $this->chatFunction($res[0]->user_id); # Get chat function access
            $temp->color_bonus_access = $this->colorBonusAccess($res[0]->user_id); # Get color bonus access
            $temp->color_presents_access = $this->colorPresentsAccess($res[0]->user_id); # Get color presents access
            $temp->remaining_advertisement = $this->getRemainingOf($res[0]->user_id, 'advertisement'); # Get remaining advertisements
            $temp->remaining_video = $this->getRemainingOf($res[0]->user_id, 'video_recording_dist'); # Get remaining video recording distribution
            $temp->remaining_group_message = $this->getRemainingOf($res[0]->user_id, 'group_message'); # Get remaining group message
            $temp->remaining_live_webinar = $this->getRemainingOf($res[0]->user_id, 'live_webinar'); # Get remaining live webinar

            $res[0]->privileges = $temp;

            if(@$res[0]->profile_pic == null)
            {
                $res[0]->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $res[0]->profile_pic = base_url("uploads/user_profile_pic/" . $res[0]->profile_pic);
            }

            /**
             * Check if user has pending payment(paynamics)
             */
            $pending_payment = $this->payment_model->checkPendingPaidPayment($res[0]->user_id);
            if ($pending_payment) {
                $res[0]->with_pending_payment = "Yes";
            }
            else {
                $res[0]->with_pending_payment = "No";
            }

            $return = $res[0];
        }

        return $return;
    }


    public function verifyEmail($email)
    {
        $this->db->select("COUNT(user_id) as total");
        $this->db->where("LOWER(email) = '" . strtolower($email) . "'");
        $res = $this->db->get("app_users")->result()[0]->total;
        if($res == 0 && filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function verifySnsToken($token)
    {
        $this->db->select("COUNT(user_id) as total");
        $this->db->where("sns_token = '$token'");
        $res = $this->db->get("app_users")->result()[0]->total;
        if($res == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function verifyUsername($username)
    {
        $this->db->select("COUNT(user_id) as total");
        $this->db->where("LOWER(username) = '" . strtolower($username) . "'");
        $res = $this->db->get("app_users")->result()[0]->total;
        if($res == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function addVideo($post)
    {
        $this->db->insert('app_users_videos', $post);
        $last_id = $this->db->insert_id();
        return $this->getVideo($last_id);
    }

    public function getVideo($id)
    {
        $this->db->where('video_id', $id);
        $res = $this->db->get('app_users_videos')->result();
        return $res;
    }

    public function allVideos($user_id)
    {
        $this->table = "app_users_videos";
        $res = $this->all_by_user_id($user_id);
        return $res;
    }

    public function allRaw($search_keyword = NULL)
    {
        $this->db->select('app_users.*, color_rank.rank_type');
        $this->db->from('app_users');
        $this->db->join('color_rank', 'color_rank.color_rank_id = app_users.rank');
        if (@$search_keyword) {
            $this->db->group_start();
            $this->db->or_like("LOWER(app_users.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(app_users.fname, ' ', app_users.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.email)", strtolower($search_keyword), "both");
            $this->db->group_end();
        }
        $this->db->order_by('app_users.created_at', 'DESC');
        $users = $this->db->get()->result();
        foreach ($users as $key => $user) {
            if (@$user->profile_pic == null) {
                $users[$key]->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $users[$key]->profile_pic = base_url("uploads/user_profile_pic/" . $user->profile_pic);
            }
        }
        return $users;
    }

    public function all($search_keyword = NULL)
    {
        $this->db->select("user_id, username, email, fname, lname, CONCAT(fname, ' ', lname) as fullname, membership_plan_id, market_place_id, app_users.created_at, app_users.updated_at, password, first_log,profile_pic, referral_code, rank AS rank_id, rank_type");
        $this->db->from($this->table);
        $this->db->join("color_rank", "color_rank.color_rank_id = app_users.rank");
        if (@$search_keyword) {
            $this->db->group_start();
            $this->db->or_like("LOWER(app_users.fname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.lname)", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(CONCAT(app_users.fname, ' ', app_users.lname))", strtolower($search_keyword), "both");
            $this->db->or_like("LOWER(app_users.email)", strtolower($search_keyword), "both");
            $this->db->group_end();
        }
        $users = $this->db->get()->result();
        foreach ($users as $key => $user) {
            if (@$user->profile_pic == null) {
                $users[$key]->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $users[$key]->profile_pic = base_url("uploads/user_profile_pic/" . $user->profile_pic);
            }
        }
        return $users;
    }

    public function follow($data)
    {
        if ($this->is_following($data)) {
            # delete
            $this->db->where('follower_id', $data['follower_id']);
            $this->db->where('following_id', $data['following_id']);
            $this->db->delete("follow");
            $res['data']['follower'] = $this->getUser($data['follower_id']);
            $res['data']['following'] = $this->getUser($data['following_id']);
            $res['msg'] = "User successfully unfollowed";
        }
        else {
            # insert
            $this->db->insert("follow", $data);
            $res['data']['follower'] = $this->getUser($data['follower_id']);
            $res['data']['following'] = $this->getUser($data['following_id']);
            $res['msg'] = "User successfully followed";
        }

        return $res;
    }

    public function is_following($data)
    {
        $this->db->where('follower_id', $data['follower_id']);
        $this->db->where('following_id', $data['following_id']);
        $res = $this->db->get('follow')->result();
        return $res;
    }

    public function getFollowing($user_id)
    {
        $this->db->select("user_id, username, email, fname, lname, CONCAT(fname, ' ', lname) AS fullname, referral_code, profile_pic");
        $this->db->from("app_users");
        $this->db->join('follow', 'follow.following_id = app_users.user_id');
        $this->db->where('follow.follower_id = ' . $user_id);
        $res = $this->db->get()->result();

        /**
         * Change NULL values to empty string
         */
        foreach ($res as $key => $value) {
            foreach ($res[$key] as $key2 => $val) {
                if ($val == NULL) {
                    $res[$key]->$key2 = "";
                }
            }

            if(@$res[$key]->profile_pic == null)
            {
                $res[$key]->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $res[$key]->profile_pic = base_url("uploads/user_profile_pic/" . $res[$key]->profile_pic);
            }

        }
        return $res;
    }

    public function getUser($id)
    {
        $this->db->where('user_id', $id);
        $res = $this->db->get("app_users")->result();
        if (!$res) {
            return false;
        }
        else {
            $res = $res[0];
        }
        unset($res->password);
        // unset($res->forgot_token);
        if(@$res->profile_pic == null)
        {
            $res->profile_pic = base_url("uploads/user_profile_pic/default.png");
        }
        else {
            $res->profile_pic = base_url("uploads/user_profile_pic/" . $res->profile_pic);
        }

        $res->fullname = $res->fname . " " . $res->lname;

        /**
         * Change NULL values to empty string
         */
        foreach ($res as $key => $value) {
            if ($value == NULL) {
                $res->$key = "";
            }
        }

        return $res;
    }

    public function updateUser($user_id, $data)
    {
        return $this->db->update('app_users', $data, array('user_id' => $user_id));
    }

    public function getPassword($user_id)
    {
        return $this->db->get_where($this->table, ['user_id' => $user_id])->row()->password;
    }

    public function get($user_id)
    {
        $this->db->reset_query();
        $this->db->select('user_id, username, email,  login_type, fname, lname, mname, CONCAT(fname, " ", lname) AS fullname, referral_code, market_place_id, referred_by, points, bonus, referrals, profile_pic, market_place_name, rank AS rank_id, rank_type, app_users.created_at, app_users.updated_at, rank_photo, membership_plans.name AS plan_name, membership_plans.membership_plan_id, membership_plans.name AS membership_plan_name');
        $this->db->from("app_users");
        $this->db->join("color_rank", "color_rank.color_rank_id = app_users.rank", "left");
        $this->db->join("membership_plans", "membership_plans.membership_plan_id = app_users.membership_plan_id", "left");
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $res = $query->result();
        if ($res) {
            $res = $res[0];
            $res->mname = "";

            if ($res->rank_photo == "") {
                $res->rank_photo = base_url("frontend/img/rank_logo/" . str_replace(" Member", "_logo", $res->rank_type) . ".png");
            }
            else {
                $res->rank_photo = base_url("frontend/img/rank_logo/" . $result->rank_photo);
            }

            # Get membership plan name
            if ($res->membership_plan_id != 0) {
                $res->membership_plan_name = $this->plans_model->getPlan($res->membership_plan_id)->name;
            }
            else {
                $res->membership_plan_name = "Not yet a member";
            }

            # Get current membership plan details
            $current_plan = $this->plans_model->getCurrentPlan($res->user_id);
            if ($current_plan) {
                $res->membership_expiry = $current_plan->expiration_date;
                if ($current_plan->expiration_date == "0000-00-00 00:00:00") {
                    $res->membership_expiry = date("Y-m-d H:i:s", strtotime(date("Y-m-d") . "+30 days"));
                }
                
            }
            else {
                $res->membership_expiry = "";
            }

            /**
             * Check if user has pending payment(paynamics)
             */
            $pending_payment = $this->payment_model->checkPendingPaidPayment($res->user_id);
            if ($pending_payment) {
                $res->with_pending_payment = "Yes";
            }
            else {
                $res->with_pending_payment = "No";
            }

            $temp = new stdClass();
            $temp->market_place_access = (object)$this->getMarketPlaceAccess($res->user_id); # Get market place access
            $temp->chat_function = $this->chatFunction($res->user_id); # Get chat function access
            $temp->color_bonus_access = $this->colorBonusAccess($res->user_id); # Get color bonus access
            $temp->color_presents_access = $this->colorPresentsAccess($res->user_id); # Get color presents access
            $temp->remaining_advertisement = $this->getRemainingOf($res->user_id, 'advertisement'); # Get remaining advertisements
            $temp->remaining_video = $this->getRemainingOf($res->user_id, 'video_recording_dist'); # Get remaining video recording distribution
            $temp->remaining_group_message = $this->getRemainingOf($res->user_id, 'group_message'); # Get remaining group message
            $temp->remaining_live_webinar = $this->getRemainingOf($res->user_id, 'live_webinar'); # Get remaining live webinar

            $res->privileges = $temp;

            if(@$res->profile_pic == null)
            {
                $res->profile_pic = base_url("uploads/user_profile_pic/default.png");
            }
            else {
                $res->profile_pic = base_url("uploads/user_profile_pic/" . $res->profile_pic);
            }
        }
        
        return $res;
    }

    public function getBonus($user_id)
    {
        $this->db->select('app_users.user_id, app_users.points, color_bonus.free, color_bonus.challenger, color_bonus.economy, color_bonus.business');
        $this->db->from('color_bonus');
        $this->db->join('app_users', 'app_users.user_id = color_bonus.user_id');
        $this->db->where('color_bonus.user_id', $user_id);
        $query = $this->db->get();
        $res = $query->result()[0];
        return $res;
    }

    public function getReferrals($user_id)
    {
        $this->db->select('app_users.user_id,app_users.username, app_users.fname, app_users.lname, membership_plans.name AS membership_plan, ROUND(SUM(points_logs.points_added), 1) AS total_points, color_rank.rank_type');
        $this->db->from('app_users');
        $this->db->join('points_logs', 'points_logs.referred = app_users.user_id');
        $this->db->join('membership_plans', 'membership_plans.membership_plan_id = app_users.membership_plan_id');
        $this->db->join('color_rank', 'color_rank.color_rank_id = app_users.rank');
        $this->db->where('points_logs.referrer', $user_id);
        $this->db->group_by('points_logs.referred');
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }

    public function getReferralCode($user_id)
    {
        $this->db->select('referral_code');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('app_users');
        $res = $query->result()[0];
        return $res;
    }

    public function sendMail($to, $subject, $message)
    {
        $this->email->from('noreply@coloryourlife.com', 'Color Your Life');
        $this->email->to($to);
        // $this->email->bcc(['jcandres@myoptimind.com']);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }

    public function generateForgotToken($length = 60)
    {
        $characters = '0123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function getUserby($col, $val)
    {

        $this->db->where("$col = '$val'");
        $query = $this->db->get('app_users');
        $res = $query->row();
        return $res;
    }

    public function setForgotToken($user_id)
    {
        $token = $this->generateForgotToken();
        $this->db->where(["user_id" => $user_id]);
        return $this->db->update('app_users', ["forgot_token" => $token]);
    }

    public function updatePass($user_id, $pass)
    {
        $this->db->where(["user_id" => $user_id]);
        return $this->db->update('app_users',
            ["password" => password_hash($pass, PASSWORD_DEFAULT)]);
    }

    public function clearToken($user_id)
    {
        $this->db->where(["user_id" => $user_id]);
        return $this->db->update('app_users', ["forgot_token" => '']);
    }

    public function checkUser($post)
    {
       $post['username'] = $post['email'];
       return $this->login($post);
    }

    public function getSuccessfulReferrals($user_id)
    {
        $user = $this->getUser($user_id);

        if(!$user){
            throw new Exception("User not found");
            return false;
        }

        $success = $this->db->from('app_users')
                      ->where(['referred_by' => $user_id])
                      ->get()
                      ->num_rows();


        return $user->referrals - ($user->referrals - $success);
    }

    public function getEachReferralCount($user_id)
    {
        $ranks = $this->rank_model->all();
        foreach ($ranks as $rank) {
            $referralCount[$rank->rank_type] = 0;
        }

        $this->db->select('app_users.fname, app_users.lname, color_rank.rank_type');
        $this->db->from('app_users');
        $this->db->join('color_rank', 'color_rank.color_rank_id = app_users.rank');
        $this->db->where('referred_by', $user_id);
        $referrals = $this->db->get()->result();

        foreach($referrals as $referral)
        {
            foreach ($referralCount as $key => $count) {
                if ($referral->rank_type == $key) {
                    $referralCount[$key]++;
                }
            }
        }

        return $referralCount;
    }

    public function getColorPoints($user_id)
    {
        $this->db->select('app_users.user_id, color_rank.rank_type, app_users.points, color_rank.rank_photo');
        $this->db->from('app_users');
        $this->db->join('color_rank', 'color_rank.color_rank_id = app_users.rank');
        $this->db->where('app_users.user_id = ' . $user_id);
        $result = $this->db->get()->result()[0];

        if ($result->rank_photo == "") {
            $result->rank_photo = base_url("frontend/img/rank_logo/" . str_replace(" Member", "_logo", $result->rank_type) . ".png");
        }
        else {
            $result->rank_photo = base_url("frontend/img/rank_logo/" . $result->rank_photo);
        }

        return $result;
    }

    public function countPresentsReferral($user_id)
    {
			$ranks = $this->rank_model->all();
			unset($ranks[0]); # exclide wthitee member

			$bilog = [];

			foreach ($ranks as $key => $value) {
				$county = $this->countReferrerIds($user_id, $value->color_rank_id);
				$bilog[$value->rank_type] = $county;
			}

			return $bilog;
    }

		public function countReferrerIds($user_id, $rank)
		{
			return count($this->db->get_where($this->table, ['rank' => $rank, 'referred_by' => $user_id])->result());
		}

		public function getUserPresentStats($user_id)
		{
			$user = $this->get($user_id);
            if ($user->rank_photo == "") {
                $user->rank_photo = base_url("frontend/img/rank_logo/" . str_replace(" Member", "_logo", $user->rank_type) . ".png");
            }
            else {
                $user->rank_photo = $user->rank_photo;
            }
			return array_merge(
				$this->countPresentsReferral($user_id),
				['points' => $user->points, 'rank_type' => $user->rank_type, 'referrals' => $user->referrals, 'rank_photo' => $user->rank_photo]
			);
		}


    ##### Check available access for a user #####
    
    public function chatFunction($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'active');
        $this->db->limit(1);
        $res = $this->db->get('user_membership_plan')->result();
        $final = false;
        if ($res) {
            if ($res[0]->chat_function == "allowed") {
                $final = true;
            }
        }

        return $final;
    }

    public function colorBonusAccess($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'active');
        $this->db->limit(1);
        $res = $this->db->get('user_membership_plan')->result();
        $final = false;
        if ($res) {
            if ($res[0]->special_bonuses == "with") {
                $final = true;
            }
        }

        return $final;
    }

    public function colorPresentsAccess($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'active');
        $this->db->limit(1);
        $res = $this->db->get('user_membership_plan')->result();
        $final = false;
        if ($res) {
            if ($res[0]->presents == "allowed") {
                $final = true;
            }
        }

        return $final;
    }
    
    public function getMarketPlaceAccess($user_id)
    {
        $final = array();
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'active');
        $this->db->limit(1);
        $res = $this->db->get('user_membership_plan')->result();
        if ($res) {
            if ($res[0]->market_place_access == "-1") {
                $mp = $this->getMarketPlace();
                foreach ($mp as $value) {
                    $final[$value->name] = true;
                }
            }
            elseif ($res[0]->market_place_access != "0") {
                $mp = $this->getMarketPlace($res[0]->market_place_access);
                foreach ($mp as $value) {
                    $final[$value->name] = true;
                }   
            }
        }

        return $final;
    }

    /**
     * Types: advertisement, video_recording_dist, group_message, live_webinar
     */
    public function getRemainingOf($user_id, $type)
    {
        $this->db->select("$type as count");
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'active');
        $this->db->limit(1);
        $res = $this->db->get('user_membership_plan')->result();
        $final = 0;
        if ($res) {
            $final = $res[0]->count;
        }

        return $final;
    }

    /**
     * Types: advertisement, video_recording_dist, group_message, live_webinar
     */
    public function updateRemainingOf($user_id, $type, $num)
    {
        $this->db->where("user_id", $user_id);
        $this->db->where("status", "active");
        $this->db->set($type, "$type + $num", FALSE);
        $this->db->limit(1);
        return $this->db->update("user_membership_plan");
    }

    /**
     * Get Market Place Name
     * used in getMarketPlaceAccess()
     */
    public function getMarketPlace($market_place_id = NULL)
    {
        if ($market_place_id) {
            $this->db->where("market_place_id", $market_place_id);
        }
        
        $res = $this->db->get('market_place')->result();
        return $res;
    }

    public function updateMarketPlace($user_id, $market_place_id)
    {
        $this->db->set("market_place_access", $market_place_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('status', 'active');
        $this->db->limit(1);
        $res = $this->db->update('user_membership_plan');
        return $res;
    }

    public function getCountUserperCountry()
    {
        $query = $this->db->query('SELECT COUNT(user_id) as count, app_users.country, meta_location.local_name FROM `app_users` JOIN meta_location ON app_users.country=meta_location.iso GROUP BY app_users.country');
        return $this->formatNumber($query->result());

    }

    public function formatNumber($arr)
    {
        if (!$arr) {
            return true;
        }
        $ret = [];
        foreach ($arr as $key => $value) {
            $value->format_number = $this->formatPrice($value->count);
            $ret[] = $value;
        }

        return $ret;
    }

    public function formatPrice($n)
    {
        if ($n < 1000) {
            $f = $n;
        }else if ($n < 1000000) {
            // Anything less than a million
            $f = round(number_format($n / 1000, 3), 2);
            $f .= 'K';
        } else if ($n < 1000000000) {
            // Anything less than a billion
            $f = round(number_format($n / 1000000, 3), 2);
            $f .= 'M';
        } else {
            // At least a billion
            $f = round(number_format($n / 1000000000, 3), 2);
            $f .= 'B';
        }


        return $f;
    }
}
?>
