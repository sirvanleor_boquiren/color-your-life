<?php
class Post_reviews_model extends Crud_model {
	public function __construct()
    {
        parent::__construct();
        $this->table = "post_reviews";
        $this->table_pk = "post_review_id";
        // $this->load->database();
    }

    public function add($data)
    {
        $post_review_id = $this->checkUserPostReview($data['user_id'], $data['post_id']);
        if ($post_review_id) {
            $this->updatePostReview($post_review_id, ["status" => "inactive"]);
        }

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function deletePostReview($post_review_id)
    {
        $this->db->where("post_review_id", $post_review_id);
        return $this->db->delete($this->table);
    }

    public function updatePostReview($post_review_id, $data)
    {
        $this->db->where("post_review_id", $post_review_id);
        return $this->db->update($this->table, $data);
    }

    public function checkUserPostReview($user_id, $post_id)
    {
        $this->db->where("user_id", $user_id);
        $this->db->where("post_id", $post_id);
        $this->db->where("status", "active");
        $res = $this->db->get($this->table)->result();
        if ($res) {
            return $res[0]->post_review_id;
        }
        else {
            return false;
        }
    }

    public function allByPost($post_id)
    {
        $final = array();
        $this->db->where("post_id", $post_id);
        $this->db->where("status", "active");
        $this->db->order_by("created_at", "DESC");
        $res = $this->db->get($this->table)->result();
        foreach ($res as $obj) {
            $final[] = $obj;
        }

        return $final;
    }
}
