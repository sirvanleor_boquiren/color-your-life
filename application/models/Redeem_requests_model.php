<?php
class Redeem_requests_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "redeem_requests";
        $this->table_pk = "redeem_request_id";
    }

    public function getAllWithUserAndAffiliate()
    {
        $query = $this->db->select('redeem_requests.*,affiliates.title as affiliate_name,app_users.username as user_name')
                          ->from('redeem_requests')
                          ->join('affiliates','redeem_requests.affiliate_id = affiliates.affiliate_id')
                          ->join('app_users','redeem_requests.user_id = app_users.user_id')
                          ->get();
        return $query->result();
    }
}
?>
