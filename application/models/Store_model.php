<?php

class Store_model extends CI_model
{
	public function __construct()
	{
		# ...
	}

	#Get Stores
	public function getStores()
	{		
		$this->db->select("c_id, c_name");
		$this->db->from("clients");
		$db_results = $this->db->get();
		if($db_results->num_rows() >= 1) 
		{
			$db_data = array();
			// $db_data = $db_results->result();
			foreach ($db_results->result() as $key => $client_arr) {
				$this->db->select("s_id, c_id, s_code, s_loc_lat, s_loc_long, s_name");
				$this->db->from("stores");
				$this->db->where("c_id = ".$client_arr->c_id." AND s_is_active = 1");
				$stores_db_results = $this->db->get();
				$client_arr->stores = array();
				if($stores_db_results->num_rows() >= 1)
				{
					foreach ($stores_db_results->result() as $key => $store_arr) {
						$client_arr->stores[] = $store_arr;
					}
				}
				$db_data["clients"][] = $client_arr;
			}
			return $db_data;
		}
		else
		{
			return "false";
		}
	}

	#Add Store
	public function addStore($post)
	{
		$this->load->model("ajax_model");
		$store_cols = array("s_name", "s_code", "s_manager", "s_manager_email", "s_loc_lat", "s_loc_long");
		$client_data = array();

		if(isset($post["c_name"]) && $post["c_name"] != "")
		{
			if(!$this->ajax_model->checkExists($post, "clients", "c_id", "c_name"))
			{
				$client_data["c_name"] = $post["c_name"];
				$client_data["c_added_by"] = $this->session->userdata['id'];
				if($this->db->insert("clients", $client_data))
				{
					$client_id = $this->db->insert_id();
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		elseif(isset($post['c_id']) && $post['c_id'] != "")
		{
			$client_id = $post['c_id'];
		}
		else
		{
			return false;
		}

		$checkCode = $this->ajax_model->checkExists($post, "stores", "s_id", "s_code");
		$checkManagerMail = $this->ajax_model->checkExists($post, "stores", "s_id", "s_manager_email");
		
		if(isset($client_id) && $client_id != "" && !$checkCode && !$checkManagerMail)
		{
			$store_data = array();
			$store_data['s_added_by'] = $this->session->userdata['id'];
			$store_data['c_id'] = $client_id;
			foreach ($store_cols as $col_name) {
				$store_data[$col_name] = $post[$col_name];
			}
			if($this->db->insert("stores", $store_data))
			{
				$store_audit_data["s_id"] = $this->db->insert_id();
				$this->db->where("sa_id", $post['sa_id']);
				return $this->db->update("store_audit", $store_audit_data);
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	#Store Change Status
	public function changeStatus($id, $status)
	{
		# Status: 0 - disabled, 1 - Active
		if(!$this->db->update("stores", array("s_is_active" => $status), array("s_id" => $id)))
		{
			return array("status"=>"400", "message"=>"Changing of store status failed");
		}
	}

	# POST Check if store code for specific client exists
	public function checkStoreName($post)
	{
		$match = false;
		if(isset($post['s_name']) && isset($post['c_id']))
		{
			$this->db->select("s_id");
			$this->db->from("stores");
			$this->db->where("s_name = '".$post['s_name']."' AND c_id = '".$post['c_id']."'");
			if($this->db->get()->num_rows() != 0)
			{
				$match = true;
			}
		}

		return $match;
	}

	# CMS

	# Get Total Number of Stores
	public function getTotalStores()
	{
		$this->db->select("s_id");
		$this->db->from("stores");
		$this->db->where("s_is_active = 1");
		return $this->db->get()->num_rows();
	}
	
	public function getSpecificStore($id)
	{
		$store_name = "N/A";
		$this->db->select("stores.s_name, clients.c_name");
		$this->db->from("stores");
		$this->db->join("clients", "stores.c_id = clients.c_id");
		$this->db->where("s_is_active", "1");
		$this->db->where("s_id", $id);
		$db_result = $this->db->get();
		if($db_result->num_rows() > 0)
		{
			foreach ($db_result->result() as $store_arr) {
				$store_name = $store_arr->c_name." ".$store_arr->s_name;
			}
		}

		return $store_name;
	}

	public function getStoresList()
	{
		$return_arr = array();
		foreach ($this->getStores() as $obj_arr) {
			foreach ($obj_arr as $clients_obj) {
				foreach ($clients_obj->stores as $stores_obj) {
					$stores_obj->store_name = $clients_obj->c_name." ".$stores_obj->s_name;
					$return_arr[] = $stores_obj;
				}
			}
		}
		return $return_arr;
	}

	# Get All Clients
	public function getClients()
	{
		$this->db->from("clients");
		$db_clients = $this->db->get();
		if($db_clients->num_rows() >0)
		{
			return $db_clients->result();
		}
		else
		{
			return null;
		}
	}

	# Get All Summary of stores
	public function getSummary($is_reports = false) # $is_reports is when called for exporting data, which outputs no limit
	{
		// $date_max = "2017-06-05 23:59:59";
		$date_max = "";
		if(isset($_GET['date_range']) && $_GET['date_range'] != "" && is_numeric($_GET['date_range']))
		{
			$date_max = date("Y-m-d 23:59:59", strtotime("-".$_GET['date_range']." days"));
		}
		
		$this->db->select("stores.*, clients.c_name");
		$this->db->from("stores");
		$this->db->join("clients", "stores.c_id = clients.c_id");
		$this->db->where("s_is_active = 1");
		if(!$is_reports)
		{
			# Pagination set limit
			$limit = 0;
			if($this->uri->segment(3) !== FALSE)
			{
				$limit = $this->uri->segment(3);
			}
			$this->db->limit(TABLE_DATA_PER_PAGE, $limit); # number_per_page, start row
		}
		$db_stores = $this->db->get();
		
		$db_data = array();
		if($db_stores->num_rows() > 0)
		{
			foreach ($db_stores->result() as $store) {
				# Final Store name
				$store->store_name = $store->c_name." ".$store->s_name;

				# Set Default Values incase of no audits
				$store->sa_visit_date = "No Audits recorded yet";
				$store->store_rating = "";
				$store->average_grade = "N/A";

				# Get all audits for the store
				$this->db->select("sa_id, sa_visit_date");
				$this->db->from("store_audit");
				$this->db->where("s_id = ".$store->s_id);
				$this->db->order_by("sa_visit_date", "asc");
				$db_audits = $this->db->get();
				if($db_audits->num_rows() > 0)
				{
					$all_sa_id = array();
					foreach ($db_audits->result() as $audit) {
						$all_sa_id[] = $audit->sa_id;

						# Final Most recent Visit date of store
						$store->sa_visit_date = $audit->sa_visit_date;
					}

					# Get all Ratings for specific Store
					$this->db->select("ROUND(AVG(r_rate), 2) as total_rate");
					$this->db->from("ratings");
					$this->db->where("sa_id IN (".implode(",", $all_sa_id).")");
					$db_ratings = $this->db->get();
					
					if($db_ratings->num_rows() > 0)
					{
						$store->store_rating = "";
						$average_rate = 0;
						foreach ($db_ratings->result() as $ratings) {
							$average_rate = $ratings->total_rate;
						}
						# Final Average grade performance
						$store->average_grade = $average_rate;
						for ($i=1; $i <= 5; $i++) { 
							$star = "star ";

							# IF star is empty
							if($average_rate <= 0)
							{
								$star = "star-disabled ";
							}
							
							# IF star is half
							if($average_rate < 1 && $average_rate > 0)
							{
								$star = "star-half ";
							}

							# Append return data for store rating		
							$store->store_rating .= "<span class='".$star."'></span>";

							# Decrement percent, 100 (percent) / $i (amount of stars)
							$average_rate--;
						}
					}

				}

				if($date_max != "")
				{
					if($store->sa_visit_date == "No Audits recorded yet" || $store->sa_visit_date <= $date_max)
					{
						$db_data[] = $store;
					}
				}
				else
				{
					$db_data[] = $store;
				}

				
			}
		}
		return $db_data;
	}

}

?>