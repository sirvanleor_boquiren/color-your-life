<?php

class Follow_model extends CI_model
{
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * [getFollowers description]
   * @param  string $value [description]
   * @return array of follower ids        [description]
   */
  public function getFollowers($user_id)
  {
    $this->db->select('follower_id');
    $this->db->where('following_id', $user_id);
    $res = [];
    $data = $this->db->get('follow')->result();
    foreach ($data as $key => $value) {
      $res[] = $value->follower_id;
    }
    return $res;
  }

}
