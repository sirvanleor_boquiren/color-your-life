<?php
class User_device_ids_model extends Crud_model {

	public function __construct()
	{
		parent::__construct();
		$this->table = "user_device_ids";
		$this->table_pk = "udi_id";

		// $this->load->database();
	}

	/**
	 * @author @jjjjcccjjf
	 */
	public function getDeviceIdsByUserId($user_id)
	{
		$res = $this->db->get_where($this->table, ['user_id' => $user_id, 'is_active' => 1])->result();
		$device_ids_arr = [];
		foreach ($res as $key => $value) {
			$device_ids_arr[] = $value->firebase_id;
		}
		return $device_ids_arr;
	}

	public function loginDevice($user_id, $device_id, $firebase_id)
	{
		$this->logoutDevice($device_id); # let all devices not receive notifs

		$udi_id = $this->checkExists($user_id, $device_id, $firebase_id); # Check the existing PK if there is one

		if(!$udi_id){ # if there is no record in the database
			$last_id = $this->addDevice($user_id, $device_id, $firebase_id);
			$res = $this->activateDevice($last_id);
		} else {
			$res = $this->activateDevice($udi_id);
		}
		return $res;
	}

	/**
	 * [activateDevice description]
	 * @param  [type] $udi_id PK of user device ID
	 * @return [type]         [description]
	 */
	public function activateDevice($udi_id)
	{
		$this->db->where('udi_id', $udi_id);
		return $this->db->update($this->table, ['is_active' => 1]);
	}

	public function addDevice($user_id, $device_id, $firebase_id)
	{
		$this->db->insert($this->table, ['user_id' => $user_id,
		'device_id' => $device_id,
		'firebase_id' => $firebase_id]);

		return $this->db->insert_id();
	}

	public function logoutDevice($device_id)
	{
		$this->db->where('device_id', $device_id);
		return $this->db->update($this->table, ['is_active' => 0]);
	}

	public function checkExists($user_id, $device_id, $firebase_id)
	{
		return @$this->db->get_where($this->table, ['user_id' => $user_id, 'device_id' => $device_id, 'firebase_id' => $firebase_id])->row()->udi_id;
	}

}
?>
