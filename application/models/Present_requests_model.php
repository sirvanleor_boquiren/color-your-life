<?php
class Present_requests_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "present_requests";
        $this->table_pk = "present_request_id";
    }

    public function add($data)
    {
        $selected_presents = $data['presents'];
        unset($data['presents']);
        $added = parent::add($data);
        $request_id = $this->db->insert_id();
        
        foreach($selected_presents as $present_id)
        {
            $this->db->insert('request_present',[
                'request_id' => $request_id,
                'present_id' => $present_id
            ]);
        }

        return (boolean)$added;

        
    }

    public function getAllWithUser()
    {
        $query = $this->db->select('present_requests.*,app_users.username as user_name')
                          ->from('present_requests')
                          ->join('app_users','present_requests.user_id = app_users.user_id');
        return $query->get()->result();
    }

}
?>
