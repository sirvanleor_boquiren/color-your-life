<?php
class Categories_model extends Crud_model {

	public function __construct()
    {
        parent::__construct();
        $this->table = "categories";
        $this->table_pk = "category_id";
    }

    # API

    public function all_categories($market_place_id)
    {
        $this->db->select("category_id, name, market_place_id");
        if ($market_place_id) {
            $this->db->where("market_place_id", $market_place_id);
        }
        $this->db->order_by("name", "ASC");
        $this->db->limit(0);
        return $this->db->get($this->table)->result();
    }
}
?>