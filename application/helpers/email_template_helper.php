<?php
function send_message_template($data)
{
  $body = '
  <center>
    <p><img src="' . $data['sender_profile_pic'] . '" width="100" height="100"></p>
    <p><b>' . $data['sender_fullname'] . '</b></p>
  </center>
  <br>
  <center><p>"' . $data['message'] . '"</p></center>
  ';
  return default_1_email_template('message', 'New Message!', $body);
}

function reviewed_user_template($data)
{
  $body = '
  <center><p>Someone wrote a you a review</p></center>
  <br>
  <center>
    <p><img src="' . $data['reviewer_profile_pic'] . '" width="100" height="100"></p>
    <p><b>' . $data['reviewer_fullname'] . '</b></p>
  </center>
  <br>
  <center><p>"' . $data['review'] . '"</p></center>
  ';
  return default_1_email_template('new_user_review', 'Hey there, ' . $data['fname'] . '!', $body);
}

function reviewed_post_template($data)
{
  $body = '
  <center><p>Someone wrote a review on your <i>' . $data['post_name'] . '</i> post</p></center>
  <br>
  <center>
    <p><img src="' . $data['reviewer_profile_pic'] . '" width="100" height="100"></p>
    <p><b>' . $data['reviewer_fullname'] . '</b></p>
  </center>
  <br>
  <center><p>"' . $data['review'] . '"</p></center>
  ';
  return default_1_email_template('new_post_review', 'Hey there, ' . $data['fname'] . '!', $body);
}

function liked_post_template($data)
{
  $body = '
  <center>
    <p><img src="' . $data['liker_profile_pic'] . '" width="100" height="100"></p>
    <p><b>' . $data['liker_fullname'] . '</b></p>
  </center>
  <br>
  <center><p>liked your <i>' . $data['post_name'] . '</i> post</center>
  ';
  return default_1_email_template('liked_post', 'Hey there, ' . $data['fname'] . '!', $body);
}

function new_referral_template($data)
{
  $body = '<p>Hey there, ' . $data['fname'] . '!</p>
  <p>Great news, someone just signed up through your referral code.</p>
  <br>
  <center>
    <p><img src="' . $data['referral_profile_pic'] . '" width="100" height="100"></p>
    <p><b>' . $data['referral_fullname'] . '</b></p>
  </center>
  ';
  return default_1_email_template('new_referral', 'Successful Referral!', $body);
}

function user_follow_template($data)
{
  $body = '<p>Hey there, ' . $data['fname'] . '!</p>
  <p>You have a new follower on Color Your Life App.</p>
  <br>
  <center>
    <p><img src="' . $data['follower_profile_pic'] . '" width="100" height="100"></p>
    <p><b>' . $data['follower_fullname'] . '</b><br><span style="font-size: 15px;">' . $data['follower_marketplace_name'] . '</span></p>
  </center>
  ';
  return default_1_email_template('user_follow', 'A new follower!', $body);
}

function new_post_template($data)
{
  return default_1_email_template('new_post', 'Hi ' . $data['fullname'], $data['username'] . ' post ' . $data['post_name']);
}

function default_1_email_template($email_type, $hero = "Color Your Life Ltd.", $copy_block = "Let's make your life more colorful!", $logo = "https://coloryourlifeapp.com/app/frontend/img/cyl_logo.png")
{
  $logo_img = '<img src="' . $logo . '" width="283" height="185" style="display: block;  font-family: \'Lato\', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;" border="0">';
  $hero = '<h1 style="font-size: 32px; font-weight: 400; margin: 0;">' . $hero . '</h1>';
    $copy_block = '<p style="margin: 0;">' . $copy_block . '</p>';
  
  if ($email_type == "message") {
    $copy_callout = '
          <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
              <table border="0" cellpadding="0" cellspacing="0" width="480">
                <!-- COPY -->
                <tr>
                  <td bgcolor="#111111" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: \'Lato\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                    <p style="margin: 0;">Reply through the Color Your Life App</p>
                    <p style="margin: 0;">Go now!</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
    ';
  }
  else {
    $copy_callout = '
          <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
              <table border="0" cellpadding="0" cellspacing="0" width="480">
                <!-- COPY -->
                <tr>
                  <td bgcolor="#111111" align="left" style="padding: 20px 30px 20px 30px; color: #666666; font-family: \'Lato\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                    <p style="margin: 0;">Open the Color Your Life mobile app now!</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
    ';
  }

  $footer = '
        <tr>
          <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="480">

              <!-- PERMISSION REMINDER -->

              <!-- ADDRESS -->
              <tr>
                <td bgcolor="#f4f4f4" align="left" style="padding: 0px 30px 30px 30px; color: #666666; font-family: \'Lato\', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;">
                  <p style="margin: 0;">This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them in this email address. <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
  ';
  

  return '
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  </head>
  <body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <!-- LOGO -->
      <tr>
        <td bgcolor="#7c72dc" align="center">
          <table border="0" cellpadding="0" cellspacing="0" width="480">
                <tr>
                  <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">
                    <a href="" target="_blank">
                      ' . $logo_img . '
                    </a>
                  </td>
                </tr>
              </table>
        </td>
      </tr>

      <!-- HERO -->
        <tr>
          <td bgcolor="#7c72dc" align="center" style="padding: 0px 10px 0px 10px;">
              <table border="0" cellpadding="0" cellspacing="0" width="480">
                <tr>
                  <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: \'Lato\', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                      ' . $hero . '
                    </td>
                </tr>
              </table>
          </td>
        </tr>

        <!-- COPY BLOCK -->
        <tr>
          <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="480">
              <!-- COPY -->
              <tr>
                <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: \'Lato\', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                  ' . $copy_block . '
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <!-- COPY CALLOUT -->
        ' . $copy_callout . '

        <!-- FOOTER -->
        ' . $footer . '

    </table>
  </body>
</html>
  ';
}

function rejected_color_presents_template($name)
{
  return '
<p>Hi <b>' . $name . '</b>,</p>
<p>We are sorry to say that your request for Color Present has been rejected.</p>
<p>Please try again with other presents. Thank you!</p>
<p>This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them in this email address. <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></p>
  ';
}

function approved_color_presents_template($name)
{
  return '
<p>Hi <b>' . $name . '</b>,</p>
<p>Congratulations! Your request for Color Present has been approved.</p>
<p>We will email you the instructions on how to claim your present/s.</p>
<p>This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them in this email address. <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></p>
  ';
}

function apply_for_color_presents_template($name = "")
{
  return '
<p>Hi <b>' . $name . '</b>,<p>
<p>You have successfully applied for a Color Presents</p>
<p>Please wait while we evaluate your request</p>
<p>Thank you!</p>
<p>This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them in this email address. <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></p>
  ';
}

function rejected_color_points_template($name)
{
  return '
<p>Hi <b>' . $name . '</b>,</p>
<p>Your request to redeem color points has been rejected.</p>
<p>&nbsp;</p>
<p>This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them in this email address. <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></p>
  ';
}

function approved_color_points_template($name)
{
  return '
<p>Hi <b>' . $name . '</b>,</p>
<p>Your request to redeem color points has been approved.</p>
<p>Please wait for your item to be delivered.</p>
<p>This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them in this email address. <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></p>
  ';
}

function redeem_color_points_template($affiliate, $link)
{
  return '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="format-detection" content="telephone=no" />
  <!-- disable auto telephone linking in iOS -->
  <title>Color Your Life</title>
  <style type="text/css">
    /* RESET STYLES */

    html {
      background-color: #E1E1E1;
      margin: 0;
      padding: 0;
    }

    body,
    #bodyTable,
    #bodyCell,
    #bodyCell {
      height: 100% !important;
      margin: 0;
      padding: 0;
      width: 100% !important;
      font-family: Helvetica, Arial, "Lucida Grande", sans-serif;
    }

    table {
      border-collapse: collapse;
    }

    table[id=bodyTable] {
      width: 100% !important;
      margin: auto;
      max-width: 500px !important;
      color: #7A7A7A;
      font-weight: normal;
    }

    img,
    a img {
      border: 0;
      outline: none;
      text-decoration: none;
      height: auto;
      line-height: 100%;
    }

    a {
      text-decoration: none !important;
      border-bottom: 1px solid;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      color: #5F5F5F;
      font-weight: normal;
      font-family: Helvetica;
      font-size: 20px;
      line-height: 125%;
      text-align: Left;
      letter-spacing: normal;
      margin-top: 0;
      margin-right: 0;
      margin-bottom: 10px;
      margin-left: 0;
      padding-top: 0;
      padding-bottom: 0;
      padding-left: 0;
      padding-right: 0;
    }

    /* CLIENT-SPECIFIC STYLES */

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    /* Force Hotmail/Outlook.com to display emails at full width. */

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    /* Force Hotmail/Outlook.com to display line heights normally. */

    table,
    td {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    /* Remove spacing between tables in Outlook 2007 and up. */

    #outlook a {
      padding: 0;
    }

    /* Force Outlook 2007 and up to provide a "view in browser" message. */

    img {
      -ms-interpolation-mode: bicubic;
      display: block;
      outline: none;
      text-decoration: none;
    }

    /* Force IE to smoothly render resized images. */

    body,
    table,
    td,
    p,
    a,
    li,
    blockquote {
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      font-weight: normal !important;
    }

    /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */

    .ExternalClass td[class="ecxflexibleContainerBox"] h3 {
      padding-top: 10px !important;
    }

    /* Force hotmail to push 2-grid sub headers down */

    /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

    /* ========== Page Styles ========== */

    h1 {
      display: block;
      font-size: 26px;
      font-style: normal;
      font-weight: normal;
      line-height: 100%;
    }

    h2 {
      display: block;
      font-size: 20px;
      font-style: normal;
      font-weight: normal;
      line-height: 120%;
    }

    h3 {
      display: block;
      font-size: 17px;
      font-style: normal;
      font-weight: normal;
      line-height: 110%;
    }

    h4 {
      display: block;
      font-size: 18px;
      font-style: italic;
      font-weight: normal;
      line-height: 100%;
    }

    .flexibleImage {
      height: auto;
    }

    .linkRemoveBorder {
      border-bottom: 0 !important;
    }

    table[class=flexibleContainerCellDivider] {
      padding-bottom: 0 !important;
      padding-top: 0 !important;
    }

    body,
    #bodyTable {
      background-color: #E1E1E1;
    }

    #emailHeader {
      background-color: #E1E1E1;
    }

    #emailBody {
      background-color: #FFFFFF;
    }

    #emailFooter {
      background-color: #E1E1E1;
    }

    .nestedContainer {
      background-color: #F8F8F8;
      border: 1px solid #CCCCCC;
    }

    .emailButton {
      background-color: #205478;
      border-collapse: separate;
    }

    .buttonContent {
      color: #FFFFFF;
      font-family: Helvetica;
      font-size: 18px;
      font-weight: bold;
      line-height: 100%;
      padding: 15px;
      text-align: center;
    }

    .buttonContent a {
      color: #FFFFFF;
      display: block;
      text-decoration: none !important;
      border: 0 !important;
    }

    .emailCalendar {
      background-color: #FFFFFF;
      border: 1px solid #CCCCCC;
    }

    .emailCalendarMonth {
      background-color: #205478;
      color: #FFFFFF;
      font-family: Helvetica, Arial, sans-serif;
      font-size: 16px;
      font-weight: bold;
      padding-top: 10px;
      padding-bottom: 10px;
      text-align: center;
    }

    .emailCalendarDay {
      color: #205478;
      font-family: Helvetica, Arial, sans-serif;
      font-size: 60px;
      font-weight: bold;
      line-height: 100%;
      padding-top: 20px;
      padding-bottom: 20px;
      text-align: center;
    }

    .imageContentText {
      margin-top: 10px;
      line-height: 0;
    }

    .imageContentText a {
      line-height: 0;
    }

    #invisibleIntroduction {
      display: none !important;
    }

    /* Removing the introduction text from the view */

    /*FRAMEWORK HACKS & OVERRIDES */

    span[class=ios-color-hack] a {
      color: #275100 !important;
      text-decoration: none !important;
    }

    /* Remove all link colors in IOS (below are duplicates based on the color preference) */

    span[class=ios-color-hack2] a {
      color: #205478 !important;
      text-decoration: none !important;
    }

    span[class=ios-color-hack3] a {
      color: #8B8B8B !important;
      text-decoration: none !important;
    }

    .a[href^="tel"],
    a[href^="sms"] {
      text-decoration: none !important;
      color: #606060 !important;
      pointer-events: none !important;
      cursor: default !important;
    }

    .mobile_link a[href^="tel"],
    .mobile_link a[href^="sms"] {
      text-decoration: none !important;
      color: #606060 !important;
      pointer-events: auto !important;
      cursor: default !important;
    }


    /* MOBILE STYLES */

    @media only screen and (max-width: 480px) {
      /*////// CLIENT-SPECIFIC STYLES //////*/
      body {
        width: 100% !important;
        min-width: 100% !important;
      }
      /* Force iOS Mail to render the email at full width. */
      /* FRAMEWORK STYLES */
      /*
        CSS selectors are written in attribute
        selector format to prevent Yahoo Mail
        from rendering media query styles on
        desktop.
        */
      /*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
      table[id="emailHeader"],
      table[id="emailBody"],
      table[id="emailFooter"],
      table[class="flexibleContainer"],
      td[class="flexibleContainerCell"] {
        width: 100% !important;
      }
      td[class="flexibleContainerBox"],
      td[class="flexibleContainerBox"] table {
        display: block;
        width: 100%;
        text-align: left;
      }
      td[class="imageContent"] img {
        height: auto !important;
        width: 100% !important;
        max-width: 100% !important;
      }
      img[class="flexibleImage"] {
        height: auto !important;
        width: 100% !important;
        max-width: 100% !important;
      }
      img[class="flexibleImageSmall"] {
        height: auto !important;
        width: auto !important;
      }


      /*
        Create top space for every second element in a block
        */
      table[class="flexibleContainerBoxNext"] {
        padding-top: 10px !important;
      }

      /*
        Make buttons in the email span the
        full width of their container, allowing
        for left- or right-handed ease of use.
        */
      table[class="emailButton"] {
        width: 100% !important;
      }
      td[class="buttonContent"] {
        padding: 0 !important;
      }
      td[class="buttonContent"] a {
        padding: 15px !important;
      }

    }

    /*  CONDITIONS FOR ANDROID DEVICES ONLY
      *   https://developer.android.com/guide/webapps/targeting.html
      *   https://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
      =====================================================*/

    @media only screen and (-webkit-device-pixel-ratio:.75) {
      /* Put CSS for low density (ldpi) Android layouts in here */
    }

    @media only screen and (-webkit-device-pixel-ratio:1) {
      /* Put CSS for medium density (mdpi) Android layouts in here */
    }

    @media only screen and (-webkit-device-pixel-ratio:1.5) {
      /* Put CSS for high density (hdpi) Android layouts in here */
    }

    /* end Android targeting */

    /* CONDITIONS FOR IOS DEVICES ONLY
      =====================================================*/

    @media only screen and (min-device-width: 320px) and (max-device-width:568px) {}

    /* end IOS targeting */

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  padding: 0 4px;
}

.column {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
  max-width: 20%;
  padding: 0 13px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  width: 100%;
}

  </style>
</head>

<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

  <!-- CENTER THE EMAIL // -->
  <center style="background-color:#E1E1E1;">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
      <tr>
        <td align="center" valign="top" id="bodyCell">

          <!-- EMAIL HEADER // -->
          <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="600" id="emailHeader">

            <!-- HEADER ROW // -->
            <tr>
              <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="20" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td valign="top" width="600" class="flexibleContainerCell">
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

          </table>
          <!-- // END -->

          <!-- EMAIL BODY // -->

          <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody">

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">

                            <!-- CONTENT TABLE // -->
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top" class="textContent">
                                  <img style="width: 300px;" src="https://coloryourlifeapp.com/wp-content/themes/color-your-life/assets/images/color-your-life.png" alt="CYL" />
                                </td>
                              </tr>
                            </table>
                            <!-- // CONTENT TABLE -->

                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#2e1b6e">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top">

                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td valign="top" class="textContent">
                                        <h3 style="color:#00a15e;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:center;">We have successfully received your requests.</h3>
                                        <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#FFF;line-height:135%;">
                                          <ul>
                                            <li>Affiliate: <b>' . $affiliate .'</b></li>
                                            <li>Link: <b>' . $link . '</b></li>
                                          </ul>
                                        </div>
                                        <h3 style="color:#FFF;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:center;">Please wait while we verify your request. Thank you.</h3>
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- // CONTENT TABLE -->

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#fcdc75">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top">

                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td valign="top" class="textContent">
                                        <h3 style="color:#2a1865;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:left;">This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them below email address. </h3>
                                        <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#000;line-height:135%;">email: <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></div>
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- // CONTENT TABLE -->

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

          </table>
          <!-- // END -->

          <!-- EMAIL FOOTER // -->
          <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="600" id="emailFooter">

            <!-- FOOTER ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td valign="top" bgcolor="#E1E1E1">

                                  <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                    <div>Color Your Life Limited</div>
                                    <div>We Make Your Life More Colorful</div>
                                  </div>
                                  <br>
                                  <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                    <div>Unit 1233, Star House, 3 Salisbury Road, Tsim Sha Tsui, Kowloon, Hong Kong</div>
                                    <div><a href="https://www.coloryourlifeapp.com">www.coloryourlifeapp.com</a></div>
                                  </div>

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>

          </table>
          <!-- // END -->

        </td>
      </tr>
    </table>
  </center>
</body>

</html>
  ';
}

function registration_template()
{
	return '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="format-detection" content="telephone=no" />
  <!-- disable auto telephone linking in iOS -->
  <title>Color Your Life</title>
  <style type="text/css">
    /* RESET STYLES */

    html {
      background-color: #E1E1E1;
      margin: 0;
      padding: 0;
    }

    body,
    #bodyTable,
    #bodyCell,
    #bodyCell {
      height: 100% !important;
      margin: 0;
      padding: 0;
      width: 100% !important;
      font-family: Helvetica, Arial, "Lucida Grande", sans-serif;
    }

    table {
      border-collapse: collapse;
    }

    table[id=bodyTable] {
      width: 100% !important;
      margin: auto;
      max-width: 500px !important;
      color: #7A7A7A;
      font-weight: normal;
    }

    img,
    a img {
      border: 0;
      outline: none;
      text-decoration: none;
      height: auto;
      line-height: 100%;
    }

    a {
      text-decoration: none !important;
      border-bottom: 1px solid;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      color: #5F5F5F;
      font-weight: normal;
      font-family: Helvetica;
      font-size: 20px;
      line-height: 125%;
      text-align: Left;
      letter-spacing: normal;
      margin-top: 0;
      margin-right: 0;
      margin-bottom: 10px;
      margin-left: 0;
      padding-top: 0;
      padding-bottom: 0;
      padding-left: 0;
      padding-right: 0;
    }

    /* CLIENT-SPECIFIC STYLES */

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    /* Force Hotmail/Outlook.com to display emails at full width. */

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    /* Force Hotmail/Outlook.com to display line heights normally. */

    table,
    td {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    /* Remove spacing between tables in Outlook 2007 and up. */

    #outlook a {
      padding: 0;
    }

    /* Force Outlook 2007 and up to provide a "view in browser" message. */

    img {
      -ms-interpolation-mode: bicubic;
      display: block;
      outline: none;
      text-decoration: none;
    }

    /* Force IE to smoothly render resized images. */

    body,
    table,
    td,
    p,
    a,
    li,
    blockquote {
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      font-weight: normal !important;
    }

    /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */

    .ExternalClass td[class="ecxflexibleContainerBox"] h3 {
      padding-top: 10px !important;
    }

    /* Force hotmail to push 2-grid sub headers down */

    /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

    /* ========== Page Styles ========== */

    h1 {
      display: block;
      font-size: 26px;
      font-style: normal;
      font-weight: normal;
      line-height: 100%;
    }

    h2 {
      display: block;
      font-size: 20px;
      font-style: normal;
      font-weight: normal;
      line-height: 120%;
    }

    h3 {
      display: block;
      font-size: 17px;
      font-style: normal;
      font-weight: normal;
      line-height: 110%;
    }

    h4 {
      display: block;
      font-size: 18px;
      font-style: italic;
      font-weight: normal;
      line-height: 100%;
    }

    .flexibleImage {
      height: auto;
    }

    .linkRemoveBorder {
      border-bottom: 0 !important;
    }

    table[class=flexibleContainerCellDivider] {
      padding-bottom: 0 !important;
      padding-top: 0 !important;
    }

    body,
    #bodyTable {
      background-color: #E1E1E1;
    }

    #emailHeader {
      background-color: #E1E1E1;
    }

    #emailBody {
      background-color: #FFFFFF;
    }

    #emailFooter {
      background-color: #E1E1E1;
    }

    .nestedContainer {
      background-color: #F8F8F8;
      border: 1px solid #CCCCCC;
    }

    .emailButton {
      background-color: #205478;
      border-collapse: separate;
    }

    .buttonContent {
      color: #FFFFFF;
      font-family: Helvetica;
      font-size: 18px;
      font-weight: bold;
      line-height: 100%;
      padding: 15px;
      text-align: center;
    }

    .buttonContent a {
      color: #FFFFFF;
      display: block;
      text-decoration: none !important;
      border: 0 !important;
    }

    .emailCalendar {
      background-color: #FFFFFF;
      border: 1px solid #CCCCCC;
    }

    .emailCalendarMonth {
      background-color: #205478;
      color: #FFFFFF;
      font-family: Helvetica, Arial, sans-serif;
      font-size: 16px;
      font-weight: bold;
      padding-top: 10px;
      padding-bottom: 10px;
      text-align: center;
    }

    .emailCalendarDay {
      color: #205478;
      font-family: Helvetica, Arial, sans-serif;
      font-size: 60px;
      font-weight: bold;
      line-height: 100%;
      padding-top: 20px;
      padding-bottom: 20px;
      text-align: center;
    }

    .imageContentText {
      margin-top: 10px;
      line-height: 0;
    }

    .imageContentText a {
      line-height: 0;
    }

    #invisibleIntroduction {
      display: none !important;
    }

    /* Removing the introduction text from the view */

    /*FRAMEWORK HACKS & OVERRIDES */

    span[class=ios-color-hack] a {
      color: #275100 !important;
      text-decoration: none !important;
    }

    /* Remove all link colors in IOS (below are duplicates based on the color preference) */

    span[class=ios-color-hack2] a {
      color: #205478 !important;
      text-decoration: none !important;
    }

    span[class=ios-color-hack3] a {
      color: #8B8B8B !important;
      text-decoration: none !important;
    }

    .a[href^="tel"],
    a[href^="sms"] {
      text-decoration: none !important;
      color: #606060 !important;
      pointer-events: none !important;
      cursor: default !important;
    }

    .mobile_link a[href^="tel"],
    .mobile_link a[href^="sms"] {
      text-decoration: none !important;
      color: #606060 !important;
      pointer-events: auto !important;
      cursor: default !important;
    }


    /* MOBILE STYLES */

    @media only screen and (max-width: 480px) {
      /*////// CLIENT-SPECIFIC STYLES //////*/
      body {
        width: 100% !important;
        min-width: 100% !important;
      }
      /* Force iOS Mail to render the email at full width. */
      /* FRAMEWORK STYLES */
      /*
				CSS selectors are written in attribute
				selector format to prevent Yahoo Mail
				from rendering media query styles on
				desktop.
				*/
      /*td[class="textContent"], td[class="flexibleContainerCell"] { width: 100%; padding-left: 10px !important; padding-right: 10px !important; }*/
      table[id="emailHeader"],
      table[id="emailBody"],
      table[id="emailFooter"],
      table[class="flexibleContainer"],
      td[class="flexibleContainerCell"] {
        width: 100% !important;
      }
      td[class="flexibleContainerBox"],
      td[class="flexibleContainerBox"] table {
        display: block;
        width: 100%;
        text-align: left;
      }
      td[class="imageContent"] img {
        height: auto !important;
        width: 100% !important;
        max-width: 100% !important;
      }
      img[class="flexibleImage"] {
        height: auto !important;
        width: 100% !important;
        max-width: 100% !important;
      }
      img[class="flexibleImageSmall"] {
        height: auto !important;
        width: auto !important;
      }


      /*
				Create top space for every second element in a block
				*/
      table[class="flexibleContainerBoxNext"] {
        padding-top: 10px !important;
      }

      /*
				Make buttons in the email span the
				full width of their container, allowing
				for left- or right-handed ease of use.
				*/
      table[class="emailButton"] {
        width: 100% !important;
      }
      td[class="buttonContent"] {
        padding: 0 !important;
      }
      td[class="buttonContent"] a {
        padding: 15px !important;
      }

    }

    /*  CONDITIONS FOR ANDROID DEVICES ONLY
			*   https://developer.android.com/guide/webapps/targeting.html
			*   https://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/ ;
			=====================================================*/

    @media only screen and (-webkit-device-pixel-ratio:.75) {
      /* Put CSS for low density (ldpi) Android layouts in here */
    }

    @media only screen and (-webkit-device-pixel-ratio:1) {
      /* Put CSS for medium density (mdpi) Android layouts in here */
    }

    @media only screen and (-webkit-device-pixel-ratio:1.5) {
      /* Put CSS for high density (hdpi) Android layouts in here */
    }

    /* end Android targeting */

    /* CONDITIONS FOR IOS DEVICES ONLY
			=====================================================*/

    @media only screen and (min-device-width: 320px) and (max-device-width:568px) {}

    /* end IOS targeting */

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  padding: 0 4px;
}

.column {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
  max-width: 20%;
  padding: 0 13px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  width: 100%;
}

  </style>
</head>

<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">

  <!-- CENTER THE EMAIL // -->
  <center style="background-color:#E1E1E1;">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
      <tr>
        <td align="center" valign="top" id="bodyCell">

          <!-- EMAIL HEADER // -->
          <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="600" id="emailHeader">

            <!-- HEADER ROW // -->
            <tr>
              <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="20" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td valign="top" width="600" class="flexibleContainerCell">
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

          </table>
          <!-- // END -->

          <!-- EMAIL BODY // -->

          <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" width="600" id="emailBody">

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">

                            <!-- CONTENT TABLE // -->
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top" class="textContent">
                                  <img style="width: 300px;" src="https://coloryourlifeapp.com/wp-content/themes/color-your-life/assets/images/color-your-life.png" alt="CYL" />
                                  <h1><center>Welcome to Color Your Life!</center></h1>
                                  <h3><center>We Make Your Life More Colorful</center></h3>
                                </td>
                              </tr>
                            </table>
                            <!-- // CONTENT TABLE -->

                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#2e1b6e">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top">

                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td valign="top" class="textContent">
                                        <h3 style="color:#00a15e;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:center;">Thanks for signing up to receive our emails.</h3>
                                        <h3 style="color:#FFF;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:center;">As a new member, you\'ll enjoy:</h3>
                                        <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#FFF;line-height:135%;">
                                          <ol>
                                            <li>Color\'s Market</li>
                                            <li>Earn points and get rewards!</li>
                                          </ol>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- // CONTENT TABLE -->

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f9d5ca">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top">

                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td valign="top" class="textContent">
                                        <h3 style="color:#f67d29;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:center;">Get started by linking up with</h3>
                                        <div style="text-align:center;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#000;line-height:135%;">
                                          <div class="row"> 
                                            <div class="column">
                                              <a href="https://www.facebook.com/COLOR-YOUR-LIFE-245571476014593/">
                                                <img src="' . base_url('frontend/img/fb_logo.png') . '">
                                              </a>
                                            </div>
                                            <div class="column">
                                              <a href="https://www.twitter.com/COLORS_MEMBER">
                                                <img src="' . base_url('frontend/img/twitter_logo.png') . '">
                                              </a>
                                            </div>
                                            <div class="column">
                                              <a href="https://www.instagram.com/wemakeyourlifemorecolorful">
                                                <img src="' . base_url('frontend/img/instagram_logo.png') . '">
                                              </a>
                                            </div>
                                            <div class="column">
                                              <a href="https://www.youtube.com/channel/UC63PN3Qp32LwH9551oPgWeA">
                                                <img src="' . base_url('frontend/img/youtube_logo.png') . '">
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- // CONTENT TABLE -->

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#7fd663">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top">

                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td valign="top" class="textContent">
                                        <h1 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:36px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:center;">We would like <b>YOU</b> to Have Fun!!</h1>
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- // CONTENT TABLE -->

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

            <!-- MODULE ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#fcdc75">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td align="center" valign="top">

                                  <!-- CONTENT TABLE // -->
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td valign="top" class="textContent">
                                        <h3 style="color:#2a1865;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:normal;margin-top:0;margin-bottom:10px;text-align:left;">This email was sent to you by Color Your Life Limited. We kindly ask you not to reply to this email. If you have any questions, please submit them below email address. </h3>
                                        <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#000;line-height:135%;">email: <a href="mailto:info@coloryourlifeapp.com">info@coloryourlifeapp.com</a></div>
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- // CONTENT TABLE -->

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>
            <!-- // MODULE ROW -->

          </table>
          <!-- // END -->

          <!-- EMAIL FOOTER // -->
          <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="600" id="emailFooter">

            <!-- FOOTER ROW // -->
            <tr>
              <td align="center" valign="top">
                <!-- CENTERING TABLE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <!-- FLEXIBLE CONTAINER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                        <tr>
                          <td align="center" valign="top" width="600" class="flexibleContainerCell">
                            <table border="0" cellpadding="30" cellspacing="0" width="100%">
                              <tr>
                                <td valign="top" bgcolor="#E1E1E1">

                                  <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                    <div>Color Your Life Limited</div>
                                    <div>We Make Your Life More Colorful</div>
                                  </div>
                                  <br>
                                  <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                    <div>Unit 1233, Star House, 3 Salisbury Road, Tsim Sha Tsui, Kowloon, Hong Kong</div>
                                    <div><a href="https://www.coloryourlifeapp.com">www.coloryourlifeapp.com</a></div>
                                  </div>

                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // FLEXIBLE CONTAINER -->
                    </td>
                  </tr>
                </table>
                <!-- // CENTERING TABLE -->
              </td>
            </tr>

          </table>
          <!-- // END -->

        </td>
      </tr>
    </table>
  </center>
</body>

</html>
	';
}