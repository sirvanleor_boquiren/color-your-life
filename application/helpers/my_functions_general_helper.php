<?php

function check_login($session_var)
{
	if(isset($session_var->userdata['logged_in']))
	{
		return true;
	}
	else
	{
		return false;
	}
}

// function check_header_cred()
// {
// 	# check access
// 	$access = false;
// 	foreach(getallheaders() as $key => $value){
// 		if(strtoupper($key) == "G8-HEADER-KEY")
// 		{
// 			if($value == G8_HEADER_KEY)
// 			{
// 				$access = true;
// 			}
// 		}
// 	}
// 	return $access;
// }

function getMarketIdByName($name)
{
	$arr = array(
		"products" => "1",
		"services" => "2"
	);
	if(array_key_exists($name, $arr))
	{
		return $arr[$name];
	}
	else
	{
		return null;
	}
}


function getDirPerMarketPlace($market_place_id)
{
	$arr = array(
		"1" => "products",
		"2" => "services"
	);
	return "posts/".$arr[$market_place_id]."/";
}


function api_url($class)
{
  return base_url() . "api/" . strtolower(get_class($class)) . "/";
}

function check_password($password, $confirm_password)
{
	$final['passed'] = false;
	$final['msg'] = array("Password and Confirm Password must match");
	if($password == $confirm_password)
	{
		$passwordErr = array();
		if (strlen($password) < 10) 
		{
	        $passwordErr[] = "Password Must Contain At Least 10 Characters.";
	    }
	    
	    if(!preg_match("#[0-9]+#",$password)) 
	    {
	        $passwordErr[] = "Password Must Contain At Least 1 Number.";
	    }
	    
	    if(!preg_match("#[A-Z]+#",$password)) 
	    {
	        $passwordErr[] = "Password Must Contain At Least 1 Capital Letter.";
	    }
	    
	    if(!preg_match("#[a-z]+#",$password)) 
	    {
	        $passwordErr[] = "Password Must Contain At Least 1 Lowercase Letter.";
	    }
	    
	    if(!preg_match("#[\W]+#",$password)) 
	    {
	    	$passwordErr[] = "Password Must Contain At Least 1 Special Character.";
	    }

	    $final['passed'] = true;
	    $final['msg'] = array("Password Passed");
	    if(count($passwordErr) > 0)
	    {
	    	$final['passed'] = false;
	    	$final['msg'] = $passwordErr;
	    }
	}

	return $final;

}

function concat_existing_get()
{
	$pagination_a_href = "";
	$string_get = "";
	if(isset($_GET))
	{
	  $existing_get = array();
	  foreach ($_GET as $get_key => $get_value) {
	    $existing_get[] = $get_key."=".$get_value;
	    $string_get = implode("&", $existing_get);
	  }
	}
	if($string_get != "")
	{
		$pagination_a_href = "?".$string_get;
	}
	
	return $pagination_a_href;
}



function check_important_posts($post_data, $important_keys)
{
	$check_status = true;
	foreach ($important_keys as $post_key) {
        if(isset($post_data[$post_key]))
        {
            if($post_data[$post_key] == "" || $post_data[$post_key] == null)
            {
                $check_status = false;
            }
        }
        else
        {
            $check_status = false;
        }
    }

    return $check_status;
}

function send_email($that, $recipient, $subject, $body)
{
	$that->load->library('email');

	$config_mail['protocol']='smtp';
	$config_mail['smtp_host']='mail.smtp2go.com';
	$config_mail['smtp_port']='80';
	$config_mail['smtp_user']='coloryourlife';
	$config_mail['smtp_pass']='aDUybDhxcDB4cjAw';
	$config_mail['charset']='utf-8';
	$config_mail['newline']="\r\n";
	$config_mail['wordwrap'] = TRUE;
	$config_mail['mailtype'] = 'html';
	$that->email->initialize($config_mail);

	$that->email->from('noreply@coloryourlife.com', 'Color Your Life');
	$that->email->to($recipient);
	$that->email->bcc(['jcandres@myoptimind.com']);
	$that->email->subject($subject);
	$that->email->message($body);
	return $that->email->send();
}

function randomString($len = 10)
{
	$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $len; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function sortCountriesByPosition($a, $b)
{
	return $a['pos'] - $b['pos'];
}

function sortCountriesByName($a, $b)
{
	return strcmp($a['country'], $b['country']);
}

function sortDateCreated($a, $b)
{
	return $a->created_at < $b->created_at;
}
	
?>