<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Redeem Requests
					</header>
					<div class="panel-body">
						<!-- <div class="row">
			            	<div class="col-md-12 mbot15">
				            	<div class="pull-right">
									<a class="btn btn-info btn-sm" data-toggle="modal" href="#addAffiliate"><i class="fa fa-plus"></i>&nbsp; Add Affiliate</a>
								</div>
			            	</div>
			            </div> -->
						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>
						<table class="table table-bordered">
							<thead>
								<tr>
                                    <th>Username</th>
                                    <th>Affiliate Name</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Mobile No.</th>
                                    <th>Address</th>
                                    <th>Points to Redeem</th>
                                    <th>Url</th>
                                    <th>Notes</th>
                                    <th>Status</th>
                                    <th>Created At</th>
									<th colspan="2">Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php foreach($redeem_requests as $redeem_request): ?>
									<tr>
                                        <td><?=$redeem_request->user_name?></td>
                                        <td><?=$redeem_request->affiliate_name?></td>
										<td><?=$redeem_request->fname?></td>
                                        <td><?=$redeem_request->lname?></td>
                                        <td><?=$redeem_request->mobile_no?></td>
                                        <td><?=$redeem_request->address?></td>
                                        <td><?=$redeem_request->points_to_redeem?></td>
                                        <td><?=$redeem_request->url?></td>
                                        <td><?=$redeem_request->notes?></td>
                                        <td><?=$redeem_request->status?></td>
                                        <td><?=$redeem_request->created_at?></td>
										<td>
											<!-- <button
											type="button"
											data-target="#editRedeemRequest"
											data-toggle="modal"
											class="btn btn-primary btn-xs"
											data-payload=<?php //json_encode(array_merge(['base_url' => base_url(),'status' => $redeem_request->status,'id' => $redeem_request->redeem_request_id]))?>
                                            >Edit</button> -->
                                            <?php if($redeem_request->status == 'pending'): ?>
                                            <form onsubmit="return confirm('Are you sure you want to do this?')" action="<?=base_url()?>cms/redeem_requests/process_redeem_request/<?=$redeem_request->redeem_request_id?>/accept">
                                                <button type="submit" class="btn btn-primary btn-xs">
                                                    Accept
                                                </button>
                                            </form>
                                            <form onsubmit="return confirm('Are you sure you want to do this?')" action="<?=base_url()?>cms/redeem_requests/process_redeem_request/<?=$redeem_request->redeem_request_id?>/decline">
                                                <button type="submit" class="btn btn-danger btn-xs">
                                                    Decline
                                                </button>
                                            </form>
                                            <?php endif; ?>
											<!-- <button class="btn btn-danger btn-xs"
											data-target="#deleteredeem_request"
											data-toggle="modal"
											data-payload=<?php//json_encode(['id' => $redeem_request->redeem_request_id,'base_url' => base_url()])?>
											>Delete</button> -->
										</td>
									</tr>
							<?php endforeach; ?>    
                            </tbody>
							<tfoot>
								<tr>
                                    <th>Username</th>
                                    <th>Affiliate Name</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Mobile No.</th>
                                    <th>Address</th>
                                    <th>Points to Redeem</th>
                                    <th>Url</th>
                                    <th>Notes</th>
                                    <th>Status</th>
                                    <th>Created At</th>
									<th colspan="2">Action</th>
								</tr>
							</tfoot>
						</table>
                        </div>
					</div>
				</section>
			</div>
		</div>



		<!-- edit affiliate modal end -->
		<div class="modal fade top-modal-without-space in" id="deleteAffiliate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content-wrap">
					<div class="modal-content">
						<!-- <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title">Modal Tittle</h4>
						</div> -->
						<div class="modal-body">

							Are you sure you want to delete this? type DELETE to confirm.
							<form id="deleteForm" method="POST"></form>
								<input type="text" id="deleteInput">
							

						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
							<button id="deleteButton" class="btn btn-warning" type="submit"> Confirm</button>
						</div>
						
					</div>
				</div>
			</div>
		</div>

    </section>
</section>

<script src="<?=base_url()?>frontend/js/jquery-latest.js"></script>
<script src="<?=base_url()?>frontend/js/bootstrap.min.js"></script>
<!-- <script src="base_url()?>frontend/js/cms/redeem_requests.js"></script> -->

<!--main content end -->
<!-- base_url redeem_requests/process_redeem_request{$id -->