<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-2">
			</div>
			<div class="col-sm-8">
				<section class="panel">
					<header class="panel-heading">
						<?php echo $panel_header; ?>
					</header>
					<div class="panel-body">
						<form id="add_store" name="add_store" class="form-horizontal tasi-form" method="POST" action="<?php echo $form_action; ?>">
							<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <?php endif; ?>
							<div class="form-group">
								<label class="col-sm-3 control-label">Client Name <span class="required">*</span></label>
								<div class="col-sm-9">
								<?php if($store->c_id == 0): ?>
									<input class="form-control" type="text" name="c_name" id="c_name" value="<?php echo isset($store) ? $store->c_name : ""; ?>" placeholder="Enter Client Name" onkeyup="check_client_name();" required>
									<p class="help-block" id="invalid_client_name" style="color:red; display:none;">Client already registered</p>
								<?php else: ?>
									<select class="form-control" name="c_id" id="c_id" required>
										<option value="">Select Client</option>
										<?php foreach ($clients as $client_arr) { ?>
										<option value="<?php echo $client_arr->c_id; ?>" <?php echo isset($store->c_id) && $store->c_id == $client_arr->c_id ? "selected" : ""; ?>><?php echo $client_arr->c_name; ?></option>
										<?php } ?>
									</select>
								<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Store Name <span class="required">*</span></label>
								<div class="col-sm-9">
									<input class="form-control" type="text" name="s_name" id="s_name" value="<?php echo isset($store) ? $store->s_name : ""; ?>" placeholder="Enter Store Name" required <?php echo $store->c_id != 0 ? 'onkeyup="check_store_name();"' : ""; ?>>
									<p class="help-block" id="invalid_store_name" style="color:red; display:none;">Store Name already registered under this client</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Store Code <span class="required">*</span></label>
								<div class="col-sm-9">
									<input class="form-control" type="text" name="s_code" id="s_code" value="" placeholder="Enter Store Code" onkeyup="check_store_code();" required>
									<p class="help-block" id="invalid_store_code" style="color:red; display:none;">Store Code already registered</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Manager Name <span class="required">*</span></label>
								<div class="col-sm-9">
									<input class="form-control" type="text" name="s_manager" id="s_manager" value="<?php echo isset($store) ? $store->s_manager : ""; ?>" placeholder="Enter Manager Name" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Manager Email <span class="required">*</span></label>
								<div class="col-sm-9">
									<input class="form-control" type="text" name="s_manager_email" id="s_manager_email" value="<?php echo isset($store) ? $store->s_manager_email : ""; ?>" placeholder="Enter Email" onkeyup="check_manager_email();" required>
									<p class="help-block" id="invalid_manager_email" style="color:red; display:none;">Email is already registered</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Geolocation</label><br><br>
								<div id="map" class="map-store"></div>
							</div>
							<div class="pull-right">
								<?php if(isset($store->s_id)): ?>
								<input type="hidden" name="s_id" id="s_id" value="<?php echo $store->s_id; ?>">
								<?php endif; ?>
								<?php if(isset($store->s_loc_lat) && isset($store->s_loc_long)): ?>
								<input type="hidden" name="s_loc_lat" value="<?php echo $store->s_loc_lat; ?>">
								<input type="hidden" name="s_loc_long" value="<?php echo $store->s_loc_long; ?>">
								<?php endif; ?>
								<?php if(isset($store->sa_id) && $store->sa_id != ""): ?>
								<input type="hidden" name="sa_id" value="<?php echo $store->sa_id; ?>">
								<?php endif; ?>
								<a href="<?php echo base_url($back_page); ?>" class="btn btn-info btn-shadow">< Back to Table</a>
								<?php if(isset($reject) && $reject != ''): ?>
								<a href="<?php echo base_url($reject); ?>" class="btn btn-danger btn-shadow">Reject Audit</a>
								<?php endif; ?>
								<button type="submit" id="submit_edit" class="btn btn-success btn-shadow">Submit</button>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->

<script type="text/javascript">
var map;
function initMap() {
	var uluru = {lat: <?php echo $store->s_loc_lat; ?>, lng: <?php echo $store->s_loc_long; ?>};
	map = new google.maps.Map(document.getElementById('map'), {
	  center: uluru,
	  zoom: 17
	});
	 var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
}
<?php if(isset($store)): ?>
function check_store_code()
{
	if($('#s_code').val() != "")
	{
		$.ajax({
		    url:"<?php echo base_url("Ajax/checkStoreCode"); ?>",
		    type: "POST",
		    data: {"s_code": $('#s_code').val() },
		    success:function(data){ // data is the id of the row
		      if(data == true)
		      {
		      	$('#submit_edit').prop("disabled", "true");
		      	$('#invalid_store_code').attr("style", "color:red; display:block");
		      }
		      else
		      {
		      	$('#submit_edit').removeAttr("disabled");
		      	$('#invalid_store_code').attr("style", "color:red; display:none");
		      }
		    },
		    error: function(e){
		      console.log(e);
		    }
		  });
	}
}

<?php endif; ?>

function check_manager_email()
{
	if($('#s_manager_email').val() != "")
	{
		$.ajax({
		    url:"<?php echo base_url("Ajax/checkManagerEmail"); ?>",
		    type: "POST",
		    data: {"s_manager_email": $('#s_manager_email').val() },
		    success:function(data){ // data is the id of the row
		      if(data == true)
		      {
		      	$('#submit_edit').prop("disabled", "true");
		      	$('#invalid_manager_email').attr("style", "color:red; display:block");
		      }
		      else
		      {
		      	$('#submit_edit').removeAttr("disabled");
		      	$('#invalid_manager_email').attr("style", "color:red; display:none");
		      }
		    },
		    error: function(e){
		      console.log(e);
		    }
		});
	}
}
<?php if($store->c_id != 0): ?>
function check_store_name()
{
	if($('#s_name').val() != "")
	{
		$.ajax({
		    url:"<?php echo base_url("Ajax/checkStoreName"); ?>",
		    type: "POST",
		    data: {"s_name": $('#s_name').val(), "c_id" : $('#c_id').val() },
		    success:function(data){ // data is the id of the row
		      if(data == true)
		      {
		      	$('#submit_edit').prop("disabled", "true");
		      	$('#invalid_store_name').attr("style", "color:red; display:block");
		      }
		      else
		      {
		      	$('#submit_edit').removeAttr("disabled");
		      	$('#invalid_store_name').attr("style", "color:red; display:none");
		      }
		    },
		    error: function(e){
		      console.log(e);
		    }
		});
	}
}
<?php else:  ?>
function check_client_name()
{
	if($('#c_name').val() != "")
	{
		$.ajax({
		    url:"<?php echo base_url("Ajax/checkClientName"); ?>",
		    type: "POST",
		    data: {"c_name": $('#c_name').val() },
		    success:function(data){ // data is the id of the row
		      if(data == true)
		      {
		      	$('#submit_edit').prop("disabled", "true");
		      	$('#invalid_client_name').attr("style", "color:red; display:block");
		      }
		      else
		      {
		      	$('#submit_edit').removeAttr("disabled");
		      	$('#invalid_client_name').attr("style", "color:red; display:none");
		      }
		    },
		    error: function(e){
		      console.log(e);
		    }
		});
	}
}
<?php endif; ?>
</script>
<!-- async defer for google map -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCxkBwVSWJ5DX4E5E6Gg_AfyhubdTbV0Y&callback=initMap">
</script>
