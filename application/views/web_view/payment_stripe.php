<!-- stripe Simple -->
<!-- <form action="<?php echo base_url('membership/charge'); ?>" method="POST">
  <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
  <input type="hidden" name="membership_id" value="<?php echo $membership_id; ?>">
  <input type="hidden" name="amount" value="<?php echo $membership_plan->rate . '00'; ?>">
  <input type="hidden" name="plan_name" value="<?php echo $membership_plan->name; ?>">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_vmhBKD7yeogaEtwIsLL9oEo2"
    data-amount="<?php echo $membership_plan->rate . '00'; ?>"
    data-name="Color Your Life Ltd."
    data-description="Payment"
    data-image=""
    data-locale="auto"
    data-currency="hkd"
    data-label="Pay with Stripe">
  </script>
</form> -->

<form action="<?php echo base_url('membership/charge'); ?>" method="POST">
  <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
  <input type="hidden" name="membership_id" value="<?php echo $membership_id; ?>">
  <input type="hidden" name="amount" value="<?php echo $membership_plan->rate . '00'; ?>">
  <input type="hidden" name="plan_name" value="<?php echo $membership_plan->name; ?>">
<script src="https://checkout.stripe.com/checkout.js"></script>
<button id="customButton">Pay with <img width="100" src="<?php echo base_url('frontend/img/payment_gateway/stripe_logo_blue.png'); ?>"></button>
<script>
var handler = StripeCheckout.configure({
  key: 'pk_test_vmhBKD7yeogaEtwIsLL9oEo2',
  image: '',
  locale: 'auto',
  token: function(token) {
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
  }
});

document.getElementById('customButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler.open({
    name: 'Color Your Life Ltd.',
    description: 'Payment',
    zipCode: true,
    amount: <?php echo $membership_plan->rate . '00'; ?>,
    currency: 'usd'
  });
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
</script>
</form>
<!-- stripe Simple -->
<hr>
<!-- DOKU Payment Simple -->
<?php
if (@$membership_plan->idr_rate) {
?>
<script language="JavaScript" type="text/javascript" src="https://staging.doku.com/dateformat.js"></script>
<script language="JavaScript" type="text/javascript" src="https://staging.doku.com/sha-1.js"></script>
<style type="text/css">
.bt_submit {
  background-color: #CCC;
  border: 2px solid #F30;
  color: #900;
  padding-right: 20px;
  padding-left: 20px;
  padding-top: 5px;
  padding-bottom: 5px;
  font-weight: bold;
}
</style>
<?php
$mallid = "11627726";
$sharedkey = "BzhoRS9nOeeU";
$currency = 360; //IDR
?>
<form action="https://staging.doku.com/Suite/Receive" id="MerchatPaymentPage" name="MerchatPaymentPage" method="post">
  <input name="MALLID" type="hidden" id="MALLID" value="<?php echo $mallid; ?>" size="12" />
  <input name="SHAREDKEY" type="hidden" id="SHAREDKEY" value="<?php echo $sharedkey; ?>" size="15" maxlength="12"/>
  <input name="AMOUNT" type="hidden" id="AMOUNT" value="<?php echo $membership_plan->idr_rate; ?>" size="12" />
  <input name="PURCHASEAMOUNT" type="hidden" id="PURCHASEAMOUNT" value="<?php echo $membership_plan->idr_rate; ?>" size="12" />
  <input name="CURRENCY" type="hidden" id="CURRENCY" value="<?php echo $currency; ?>" size="3" maxlength="3" />
  <input name="PURCHASECURRENCY" type="hidden" id="PURCHASECURRENCY" value="<?php echo $currency; ?>" size="3" maxlength="3" />
  <input type="hidden" id="WORDS" name="WORDS" size="60" />
  <input name="REQUESTDATETIME" type="hidden" id="REQUESTDATETIME" size="14" maxlength="14" />
  <input type="hidden" id="SESSIONID" name="SESSIONID" />

  <input type="hidden" id="PAYMENTCHANNEL" name="PAYMENTCHANNEL" value="" />
  <input name="EMAIL" type="hidden" id="EMAIL" value="<?php echo $user->email; ?>" size="12" />
  <input name="NAME" type="hidden" id="NAME" value="<?php echo $user->fname . " " . $user->lname; ?>" size="30" maxlength="50" />

  <input name="CHAINMERCHANT" type="hidden" id="CHAINMERCHANT" value="NA" size="12" />
  <input name="TRANSIDMERCHANT" type="hidden" id="TRANSIDMERCHANT" size="16" />


  <input name="submit" type="submit" class="bt_submit" id="submit" value="Pay with DOKU" />
</form>

<script type="text/javascript">
function randomString(STRlen) {
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz";
  var string_length = STRlen;
  var randomstring = '';
  for (var i=0; i<string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum,rnum+1);
  }

  return randomstring;

}

function genInvoice() {
  document.getElementById("TRANSIDMERCHANT").value = randomString(12);
}

function getWords() {
  var msg = document.getElementById("AMOUNT").value + document.getElementById("MALLID").value + document.getElementById("SHAREDKEY").value + document.getElementById("TRANSIDMERCHANT").value;

  document.getElementById("WORDS").value = SHA1(msg);
}

function getRequestDateTime() {
  var now = new Date();

  document.getElementById("REQUESTDATETIME").value = dateFormat(now, "yyyymmddHHMMss");
}

function genSessionID() { 
  document.getElementById("SESSIONID").value = randomString(20);
}


genInvoice();
getWords();
getRequestDateTime();
genSessionID();
</script>
<?php
}
else {
  echo "<p>Something went wrong with DOKU payment.</p>";
}
?>
<!-- DOKU Payment Simple -->