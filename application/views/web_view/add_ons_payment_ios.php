<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<style type="text/css">
body { background: #ececec; }
.container { max-width: 720px; margin: 24px auto 48px auto; }
.row { margin-top: 12px; }
.column { display: inline-block; text-align: center; background-color: transparent; border-color: transparent; }
figure { overflow: hidden; }
a p { color: black; margin-top: 8px; font-family: "Open Sans", sans-serif; }
a:hover { text-decoration: none; }
.column img { display: block; width: auto; height: 120px; margin-left: auto; margin-right: auto;}
button { border: none; background: #ececec; }
#MerchatPaymentPage { text-align: center; }
.stripe-hkd-payment form, .paynamics-payment form, .doku-payment form, .stripe-usd-payment form { text-align: center; margin-bottom: 0px; }
.stripe-hkd-payment, .paynamics-payment, .doku-payment, .stripe-usd-payment { padding: 30px 0px; height: 300px; text-align: center; border-bottom: 1px solid black; }
h1 { font-size: 90px; }
span.col-xs-12 { font-size: 40px; }
label span { color: red; }
.modal-dialog { width: 95%; }
.modal .close { font-size: 70px; }
.modal h4 { font-size: 50px; }
.modal label { font-size: 35px; }
.form-control { height: 70px; font-size: 30px; }
.btn-lg { font-size: 35px; margin: 30px; }
#modal-wechat-qr form { text-align: center; }
#modal-wechat-qr form img { width: 100%; }
.hide { display: none; }
.font-size70 { font-size: 70px; }
.font-size50 { font-size: 50px; }
.font-size40 { font-size: 40px; }
.border-black { border: 1px solid black; }

#load{
    width:100%;
    height:100%;
    position:fixed;
    z-index:9999;
    background:url("<?php echo base_url('frontend/img/cyl_loading.gif'); ?>") no-repeat center center rgba(0,0,0,0.25);
}
</style>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<div id="load"></div>

<?php if (@$_POST['paynamics-submit']): # For Paynamics
if ($_POST['mname'] != "") {
  $fullname = $_POST['fname'] . " " . $_POST['mname'] . " " . $_POST['lname'];
}
else {
  $fullname = $_POST['fname'] . " " . $_POST['lname'];
}

$types_arr = array('ads' => "Advertisements", 'videos' => "Videos", 'webinars' => "Webinars", 'group_messages' => "Group Message");

$_mid = PAYNAMICS_MERCHANT_ID; //<-- your merchant id
$_requestid = substr(uniqid(), 0, 13);
$_ipaddress = "148.72.198.55";
$_noturl = base_url("paynamics_beta/notification"); // url where response is posted
$_resurl = base_url("paynamics_beta/res_add_ons/$user->user_id/$payment_logs_id"); //url of merchant landing page
// $_resurl = base_url("paynamics/response"); //url of merchant landing page
$_cancelurl = base_url("paynamics_beta/cancel"); //url of merchant landing page
$_fname = $_POST['fname'];
$_mname = $_POST['mname'];
$_lname = $_POST['lname'];
$_addr1 = $_POST['addr1'];
$_addr2 = $_POST['addr2'];
$_city = $_POST['city'];
$_state = $_POST['state'];
$_country = "PH";
$_zip = $_POST['zip_code'];
$_sec3d = "try3d";//enabled
$_email = $_POST['email'];
$_phone = $_POST['phone_no'];
$_mobile = $_POST['mobile_no'];
$_clientip = $_SERVER['REMOTE_ADDR'];
$_amount = $amount_php . ".00";
$_currency = "PHP";
$forSign = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;

$cert = PAYNAMICS_MERCHANT_KEY; //<-- your merchant key

$_sign = hash("sha512", $forSign.$cert);
$strxml = "";
$strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
$strxml = $strxml . "<Request>";
$strxml = $strxml . "<orders>";
$strxml = $strxml . "<items>";
$strxml = $strxml . "<Items>";
$strxml = $strxml . "<itemname>" . $types_arr[$type] . " (Add-ons)</itemname><quantity>1</quantity><amount>" . $amount_php . "</amount>";
$strxml = $strxml . "</Items>";
$strxml = $strxml . "</items>";
$strxml = $strxml . "</orders>";
$strxml = $strxml . "<mid>" . $_mid . "</mid>";
$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
$strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
$strxml = $strxml . "<mtac_url>https://www.paynamics.com/index.html</mtac_url>";
$strxml = $strxml . "<descriptor_note>'My Descriptor .18008008008'</descriptor_note>";
$strxml = $strxml . "<fname>" . $_fname . "</fname>";
$strxml = $strxml . "<lname>" . $_lname . "</lname>";
$strxml = $strxml . "<mname>" . $_mname . "</mname>";
$strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
$strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
$strxml = $strxml . "<city>" . $_city . "</city>";
$strxml = $strxml . "<state>" . $_state . "</state>";
$strxml = $strxml . "<country>" . $_country . "</country>";
$strxml = $strxml . "<zip>" . $_zip . "</zip>";
$strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
$strxml = $strxml . "<trxtype>sale</trxtype>";
$strxml = $strxml . "<email>" . $_email . "</email>";
$strxml = $strxml . "<phone>" . $_phone . "</phone>";
$strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
$strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
$strxml = $strxml . "<amount>" . $_amount . "</amount>";
$strxml = $strxml . "<currency>" . $_currency . "</currency>";
$strxml = $strxml . "<mlogo_url>" . base_url('frontend/img/color-your-life-logo-s.png') . "</mlogo_url>";
$strxml = $strxml . "<pmethod></pmethod>";//CC, GC, PP, DP
$strxml = $strxml . "<signature>" . $_sign . "</signature>";
$strxml = $strxml . "</Request>";
$b64string =  base64_encode($strxml);

?>

<div class="container">
  <div class="row text-center">
    <h1 class="font-size70">Customer and Billing information</h1>
  </div>
  <div class="row font-size40">
    <div class="col-xs-12 form-group border-black">
      <span>Name:</span>
      <div class="col-xs-12">
        <label><?php echo $fullname; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>Email Address:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['email']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>Telephone No.:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['phone_no']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>Mobile No.:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['mobile_no']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>Address:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['addr1']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>Address 2:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['addr2']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>City:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['city']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>State/Province:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['state']; ?></label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>Country:</span>
      <div class="col-xs-12">
        <label>PH</label>
      </div>
    </div>
    <div class="col-xs-12 form-group border-black">
      <span>ZIP/Postal Code:</span>
      <div class="col-xs-12">
        <label><?php echo $_POST['zip_code']; ?></label>
      </div>
    </div>
  </div>
  <div class="row">
    <form method="POST" action="<?php echo PAYNAMICS_API_URL; ?>" class="text-center">
      <input type="hidden" name="paymentrequest" id="paymentrequest" value="<?php echo $b64string; ?>">
      <button type="submit" class="btn btn-info btn-lg">Submit</button>
    </form>
  </div>
  <div class="row text-center font-size40" id="paynamics-back-btn">
    <a href="<?php echo base_url("payment/" . $type . "/" . $payment_logs_id . "/ios") ?>"><< Back</a>
  </div>
</div>

<?php else: ?>

<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- For Doku -->
<?php if (@$amount_idr): ?>
<script language="JavaScript" type="text/javascript" src="https://staging.doku.com/dateformat.js"></script>
<script language="JavaScript" type="text/javascript" src="https://staging.doku.com/sha-1.js"></script>
<?php endif ?>
<!-- For Doku -->

<div class="container">

  <div class="row">
    <!-- Stripe -->
    <div class="col-xs-12 stripe-usd-payment">
      <form action="<?php echo base_url('payment/charge'); ?>" method="POST" id="us_stripe">
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <input type="hidden" name="payment_logs_id" value="<?php echo $payment_logs_id; ?>">
        <input type="hidden" name="amount" value="<?php echo $amount . '00'; ?>">
        <input type="hidden" name="type" value="<?php echo $type; ?>">
        <input type="hidden" name="currency" value="usd">
        <script src="https://checkout.stripe.com/checkout.js"></script>
        <button id="customButton" class="column" type="submit" data-submit="us_stripe">
          <img src = "<?php echo base_url('frontend/img/payment_gateway/stripe_logo_blue.png'); ?>">
        </button>
      </form>
      <span class="col-xs-12">US Dollar</span>
    </div>
    <!-- Stripe -->

    <!-- Stripe HK Dollar -->
    <div class="col-xs-12 stripe-hkd-payment">
      <form action="<?php echo base_url('payment/charge'); ?>" method="POST" id="hk_stripe">
        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <input type="hidden" name="payment_logs_id" value="<?php echo $payment_logs_id; ?>">
        <input type="hidden" name="amount" value="<?php echo $amount_hk . '00'; ?>">
        <input type="hidden" name="type" value="<?php echo $type; ?>">
        <input type="hidden" name="currency" value="hkd">
        <script src="https://checkout.stripe.com/checkout.js"></script>
        <button id="customButton2" class="column" type="submit" data-submit="hk_stripe">
          <img src = "<?php echo base_url('frontend/img/payment_gateway/stripe_logo_blue.png'); ?>">
        </button>
      </form>
      <span class="col-xs-12">Hong Kong Dollar</span>
    </div>
    <!-- Stripe HK Dollar -->

    <!-- Alipay (Stripe) -->
    <div class="col-xs-12 stripe-hkd-payment">
      <a href="<?php echo $alipay_url; ?>" class="column">
        <img src = "<?php echo base_url('frontend/img/payment_gateway/alipay.png'); ?>">
      </a>
      <span class="col-xs-12">Hong Kong Dollar</span>
    </div>
    <!-- Alipay (Stripe) -->

    <!-- WeChat Pay (Stripe) -->
    <!-- <div class="col-xs-12 stripe-hkd-payment">
      <button type="button" class="column" data-toggle="modal" data-target="#modal-wechat-qr">
        <img src = "<?php //echo base_url('frontend/img/payment_gateway/wechat_pay.png'); ?>">
      </button>
      <span class="col-xs-12">Hong Kong Dollar</span>
    </div> -->

    <!-- Modal -->
    <!-- <div class="modal fade" id="modal-wechat-qr">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">WeChat Pay</h4>
          </div>
          <div class="modal-body">
            <form method="GET" action="<?php //echo base_url('payment/charge_add_ons'); ?>">
              <div class="form-group">
                <img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php //echo urlencode($wechat['url']); ?>&choe=UTF-8" />
              </div>
              <div class="form-group col-xs-12">
                <span class="col-xs-12">*Scan the QR Code using the we chat pay app then tap the button below once payment is done.</span>
              </div>
              <div class="form-group">
                <input type="hidden" name="source" value="<?php //echo $wechat['source']; ?>">
                <input type="hidden" name="amount" value="<?php //echo $wechat['amount']; ?>">
                <input type="hidden" name="currency" value="hkd">
                <input type="hidden" name="user_id" value="<?php //echo $wechat['user_id']; ?>">
                <input type="hidden" name="type" value="<?php //echo $wechat['type']; ?>">
                <input type="hidden" name="payment_logs_id" value="<?php //echo $wechat['payment_logs_id']; ?>">
                <input type="hidden" name="email" value="<?php //echo $wechat['email']; ?>">
                <input type="submit" class="btn btn-info btn-lg" name="paynamics-submit" value="Charge">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div> -->
    <!-- Modal -->

    <!-- WeChat Pay (Stripe) -->

    <!-- Doku -->
    <div class="col-xs-12 doku-payment">
      <?php
      if (@$amount_idr) {
        $mallid = DOKU_MALL_ID;
        $sharedkey = DOKU_SHARED_KEY;
        $currency = 360; //IDR
      ?>
      <form action="<?php echo DOKU_API_URL; ?>" id="MerchatPaymentPage" name="MerchatPaymentPage" method="post">
        <input name="MALLID" type="hidden" id="MALLID" value="<?php echo $mallid; ?>" size="12" />
        <input name="SHAREDKEY" type="hidden" id="SHAREDKEY" value="<?php echo $sharedkey; ?>" size="15" maxlength="12"/>
        <input name="AMOUNT" type="hidden" id="AMOUNT" value="<?php echo $amount_idr; ?>" size="12" />
        <input name="PURCHASEAMOUNT" type="hidden" id="PURCHASEAMOUNT" value="<?php echo $amount_idr; ?>" size="12" />
        <input name="CURRENCY" type="hidden" id="CURRENCY" value="<?php echo $currency; ?>" size="3" maxlength="3" />
        <input name="PURCHASECURRENCY" type="hidden" id="PURCHASECURRENCY" value="<?php echo $currency; ?>" size="3" maxlength="3" />
        <input type="hidden" id="WORDS" name="WORDS" size="60" />
        <input name="REQUESTDATETIME" type="hidden" id="REQUESTDATETIME" size="14" maxlength="14" />
        <input type="hidden" id="SESSIONID" name="SESSIONID" value="<?php echo $sessionid; ?>" />

        <input type="hidden" id="PAYMENTCHANNEL" name="PAYMENTCHANNEL" value="" />
        <input name="EMAIL" type="hidden" id="EMAIL" value="<?php echo $user->email; ?>" size="12" />
        <input name="NAME" type="hidden" id="NAME" value="<?php echo $user->fname . " " . $user->lname; ?>" size="30" maxlength="50" />

        <input name="CHAINMERCHANT" type="hidden" id="CHAINMERCHANT" value="NA" size="12" />
        <input name="TRANSIDMERCHANT" type="hidden" id="TRANSIDMERCHANT" size="16" />

        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
        <button type="submit" class="column"><img src = "<?php echo base_url('frontend/img/payment_gateway/doku_logo.png'); ?>"></button>
      </form>
      <span class="col-xs-12">Indonesian Rupia</span>
      <?php
      }
      else {

      }
      ?>
    </div>
    <!-- Doku -->

    <!-- Paynamics -->
    <div class="col-xs-12 paynamics-payment">
      <button type="button" class="column" data-toggle="modal" data-target="#modal-1">
        <img src="<?php echo base_url('frontend/img/payment_gateway/paynamics_logo2.png'); ?>">
      </button>
      <span class="col-xs-12">Philippine Peso</span>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Customer and Billing information</h4>
          </div>
          <div class="modal-body">
            <form method="POST">
              <div class="form-group col-xs-6">
                <label>First Name<span>*</span></label>
                <input type="text" value="<?php echo $user->fname; ?>" name="fname" class="form-control" placeholder="Input First Name" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Middle Name</label>
                <input type="text" value="<?php echo $user->mname; ?>" name="mname" class="form-control" placeholder="Input Middle Name">
              </div>
              <div class="form-group col-xs-6">
                <label>Last Name<span>*</span></label>
                <input type="text" value="<?php echo $user->lname; ?>" name="lname" class="form-control" placeholder="Input Last Name" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Email<span>*</span></label>
                <input type="text" value="<?php echo $user->email; ?>" name="email" class="form-control" placeholder="Input Email" required>
              </div>
              <div class="form-group col-xs-12">
                <label>Address 1<span>*</span></label>
                <input type="text" name="addr1" class="form-control" placeholder="Input Address 1" required>
              </div>
              <div class="form-group col-xs-12">
                <label>Address 2</label>
                <input type="text" name="addr2" class="form-control" placeholder="Input Address 2">
              </div>
              <div class="form-group col-xs-12">
                <label>City<span>*</span></label>
                <input type="text" name="city" class="form-control" placeholder="Input City" required>
              </div>
              <div class="form-group col-xs-6">
                <label>State<span>*</span></label>
                <input type="text" name="state" class="form-control" placeholder="" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Zip Code<span>*</span></label>
                <input type="text" name="zip_code" class="form-control" placeholder="" required>
              </div>
              <div class="form-group col-xs-6">
                <label>Phone No.</label>
                <input type="text" name="phone_no" class="form-control" placeholder="">
              </div>
              <div class="form-group col-xs-6">
                <label>Mobile No.<span>*</span></label>
                <input type="text" name="mobile_no" class="form-control" placeholder="" required>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-info btn-lg" name="paynamics-submit">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <!-- Paynamics -->

  </div>

</div>

<!-- For Doku -->
<?php if (@$amount_idr): ?>
<script type="text/javascript">
function randomString(STRlen) {
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz";
  var string_length = STRlen;
  var randomstring = '';
  for (var i=0; i<string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum,rnum+1);
  }

  return randomstring;

}

function genInvoice() {
  document.getElementById("TRANSIDMERCHANT").value = randomString(12);
}

function getWords() {
  var msg = document.getElementById("AMOUNT").value + document.getElementById("MALLID").value + document.getElementById("SHAREDKEY").value + document.getElementById("TRANSIDMERCHANT").value;

  document.getElementById("WORDS").value = SHA1(msg);
}

function getRequestDateTime() {
  var now = new Date();

  document.getElementById("REQUESTDATETIME").value = dateFormat(now, "yyyymmddHHMMss");
}

// function genSessionID() { 
//   document.getElementById("SESSIONID").value = randomString(20);
// }


genInvoice();
getWords();
getRequestDateTime();
// genSessionID();
</script>
<?php endif ?>
<!-- For Doku -->

<!-- Script for Stripe -->
<script>
var handler_us = StripeCheckout.configure({
  key: '<?php echo STRIPE_PUBLISHABLE_KEY; ?>',
  image: '',
  locale: 'auto',
  token: function(token) {
    var form = $("#us_stripe");
    
    var stripeToken = document.createElement("input");
    stripeToken.value = token.id;
  stripeToken.name = "stripeToken";
  stripeToken.type = "hidden";
  form.append(stripeToken);

  var stripeEmail = document.createElement("input");
  stripeEmail.value = token.email;
  stripeEmail.name = "stripeEmail";
  stripeEmail.type = "hidden";
  form.append(stripeEmail);

  form.submit();
  }
});

var handler_hk = StripeCheckout.configure({
  key: '<?php echo STRIPE_PUBLISHABLE_KEY; ?>',
  image: '',
  locale: 'auto',
  token: function(token) {
    var form = $("#hk_stripe");

    var stripeToken = document.createElement("input");
    stripeToken.value = token.id;
  stripeToken.name = "stripeToken";
  stripeToken.type = "hidden";
  form.append(stripeToken);

  var stripeEmail = document.createElement("input");
  stripeEmail.value = token.email;
  stripeEmail.name = "stripeEmail";
  stripeEmail.type = "hidden";
  form.append(stripeEmail);

  form.submit();
  }
});

document.getElementById('customButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler_us.open({
    name: 'Color Your Life Ltd.',
    description: 'Payment',
    zipCode: true,
    amount: <?php echo $amount . '00'; ?>,
    currency: 'usd'
  });
  e.preventDefault();
});

document.getElementById('customButton2').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler_hk.open({
    name: 'Color Your Life Ltd.',
    description: 'Payment',
    zipCode: true,
    amount: <?php echo $amount_hk * 100; ?>,
    currency: 'hkd'
  });
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
</script>
<!-- Script for Stripe -->

<?php endif ?>

<!-- Common Script -->
<script type="text/javascript">
  document.onreadystatechange = function () {
    var state = document.readyState
    console.log(state);
    if (state == 'interactive') {
      $(".container").css("display", "none");
    } else if (state == 'complete') {
      setTimeout(function(){
        document.getElementById('interactive');
        $("#load").css("display", "none");
        $(".container").css("display", "block");
      },1000);
    }
  }

  $("#modal-1").submit(function(){
    $(".container").css("display", "none");
    $("#load").css("display", "block");
  });

  $(".doku-payment form").submit(function(){
    $(".container").css("display", "none");
    $("#load").css("display", "block");
  });

  $(".stripe-hkd-payment a").click(function(){
    $(".container").css("display", "none");
    $("#load").css("display", "block");
  });

  $("#paynamics-back-btn a").click(function(){
    $(".container").css("display", "none");
    $("#load").css("display", "block");
  });
</script>