<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Montserrat:400,700);
div.gallery img {
  width: 80%;
  height: auto;
}

div.desc {
  text-align: center;
  font-size: 10px;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 6px;
  width: 33.3%;
  margin: 6px 0;
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

div.polaroid {
	text-align: center;
  width: 80%;
  background-color: white;
  margin: 10%;
  margin-bottom: 5px;
}
p { margin: 0px; font-family: 'Montserrat', sans-serif; }
body { margin-top: 0px; }
.parent { -webkit-flex-flow: row wrap; justify-content: center; flex-wrap: wrap; width: 100%; height: auto; overflow: visible; display: -webkit-flex; }
</style>

<div class="container">
	<div class="parent">
		<div class="responsive">
			<div class="gallery polaroid">
				<img src="<?php echo base_url('frontend/img/rank_logo/White_logo.png'); ?>" alt="Cinque Terre" width="600" height="400">
			</div>
			<div class="desc">
				<p><b>1 - 49</b></p>
				<p>REFERRALS</p>
			</div>
		</div>

		<div class="responsive">
			<div class="gallery polaroid">
				<img src="<?php echo base_url('frontend/img/rank_logo/Copper_logo.png'); ?>" alt="Cinque Terre" width="600" height="400">
			</div>
			<div class="desc">
				<p><b>50 - 99</b></p>
				<p>REFERRALS</p>
			</div>
		</div>

		<div class="responsive">
			<div class="gallery polaroid">
				<img src="<?php echo base_url('frontend/img/rank_logo/Silver_logo.png'); ?>" alt="Cinque Terre" width="600" height="400">
			</div>
			<div class="desc">
				<p><b>100 - 149</b></p>
				<p>REFERRALS</p>
			</div>
		</div>

		<div class="responsive">
			<div class="gallery polaroid">
				<img src="<?php echo base_url('frontend/img/rank_logo/Gold_logo.png'); ?>" alt="Cinque Terre" width="600" height="400">
			</div>
			<div class="desc">
				<p><b>150 - 199</b></p>
				<p>REFERRALS</p>
			</div>
		</div>

		<div class="responsive">
			<div class="gallery polaroid">
				<img src="<?php echo base_url('frontend/img/rank_logo/Platinum_logo.png'); ?>" alt="Cinque Terre" width="600" height="400">
			</div>
			<div class="desc">
				<p><b>200 and above</b></p>
				<p>REFERRALS</p>
			</div>
		</div>
	</div>
</div>