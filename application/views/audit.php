<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
			<!-- First row -->
			<div class="row">
				<!-- First Column, User and Audit Details -->
				<div class="col-sm-4">
					<section class="panel">
						<header class="panel-heading" style="font-size:14px;">
							<i class="fa fa-user"></i>
							<?php echo $audit->user->u_lname.", ".$audit->user->u_fname; ?> | 
							
							<?php if($audit->user->u_email != ""): ?>
                      		<i class="fa fa-envelope"></i>
                      		<?php echo $audit->user->u_email; ?>
                      		<?php endif; ?>

							<?php if($audit->user->u_mobile != ""): ?> |
                      		<i class="fa fa-phone"></i>
                      		<?php echo $audit->user->u_mobile; ?>  
                          	<?php endif; ?>

						</header>
						<div class="panel-body">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th>Date of visit</th>
										<td><?php echo date("m/d/Y H:i:s", strtotime($audit->sa_visit_date)); ?></td>
									</tr>
									<tr>
										<th>Store Cell No.</th>
										<td><?php echo $audit->sa_store_cell_num; ?></td>
									</tr>
									<tr>
										<th>Client</th>
										<td><?php echo $audit->client_name; ?></td>
									</tr>
									<tr>
										<th>Branch</th>
										<td><?php echo $audit->store->s_name; ?></td>
									</tr>
									<tr>
										<th>Dizer Name</th>
										<td><?php echo $audit->sa_dizer; ?></td>
									</tr>
									<tr>
										<th>Dizer Cell No.</th>
										<td><?php echo $audit->sa_dizer_cell_num; ?></td>
									</tr>
								</tbody>

							</table>
                        </div>
					</section>

					<section class="panel">
						<header class="panel-heading">
							Geolocation | ( S - Store / A - Analyst )
						</header>
						<div class="panel-body">
            				<div id="map" class="map-audit" style="height:350px;"></div>
							</div>
					</section>

				</div>

				<!-- Third Column, Ratings  -->
				<div class="col-sm-4">

					<section class="panel">
						<header class="panel-heading">
							Racks
						</header>
						<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Type</th>
									<th>Existing</th>
									<th>Request</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($audit->racks as $rack) { ?>
								<tr>
									<td><?php echo get_racks_desc($rack->ri_type); ?></td>
									<td><?php echo $rack->ri_existing != 0 && $rack->ri_existing != "" ? $rack->ri_existing : "N/A"; ?></td>
									<td><?php echo $rack->ri_request != 0 && $rack->ri_request != "" ? $rack->ri_request : "N/A"; ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
						</div>
					</section>

					<section class="panel">
						<header class="panel-heading">
							Attachments ( Click to see actual size )
						</header>
						<div class="panel-body">
						<?php if(isset($audit->attachments) && $audit->attachments != "N/A"): ?>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Before Picture</th>
										<th>After Picture</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$current_id = null;
								foreach ($audit->attachments as $att) { 

									if($att->a_before_id == null)
									{
										echo "<tr>";
										echo "<td><center><a href='".base_url("uploads/sa_attachments/".$att->a_file)."' target='_blank'><img src='".base_url("uploads/sa_attachments/".$att->a_file)."' width='130px'></a></center></td>";
										$current_id = $att->a_id;
									}
									else
									{
										if($current_id == $att->a_before_id)
										{
											echo "<td><center><a href='".base_url("uploads/sa_attachments/".$att->a_file)."' target='_blank'><img src='".base_url("uploads/sa_attachments/".$att->a_file)."' width='130px'></a></center></td>";
											echo "</tr>";
										}
									}
								?>
								<?php } ?>
								</tbody>

							</table>
						<?php else: ?>
						No Attachments uploaded.
						<?php endif; ?>
						</div>
					</section>

				</div>

				<!-- Second Column, Racks  -->
				<div class="col-sm-4">

					<section class="panel">
						<header class="panel-heading">
							Ratings
						</header>
						<div class="panel-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Category</th>
										<th>Rate</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$total = 0;
								foreach ($audit->ratings as $rating) { $total = $total + $rating->r_rate; ?>
									<tr>
										<td><?php echo get_rating_desc($rating->r_type); ?></td>
										<td><?php echo $rating->r_rate; ?></td>
									</tr>
								<?php } ?>
									<tr>
										<td>Overall</td>
										<td><?php echo $total / 4; ?></td>
									</tr>
								</tbody>

							</table>
						</div>
					</section>

					<section class="panel">
						<header class="panel-heading">
							Other Details
						</header>
						<div class="panel-body">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th>Stock Value</th>
										<td><?php echo $audit->sa_stock_value ?></td>
									</tr>
									<tr>
										<th>Pull-out</th>
										<td><?php echo $audit->sa_pullout; ?></td>
									</tr>
									<tr>
										<th>Month</th>
										<td><?php echo $audit->sa_month; ?></td>
									</tr>
									<tr>
										<th>Sales</th>
										<td><?php echo $audit->sa_sales; ?></td>
									</tr>
									<tr>
										<th>Comment</th>
										<td><?php echo $audit->sa_comments; ?></td>
									</tr>
									<tr>
										<th>Analyst Name</th>
										<td><?php echo $audit->sa_analyst; ?></td>
									</tr>
								</tbody>
							</table>
							<h5>Uploaded Reports</h5>
							<?php if(isset($audit->reports_attachment) && $audit->reports_attachment != "N/A"): ?>
							<ol>
								<?php $i = 1; foreach ($audit->reports_attachment as $report) { ?>
								<li><a href="<?php echo base_url('uploads/sa_reports/'.$report->ra_file); ?>" target="_blank">File <?php echo $i; ?></a></li>
								<?php $i++; } ?>
							</ol>
						<?php else: ?>
							No Reports uploaded.
						<?php endif; ?>
						</div>
					</section>
				</div>
				
			</div>				
		<!-- page end -->
    </section>
</section>
<!--main content end -->

<!-- Scripts -->
<script>

// Map Funcitons
function initMap() {
	var uluru = {lat: <?php echo $audit->sa_loc_lat; ?>, lng: <?php echo $audit->sa_loc_long; ?>};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 17,
	  center: uluru
	});
	var marker = new google.maps.Marker({
		position: {lat: <?php echo $audit->store->s_loc_lat; ?>, lng: <?php echo $audit->store->s_loc_long; ?>},
		label:'S',
		map: map
	});	
	var marker = new google.maps.Marker({
	  position: uluru,
	  label:'A',
	  map: map
	});
}

</script>
<!-- async defer for google map -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCxkBwVSWJ5DX4E5E6Gg_AfyhubdTbV0Y&callback=initMap">
</script>
