<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Color Points - Rejected
					</header>
					<div class="panel-body">

						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>

						<!-- Search -->
						<form class="form-group" method="GET" action="<?php echo base_url('cms/color_points/rejected'); ?>">
							<?php if (@$search_keyword): ?>
								<div class="col-sm-9">
									<span class="form-control" style="border: 0px;">Search result for: <label><?php echo @$search_keyword; ?></label></span>
								</div>
							<?php endif ?>
							<div class="col-sm-3 pull-right">
								<div class="input-group m-bot15">
									<input type="text" class="form-control" placeholder="search.." name="search_keyword" value="<?php echo (@$search_keyword) ? $search_keyword : ''; ?>">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</form>
						<!-- /Search -->

						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Requestor</th>
                                    <th>Name</th>
                                    <th>Mobile Number</th>
									<th>Address</th>
									<th>Membership</th>
									<th>Affiliate</th>
									<th>Url</th>
									<th>Notes</th>
									<th>Current Points</th>
									<th>Points to Redeem</th>
									<th>Date Rejected</th>
									<th>Date Requested</th>
								</tr>
							</thead>
							<tbody>
                            <?php if (count($redeem_requests) > 0) :
                             foreach($redeem_requests as $request): ?>
									<tr>
										<td><?php echo $request->fullname; ?><br>(<?php echo $request->email; ?>)</td>
										<td><?php echo $request->fname." ".$request->lname;?></td>
										<td><?php echo $request->mobile_no; ?></td>
										<td><?php echo $request->address; ?></td>
										<td><?php echo $request->name; ?></td>
										<td><?php echo $request->title; ?></td>
										<td><?php echo $request->url; ?></td>
										<td><?php echo $request->notes; ?></td>
										<td><?php echo $request->points; ?></td>
										<td><?php echo $request->points_to_redeem; ?></td>
										<td><?php echo date("M j, y g:i a", strtotime($request->updated_at)); ?></td>
										<td><?php echo date("M j, y g:i a", strtotime($request->created_at)); ?></td>
									</tr>
							<?php endforeach;
							else: ?>
								<tr>
									<td colspan="9" align="center">No results available.</td>
								</tr>
							<?php endif;?>  
                            </tbody>
							<tfoot>
								<tr>
									<th>Requestor</th>
									<th>Name</th>
                                    <th>Mobile Number</th>
									<th>Address</th>
									<th>Membership</th>
									<th>Affiliate</th>
									<th>Url</th>
									<th>Notes</th>
									<th>Current Points</th>
									<th>Points to Redeem</th>
									<th>Date Rejected</th>
									<th>Date Requested</th>
								</tr>
							</tfoot>
						</table>
						<div class="text-center">
				            <ul class="pagination">
				              <?php echo $pagination; ?>
				            </ul>
				          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>


		

    </section>
</section>
<!--main content end -->
