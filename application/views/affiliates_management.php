<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Affiliates
					</header>
					<div class="panel-body">
						<div class="row">
			            	<div class="col-md-12 mbot15">
				            	<div class="pull-right">
									<a class="btn btn-info btn-sm" data-toggle="modal" href="#addAffiliate"><i class="fa fa-plus"></i>&nbsp; Add Affiliate</a>
								</div>
			            	</div>
			            </div>
						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>

						<!-- Search -->
						<form class="form-group" method="GET" action="<?php echo base_url('cms/color_points/affiliates'); ?>">
							<?php if (@$search_keyword): ?>
								<div class="col-sm-9">
									<span class="form-control" style="border: 0px;">Search result for: <label><?php echo @$search_keyword; ?></label></span>
								</div>
							<?php endif ?>
							<div class="col-sm-3 pull-right">
								<div class="input-group m-bot15">
									<input type="text" class="form-control" placeholder="search.." name="search_keyword" value="<?php echo (@$search_keyword) ? $search_keyword : ''; ?>">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</form>
						<!-- /Search -->

						<table class="table table-bordered">
							<thead>
								<tr>
                                    <th>Title</th>
                                    <th>Site Link</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php foreach($affiliates as $affiliate): ?>
									<tr>
										<td><?php echo $affiliate->title; ?></td>
										<td><?php echo $affiliate->site_link; ?></td>
										<td>
											<a href="<?php echo $affiliate->image_path; ?>" target="_blank">Preview</a>
										</td>
										<td>
											<button
											type="button"
											data-target="#editAffiliate"
											data-toggle="modal"
											class="btn btn-primary btn-xs edit_affiliate"
											data-id="<?php echo $affiliate->affiliate_id; ?>"
											data-title="<?php echo $affiliate->title; ?>"
											data-image="<?php echo $affiliate->image_path; ?>"
											data-site_link="<?php echo $affiliate->site_link; ?>"
											data-url="<?php echo base_url(); ?>"
											>Edit</button>
											<form method="POST" action="<?php echo base_url(); ?>cms/color_points/delete/<?php echo $affiliate->affiliate_id; ?>" onsubmit="return confirm('Are you sure you want to delete this?');">
												<input type="hidden" name="affiliate_id" value="<?php echo $affiliate->affiliate_id; ?>">
												<input type="submit" class="btn btn-danger btn-xs" value="Delete">
											</form>
										</td>
									</tr>
							<?php endforeach; ?>    
                            </tbody>
							<tfoot>
								<tr>
									<th>Title</th>
									<th>Site Link</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
						<div class="text-center">
							<ul class="pagination">
								<?php echo $pagination; ?>
							</ul>
						</div>
                        </div>
					</div>
				</section>
			</div>
		</div>


		<!-- add affiliate modal start -->
		<div class="modal fade " id="addAffiliate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form method="POST" action="<?=base_url()?>cms/color_points/create" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Affiliates</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>Title <span class="required">*</span></label>
			        			<input type="text" required name="title" class="form-control" placeholder="Input Affiliate title here">
			        		</div>
			        		<div class="form-group">
			        			<label>Image <span class="required">*</span></label>
			        			<input type="file" required name="affiliate_image" class="form-control">
			        		</div>
			        		<div class="form-group">
			        			<label>Site url <span class="required">*</span></label>
			        			<input type="text" required name="site_link" class="form-control" placeholder="https://www.site.com">
			        		</div>  
                        </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                    	<button type="Submit" class="btn btn-info btn-block" id="add_affiliate_submit">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- add affiliate modal end -->

		<!-- edit affiliate modal start -->
		<div class="modal fade " id="editAffiliate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form id="editForm" method="POST" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Edit Affiliates</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>Affiliate Title <span class="required">*</span></label>
			        			<input type="text" required name="title" class="form-control" placeholder="Input Affiliate title here">
			        		</div>
							<div id="imageHolder"></div>
			        		<div class="form-group">
			        			<label>Affiliate Image <span class="required">*</span></label>
			        			<input type="file" name="affiliate_image" class="form-control">
			        		</div>  
			        		<div class="form-group">
			        			<label>Site url <span class="required">*</span></label>
			        			<input type="text" required name="site_link" class="form-control" placeholder="https://www.site.com">
			        		</div>
	                    </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                    	<button type="Submit" class="btn btn-info btn-block" id="add_affiliate_submit">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- edit affiliate modal end -->

    </section>
</section>
<!--main content end -->


<script type="text/javascript">
$(".edit_affiliate").on("click", function(){
	$("#editAffiliate").find('input[name="title"]').val($(this).data('title'));
	$("#editAffiliate").find('#imageHolder').html('<img height="200" width="200" src="' + $(this).data('image') + '" />');
	$("#editAffiliate").find('input[name="site_link"]').val($(this).data('site_link'));
	$("#editAffiliate").find('#editForm').attr('action', $(this).data('url') + 'cms/color_points/edit/' + $(this).data('id'));
});
</script>