<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Color Your Life - Reset</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('frontend/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontend/css/bootstrap-reset.css') ?>" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('frontend/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontend/css/style-responsive.css') ?>" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="<?php echo base_url('users/validate_reset'); ?>" method="post">
        <h2 class="form-signin-heading">COLOR YOUR LIFE</h2>
        <div class="login-wrap">
          <!-- Improved Flashdata Start -->
          <?php
            $alert_msg = $this->session->flashdata('alert_msg');
            if($alert_msg != ""): ?>
            <div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
              <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="fa fa-times"></i>
              </button>
              <?php echo $alert_msg; ?>
            </div>
          <?php else: ?>
            <input id="pass" type="password" name="password" class="form-control" placeholder="New Password" autofocus required="">
            <input id="c_pass" type="password" name="c_password" class="form-control" placeholder="Re-type New Password" required="">
            <input type="hidden" name="forgot-token" class="form-control" value="<?php echo $this->uri->segment(3) ?>">

            <br><br>
            <button class="btn btn-lg btn-login btn-block" type="submit">Reset Password</button>
          <?php endif; ?>
        <!-- Improved Flashdata End -->
        </div>

      </form>

    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('frontend/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('frontend/js/bootstrap.min.js'); ?>"></script>

    <script>
      $('form').on('submit', function(e){
        if ($('#c_pass').val() != $('#pass').val()) {
          alert('Password doesn\'t match.');
          e.preventDefault();

        }
      });
    </script>
  </body>
</html>
