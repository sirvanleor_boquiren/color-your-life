<!-- main content start-->
<style>
      #map {
        width: 100%;
        height: 500px;
        background-color: grey;
      }
    </style>
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Store Summary
					</header>
					<div class="panel-body">
						<div class="table-responsive">
						<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<br>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <br>
					        <?php endif; ?>
					        <div class="row" style="width:95%">
					        <?php
					        $days_select = array("15", "30", "45", "60");
					        ?>
					        	<form name="filters" id="filters" method="get">
					        		<!-- <div class="col-lg-2">
					        			Filter Stores that has not been audited by: 
					        		</div>
						        	<div class="col-lg-5">
						        		<select class="form-control" name="date_range">
						        			<option value="">Select Days Filter</option>
						        			<?php foreach ($days_select as $days) { ?>
						        			<option value="<?php echo $days ?>" <?php echo isset($_GET['date_range']) && $_GET['date_range'] == $days ? "selected" : ""; ?>><?php echo $days ?> Days</option>
						        			<?php } ?>
						        		</select>
						        	</div> -->
						        	<div class="col-lg-4">
						        		<!-- <input type="submit" class="btn btn-primary" value="Filter"> -->
						        		<div class="btn-group">
		                                  	<button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">Export&nbsp;&nbsp;<span class="caret"></span></button>
		                                  	<ul role="menu" class="dropdown-menu">
												<li><a href="<?php echo $csv_export_link; ?>">CSV</a></li>
												<li class="divider"></li>
												<li><a href="<?php echo $pdf_export_link; ?>" target="_blank">PDF</a></li>
		                                  	</ul>
		                              	</div>
	                            		<!-- <a href="<?php echo base_url()."Reports/Store"; ?>" class="btn btn-warning">Clear Filters</a> -->
						        	</div>
					        	</form>
					        </div>
					        <br>
                          <table class="table table-bordered">
                              <thead>
								<tr>
									<th>Last Audit Date</th>
									<th>Store Name</th>
									<th>Store Rating (Stars)</th>
									<th>Store Performance</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
							<?php foreach ($all_summary as $summary_id => $summary) { ?>
							 	<tr>
									<td><?php echo $summary->sa_visit_date; ?></td>
									<td><?php echo $summary->store_name; ?></td>
									<td>
									<?php if($summary->store_rating == ""): ?>
										N/A
									<?php else: ?>
										<span class="rating">
											<?php echo $summary->store_rating; ?>
										</span>
									<?php endif; ?>
									</td>
									<td><?php echo $summary->average_grade; ?></td>
									<td>
										<a href="" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
										<a href="" class="btn btn-danger btn-xs"><i class="fa fa-lock"></i></a>
									</td>
								</tr>
							<?php } ?>
							</tbody>
							<tfoot>
								<tr>
								  	<th>Audit Date</th>
									<th>Store Name</th>
									<th>Store Rating (Stars)</th>
									<th>Store Performance</th>
									<th>Action</th>
								</tr>
							</tfoot>
                          </table>
                          <div class="text-center">
	                          <ul class="pagination">
	                          	<?php echo $pagination; ?>
	                          </ul>
                          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->

<!-- Modal: Geolocation / Start -->
<div class="modal fade" id="modal_map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Default Modal Tittle</h4>
          </div>
          <div class="modal-body" id="modal_body">
          	 <div class="alert alert-info fade in">
	              <button data-dismiss="alert" class="close close-sm" type="button">
	                  <i class="fa fa-times"></i>
	              </button>
	              <strong>Heads up!</strong><br><strong>S</strong> = Store Location / <strong>A</strong> = Analyst Location
	          </div>
            <div id="map"></div>
          </div>
          <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
          </div>
      </div>
  </div>
</div>						
<!-- Modal: Geolocation / End -->

<!-- Scripts -->
<script>

// Map Funcitons
function initMap(loc_lat, loc_long) {
        var uluru = {lat: loc_lat, lng: loc_long};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          label:'A',
          map: map
        });
        var marker = new google.maps.Marker({
        	position: {lat:10.282069, lng:123.880814},
        	label:'S',
        	map: map
        });
        $("#modal_map").on("shown.bs.modal", function () {
		    google.maps.event.trigger(map, "resize");
		    map.setCenter(uluru);
		});
      }

function showMap(lat, long) {
	// $('#modal_body').html('');
	var dialog = $('#modal_map').modal();
		dialog.removeData('bs.modal');
		dialog.modal("show");	
	initMap(lat, long);
}


</script>
<!-- async defer for google map -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCxkBwVSWJ5DX4E5E6Gg_AfyhubdTbV0Y&callback=initMap">
</script>
