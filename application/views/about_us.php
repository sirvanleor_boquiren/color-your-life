<section id="main-content">
	<section class="wrapper">

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						About Us
					</header>
					<?php
					$alert_msg = $this->session->flashdata('alert_msg');
					if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
								<i class="fa fa-times"></i>
							</button>
							<?php echo $alert_msg; ?>
						</div>
					<?php endif; ?>
					<div class="panel-body">
						<form action="<?php echo base_url('cms/company_info/add'); ?>" method="POST" class="form-horizontal tasi-form">
							<div class="form-group">
								<div class="col-md-12">
									<label>Company Logo</label>
									<input type="file" name="company_logo" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>About the company</label>
									<textarea class="form-control" rows="10" name="about_the_company"><?php echo @$data['about_the_company']; ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>CEO message</label>
									<textarea class="form-control" rows="10" name="ceo_message"><?php echo @$data['ceo_message']; ?></textarea>
								</div>
							</div>
							<!-- <div class="form-group">
								<div class="col-md-12">
									<label>Address</label>
									<input type="text" name="address" class="form-control" value="<?php //echo @$data['address']; ?>">
								</div>
							</div> -->
							<input type="submit" name="save_about_us" value="Save" class="btn btn-info btn-block">
						</form>
					</div>
				</section>
			</div>
		</div>

	</section>
</section>