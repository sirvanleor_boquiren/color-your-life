<!-- main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
		
			<?php $this->load->view("partials/user-sidebar"); ?>

			<aside class="profile-info col-lg-9">
				<section class="panel">
					<div class="bio-graph-heading">
						Profile Information
					</div>
					<div class="panel-body bio-graph-info">
						
						<div class="row">
							<div class="bio-row">
								<p><span>First Name </span>: <?php echo $details->fname; ?></p>
							</div>
							<div class="bio-row">
								<p><span>Last Name </span>: <?php echo $details->lname; ?></p>
							</div>
							<div class="bio-row">
								<p><span>Email </span>: <?php echo $details->email; ?></p>
							</div>
							<div class="bio-row">
								<p><span>Referral Code </span>: <?php echo $details->referral_code; ?></p>
							</div>
							<div class="bio-row">
								<p><span>Color Points</span>: <?php echo $details->points; ?></p>
							</div>
							<div class="bio-row">
								<p><span>Referrals</span>: <?php echo $details->referrals; ?></p>
							</div>
							<div class="bio-row">
								<p><span>Membership</span>: <?php echo $details->membership_plan_name; ?> Plan</p>
							</div>
							<div class="bio-row">
								<p><span>Expiry </span>: <?php echo date("F d, Y", strtotime($details->membership_expiry)); ?></p>
							</div>
						</div>
					</div>
				</section>

				<?php if ($this->session->userdata['type'] == "Special Admin"): ?>
				<section>
					<h3>Color Bonus</h3>
					<div class="row state-overview">
						<div class="col-lg-6 col-sm-6">
							<section class="panel">
								<div class="symbol terques">
									<i class="fa fa-user"></i>
								</div>
								<div class="value">
									<h1 class="countFree">
										0
									</h1>
									<p>Free</p>
								</div>
							</section>
						</div>
						<div class="col-lg-6 col-sm-6">
							<section class="panel">
								<div class="symbol red">
									<i class="fa fa-user"></i>
								</div>
								<div class="value">
									<h1 class="countChallenger">
										0
									</h1>
									<p>Challenger</p>
								</div>
							</section>
						</div>
						<div class="col-lg-6 col-sm-6">
							<section class="panel">
								<div class="symbol yellow">
									<i class="fa fa-user"></i>
								</div>
								<div class="value">
									<h1 class="countEconomy">
										0
									</h1>
									<p>Economy</p>
								</div>
							</section>
						</div>
						<div class="col-lg-6 col-sm-6">
							<section class="panel">
								<div class="symbol blue">
									<i class="fa fa-user"></i>
								</div>
								<div class="value">
									<h1 class="countBusiness">
										0
									</h1>
									<p>Business</p>
								</div>
							</section>
						</div>
					</div>
				</section>
				<?php endif ?>

			</aside>


		</div>
		<!-- page end-->		
	</section>
</section>
<!--main content end -->
<script src="<?php echo base_url('frontend/assets/jquery-knob/js/jquery.knob.js'); ?>"></script>
<script type="text/javascript">
$(".knob").knob();
$(function(){
	countUp(<?php echo $bonus->free; ?>, "countFree");
	countUp(<?php echo $bonus->challenger; ?>, "countChallenger");
	countUp(<?php echo $bonus->economy; ?>, "countEconomy");
	countUp(<?php echo $bonus->business; ?>, "countBusiness");
});
</script>