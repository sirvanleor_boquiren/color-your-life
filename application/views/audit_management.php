<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Report Data
					</header>
					<div class="panel-body">
						<div class="table-responsive">
						<?php if(null !== $this->session->flashdata('alert_msg')): ?>
							<br>
							<div class="form-group">
					          <center>
					            <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
					              <?php echo $this->session->flashdata('alert_msg'); ?>
					            </span>
					          </center>
					        </div>
					        <br>
					        <?php endif; ?>
					        <div class="row" style="width:100%">
					        	<form name="filters" id="filters" method="get">
						        	<div class="col-lg-4">
							        	<div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
		                                  <input type="text" class="form-control dpd1" name="from" placeholder="Start Audit Date" <?php echo isset($_GET['from']) && $_GET['from'] != "" ? 'value="'.date("m/d/Y", strtotime($_GET['from'])).'"' : "";  ?>>
		                                  <span class="input-group-addon">To</span>
		                                  <input type="text" class="form-control dpd2" name="to" placeholder="End Audit Date" <?php echo isset($_GET['to']) && $_GET['to'] != "" ? 'value="'.date("m/d/Y", strtotime($_GET['to'])).'"' : "";  ?>>
		                              	</div>
	                            	</div>
	                            	<div class="col-lg-2">
	                            		<select name="store" id="store" class="form-control">
	                            			<option value="">Select Store</option>
	                            			<?php foreach ($store_list as $store_obj) { ?>
	                            				<option value="<?php echo $store_obj->s_id ?>" <?php echo isset($_GET['store']) && $_GET['store'] == $store_obj->s_id ? "selected" : ""; ?>><?php echo $store_obj->store_name; ?></option>
	                            			<?php } ?>
	                            		</select>
	                            	</div>
	                            	<div class="col-lg-3">
	                            	<?php // var_dump($auditor_list); ?>
	                            		<select name="auditor" id="auditor" class="form-control">
	                            			<option value="">Select Auditor</option>
	                            			<?php foreach ($auditor_list as $auditor_obj) { ?>
	                            			<option value="<?php echo $auditor_obj->u_id; ?>" <?php echo isset($_GET['auditor']) && $_GET['auditor'] == $auditor_obj->u_id ? "selected" : ""; ?>><?php echo $auditor_obj->fullname; ?></option>
	                            			<?php } ?>
	                            		</select>
	                            	</div>
	                            	<div class="col-lg-3">
	                            		<input type="submit" class="btn btn-primary" value="Filter">
	                            		<div class="btn-group">
		                                  	<button data-toggle="dropdown" class="btn btn-info dropdown-toggle" type="button">Export&nbsp;&nbsp;<span class="caret"></span></button>
		                                  	<ul role="menu" class="dropdown-menu">
												<li><a href="<?php echo $csv_export_link; ?>">CSV</a></li>
												<li class="divider"></li>
												<li><a href="<?php echo $pdf_export_link; ?>" target="_blank">PDF</a></li>
		                                  	</ul>
		                              	</div>
	                            		<a href="<?php echo base_url("Reports/Audit"); ?>" class="btn btn-warning">Clear</a>
	                            	</div>
					        	</form>
					        	
					        </div>
					        <br>
                          <table class="table table-bordered">
                              <thead>
								<tr>
									<th style="min-width:145px">Store Code/Name</th>
									<th>Auditor</th>
									<th>Audit Date</th>
									<th>Geolocation</th>
									<th>Grade per Question</th>
									<th>Average Grade</th>
									<th style="min-width:186px">Star Rating</th>
									<th>Date Submitted</th>
									<th style="min-width:70px">Action</th>
								</tr>
							</thead>
                            <tbody>
                            <?php if(isset($all_audit) && $all_audit != null): ?>
							<?php foreach ($all_audit as $audit_id => $audit) { ?>
							 	<tr>
							 		<?php 
							 		# Add buttons to store code if newly submitted Store
							 		if($audit->new_store == "1"){
							 			$audit->store_code .= "<br><a href='".base_url("Stores/Add/".$audit->sa_id)."' class='btn btn-xs btn-success btn-block'>Check Details</a><button onclick='showAssign(".$audit->sa_id.");' class='btn btn-xs btn-info btn-block'>Assign Store</button>";

							 		} ?>
									<td><?php echo $audit->store_code; ?></td>
									<td><?php echo $audit->sa_analyst; ?></td>
									<td><?php echo $audit->sa_visit_date; ?></td>
									<td><button class="btn btn-info btn-block" onclick="showMap(<?php echo $audit->sa_loc_lat; ?>, <?php echo $audit->sa_loc_long; ?>, <?php echo $audit->s_loc_lat; ?>, <?php echo $audit->s_loc_long; ?>);"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Show in Map</button></td>
									<td><?php echo $audit->grade_per; ?></td>
									<td><?php echo $audit->average_grade; ?></td>
									<td>
										<span class="rating">
											<?php echo $audit->store_rating; ?>
										</span>
									</td>
									<td><?php echo $audit->sa_date_added; ?></td>
									<td>
										<a href="<?php echo base_url("Reports/File/$audit->sa_id"); ?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
									</td>
								</tr>
							<?php } ?>
						<?php else:?>
							<tr>
								<td colspan="9"><center>No records found.</center></td>
							</tr>
						<?php endif; ?>
							</tbody>
							<tfoot>
								<tr>
								  	<th>Store Code/Name</th>
									<th>Auditor</th>
									<th>Audit Date</th>
									<th>Geolocation</th>
									<th>Grade per Question</th>
									<th>Average Grade</th>
									<th>Star Rating</th>
									<th>Date Submitted</th>
									<th>Action</th>
								</tr>
							</tfoot>
                          </table>
                          <div class="text-center">
	                          <ul class="pagination">
	                          	<?php echo $pagination; ?>
	                          </ul>
                          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>
    </section>
</section>
<!--main content end -->

<!-- Modal: Geolocation / Start -->
<div class="modal fade" id="modal_map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Geo/Map Location</h4>
          </div>
          <div class="modal-body" id="modal_body">
          	 <div class="alert alert-info fade in">
	              <button data-dismiss="alert" class="close close-sm" type="button">
	                  <i class="fa fa-times"></i>
	              </button>
	              <strong>Heads up!</strong><br><strong>S</strong> = Store Location / <strong>A</strong> = Analyst Location
	          </div>
            <div id="map" class="map-audit"></div>
          </div>
          <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
          </div>
      </div>
  </div>
</div>
<!-- Modal: Geolocation / End -->


<!-- Modal: Assign to an existing Store / Start -->
<div class="modal fade" id="modal_assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Assign Store</h4>
          </div>
          <div class="modal-body" id="modal_body">
          	<form class="form-horizontal tasi-form" method="POST" action="<?php echo base_url("Reports/assignStore"); ?>">
          		<div class="form-group">
          			<label class="col-sm-3 col-sm-3 control-label">
          				Stores
          			</label>
          			<div class="col-sm-9">
          				<select name="s_id" id="assign_store_id" class="form-control">
		        			<option value="">Select Store</option>
		        			<?php foreach ($store_list as $store_obj) { ?>
		        				<option value="<?php echo $store_obj->s_id ?>"><?php echo $store_obj->store_name; ?></option>
		        			<?php } ?>
		        		</select>
		        		<input type="hidden" name="sa_id" id="assign_sa_id">
          			</div>
          		</div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
          	<button type="submit" class="btn btn-success">Submit</button>
          </div>
       	</form>
      </div>
  </div>
</div>
<!-- Modal: Assign to an existing Store / End -->


<!-- Scripts -->
<script>

// Map Funcitons
function initMap(user_loc_lat, user_loc_long, store_loc_lat, store_loc_long) {
	var uluru = {lat: user_loc_lat, lng: user_loc_long};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 17,
	  center: uluru
	});
	var marker = new google.maps.Marker({
	  position: uluru,
	  label:'A',
	  map: map
	});
	if(store_loc_lat != 0 && store_loc_long != 0)
	{
		var marker = new google.maps.Marker({
			position: {lat: store_loc_lat, lng: store_loc_long},
			label:'S',
			map: map
		});	
	}
	$("#modal_map").on("shown.bs.modal", function () {
	    google.maps.event.trigger(map, "resize");
	    map.setCenter(uluru);
	});
}

function showMap(lat, long, store_lat, store_long) {
	// $('#modal_body').html('');
	var dialog = $('#modal_map').modal();
		dialog.removeData('bs.modal');
		dialog.modal("show");	
	initMap(lat, long, store_lat, store_long);
}

function showAssign(sa_id) {
	$('#assign_sa_id').val(sa_id);
	var dialog = $('#modal_assign').modal();
		dialog.removeData('bs.modal');
		dialog.modal("show");	
}

</script>
<!-- async defer for google map -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCxkBwVSWJ5DX4E5E6Gg_AfyhubdTbV0Y&callback=initMap">
</script>
