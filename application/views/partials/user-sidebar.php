<aside class="profile-nav col-lg-3">
	<section class="panel">
		<div class="user-heading round">
			<a href="#">
				<img src="<?php echo $details->profile_pic; ?>" alt="">
			</a>
			<h1><?php echo $details->fullname; ?></h1>
			<p><?php echo $details->rank_type; ?></p>
		</div>
		<ul class="nav nav-pills nav-stacked">
			<li class="<?php echo uri_string() == "cms/users/details/" . $details->user_id ? 'active' : ''; ?>">
				<a href="<?php echo base_url("cms/users/details/" . $details->user_id); ?>"> 
					<i class="fa fa-user"></i> Profile Information
				</a>
			</li>

			<?php if ($this->session->userdata['type'] == "Special Admin"): ?>
				<li class="<?php echo uri_string() == "cms/users/posts/" . $details->user_id ? 'active' : ''; ?>">
					<a href="<?php echo base_url("cms/users/posts/" . $details->user_id); ?>"> 
						<i class="fa fa-edit"></i> Posts
					</a>
				</li>
				<li>
					<a href=""> 
						<i class="fa fa-edit"></i> Advertisements
					</a>
				</li>
				<li>
					<a href=""> 
						<i class="fa fa-edit"></i> Videos
					</a>
				</li>
				<li class="<?php echo uri_string() == "cms/extra/index/" . $details->user_id ? 'active' : ''; ?>">
					<a href="<?php echo base_url("cms/extra/index/" . $details->user_id); ?>"> 
						<i class="fa fa-edit"></i> Extra Details
					</a>
				</li>	
			<?php endif ?>

		</ul>
	</section>
</aside>