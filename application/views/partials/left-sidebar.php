<?php
$sub_menu = array();
$sub_menu["users"] = array("users/app", "users/admin");
$sub_menu["reports"] = array("reports/audit", "reports/store", "reports/file");
$sub_menu['cms'] = array('cms/affiliates');

?>
<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">

    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
        
      <li>
        <a <?php echo uri_string() == "" ? "class='active'" : ""; ?> href="<?php echo base_url(); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <li>--</li>

      <?php if ($this->session->userdata['type'] == "Super Admin" || $this->session->userdata['type'] == "Special Admin"): ?>
      <li>
        <a href="<?php echo base_url('cms/admin'); ?>" <?php echo uri_string() == "cms/admin" ? "class='active'" : ""; ?>>
          <i class="fa fa-users"></i>
          <span>Manage Admin</span>
        </a>
      </li>
      <?php endif ?>

      <li>
        <a href="<?php echo base_url('cms/users'); ?>" <?php echo uri_string() == "cms/users" ? "class='active'" : ""; ?>>
          <i class="fa fa-users"></i>
          <span>Manage Users</span>
        </a>
      </li>

      <li>--</li>

      <li class="sub-menu">
        <a href="javascript:;" <?php echo in_array(uri_string(), ["cms/company_info/privacy_policy", "cms/company_info/about_us", "cms/company_info/contact_us", "cms/company_info/terms_conditions"]) ? "class='active'" : ""; ?>>
          <i class="fa fa-bar-chart-o"></i>
          <span>Company Info</span>
        </a>
        <ul class="sub">

          <!-- <li <?php //echo uri_string() == "cms/company_info/privacy_policy" ? "class='active'" : ""; ?> >
            <a href="<?php //echo base_url('cms/company_info/privacy_policy') ?>">
              <i class="fa fa-book"></i>
              <span>Privacy Policy</span>
            </a>
          </li> -->

          <li <?php echo uri_string() == "cms/company_info/about_us" ? "class='active'" : ""; ?> >
            <a href="<?php echo base_url('cms/company_info/about_us') ?>">
              <i class="fa fa-book"></i>
              <span>About Us</span>
            </a>
          </li>

          <li <?php echo uri_string() == "cms/company_info/contact_us" ? "class='active'" : ""; ?> >
            <a href="<?php echo base_url('cms/company_info/contact_us') ?>">
              <i class="fa fa-book"></i>
              <span>Contact Us</span>
            </a>
          </li>

          <!-- <li <?php //echo uri_string() == "cms/company_info/terms_conditions" ? "class='active'" : ""; ?> >
            <a href="<?php //echo base_url('cms/company_info/terms_conditions') ?>">
              <i class="fa fa-book"></i>
              <span>Terms&Conditions</span>
            </a>
          </li> -->

        </ul>
      </li>

      <li>--</li>

      <!-- Color Points -->
      <li class="sub-menu">
        <a href="javascript:;" <?php echo in_array(uri_string(), ['cms/color_points/affiliates', 'cms/color_points/pending', 'cms/color_points/approved', 'cms/color_points/rejected']) ? "class='active'" : ""; ?>>
          <i class="fa fa-bar-chart-o"></i>
          <span>Color Points</span>
        </a>
        <ul class="sub">

          <li>
            <a href="<?php echo base_url('cms/color_points/affiliates') ?>" <?php echo uri_string() == "cms/color_points/affiliates" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Affiliates</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('cms/color_points/pending') ?>" <?php echo uri_string() == "cms/color_points/pending" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Pending Requests</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('cms/color_points/approved') ?>" <?php echo uri_string() == "cms/color_points/approved" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Approved Requests</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('cms/color_points/rejected') ?>" <?php echo uri_string() == "cms/color_points/rejected" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Rejected Requests</span>
            </a>
          </li>

        </ul>
      </li>
      <!-- Color Points End -->

      <li>--</li>

        <!-- Color Presents -->
      <li class="sub-menu">
        <a href="javascript:;" <?php echo in_array(uri_string(), ['cms/presents', 'cms/presents/pending', 'cms/presents/approved', 'cms/presents/rejected']) ? "class='active'" : ""; ?>>
          <i class="fa fa-bar-chart-o"></i>
          <span>Color Presents</span>
        </a>
        <ul class="sub">

          <li>
            <a href="<?php echo base_url('cms/presents') ?>" <?php echo uri_string() == "cms/presents" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Presents</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('cms/presents/pending') ?>" <?php echo uri_string() == "cms/presents/pending" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Pending Requests</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('cms/presents/approved') ?>" <?php echo uri_string() == "cms/presents/approved" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Approved Requests</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url('cms/presents/rejected') ?>" <?php echo uri_string() == "cms/presents/rejected" ? "class='active'" : ""; ?>>
              <i class="fa fa-book"></i>
              <span>Rejected Requests</span>
            </a>
          </li>

        </ul>
      </li>
      <!-- Color Presents End -->

    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->
