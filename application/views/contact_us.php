<section id="main-content">
	<section class="wrapper">

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						Contact Us
					</header>
					<?php
					$alert_msg = $this->session->flashdata('alert_msg');
					if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
								<i class="fa fa-times"></i>
							</button>
							<?php echo $alert_msg; ?>
						</div>
					<?php endif; ?>
					<div class="panel-body">
						<form action="<?php echo base_url('cms/company_info/add'); ?>" method="POST" class="form-horizontal tasi-form">
							<div class="form-group">
								<div class="col-md-12">
									<label>Mobile</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="mobile_label" class="form-control" placeholder="label" value="<?php echo @$data['mobile']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-mobile"></i>
										<input type="text" name="mobile" class="form-control" placeholder="+63XXXXXXXXXX" value="<?php echo @$data['mobile']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Email</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="email_label" class="form-control" placeholder="label" value="<?php echo @$data['email']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-envelope"></i>
										<input type="text" name="email" class="form-control" placeholder="user@mail.com" value="<?php echo @$data['email']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Website</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="website_label" class="form-control" placeholder="label" value="<?php echo @$data['website']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-globe"></i>
										<input type="text" name="website" class="form-control" placeholder="www.website.com" value="<?php echo @$data['website']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Facebook</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="facebook_label" class="form-control" placeholder="label" value="<?php echo @$data['facebook']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-facebook-square"></i>
										<input type="text" name="facebook" class="form-control" placeholder="/facebook.page" value="<?php echo @$data['facebook']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Twitter</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="twitter_label" class="form-control" placeholder="label" value="<?php echo @$data['twitter']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-twitter-square"></i>
										<input type="text" name="twitter" class="form-control" placeholder="@twitter" value="<?php echo @$data['twitter']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Instagram</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="instagram_label" class="form-control" placeholder="label" value="<?php echo @$data['instagram']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-instagram"></i>
										<input type="text" name="instagram" class="form-control" placeholder="@instagram" value="<?php echo @$data['instagram']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Youtube</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="youtube_label" class="form-control" placeholder="label" value="<?php echo @$data['youtube']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-youtube"></i>
										<input type="text" name="youtube" class="form-control" placeholder="/youtube" value="<?php echo @$data['youtube']['link']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label>Youku</label>
									<div class="iconic-input">
										<i class="fa fa-pencil-square-o"></i>
										<input type="text" name="youku_label" class="form-control" placeholder="label" value="<?php echo @$data['youku']['label']; ?>">
									</div>
									<div class="iconic-input">
										<i class="fa fa-play-circle-o"></i>
										<input type="text" name="youku" class="form-control" placeholder="/youku" value="<?php echo @$data['youku']['link']; ?>">
									</div>
								</div>
							</div>
							<input type="submit" name="save_contact_us" value="Save" class="btn btn-info btn-block">
						</form>
					</div>
				</section>
			</div>
		</div>

	</section>
</section>