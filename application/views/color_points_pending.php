<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Color Points - Pending
					</header>
					<div class="panel-body">
			            <table class="table table-bordered">
							<thead>
								<tr>
                                    <th>Membership Name</th>
                                    <th>Point Redeem Start</th>
								</tr>
							</thead>
							<tbody>
			            <?php foreach ($membership_plans as $membership) :?>
			            	<tr>
			            		<td><?php echo $membership->name; ?></td>
								<td><?php echo $membership->points_redeem_start; ?></td>
			            	</tr>
			            	
			            <?php endforeach; ?>
			             </tbody>
						</table>

						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>

						<!-- Search -->
						<form class="form-group" method="GET" action="<?php echo base_url('cms/color_points/pending'); ?>">
							<?php if (@$search_keyword): ?>
								<div class="col-sm-9">
									<span class="form-control" style="border: 0px;">Search result for: <label><?php echo @$search_keyword; ?></label></span>
								</div>
							<?php endif ?>
							<div class="col-sm-3 pull-right">
								<div class="input-group m-bot15">
									<input type="text" class="form-control" placeholder="search.." name="search_keyword" value="<?php echo (@$search_keyword) ? $search_keyword : ''; ?>">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</form>
						<!-- /Search -->

						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Requestor</th>
                                    <th>Name</th>
                                    <th>Mobile Number</th>
									<th>Address</th>
									<th>Membership</th>
									<th>Affiliate</th>
									<th>Url</th>
									<th>Notes</th>
									<th>Current Points</th>
									<th>Points to Redeem</th>
									<th>Date Requested</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php if (count($redeem_requests) > 0) :
                             foreach($redeem_requests as $request): ?>
									<tr>
										<td><?php echo $request->fullname; ?><br>(<?php echo $request->email; ?>)</td>
										<td><?php echo $request->fname." ".$request->lname;?></td>
										<td><?php echo $request->mobile_no; ?></td>
										<td><?php echo $request->address; ?></td>
										<td><?php echo $request->name; ?></td>
										<td><?php echo $request->title; ?></td>
										<td><?php echo $request->url; ?></td>
										<td><?php echo $request->notes; ?></td>
										<td><?php echo $request->points; ?></td>
										<td><?php echo $request->points_to_redeem; ?></td>
										<td><?php echo date("M j, y g:i a", strtotime($request->created_at)); ?></td>
										<td>
											
											<!-- <input type="button" class="btn btn-primary btn-xs" value="Approve" onclick=""> -->
											<a class="approveBtn btn btn-info btn-xs" data-toggle="modal" data-ptr="<?php echo $request->points_to_redeem; ?>" data-cp="<?php echo $request->points; ?>" data-rrid="<?php echo $request->redeem_request_id; ?>" href="#approveRequest">Approve</a>
											<form method="POST" action="<?php echo base_url(); ?>cms/color_points/rejected" onsubmit="return confirm('Are you sure you want to reject this request?');">
												<input type="hidden" name="id" value="<?php echo $request->redeem_request_id; ?>">
												<input type="hidden" name="action" value="decline">
												<input type="submit" class="btn btn-danger btn-xs" value="Reject">
											</form>
										</td>
									</tr>
							<?php endforeach; else: ?>
								<tr>
									<td colspan="9" align="center">No results available.</td>
								</tr>
							<?php endif; ?>    
                            </tbody>
							<tfoot>
								<tr>
									<th>Requestor</th>
									<th>Name</th>
                                    <th>Mobile Number</th>
									<th>Address</th>
									<th>Membership</th>
									<th>Affiliate</th>
									<th>Url</th>
									<th>Notes</th>
									<th>Current Points</th>
									<th>Points to Redeem</th>
									<th>Date Requested</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
						 <div class="text-center">
				            <ul class="pagination">
				              <?php echo $pagination; ?>
				            </ul>
				          </div>
                        </div>
					</div>
				</section>
			</div>
		</div>

		<!-- modal approve request -->
		<div class="modal fade " id="approveRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form method="POST" action="<?php echo base_url(); ?>cms/color_points/approved">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Approve Request</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>Points to Redeem: <span id="ptr"></span> points</label>
			        		</div>
			        		<div class="form-group">
			        			<label>Current Points: <span id="cp"></span> points</label>
			        		</div>
			        		<div class="form-group">
			        			<label>Points to Deduct<span class="required">*</span></label>
			        			<input type="number" max="100" required name="points_to_deduct" class="form-control" id="ptr_field" placeholder="0">
			        		</div>  
                        </div>
                        <input type="hidden" name="action" value="accept">
						<input type="hidden" name="token" value="<?php echo $token; ?>">
						<input type="hidden" name="id" id="redeem_request_id">
		                <div class="modal-footer">
		                	<div class="pull-right">
		                    	<button type="Submit" class="btn btn-info btn-block" value="Approve">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		

    </section>
</section>

<script>
	$(".approveBtn").on('click',function(){
		var curr = $(this).data('cp');
		var ptr = $(this).data('ptr');
		var rrid = $(this).data('rrid');

		$('span#ptr').text(ptr);
		$('span#cp').text(curr);
		$('input#ptr_field').val(ptr).focus();
		$('.modal #redeem_request_id').val(rrid);
		$("input#ptr_field").attr("max", curr);
	});
</script>
<!--main content end -->
