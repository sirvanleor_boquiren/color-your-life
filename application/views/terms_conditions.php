<section id="main-content">
	<section class="wrapper">

		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						Terms & Conditions
					</header>
					<?php
					$alert_msg = $this->session->flashdata('alert_msg');
					if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
								<i class="fa fa-times"></i>
							</button>
							<?php echo $alert_msg; ?>
						</div>
					<?php endif; ?>
					<div class="panel-body">
						<form action="<?php echo base_url('cms/company_info/add'); ?>" method="POST" class="form-horizontal tasi-form">
							<div class="form-group">
								<div class="col-md-12">
									<textarea class="form-control" rows="10" name="content"><?php echo @$data['content']; ?></textarea>
								</div>
							</div>
							<input type="submit" name="save_terms_conditions" value="Save" class="btn btn-info btn-block">
						</form>
					</div>
				</section>
			</div>
		</div>

	</section>
</section>