<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Present Requests - Rejected
					</header>
					<div class="panel-body">
			           
						<div class="row"></div>
						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>
						<table class="table table-bordered">
							<thead>
								<tr>
                                    <th>Name</th>
                                    <th>Email Address</th>
									<th>Mobile Number</th>
									<th>Presents to Redeem</th>
									<th>Bronze</th>
									<th>Silver</th>
									<th>Gold</th>
									<th>Platinum</th>
									<th>Date Rejected</th>
									<th>Date Requested</th>
								</tr>
							</thead>
							<tbody>
                            <?php 
                            $listing = $presents_requests['listing'];

                            $users = $presents_requests['user_members'];
                             if (count($listing) > 0) :
                             	$i = 0;
                             foreach($listing as $list) : 
                             	 ?>
									<tr>
										<td><?php echo $list->fname." ".$list->lname;?></td>
										<td><?php echo $list->email; ?></td>
										<td><?php echo $list->mobile_no; ?></td>
										<td><?php echo $list->present_titles; ?></td>
										<td><?php echo $users[$i]['Bronze Member']; ?></td>
										<td><?php echo $users[$i]['Silver Member']; ?></td>
										<td><?php echo $users[$i]['Gold Member']; ?></td>
										<td><?php echo $users[$i]['Platinum Member']; ?></td>
										<td><?php echo date("M j, y g:i a", strtotime($list->updated_at)); ?></td>
										<td><?php echo date("M j, y g:i a", strtotime($list->created_at)); ?></td>
									</tr>
							<?php $i ++; endforeach; else: ?>
								<tr>
									<td colspan="9" align="center">No results available.</td>
								</tr>
							<?php endif; ?>    
                            </tbody>
							<tfoot>
								<tr>
									<th>Name</th>
                                    <th>Email Address</th>
									<th>Mobile Number</th>
									<th>Presents to Redeem</th>
									<th>Bronze</th>
									<th>Silver</th>
									<th>Gold</th>
									<th>Platinum</th>
									<th>Date Rejected</th>
									<th>Date Requested</th>
								</tr>
							</tfoot>
						</table>
						<div class="text-center">
							<ul class="pagination">
								<?php echo $pagination; ?>
							</ul>
						</div>
                        </div>
					</div>
				</section>
			</div>
		</div>


		

    </section>
</section>
<!--main content end -->
