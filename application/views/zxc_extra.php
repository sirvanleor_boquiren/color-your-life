<!-- main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			
			<?php $this->load->view("partials/user-sidebar"); ?>

			<aside class="profile-info col-lg-9">
				<section class="panel">
					<header class="panel-heading tab-bg-dark-navy-blue ">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#paynamics">Paynamics</a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#referrals">Referrals</a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#color_points">Color Points</a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#color_bonus">Color Bonus</a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#privileges">Privileges</a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#sample">Sample</a>
							</li>
						</ul>
					</header>
					<div class="panel-body">
						<div class="tab-content">
							<div id="paynamics" class="tab-pane active">
								<div class="table-responsive">
									<?php
									$alert_msg = $this->session->flashdata('alert_msg');
									if ($alert_msg != "") {
									?>
									<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
										<button data-dismiss="alert" class="close close-sm" type="button">
											<i class="fa fa-times"></i>
										</button>
										<?php echo $alert_msg; ?>
									</div>
									<?php
									}
									?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>ID</th>
												<th>membership_plan_id</th>
												<th>request_id</th>
												<th>response_id</th>
												<th>status</th>
												<th>action</th>
											</tr>
										</thead>
										<tbody>
											<?php
											foreach ($paynamics_payments as $val) {
											?>
											<tr>
												<td><?php echo $val->paynamic_request_id; ?></td>
												<td><?php echo $val->membership_plan_id; ?></td>
												<td><?php echo $val->requestid; ?></td>
												<td><?php echo $val->responseid; ?></td>
												<td><?php echo $val->status; ?></td>
												<td>
													<?php
													if ($val->status == "pending") {
													?>
													<a href="<?php echo base_url("cms/extra/paynamics_request_status_change/" . $details->user_id . "/" . $val->paynamic_request_id . "/paid"); ?>" class="btn btn-success btn-xs">Change to 'Paid'</a>
													<?php
													}
													elseif ($val->status == "paid") {
													?>
													<a href="<?php echo base_url("cms/extra/paynamics_request_status_change/" . $details->user_id . "/" . $val->paynamic_request_id . "/pending"); ?>" class="btn btn-info btn-xs">Change to 'Pending'</a>
													<?php
													}
													?>
												</td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>

							<div id="referrals" class="tab-pane col-md-9">
								<?php
								$alert_msg = $this->session->flashdata('alert_msg');
								if ($alert_msg != "") {
									?>
									<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
										<button data-dismiss="alert" class="close close-sm" type="button">
											<i class="fa fa-times"></i>
										</button>
										<?php echo $alert_msg; ?>
									</div>
									<?php
								}
								?>
								<form method="POST" action="<?php echo base_url("cms/extra/update_referrals/" . $details->user_id); ?>">
									<div class="form-group col-md-6">
										<label>referrals for ranking up</label>
										<input type="number" name="rank_up_referrals" class="form-control" value="<?php echo $referrals->rank_up; ?>">
									</div>
									<div class="form-group col-md-6">
										<label>total referrals</label>
										<input type="number" name="referrals" class="form-control" value="<?php echo $referrals->all; ?>">
									</div>
									<div class="form-group col-md-12">
										<button type="Submit" class="btn btn-info btn-xs btn-block">Submit</button>
									</div>
								</form>
							</div>

							<div id="color_points" class="tab-pane">
								<?php
								$alert_msg = $this->session->flashdata('alert_msg');
								if ($alert_msg != "") {
									?>
									<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
										<button data-dismiss="alert" class="close close-sm" type="button">
											<i class="fa fa-times"></i>
										</button>
										<?php echo $alert_msg; ?>
									</div>
									<?php
								}
								?>
								<form method="POST" action="<?php echo base_url("cms/extra/update_points/" . $details->user_id); ?>">
									<div class="form-group col-md-6">
										<label>color points</label>
										<input type="number" name="points" class="form-control" value="<?php echo $details->points; ?>">
									</div>
									<div class="form-group col-md-12">
										<button type="Submit" class="btn btn-info btn-xs btn-block">Submit</button>
									</div>
								</form>
							</div>

							<div id="color_bonus" class="tab-pane">
								<?php
								$alert_msg = $this->session->flashdata('alert_msg');
								if ($alert_msg != "") {
									?>
									<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
										<button data-dismiss="alert" class="close close-sm" type="button">
											<i class="fa fa-times"></i>
										</button>
										<?php echo $alert_msg; ?>
									</div>
									<?php
								}
								?>
								<form method="POST" action="<?php echo base_url("cms/extra/update_bonus/" . $details->user_id); ?>">
									<div class="form-group col-md-6">
										<label>Free</label>
										<input type="number" name="free" class="form-control" value="<?php echo $color_bonus->free; ?>">
									</div>
									<div class="form-group col-md-6">
										<label>Challenger</label>
										<input type="number" name="challenger" class="form-control" value="<?php echo $color_bonus->challenger; ?>">
									</div>
									<div class="form-group col-md-6">
										<label>Economy</label>
										<input type="number" name="economy" class="form-control" value="<?php echo $color_bonus->economy; ?>">
									</div>
									<div class="form-group col-md-6">
										<label>Business</label>
										<input type="number" name="business" class="form-control" value="<?php echo $color_bonus->business; ?>">
									</div>
									<div class="form-group col-md-12">
										<button type="Submit" class="btn btn-info btn-xs btn-block">Submit</button>
									</div>
								</form>
							</div>

							<div id="privileges" class="tab-pane">
								<?php
								$alert_msg = $this->session->flashdata('alert_msg');
								if ($alert_msg != "") {
									?>
									<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
										<button data-dismiss="alert" class="close close-sm" type="button">
											<i class="fa fa-times"></i>
										</button>
										<?php echo $alert_msg; ?>
									</div>
									<?php
								}
								?>
								<form method="POST" action="<?php echo base_url("cms/extra/update_privileges/" . $details->user_id); ?>">
									<div class="form-group col-md-6">
										<label>remaining_advertisement</label>
										<input type="number" name="advertisement" class="form-control" value="<?php echo $privileges->remaining_advertisement; ?>">
									</div>

									<div class="form-group col-md-6">
										<label>remaining_video</label>
										<input type="number" name="video_recording_dist" class="form-control" value="<?php echo $privileges->remaining_video; ?>">
									</div>

									<div class="form-group col-md-6">
										<label>remaining_group_message</label>
										<input type="number" name="group_message" class="form-control" value="<?php echo $privileges->remaining_group_message; ?>">
									</div>

									<div class="form-group col-md-6">
										<label>remaining_webinar</label>
										<input type="number" name="live_webinar" class="form-control" value="<?php echo $privileges->remaining_webinar; ?>">
									</div>
									<div class="form-group col-md-12">
										<button type="Submit" class="btn btn-info btn-xs btn-block">Submit</button>
									</div>
								</form>

							</div>

							<div id="sample" class="tab-pane">Sample</div>
						</div>
					</div>

				</section>
			</aside>

		</div>
		<!-- page end-->		
	</section>
</section>
<!--main content end -->

<script type="text/javascript">
$(function(){
	var x = location.hash;
	if (x == "#referrals") {
		$('.nav-tabs li:eq(1) a').tab('show'); 
	}
	else if (x == "#color_points") {
		$('.nav-tabs li:eq(2) a').tab('show');
	}
	else if (x == "#color_bonus") {
		$('.nav-tabs li:eq(3) a').tab('show');	
	}
	else if (x == "#privileges") {
		$('.nav-tabs li:eq(4) a').tab('show');	
	}
});
</script>