<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Presents
					</header>
					<div class="panel-body">
						<div class="row">
			            	<div class="col-md-12 mbot15">
				            	<div class="pull-right">
									<a class="btn btn-info btn-sm" data-toggle="modal" href="#addPresent" ><i class="fa fa-plus"></i>&nbsp; Add Present</a>
								</div>
			            	</div>
			            </div>
						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>
						<table class="table table-bordered">
							<thead>
								<tr>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>No. of Members</th>
									<th>Image</th>
                                    <th>Featured</th>
									<th colspan="2">Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php foreach($presents as $present): ?>
									<tr>
										<td>
											<?=$present->title?>
										</td>
                                        <td>
                                            <?=$present->type?>
                                        </td>
                                        <td>
                                            <?=$present->no_of_members?>
                                        </td>
										<td>
											<img width="150" src="<?=$present->image_path?>" alt="<?=$present->title?>" />
                                        </td>
                                        <td>
                                            <?=$present->is_featured ? "Yes" : "No"?>
                                        </td>
										<td>
											<button
											type="button"
											data-target="#editPresent"
											data-toggle="modal"
											class="btn btn-primary btn-xs"
											data-payload='<?=json_encode(['id' => $present->present_id,'title' => $present->title,'image_name' => $present->image_path, 'type' => $present->type,'is_featured' => $present->is_featured,'no_of_members' =>$present->no_of_members , 'base_url' => base_url()])?>'
											
                                            >Edit</button>
											<button class="btn btn-danger btn-xs"
											data-target="#deletePresent"
											data-toggle="modal"
											data-payload=<?=json_encode(['id' => $present->present_id,'base_url' => base_url()])?>
											>Delete</button>
										</td>
									</tr>
							<?php endforeach; ?>    
                            </tbody>
							<tfoot>
								<tr>
                                <th>Title</th>
                                    <th>Type</th>
                                    <th>No. of Members</th>
									<th>Image</th>
                                    <th>Featured</th>
									<th colspan="2">Action</th>
								</tr>
							</tfoot>
						</table>
						<div class="text-center">
							<ul class="pagination">
								<?php echo $pagination; ?>
							</ul>
						</div>
                        </div>
					</div>
				</section>
			</div>
		</div>


		<!-- add present modal start -->
		<div class="modal fade " id="addPresent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form method="POST" action="<?=base_url()?>cms/presents/create" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Presents</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>Present Title <span class="required">*</span></label>
			        			<input type="text" required name="title" class="form-control" placeholder="Input Present title here">
			        		</div>  
                        </div>
                        <div class="modal-body">
			        		<div class="form-group">
			        			<label>Type <span class="required">*</span></label>
			        			<select id="selectType" required class="form-control m-bot15" name="type" data-baseurl="<?=base_url()?>">
											  <option value="">Select Type</option>
                                              <option value="bronze">Bronze</option>
                                              <option value="silver">Silver</option>
                                              <option value="gold">Gold</option>
                                              <option value="platinum">Platinum</option>
                                </select>
			        		</div>  
						</div>
						<div class="modal-body">
			        		<div class="form-group">
			        			<label>No. of Members Required <span class="required">*</span></label>
			        			<input type="text" required name="no_of_members" class="form-control" placeholder="0">
			        		</div>  
                        </div>
						<div class="modal-body" id="featurable"></div>
                        <div class="modal-body">
			        		<div class="form-group">
			        			<label>Present Image <span class="required">*</span></label>
			        			<input type="file" required name="presents_image" class="form-control">
			        		</div>  
	                    </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                    	<button type="Submit" class="btn btn-info btn-block" id="add_present_submit">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- add present modal end -->

		<!-- edit present modal start -->
		<div class="modal fade " id="editPresent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form id="editForm" method="POST" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Edit Presents</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>Present Title <span class="required">*</span></label>
			        			<input type="text" required name="title" class="form-control" placeholder="Input Present title here">
			        		</div>  
                        </div>
                        <div class="modal-body">
			        		<div class="form-group">
			        			<label>Type <span class="required">*</span></label>
			        			<select required 	class="form-control m-bot15" name="type">
                                              <option value="bronze">Bronze</option>
                                              <option value="silver">Silver</option>
                                              <option value="gold">Gold</option>
                                              <option value="platinum">Platinum</option>
                                </select>
			        		</div>  
                        </div>
                        	<div class="modal-body">
			        		<div class="form-group">
			        			<label>No. of Members Required <span class="required">*</span></label>
			        			<input type="text" required name="no_of_members" class="form-control" placeholder="0">
			        		</div>  
                        </div>
                        <div class="modal-body">
			        		<div class="form-group">
			        			<label>Include in Featured<span class="required"></span></label>
			        			<div class="checkbox">
                                              <label>
                                                  <input type="checkbox" name="is_featured" value="1">
                                              </label>
                                </div>
			        		</div>  
                        </div>
                        <div class="modal-body">
							<div id="imageHolder"></div>
			        		<div class="form-group">
			        			<label>Present Image <span class="required">*</span></label>
			        			<input type="file" name="presents_image" class="form-control">
			        		</div>  
	                    </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                    	<button type="Submit" class="btn btn-info btn-block" id="add_present_submit">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- edit present modal end -->
		<div class="modal fade top-modal-without-space in" id="deletePresent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content-wrap">
					<div class="modal-content">
						<!-- <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title">Modal Tittle</h4>
						</div> -->
						<div class="modal-body">

							Are you sure you want to delete this? type DELETE to confirm.
							<form id="deleteForm" method="POST"></form>
								<input type="text" id="deleteInput">
							

						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
							<button id="deleteButton" class="btn btn-warning" type="submit"> Confirm</button>
						</div>
						
					</div>
				</div>
			</div>
		</div>

    </section>
</section>

<!-- <script src="<?=base_url()?>frontend/js/jquery-latest.js"></script>
<script src="<?=base_url()?>frontend/js/bootstrap.min.js"></script>
 --><script src="<?=base_url()?>frontend/js/cms/presents.js"></script>

<!--main content end -->
