<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Present Requests
					</header>
					<div class="panel-body">
						<!-- <div class="row">
			            	<div class="col-md-12 mbot15">
				            	<div class="pull-right">
									<a class="btn btn-info btn-sm" data-toggle="modal" href="#addAffiliate"><i class="fa fa-plus"></i>&nbsp; Add Affiliate</a>
								</div>
			            	</div>
			            </div> -->
						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>
						<table class="table table-bordered">
							<thead>
								<tr>
                                    <th>Username</th>
                                    <th>Presents</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>Status</th>
                                    <th>Created At</th>
									<th colspan="2">Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php foreach($present_requests as $present_request): ?>
									<tr>
                                        <td><?=$present_request->user_name?></td>
                                        <td>
                                            <?php foreach($present_request->presents as $present): ?>
                                                <li><?=$present->title?></li>
                                            <?php endforeach; ?>
                                        </td>
                                        <td><?=$present_request->fname?></td>
                                        <td><?=$present_request->lname?></td>
                                        <td><?=$present_request->email?></td>
                                        <td><?=$present_request->mobile_no?></td>
                                        <td><?=$present_request->status?></td>
                                        <td><?=$present_request->created_at?></td>
										<td>
											<!-- <button
											type="button"
											data-target="#editRedeemRequest"
											data-toggle="modal"
											class="btn btn-primary btn-xs"
											data-payload=<?php //json_encode(array_merge(['base_url' => base_url(),'status' => $present_request->status,'id' => $present_request->present_request_id]))?>
                                            >Edit</button> -->
                                            <?php if($present_request->status == 'pending'): ?>
                                            <form onsubmit="return confirm('Are you sure you want to do this?')" action="<?=base_url()?>cms/present_requests/present_request/<?=$present_request->present_request_id?>/accept">
                                                <button type="submit" class="btn btn-primary btn-xs">
                                                    Accept
                                                </button>
                                            </form>
                                            <form onsubmit="return confirm('Are you sure you want to do this?')" action="<?=base_url()?>cms/present_requests/present_request/<?=$present_request->present_request_id?>/decline">
                                                <button type="submit" class="btn btn-danger btn-xs">
                                                    Decline
                                                </button>
                                            </form>
                                            <?php endif; ?>
										</td>
									</tr>
							<?php endforeach; ?>    
                            </tbody>
							<tfoot>
								<tr>
                                    <th>Username</th>
                                    <th>Presents</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>Status</th>
                                    <th>Created At</th>
									<th colspan="2">Action</th>
								</tr>
							</tfoot>
						</table>
                        </div>
					</div>
				</section>
			</div>
		</div>

    </section>
</section>

<script src="<?=base_url()?>frontend/js/jquery-latest.js"></script>
<script src="<?=base_url()?>frontend/js/bootstrap.min.js"></script>
<!-- <script src="base_url()?>frontend/js/cms/present_requests.js"></script> -->

<!--main content end -->
<!-- base_url present_requests/process_redeem_request{$id -->