<!-- main content start-->
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-sm-12">
        <section class="panel">
          <header class="panel-heading">
            User management
          </header>
          <div class="panel-body">
            <div class="table-responsive">
              <?php
              $alert_msg = $this->session->flashdata('alert_msg');
              if ($alert_msg != "") {
              ?>
              <div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                  <i class="fa fa-times"></i>
                </button>
                <?php echo $alert_msg; ?>
              </div>
              <?php
              }
              ?>


              <!-- Search -->
              <form class="form-group" method="GET" action="<?php echo base_url('cms/users'); ?>">
                <?php if (@$search_keyword): ?>
                <div class="col-sm-9">
                  <span class="form-control" style="border: 0px;">Search result for: <label><?php echo @$search_keyword; ?></label></span>
                </div>
                <?php endif ?>
                <div class="col-sm-3 pull-right">
                  <div class="input-group m-bot15">
                    <input type="text" class="form-control" placeholder="search.." name="search_keyword" value="<?php echo (@$search_keyword) ? $search_keyword : ''; ?>">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </form>
              <!-- /Search -->

              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Points</th>
                    <th>Membership</th>
                    <th>Is active</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($res) > 0)
                  {
                    foreach ($res as $key => $value) { ?>
                    <tr>
                      <td><?php echo $value->fname . " " . $value->lname; ?></td>
                      <td><?php echo $value->email; ?></td>
                      <td><?php echo $value->points; ?></td>
                      <td><?php echo @$plans[$value->membership_plan_id]; ?></td>
                      <td><?php echo ($value->status) ? '<span style="color:mediumseagreen;font-weight:bold">Active</span>' : '<span style="color:red;font-weight:bold">Inactive</span>' ; ?></td>
                      <td>
                        <a href="<?php echo base_url("cms/users/details/" . $value->user_id); ?>" class="btn btn-info btn-xs">View</a>
                        <a href="#editUser" data-toggle="modal" class="edit-user btn btn-primary btn-xs"
                        data-id="<?php echo $value->user_id; ?>" 
                        data-lname="<?php echo $value->lname; ?>" 
                        data-fname="<?php echo $value->fname; ?>"
                        data-username="<?php echo $value->username; ?>"
                        data-email="<?php echo $value->email; ?>">Edit</a>

                        <?php if ($value->status): ?>
                          <a href="<?php echo base_url("cms/users/deactivate/").$value->user_id; ?>" class="btn btn-danger btn-xs">
                            Make inactive
                          </a>
                        <?php else: ?>
                          <a href="<?php echo base_url("cms/users/activate/").$value->user_id; ?>" class="btn btn-success btn-xs">
                            Make active
                          </a>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <?php
                  }
                }
                else
                  { ?>
                <tr>
                  <td colspan="4"><center>No records found.<center></td>
                </tr>
                <?php
              } ?>
            </tbody>
            <tfoot>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Points</th>
                <th>Membership</th>
                <th>Is active</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
          <div class="text-center">
            <ul class="pagination">
              <?php echo $pagination; ?>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<!-- Edit user modal start -->
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" action="<?php echo base_url($edit_link); ?>">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Edit User</h4>
        </div>

        <div class="modal-body">
          <div class="form-group">
            <label>First Name <span class="required">*</span></label>
            <input type="text" required name="fname" class="form-control" id="fname" placeholder="Input First Name here">
          </div>
          <div class="form-group">
            <label>Last Name <span class="required">*</span></label>
            <input type="text" required name="lname" class="form-control" id="lname" placeholder="Input Last Name here">
          </div>
          <div class="form-group">
            <label>Username <span class="required">*</span></label>
            <input type="text" required name="username" class="form-control" id="username" placeholder="Input Username here">
            <p class="help-block" id="username_help"></p>
          </div>
          <div class="form-group">
            <label>Email <span class="required">*</span></label>
            <input type="email" required name="email" class="form-control" id="email" placeholder="Input Email here">
            <p class="help-block" id="email_help"></p>
          </div>
        </div>

        <div class="modal-footer">
          <div class="pull-right">
            <input type="hidden" name="user_id" id="user_id">
            <button type="Submit" class="btn btn-info btn-block">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit user modal end -->

</section>
</section>
<!--main content end -->
<script type="text/javascript">
$(function(){
  $(".edit-user").on("click", function(){
    $("#editUser .modal-footer #user_id").val($(this).data("id"));
    $("#editUser .modal-body #fname").val($(this).data("fname"));
    $("#editUser .modal-body #lname").val($(this).data("lname"));
    $("#editUser .modal-body #email").val($(this).data("email"));
    $("#editUser .modal-body #username").val($(this).data("username"));
  });
});
</script>