<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
	<!-- page start-->
		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						<?php echo $table_header; ?>
					</header>
					<div class="panel-body">
						<div class="row">
			            	<div class="col-md-12 mbot15">
				            	<div class="pull-right">
									<a class="btn btn-info btn-sm" data-toggle="modal" href="#addAdmin"><i class="fa fa-plus"></i>&nbsp; Add User</a>
								</div>
			            	</div>
			            </div>
						<div class="table-responsive">
						<br>
						<?php
						$alert_msg = $this->session->flashdata('alert_msg');
						if($alert_msg != ""): ?>
						<div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
							<button data-dismiss="alert" class="close close-sm" type="button">
							  <i class="fa fa-times"></i>
							</button>
						<?php echo $alert_msg; ?>
						</div>
						<?php endif; ?>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
							if(count($all_users) > 0)
							{
								foreach ($all_users as $user_id => $user_arr) { ?>
								<tr>
									<td><?php echo $user_arr->fullname; ?></td>
									<td><?php echo $user_arr->username; ?></td>
									<td><?php echo $user_arr->email; ?></td>
									<td><?php echo ($user_arr->status == "Active") ? '<span style="color:mediumseagreen;font-weight:bold">Active</span>' : '<span style="color:red;font-weight:bold">Inactive</span>' ; ?></td>
									<td>
										<a href="#editAdmin" data-toggle="modal" class="edit-admin btn btn-primary btn-xs"
										data-id="<?php echo $user_arr->admin_id; ?>" 
										data-lname="<?php echo $user_arr->lname; ?>" 
										data-fname="<?php echo $user_arr->fname; ?>"
										data-username="<?php echo $user_arr->username; ?>"
										data-email="<?php echo $user_arr->email; ?>"
										>Edit</a>
										<a href="#changePassword" data-toggle="modal" class="change-password btn btn-warning btn-xs"
										data-id="<?php echo $user_arr->admin_id; ?>" 
										data-username="<?php echo $user_arr->username; ?>"
										>Change Password</a>
										<?php
										if ($user_arr->status == "Active") {
										?>
											<a href="<?php echo base_url("users/deactivateAdmin/") . $user_arr->admin_id; ?>" class="btn btn-danger btn-xs">Deactivate</a>
										<?php
										}
										else {
										?>
											<a href="<?php echo base_url("users/activateAdmin/") . $user_arr->admin_id; ?>" class="btn btn-success btn-xs">Activate</a>
										<?php
										}
										?>
										
									</td>
								</tr>
								<?php
								}
							}
							else
							{ ?>
							<tr>
								<td colspan="7"><center>No records found.<center></td>
							</tr>
							<?php
							} ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
	                        <div class="text-center">
								<ul class="pagination">
								<?php echo $pagination; ?>
								</ul>
	                        </div>
                        </div>
					</div>
				</section>
			</div>
		</div>


		<!-- add Promo modal start -->
		<div class="modal fade " id="addAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form method="POST" action="<?php echo base_url($add_link); ?>" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Admin</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>First Name <span class="required">*</span></label>
			        			<input type="text" required name="fname" class="form-control" placeholder="Input First Name here">
			        		</div>
			        		<div class="form-group">
			        			<label>Last Name <span class="required">*</span></label>
			        			<input type="text" required name="lname" class="form-control" placeholder="Input Last Name here">
			        		</div>
			        		<div class="form-group">
			        			<label>Username <span class="required">*</span></label>
			        			<input type="text" required name="username" class="form-control" placeholder="Input Username here">
			        			<p class="help-block" id="username_help"></p>
			        		</div>
			        		<div class="form-group">
			        			<label>Email <span class="required">*</span></label>
			        			<input type="email" required name="email" class="form-control" placeholder="Input Email here">
			        			<p class="help-block" id="email_help"></p>
			        		</div>
			        		<div class="form-group">
			        			<label>Password <span class="required">*</span></label>
			        			<input type="password" required name="password" class="form-control" placeholder="Input Password here">
			        			<p class="help-block" id="password_help"></p>
			        		</div>
			        		<div class="form-group">
			        			<label>Confirm Password <span class="required">*</span></label>
			        			<input type="password" required name="confirm_password" class="form-control" placeholder="Input Confirm Password here. Must match password">
			        			<p class="help-block" id="confim_help"></p>
			        		</div>
	                    </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                    	<button type="Submit" class="btn btn-info btn-block" id="add_promo_submit">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- add promo modal end -->

		<!-- edit admin modal start -->
		<div class="modal fade " id="editAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form method="POST" action="<?php echo base_url($edit_link); ?>" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Admin</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>First Name <span class="required">*</span></label>
			        			<input type="text" required name="fname" class="form-control" id="fname" placeholder="Input First Name here">
			        		</div>
			        		<div class="form-group">
			        			<label>Last Name <span class="required">*</span></label>
			        			<input type="text" required name="lname" class="form-control" id="lname" placeholder="Input Last Name here">
			        		</div>
			        		<div class="form-group">
			        			<label>Username <span class="required">*</span></label>
			        			<input type="text" required name="username" class="form-control" id="username" placeholder="Input Username here">
			        			<p class="help-block" id="username_help"></p>
			        		</div>
			        		<div class="form-group">
			        			<label>Email <span class="required">*</span></label>
			        			<input type="email" required name="email" class="form-control" id="email" placeholder="Input Email here">
			        			<p class="help-block" id="email_help"></p>
			        		</div>
	                    </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                		<input type="hidden" name="admin_id" id="admin_id">
		                    	<button type="Submit" class="btn btn-info btn-block">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- edit admin modal end -->

		<!-- Change Password modal -->
		<div class="modal fade " id="changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		        	<form method="POST" action="<?php echo base_url($change_pass_link); ?>" enctype="multipart/form-data">
		        		<div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Change Password</h4>
	                    </div>
	                    <div class="modal-body">
			        		<div class="form-group">
			        			<label>Username</label>
			        			<input type="text" required name="username" class="form-control" id="username" placeholder="Input Username here" disabled>
			        			<p class="help-block" id="username_help"></p>
			        		</div>
			        		<div class="form-group">
			        			<label>Password <span class="required">*</span></label>
			        			<input type="password" required name="password" id="password" class="form-control" placeholder="Input Password here">
			        			<p class="help-block" id="password_help"></p>
			        		</div>
			        		<div class="form-group">
			        			<label>Confirm Password <span class="required">*</span></label>
			        			<input type="password" required name="confirm_password" id="confirm_password" class="form-control" placeholder="Input Confirm Password here. Must match password">
			        			<p class="help-block" id="confim_help"></p>
			        		</div>
	                    </div>
		                <div class="modal-footer">
		                	<div class="pull-right">
		                		<input type="hidden" name="admin_id" id="admin_id">
		                    	<button type="Submit" class="btn btn-info btn-block">Submit</button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		<!-- Change Password modal end -->
    </section>
</section>
<!--main content end -->

<script type="text/javascript">
$(function(){
	$(".edit-admin").on("click", function(){
		$("#editAdmin .modal-footer #admin_id").val($(this).data("id"));
		$("#editAdmin .modal-body #fname").val($(this).data("fname"));
		$("#editAdmin .modal-body #lname").val($(this).data("lname"));
		$("#editAdmin .modal-body #email").val($(this).data("email"));
		$("#editAdmin .modal-body #username").val($(this).data("username"));
	});

	$(".change-password").on("click", function(){
		$("#changePassword .modal-footer #admin_id").val($(this).data("id"));
		$("#changePassword .modal-body #username").val($(this).data("username"));
	});
});
</script>
